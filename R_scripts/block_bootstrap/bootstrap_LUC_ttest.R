#bootstrapping for testing LUC experiments
#Rprof(filename = "Rprof_bootstr_10test.out", interval = 0.5, line.profiling = TRUE)

library(ncdf)

###set-up
# input directory
file_dir <- Sys.getenv("WORKDIR")

# run id for CTL
runid_ctl=Sys.getenv("RUNID1")
ctl=Sys.getenv("RUNNAME1")

# weakly or strongly coupled region
region<-Sys.getenv("REGION")
print(region)

# run id's for experiments depend on region
if (region == "sc"){
runid<-list("uaoyc","uaoye","uaoyg","uaoyi","uaoyk","uaoym","uaoyo","uaoya","vacdq")
runname<-list("001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc","242GP")
} else if (region == "wc"){
runid<-list("uaoyd","uaoyf","uaoyh","uaoyj","uaoyl","uaoyn","uaoyp","uaoyb","vacdt")
runname<-list("001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc","allAMZ")
} else {
  print("Wrong region")
}

# output directory
out_dir <- Sys.getenv("OUTDIR")

# significance level for t-test, e.g. 0.05
siglev <- Sys.getenv("siglev")

# starts and ends for CTL (1) and experiments (2)
R1.Start<-Sys.getenv("R1_START")
R1.End<-Sys.getenv("R1_STOP")
R2.Start<-Sys.getenv("R2_START")
R2.End<-Sys.getenv("R2_STOP")
#start and end of analysis period
A.Start<-as.numeric(Sys.getenv("A_START"))
A.End<-as.numeric(Sys.getenv("A_STOP"))

# time data frame for analysis period
time.ind<- c(1:((A.End-A.Start+1)*12))
time.m <- rep(seq(1,12),(A.End-A.Start+1))
time.y <- c()
ind =1
for (yr in A.Start:A.End){
        time.y[ind:(ind+11)] <- rep(yr,12)
        ind = ind+12
}
time <- data.frame(time.y,time.m,time.ind)
# number of years in analysis period
nyears=A.End-A.Start+1

###read data from netcdfs
print("read data")
# first read land sea mask, determines number of lats and lons
file_lsm=paste("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc",sep="")
        print(file_lsm)
        ex.nc = open.ncdf(file_lsm)
        print(ex.nc)
        lsm=get.var.ncdf( ex.nc, "lsm") #lon,lat,surface,time
        lon=get.var.ncdf( ex.nc, "longitude")
        lat=get.var.ncdf( ex.nc, "latitude")

	dims=dim(lsm)
	nlon=dims[1]
	nlat=dims[2]

###loop over runs
for (run in 1:9){

    #check if output file exists, if not create it
    dir.create(file.path(out_dir,runname[run]), showWarnings = FALSE)

    ###loop over variables
    for (v in 1:2){
    	if ( v==1 ){
       	   var<-"tas"
       	} else if (v==2){
       	   var<-"pr"
        }

       	file_exp=paste(file_dir,runid[run],".monthly_TS.",R2.Start,"_",R2.End,".nc",sep="")
        print(file_exp)
        ex.nc = open.ncdf(file_exp)

	time_exp_units<-att.get.ncdf( ex.nc,"time","units")
	base_date_exp=as.character(lapply(strsplit(as.character(time_exp_units$value),split=" "),"[",3))	
	time_exp=get.var.ncdf( ex.nc,"time")
	time_exp_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_exp))
	time_exp.y<-format(time_exp_d, "%Y")
	time_exp.m<-format(time_exp_d, "%m")

	t1=min(which(as.numeric(time_exp.y)>=A.Start))
	t2=(A.End-A.Start+1)*12
        var_exp=get.var.ncdf( ex.nc, var, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time
	unit_var<- att.get.ncdf(ex.nc, var, "units")

	file_ctl=paste(file_dir,runid_ctl,".monthly_TS.",R1.Start,"_",R1.End,".nc",sep="")
        print(file_ctl)
        ex.nc = open.ncdf(file_ctl)

	time_ctl_units<-att.get.ncdf( ex.nc,"time","units")
	base_date_ctl=as.character(lapply(strsplit(as.character(time_ctl_units$value),split=" "),"[",3))	
	time_ctl=get.var.ncdf( ex.nc,"time")
	time_ctl_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_ctl))
	time_ctl.y<-format(time_ctl_d, "%Y")
	time_ctl.m<-format(time_ctl_d, "%m")

	t1=min(which(as.numeric(time_ctl.y)>=A.Start))
	t2=(A.End-A.Start+1)*12
        var_ctl=get.var.ncdf( ex.nc, var, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time

	dims=dim(var_exp)
	ntim=dims[3]

	#detrend time series?
	#var_exp_dtr<-array(NA,dim=c(nlon,nlat,nyears*12))
	#var_ctl_dtr<-array(NA,dim=c(nlon,nlat,nyears*12))

	#for ( lon in 1:nlon){
	#    for ( lat in 1:nlat){
	#    	var_exp_dtr[lon,lat,]<-detrend(var_exp[lon,lat,],'linear')
	#	var_ctl_dtr[lon,lat,]<-detrend(var_ctl[lon,lat,],'linear')
	#	}
	#}

	###loop over seasons,
	var_exp_seas<-array(NA,dim=c(nlon,nlat,nyears,4))
	var_ctl_seas<-array(NA,dim=c(nlon,nlat,nyears,4))
	data.seas<-array(NA,dim=c(nlon,nlat,nyears,4))

        Tval_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
        Pval_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
        Hval_exp_ctl<-array(NA,dim=c(nlon,nlat,4))

	anom.seas<-array(NA,dim=c(nlon,nlat,4))
	estimate<-array(NA,dim=c(nlon,nlat,4))
	#confint<-array(NA,dim=c(nlon,nlat,4,2))
	pval<-array(NA,dim=c(nlon,nlat,4))
	#signif_c<-array(NA,dim=c(nlon,nlat,4))
	signif_p<-array(NA,dim=c(nlon,nlat,4))

	for (j in 1:4){ 
                print(paste("Season ",j,sep=""))
		#loop over years
		y=1
		for (yr in A.Start:A.End){
                if (j==1){seas <- "DJF"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==12)|(time[,2]==1)|(time[,2]==2))),3]
                }
                if (j==2){seas <- "MAM"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==3)|(time[,2]==4)|(time[,2]==5))),3]
                }
                if (j==3){seas <- "JJA"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==6)|(time[,2]==7)|(time[,2]==8))),3]
                }
                if (j==4){seas <- "SON"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==9)|(time[,2]==10)|(time[,2]==11))),3]
                }
		
		#calculate seasonal means per year
		var_exp_seas[,,y,j]<-apply(var_exp[,,ind.seas],c(1:2),mean,na.rm=TRUE)
		var_ctl_seas[,,y,j]<-apply(var_ctl[,,ind.seas],c(1:2),mean,na.rm=TRUE)
		y=y+1
		} #end loop years

                # calculate anomalies
                data.seas[,,,j]<-var_exp_seas[,,,j]-var_ctl_seas[,,,j]
                #calculate seasonal average of anomalies
                anom.seas[,,j]<-apply(data.seas[,,,j],c(1:2),mean,na.rm=TRUE)

                #perform t-test
                test<-apply(data.seas[,,,j],c(1:2),function(x) t.test(x,mu=0,alternative = c("two.sided"),conf.level = 0.95,na.rm=T))
                Tval_exp_ctl[,,j]<-apply(test,c(1,2),function(z) z[[1]]$statistic)
                Pval_exp_ctl[,,j]<-apply(test,c(1,2),function(z) z[[1]]$p.value)
                tcrit1<-apply(test,c(1,2),function(z) z[[1]]$conf.int[1])
                tcrit2<-apply(test,c(1,2),function(z) z[[1]]$conf.int[2])

		#Moving blocks bootstrapping
		
		#block length, Wilks 1997 (JClim)
		#L=(n-L+1)^((2/3)(1-n'/n)) "largest integer no greater than L", evaluated iteravitely
		# n'~=n((1-r1)/(1+r1))
		# r1=lag-1 autocorrelation coefficient
		ACF=acf(as.numeric(var_exp_seas[,,,j]),lag.max=1,type="correlation",plot=FALSE,na.action = na.pass)
		r1=ACF$acf[2]

		n_prime<-nyears*((1-r1)/(1+r1))
		b<-(2/3)*((1-n_prime)/nyears)

		l <-sqrt(nyears)
		length<-1
   		while ( length < (nyears-l +1)^b ) {
   		      l=l+1
   		      length=l
   		}

		L<-floor(l)
		print(L)

		#bootstrapping
		nboot <- 1000 #nr of bootstrap repetitions
		data.bs <- array(NA,dim=c(nlon,nlat,nyears,nboot))
		estimate.bs <- array(NA,dim=c(nlon,nlat,nboot))
		#confint.bs <- array(NA,dim=c(2,nlon,nlat,nboot))
		tval.bs <- array(NA,dim=c(nlon,nlat,nboot))
		#pval.bs <- array(NA,dim=c(nlon,nlat,nboot))

		for (iboot in 1:nboot) {
	    	    for(i in 1:ceiling(nyears/L)) {            # fill the vector with blocks
            	    	  endpoint <- sample(L:nyears, size=1)     # by randomly sampling endpoints
	    	     	  if (max(((i-1)*L+1:L)) <= nyears) {
            	     	     data.bs[,,((i-1)*L+1:L),iboot] <- data.seas[,,(endpoint-(L:1)+1),j] # and copying blocks
	    	     	  } else {
		     	    end<-(i*nyears/ceiling(nyears/L))-nyears+1
	    	     	    data.bs[,,(min((i-1)*L+1:L):nyears),iboot] <- data.seas[,,(endpoint-(end:1)+1),j] # and copying blocks for last bit
	    	     	  }
      	    	     }
	    	#estimate.bs[,,iboot]<-apply(data.bs[,,,iboot],c(1:2),function(x) t.test(x,mu=0,alternative = c("two.sided"),conf.level = 0.95,na.rm=T)$estimate)
	    	#confint.bs[,,,iboot]<-apply(data.bs[,,,iboot],c(1:2),function(x) t.test(x,mu=0,alternative = c("two.sided"),conf.level = 0.95,na.rm=T)$conf.int)
	    	#pval.bs[,,iboot]<-apply(data.bs[,,,iboot],c(1:2),function(x) t.test(x,mu=0,alternative = c("two.sided"),conf.level = 0.95,na.rm=T)$p.value)
		tt_bs<-apply(data.bs[,,,iboot],c(1,2),function(x) t.test(x,mu=0,alternative = c("two.sided"),conf.level = 0.95,na.rm=T))
		estimate.bs[,,iboot]<-apply(tt_bs,c(1,2),function(z) z[[1]]$estimate)
#		confint.bs[,,,iboot]<-apply(tt_bs,c(1,2),function(z) z[[1]]$conf.int)
		tval.bs[,,iboot]<-apply(tt_bs,c(1,2),function(z) z[[1]]$statistic)
#		pval.bs[,,iboot]<-apply(tt_bs,c(1,2),function(z) z[[1]]$p.value)
	    	if(iboot%%100==0) print(iboot)           # report every 100 bootstrap passes
		}

		#calculate seasonal average of anomalies
		anom.seas[,,j]<-apply(data.seas[,,,j],c(1:2),mean,na.rm=TRUE)

		#average estimate, conf.int and pval over bootstrapping samples
		estimate[,,j]<-apply(estimate.bs[,,],c(1:2),mean,na.rm=TRUE)

                # compare t-statistic to bootstrapped distribution of t-statistic*,
                # find percentage of tval.bs>=Tval_exp_ctl
		for (x in 1:nlon){
	    	    for (y in 1:nlat){
			tval.larger<-sum(tval.bs[x,y,]>=Tval_exp_ctl[x,y,j])
                        pval[x,y,j]<-tval.larger/nboot

                        if (pval[x,y,j] < siglev){
                        	signif_p[x,y,j]<-1
                        } else{
                        	signif_p[x,y,j]<-0
			}
	    	    }
		}

	} #end loop over seasons

	print("Save data to netcdf")
	# save NetCDF output
	dim1 = dim.def.ncdf( "lon","degrees_north", as.double(lon))
	dim2 = dim.def.ncdf( "lat","degrees_east", as.double(lat))
	dim3 = dim.def.ncdf( "time","season", 1:4)
	#dim4 = dim.def.ncdf( "confint","-", 1:2)

	# define the EMPTY netcdf variables

	var1 = var.def.ncdf(paste(var," Anomaly",sep=""),unit_var$value, list(dim1,dim2,dim3),-999.0,
          longname=paste("Anomaly of Variable ",var,sep=""))
	var2 = var.def.ncdf(paste(var," Anomaly estimate",sep=""),unit_var$value, list(dim1,dim2,dim3),-999.0,
          longname=paste("Estimated anomaly of Variable by bootstrapping",var,sep=""))


	#var3 = var.def.ncdf("signif_c","-", list(dim1,dim2,dim3), -999.0,
          #longname="Significance of t-test after bootstrapping from conf.int")
	var4 = var.def.ncdf("signif_p","-", list(dim1,dim2,dim3), -999.0,
          longname="Significance of t-test after bootstrapping from p.value")

	var5 = var.def.ncdf("p-value","-", list(dim1,dim2,dim3), -999.0,
          longname="P-Value of t-test from bootstrapping")
	#var6 = var.def.ncdf("conf.int","-", list(dim1,dim2,dim3,dim4), -999.0,
         # longname="Confidence interval of t-test from bootstrapping")

	# associate the netcdf variable with a netcdf file   
	file.out=paste(out_dir,runname[run],"/",var,"_anomaly","_",runname[run],"_signif_pval_",nboot,"bs_",A.Start,"-",A.End,".nc",sep="")
	nc.ex = create.ncdf( file.out, list(var1 , var2, var4, var5))

	put.var.ncdf(nc.ex, var1, anom.seas)
	put.var.ncdf(nc.ex, var2, estimate)
	#put.var.ncdf(nc.ex, var3, signif_c)
	put.var.ncdf(nc.ex, var4, signif_p)
	put.var.ncdf(nc.ex, var5, pval)
	#put.var.ncdf(nc.ex, var6, confint)

	close.ncdf(nc.ex)

	rm(anom.seas)
	rm(estimate)
	#rm(signif_c)
	rm(signif_p)
	rm(pval)
	#rm(confint)

    } #end loop over variables
} # end loop over runs
#Rprof(NULL)
