calc_lag1Corr <- function(x, y = NULL, xAve, yAve = NULL){
# Calculate lag 1 correlation coefficient
# for modified t-test (large and small samples)
# Zwiers and von Storch, 1995, J.Clim, Eq. 14 (two and one sample case)

xnyrs<-length(x)

if (!is.null(y)){
    ynyrs<-length(y)
}

#------ Sum of Squares ------#
xsumsq<-0
for (yr in 1:xnyrs){
xsumsq<-xsumsq+((x[yr]-xAve)^2)
}
if (!is.null(y)){
ysumsq<-0
for (yr in 1:ynyrs){
    ysumsq<-ysumsq+((y[yr]-yAve)^2)
    }
    xysumsq<-xsumsq+ysumsq
}

#------ Lagged Sums and R1 ------#
xsumlag<-0
for (yr in 2:xnyrs){
xsumlag<-xsumlag+((x[yr]-xAve)*(x[yr-1]-xAve))
}
if (!is.null(y)){
    ysumlag<-0
    for (yr in 2:ynyrs){
    ysumlag<-ysumlag+((y[yr]-yAve)*(y[yr-1]-yAve))
    }
    xysumlag<-xsumlag+ysumlag
    r1<-xysumlag/xysumsq
}	else {
    r1<-xsumlag/xsumsq
}
return(r1)

}
