# Compare CTL and LUC experiments
# test significance with modified t-test and 
# walker's test and false discovery rate

library(ncdf)
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/modTtest_3dData.R")
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/walkers_test_package.R")
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/false_discovery_rate_package.R")

###set-up
# input directory
file_dir <- Sys.getenv("WORKDIR")

# run id's for control run
runid_ctl<-Sys.getenv("RUNID1")
ctl<-Sys.getenv("RUNNAME1")

# strongly or weakly coupled region
region<-Sys.getenv("REGION")
print(region)

# runids for experiments depend on region
if (region == "sc"){
   runname<-list("001GPsc","009GPsc","025GPsc","081GPsc","121GPsc","242GP","allAMZ")
} else if (region == "wc"){
  runname<-list("001GPwc","009GPwc","025GPwc","081GPwc","121GPwc","242GP","allAMZ")
} else {
  print("Wrong region")
}
nruns<-length(runname)

# output directory
out_dir<-Sys.getenv("OUTDIR")

# significance level for t-test
siglev<-as.numeric(Sys.getenv("siglev"))

# run start and end for CTL
R1.Start<-Sys.getenv("R1_START")
R1.End<-Sys.getenv("R1_STOP")

# run start and end for experiments
R2.Start<-Sys.getenv("R2_START")
R2.End<-Sys.getenv("R2_STOP")

# start and end of analysis period
A.Start<-as.numeric(Sys.getenv("A_START"))
A.End<-as.numeric(Sys.getenv("A_STOP"))

# number of years in analysis period
nyears=A.End-A.Start+1

###read data from netcdfs
print("read data")
# first read land sea mask, determines number of lats and lons
file_lsm=paste("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc",sep="")
        print(file_lsm)
        ex.nc = open.ncdf(file_lsm)
        print(ex.nc)
        lsm=get.var.ncdf( ex.nc, "lsm") #lon,lat,surface,time

###loop over runs
print("Loop over runs")
for (run in 1:nruns){
    #check if output file exists, if not create it
    dir.create(file.path(out_dir), showWarnings = FALSE)
    dir.create(file.path(out_dir,runname[run]), showWarnings = FALSE)

    variable1<-"mfc"
    variable2<-"mfc_int"

       	file_exp=paste(file_dir,runname[run],"_MFC.yearly_means.",R2.Start,"_",R2.End,"_ensmean.nc",sep="")
        print(file_exp)
        ex.nc = open.ncdf(file_exp)

        time_exp_units<-att.get.ncdf( ex.nc,"time","units")
        base_date_exp=as.character(lapply(strsplit(as.character(time_exp_units$value),split=" "),"[",3))
        time_exp=get.var.ncdf( ex.nc,"time")
        time_exp_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_exp))
        time_exp.y<-format(time_exp_d, "%Y")
        time_exp.m<-format(time_exp_d, "%m")

        t1=min(which(as.numeric(time_exp.y)>=A.Start))
        t2=(A.End-A.Start+1)

        lon=get.var.ncdf( ex.nc, "lon")
        lat=get.var.ncdf( ex.nc, "lat")
	nlat=dim(lat)
	nlon=dim(lon)

	levels<-get.var.ncdf( ex.nc,"lev")
	nlev<-dim(levels)

	  mfc_exp=get.var.ncdf( ex.nc, variable1, c(1,1,1,t1), c(nlon, nlat,nlev,t2)) #lon,lat,lev,time
	  unit_mfc<- att.get.ncdf(ex.nc, variable1, "units")

	  mfcint_exp=get.var.ncdf( ex.nc, variable2, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time
	  unit_mfcint<- att.get.ncdf(ex.nc, variable2, "units")

	file_ctl=paste(file_dir,ctl,"_MFC.yearly_means.",R1.Start,"_",R1.End,"_ensmean.nc",sep="")
        print(file_ctl)
        ex.nc = open.ncdf(file_ctl)

        time_ctl_units<-att.get.ncdf( ex.nc,"time","units")
        base_date_ctl=as.character(lapply(strsplit(as.character(time_ctl_units$value),split=" "),"[",3))
        time_ctl=get.var.ncdf( ex.nc,"time")
        time_ctl_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_ctl))
        time_ctl.y<-format(time_ctl_d, "%Y")
        time_ctl.m<-format(time_ctl_d, "%m")

        t1=min(which(as.numeric(time_ctl.y)>=A.Start))

	  mfc_ctl=get.var.ncdf( ex.nc, variable1, c(1,1,1,t1), c(nlon, nlat,nlev,t2)) #lon,lat,lev,time
          mfcint_ctl=get.var.ncdf( ex.nc, variable2, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time

	dims=dim(mfcint_exp)
	ntim=dims[3]

	y=1
        for (yr in 1:nyears){
	    tmp1<-var_exp[,,y]
	    tmp1[which(lsm==0)] <- NA
	    var_exp[,,y] <- tmp1
	    tmp2<-var_ctl[,,y]
	    tmp2[which(lsm==0)] <- NA
	    var_ctl[,,y] <- tmp2
	    y=y+1
        } #end loop years

	#calculate difference EXP-CTL
	mfcint_anom<-mfcint_exp-mfcint_ctl

	#apply modified t-test, compare diff to 0
	test3d<-modTtest3d(mfcint_anom,mu=0,alternative = c("two.sided"),conf.level=1-siglev)

	Pval_exp_ctl<-test3d$p.value
	Hval_exp_ctl<-array(NA,dim=c(nlon,nlat))
        for (y in 1:nlat){
            for (x in 1:nlon){
                if (all(is.na(mfcint_anom[x,y,]))) {
                   Hval_exp_ctl[x,y]<-NA
                } else {
                    if (Pval_exp_ctl[x,y]<siglev){
                       Hval_exp_ctl[x,y]<-1
                    } else {
                       Hval_exp_ctl[x,y]<-0
                    }
                }
            }
        }
	
	mfcint_exp_ave<-apply(mfcint_exp,c(1,2),mean,na.rm=TRUE)
	mfcint_ctl_ave<-apply(mfcint_ctl,c(1,2),mean,na.rm=TRUE)
	mfcint_anom_ave<-mfcint_exp_ave-mfcint_ctl_ave

	mfc_exp_ave<-apply(mfc_exp,c(1,2,3),mean,na.rm=TRUE)
	mfc_ctl_ave<-apply(mfc_ctl,c(1,2,3),mean,na.rm=TRUE)
	mfc_anom_ave<-mfc_exp_ave-mfc_ctl_ave

	###check field significance with Walker's test
	sig_walk<-walkerTest(test3d$p.value, siglev=siglev)

	###check field significance with False Discovery Rate
	sig_fdr<-fdrTest(test3d$p.value, siglev=siglev)

	###save data as netcdf to plot
	print("Save data to netcdf file for plotting")
	outfile<-paste(out_dir,runname[run],"/",variable1,"_",runname[run],"_",ctl,"_ann_modTtest_",A.Start,"_",A.End,".nc",sep="")
	print(outfile)

	#define dimension
	dimLat <- dim.def.ncdf("lat", "degrees_north", lat)
	dimLon <- dim.def.ncdf("lon", "degrees_east", lon)
	dimLev <- dim.def.ncdf("lev", "Pa", levels)

	#define variables
	FillValue<- -9999
	exp_Ave<-var.def.ncdf("exp_Ave", unit_mfcint$value, list(dimLon,dimLat), FillValue)
	ctl_Ave<-var.def.ncdf("ctl_Ave", unit_mfcint$value, list(dimLon,dimLat), FillValue)
	anom_Ave<-var.def.ncdf("anom_Ave", unit_mfcint$value, list(dimLon,dimLat), FillValue)
	exp_Ave_3D<-var.def.ncdf("exp_Ave_3D", unit_mfc$value, list(dimLon,dimLat,dimLev), FillValue)
	ctl_Ave_3D<-var.def.ncdf("ctl_Ave_3D", unit_mfc$value, list(dimLon,dimLat,dimLev), FillValue)
	anom_Ave_3D<-var.def.ncdf("anom_Ave_3D", unit_mfc$value, list(dimLon,dimLat,dimLev), FillValue)
	Hval<-var.def.ncdf("Hval","-",list(dimLon,dimLat), FillValue) #,prec="integer")
	Pval<-var.def.ncdf("Pval","-",list(dimLon,dimLat), FillValue) #,prec="double")
	field_walk<-var.def.ncdf("FS_walk","-",list(dimLon,dimLat), FillValue) #,prec="integer")
	field_fdr<-var.def.ncdf("FS_fdr","-",list(dimLon,dimLat), FillValue) #,prec="integer")

	#create netcdf
	nc <- create.ncdf( outfile, list(exp_Ave,ctl_Ave,anom_Ave,exp_Ave_3D,ctl_Ave_3D,anom_Ave_3D,Hval,Pval,field_walk,field_fdr))

	#put variables into file
	put.var.ncdf(nc, exp_Ave,mfcint_exp_ave, start = c(1, 1),  count = c(nlon, nlat))
	put.var.ncdf(nc, ctl_Ave,mfcint_ctl_ave, start = c(1, 1),  count = c(nlon, nlat))
	put.var.ncdf(nc, anom_Ave,mfcint_anom_ave, start = c(1, 1),  count = c(nlon, nlat))
	put.var.ncdf(nc, exp_Ave_3D,mfc_exp_ave, start = c(1, 1,1),  count = c(nlon, nlat, nlev))
	put.var.ncdf(nc, ctl_Ave_3D,mfc_ctl_ave, start = c(1, 1,1),  count = c(nlon, nlat, nlev))
	put.var.ncdf(nc, anom_Ave_3D,mfc_anom_ave, start = c(1, 1,1),  count = c(nlon, nlat, nlev))
	put.var.ncdf(nc, Hval,Hval_exp_ctl, start = c(1, 1),  count = c(nlon, nlat))
	put.var.ncdf(nc, Pval,Pval_exp_ctl, start = c(1, 1),  count = c(nlon, nlat))
	put.var.ncdf(nc, field_walk,sig_walk$h.value, start = c(1, 1),  count = c(nlon, nlat))
	put.var.ncdf(nc, field_fdr,sig_fdr$h.value, start = c(1, 1),  count = c(nlon, nlat))

	#close file
	close.ncdf(nc)

	rm(exp_Ave)
	rm(ctl_Ave)
	rm(anom_Ave)
	rm(exp_Ave_3D)
	rm(ctl_Ave_3D)
	rm(anom_Ave_3D)
	rm(Hval)
	rm(Pval)

}#end loop runs