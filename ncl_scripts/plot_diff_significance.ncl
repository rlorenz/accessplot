;-----------------------------------------------------------------------------;
; File Name : plot_diff_significance.ncl
; Creation Date : 23-01-2014
; Last Modified : Mon 16 Nov 2015 11:55:30 AEDT
; Created By : Ruth Lorenz
; Purpose : Plot differences of experiments with significance

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "/home/z3441306/scripts/plot_scripts/ncl_scripts/modTtest_small.ncl"
load "/home/z3441306/scripts/plot_scripts/ncl_scripts/plot_seas_panel_overlay_significance.ncl"
load "/home/z3441306/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"
begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")
outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))           ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin,PR
;-----------------------------------------------------------------------------;
varname = (/"pr","tas","tasmax","tasmin","rss","rls","rnet","hfls","hfss","alb","cancd","npp"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","SW~B~NET~N~","LW~B~NET~N~","R~B~NET~N~","LH","SH","ALB","CAN","NPP"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","W m~S~-2~N~","W m~S~-2~N~","W m~S~-2~N~","W m~S~-2~N~","W m~S~-2~N~","-","m s~S~-1~N~","g C m~S~-2 ~N~d~S~-1~N~"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;

;start loop
do v = 0,11
   if ( v .eq. 0 ) then
      Var="PR"
      print("reading NetCDFs")

      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="orange_to_blue_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.1,0.1,0.5,1,1.5,2,2.5,3/)
      ;lev=(/-1,-0.8,-0.6,-0.4,-0.2,-0.05,0.05,0.2,0.4,0.6,0.8,1/)
      pltColor_a="light_blue_to_dark_blue_12lev"
      lev_a=(/0.5,1,2,3,4,5,6,7,8/)
   else if ( v .eq. 1 ) then
      Var="T"
      print("reading NetCDFs")

      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0.5,1,1.5,2,2.5,3,3.5,4/)
      ;lev=(/-1,-0.8,-0.6,-0.4,-0.2,-0.1,-0.05,0.05,0.1,0.2,0.4,0.6,0.8,1/)
      pltColor_a="BlueGreenYellowRed"
      lev_a=(/-30,-20,-10,0,5,10,15,20,25,30,35/)
   else if ( v .eq. 2 ) then
      Var="TX"
      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")

      file2 = addfile(workDir +Runid2+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0.5,1,1.5,2,2.5,3,3.5,4/)
      ;lev=(/-1,-0.8,-0.6,-0.4,-0.2,-0.1,-0.05,0.05,0.1,0.2,0.4,0.6,0.8,1/)
      pltColor_a="BlueGreenYellowRed"
      lev_a=(/-10,0,5,10,15,20,25,30,35,40/)
   else if ( v .eq. 3 ) then
      Var="TN"
      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")

      file2 = addfile(workDir +Runid2+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-4,-3.5,-3,-2.5,-2,-1.5,-1,-0.5,0.5,1,1.5,2,2.5,3,3.5,4/)
      ;lev=(/-1,-0.8,-0.6,-0.4,-0.2,-0.1,-0.05,0.05,0.1,0.2,0.4,0.6,0.8,1/)
      pltColor_a="BlueGreenYellowRed"
      lev_a=(/-30,-20,-10,0,5,10,15,20,25,30,35/)
   else if ( v .eq. 4 ) then
      Var="SW"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-60,-50,-40,-30,-20,-10,10,20,30,40,50,60/)
      ;lev = (/-6,-5,-4,-3,-2,-1,-0.5,0.5,1,2,3,4,5,6/)
      pltColor_a="sunshine_10lev"
      lev_a=(/30,60,90,120,150,180,210,240,270/)
   else if ( v .eq. 5 ) then
      Var="LW"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-80,-60,-50,-40,-30,-20,-10,10,20,30,40,50,60,80/)
      ;lev = (/-6,-5,-4,-3,-2,-1,-0.5,0.5,1,2,3,4,5,6/)
      pltColor_a="sunshine_10lev"
      lev_a=(/-180,-160,-140,-120,-100,-80,-60,-40,-20/)
   else if ( v .eq. 6 ) then
      Var="RN"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir  +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-60,-50,-40,-30,-20,-10,10,20,30,40,50,60/)
      ;lev = (/-6,-5,-4,-3,-2,-1,-0.5,0.5,1,2,3,4,5,6/)
      pltColor_a="sunshine_10lev"
      lev_a=(/25,50,75,100,125,150,175,200,225/)
  else if ( v .eq. 7 ) then
      Var="LH"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="precip_diff_12lev"
      lev = (/-50,-40,-30,-20,-10,-5,5,10,20,30,40,50/)
      ;lev = (/-10,-8,-6,-4,-2,-0.5,0.5,2,4,6,8,10/)
      pltColor_a="precip_11lev"
      lev_a=(/-20,0,20,40,60,80,100,120,140,160,180/)
  else if ( v .eq. 8 ) then
      Var="SH"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev = (/-50,-40,-30,-20,-10,-5,5,10,20,30,40,50/)
      ;lev = (/-10,-8,-6,-4,-2,-0.5,0.5,2,4,6,8,10/)
      pltColor_a="BlueGreenYellowRed"
      lev_a=(/-60,-40,-20,0,20,40,60,80,100,120,140/)
  else if ( v .eq. 9 ) then
      Var="ALB"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_loud"
      lev = (/-0.1,-0.08,-0.06,-0.04,-0.02,-0.01,0.01,0.02,0.04,0.06,0.08,0.1/)
      ;lev = (/-0.01,-0.008,-0.006,-0.004,-0.002,-0.001,0.001,0.002,0.004,0.006,0.008,0.01/)
      pltColor_a="grey_to_white"
      lev_a=(/0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9/)
  else if ( v .eq. 10 ) then
      Var="CC"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev =(/-0.012,-0.01,-0.008,-0.006,-0.004,-0.002,0.002,0.004,0.006,0.008,0.01,0.012/)
      ;lev = (/-0.05,-0.04,-0.03,-0.02,-0.01,-0.005,0.005,0.01,0.02,0.03,0.04,0.05/)
      pltColor_a="BlueDarkOrange18"
      lev_a=(/0,0.02,0.04,0.06,0.08,0.1,0.12,0.14,0.16/)
  else if ( v .eq. 11 ) then
      Var="NPP"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +Runid2+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_loud"
      lev = (/-0.8,-0.6,-0.4,-0.2,-0.05,0.05,0.2,0.4,0.6,0.8/)
      pltColor_a="BlueGreenYellowRed"
      lev_a=(/0,0.5,1.5,2,2.5,3,3.5,4,4.5/)
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;

nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

;derive time axis
time_ctl = file1->time
time_ctl@calendar = "gregorian"
ctl_date = ut_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
lat = file1->lat
lon = file1->lon

time_exp = file2->time
time_exp@calendar = "gregorian"
exp_date = ut_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=exp_date(ind_yrs_exp,:)

if (Var .eq. "PR") then	;convert prec to mm/day
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig*(24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "T" .or. Var .eq. "TX" .or. Var .eq. "TN") then	;convert temp to degC
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig-273.15
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig-273.15
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "RN") then	;add SW and LW to Rnet
      sw_ctl = file1 ->$varname(4)$(ind_yrs_ctl,:,:)
      sw_exp = file2 ->$varname(4)$(ind_yrs_exp,:,:)
      lw_ctl = file1 ->$varname(5)$(ind_yrs_ctl,:,:)
      lw_exp = file2 ->$varname(5)$(ind_yrs_exp,:,:)
      var_ctl=sw_ctl+lw_ctl
      copy_VarMeta(sw_ctl,var_ctl)
      var_exp=sw_exp+lw_exp
      copy_VarMeta(sw_exp,var_exp)
else if (Var .eq. "NPP")
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig*(1000*24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(1000*24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)
end if
end if
end if
end if

      var_ctl@long_name     = longname(v)
      var_ctl@units         = units(v)

      var_exp@long_name     = longname(v)
      var_exp@units         = units(v)

printVarSummary(var_ctl)
printVarSummary(var_exp)

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(var_ctl)
dsize_exp=dimsizes(var_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
        print("ERROR: CTL and EXP do not have same dimension, exiting")
        exit
else
        ntim=ntim_ctl
        nlat=nlat_ctl
        nlon=nlon_ctl

        delete(ntim_ctl)
        delete(nlat_ctl)
        delete(nlon_ctl)
        delete(ntim_exp)
        delete(nlat_exp)
        delete(nlon_exp)
end if
;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
do j = 0,3
;season
        if (j .eq. 0) then
                seas="DJF"

                ind_seas_ctl=ind(new_date_ctl(:,1).eq.1 .or. \
                        new_date_ctl(:,1).eq.2 .or. new_date_ctl(:,1).eq.12)
                var_ctl_seas=(/var_ctl(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.1 .or. \
                        new_date_exp(:,1).eq.2 .or. new_date_exp(:,1).eq.12)
                var_exp_seas=(/var_exp(ind_seas_exp,:,:)/)

        else if (j .eq. 1) then
                seas="MAM"

                ind_seas_ctl=ind(new_date_ctl(:,1).eq.3 .or. \
                        new_date_ctl(:,1).eq.4 .or. new_date_ctl(:,1).eq.5)
                var_ctl_seas=(/var_ctl(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.3 .or. \
                        new_date_exp(:,1).eq.4 .or. new_date_exp(:,1).eq.5)
                var_exp_seas=(/var_exp(ind_seas_exp,:,:)/)

        else if (j .eq. 2) then
                seas="JJA"
                ind_seas_ctl=ind(new_date_ctl(:,1).eq.6 .or. \
                        new_date_ctl(:,1).eq.7 .or. new_date_ctl(:,1).eq.8)
                var_ctl_seas=(/var_ctl(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.6 .or. \
                        new_date_exp(:,1).eq.7 .or. new_date_exp(:,1).eq.8)
                var_exp_seas=(/var_exp(ind_seas_exp,:,:)/)

        else if (j .eq. 3) then
                seas="SON"

                ind_seas_ctl=ind(new_date_ctl(:,1).eq.9 .or. \
                        new_date_ctl(:,1).eq.10 .or. new_date_ctl(:,1).eq.11)
                var_ctl_seas=(/var_ctl(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.9 .or. \
                        new_date_exp(:,1).eq.10 .or. new_date_exp(:,1).eq.11)
                var_exp_seas=(/var_exp(ind_seas_exp,:,:)/)

        end if
        end if
        end if
        end if

var_ctl_seas!0="time"
var_ctl_seas!1="lat"
var_ctl_seas!2="lon"
var_ctl_seas&lon = lon
var_ctl_seas&lat = lat

var_exp_seas!0="time"
var_exp_seas!1="lat"
var_exp_seas!2="lon"
var_exp_seas&lon = lon
var_exp_seas&lat = lat

;-----------------------------------------------------------------------------;
; Calculating seasonal means
;-----------------------------------------------------------------------------;
if ( j .eq. 0) then
   var_exp_seas_avg=new((/4,nlat,nlon/),float)
end if

var_ctl_seas_avg=dim_avg_n_Wrap(var_ctl_seas,0)
var_exp_seas_avg(j,:,:)=dim_avg_n_Wrap(var_exp_seas,0)
var_exp_seas_avg@long_name= longname(v)+" "+Runname2
var_exp_seas_avg@units    = units(v)

;-----------------------------------------------------------------------------;
; Calculating differences
;-----------------------------------------------------------------------------;
if ( j .eq. 0) then
   var_diff_seas_avg=new((/4,nlat,nlon/),float)
end if

var_diff_seas_avg(j,:,:)= var_exp_seas_avg(j,:,:)-var_ctl_seas_avg

var_diff_seas_avg@long_name= longname(v)+" "+Runname2+"-"+Runname1
var_diff_seas_avg@units    = units(v)
var_diff_seas_avg!0="time"
var_diff_seas_avg!1="lat"
var_diff_seas_avg!2="lon"
var_diff_seas_avg&lon = lon
var_diff_seas_avg&lat = lat

;-----------------------------------------------------------------------------;
; Test if differences statistically significant with modified t-test
;-----------------------------------------------------------------------------;
 ctl_tmp = var_ctl_seas(lat|:,lon|:,time|:)	;reorder but do it only once [temporary]
 exp_tmp = var_exp_seas(lat|:,lon|:,time|:)
    
 ctlAve = dim_avg (ctl_tmp)              ; calculate means at each grid point 
 expAve = dim_avg (exp_tmp)
 ctlVar = dim_variance (ctl_tmp)         ; calculate variances
 expVar = dim_variance (exp_tmp)

 ctlEqv = equiv_sample_size(ctl_tmp, sigr,0)
 expEqv = equiv_sample_size(exp_tmp, sigr,0)

; if N<30 we need to use modified t-test for small nr. (look up tables)
;use the cosine of the latitides for weight
  rad    = 4.0*atan(1.0)/180.0
  clat1   = cos(lat*rad)

  AvgctlEqv   = wgt_areaave (ctlEqv, clat1, 1., 0)
  AvgexpEqv   = wgt_areaave (expEqv, clat1, 1., 0) 

  VarAvgctl = wgt_areaave (ctlVar, clat1, 1., 0)
  VarAvgexp = wgt_areaave (expVar, clat1, 1., 0)

if ( AvgctlEqv .gt. 30 ) .and. ( AvgexpEqv .gt. 30 )
   probf=ftest(VarAvgctl,AvgctlEqv,VarAvgexp,AvgexpEqv,0)
   if ( probf .ge. 0.1 ) then
      iflag = False
   else
	iflag = True  
   end if 

   prob = ttest(ctlAve,ctlVar,AvgctlEqv,expAve,expVar,AvgexpEqv,iflag,False) 
   H=where(prob .lt. sigr,1,0)

else if (AvgctlEqv .gt. 12 ) .and. ( AvgexpEqv .gt. 12 )
   modTtest=modTtest_small(var_ctl_seas,var_exp_seas,sigr,ntim/4)
   H=toint(modTtest(0,:,:))	
else
   print("sample size too small for modified t-test")
   H=new((/nlat,nlon/),integer)
   H=0
end if
end if
if ( j .eq. 0) then
   signif=new((/4,nlat,nlon/),integer)
end if
signif(j,:,:)=(/H/)

   signif!0		    = "time"
   signif!1		    = "lat"
   signif!2		    = "lon"
   signif&lat		    = lat
   signif&lon		    = lon

end do

;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;
Panel_Plot_sig(outDir,"diff_"+Runname2+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(:,:,:),signif(:,:,:),False,True,lev,True)

Panel_Plot(outDir,Runname2+"_"+varname(v),AStart,AEnd,plttype,pltColor_a,var_exp_seas_avg(:,:,:),False,True,lev_a,True)


delete(lev)
delete(lev_a)
delete(var_exp_seas_avg)
delete(var_diff_seas_avg)
delete(time_ctl)
delete(ctl_date)
delete(time_exp)
delete(exp_date)
delete(ind_yrs_ctl)
delete(new_date_ctl)
delete(ind_yrs_exp)
delete(new_date_exp)

end do ;loop over variables
;-----------------------------------------------------------------------------;
end
