;-----------------------------------------------------------------------------;
; File Name : coupling_strength_temp.ncl
; Creation Date : March 2014
; Last Modified : Mon 10 Mar 2014 08:51:23 EST
; Created By : Ruth Lorenz
; Purpose : calculate and plot coupling strength from LUC experiments
; coupling strength is 

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel_overlay_significance.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME
Region = systemfunc("echo $REGION") ; model

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))           ; critical sig lvl for r
;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin
;-----------------------------------------------------------------------------;
if ( Region .eq. "sc" ) then
   runids = (/"uaoyc","uaoye","uaoyg","uaoyi","uaoyk","uaoym","uaoyo","uaoya"/)
   runnames = (/"001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc"/)
else if ( Region .eq. "wc" ) then
   runids = (/"uaoyd","uaoyf","uaoyh","uaoyj","uaoyl","uaoyn","uaoyp","uaoyb"/)
   runnames = (/"001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc"/)
else print("wrong region name, does not exist")
end if
end if


varname = (/"pr","tas","tasmax","tasmin"/)
longname = (/"P~B~TOT~N~","Temperature at 1.5m","daily maximum Temperature","daily minimum Temperature"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C"/)

;start loop
do v = 1,3

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;

   do run = 0,7
   if ( v .eq. 0 ) then
      Var="PR"
   else if ( v .eq. 1 ) then
      Var="T"
   else if ( v .eq. 2 ) then
      Var="TX"
   else if ( v .eq. 3 ) then
      Var="TN"
   end if
   end if
   end if
   end if

      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      file1 = addfile(workDir +Runid1+ ".daily_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      end if

      file2 = addfile(workDir +runids(run)+ ".daily_TS." \
                       + R2Start + "_" + R2End + ".nc","r")

;-----------------------------------------------------------------------------;
; read temperature data
;-----------------------------------------------------------------------------;
if ( run .eq. 0 ) then
nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

;derive time axis
time_ctl = file1->time
time_ctl@calendar = "gregorian"
ctl_date = cd_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
lat = file1->lat
lon = file1->lon

time_exp = file2->time
time_exp@calendar = "gregorian"

exp_date = cd_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=exp_date(ind_yrs_exp,:)

end if

if (Var .eq. "PR") then	;convert prec to mm/day
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig*(24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)        ;temperature at 1.5m

      var_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)        ;temperature at 1.5m

end if

      var_ctl@long_name     = longname(v)
      var_ctl@units         = units(v)

      var_exp@long_name     = longname(v)
      var_exp@units         = units(v)

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(var_ctl)
dsize_exp=dimsizes(var_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
	print("ERROR: CTL and EXP do not have same dimension, exiting")
	exit
else
	ntim=ntim_ctl
	nlat=nlat_ctl
	nlon=nlon_ctl

	delete(ntim_ctl)
	delete(nlat_ctl)
	delete(nlon_ctl)
	delete(ntim_exp)
	delete(nlat_exp)
	delete(nlon_exp)
end if

;-----------------------------------------------------------------------------;
; detrend timeseries
;-----------------------------------------------------------------------------;
var_ctl_dtr=dtrend_leftdim(var_ctl,False)
var_exp_dtr=dtrend_leftdim(var_exp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
	do j = 0,3
	;season, choose indices for months of season, remove first 8(JJA,MAM)/7(SON)/6(DJF) days per season
	; in accordance with Koster et al 2006 and Seneviratne et al 2006
	  if (j .eq. 0) then
		seas="DJF"

		ind_seas_ctl=ind(new_date_ctl(:,1).eq.1 .or. (new_date_ctl(:,1).eq.2 .and. new_date_ctl(:,2).ne.29 )\
		 .or. (new_date_ctl(:,1).eq.12 .and. new_date_ctl(:,2).gt.6) )
		var_ctl_seas=var_ctl_dtr(ind_seas_ctl,:,:)

		ind_seas_exp=ind(new_date_exp(:,1).eq.1 .or. (new_date_exp(:,1).eq.2 .and. new_date_ctl(:,2).ne.29)\
		 .or. (new_date_exp(:,1).eq.12 .and. new_date_exp(:,2).gt.6))
		var_exp_seas=var_exp_dtr(ind_seas_exp,:,:)

	  else if (j .eq. 1) then
		seas="MAM"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.3 .and. new_date_ctl(:,2).gt.8) \
		 .or. new_date_ctl(:,1).eq.4 .or.new_date_ctl(:,1).eq.5 )
		var_ctl_seas=var_ctl_dtr(ind_seas_ctl,:,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.3 .and. new_date_exp(:,2).gt.8) \
		 .or. new_date_exp(:,1).eq.4 .or. new_date_exp(:,1).eq.5)
		var_exp_seas=var_exp_dtr(ind_seas_exp,:,:)

	  else if (j .eq. 2) then
		seas="JJA"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.6 .and. new_date_ctl(:,2).gt.8) \
		 .or. new_date_ctl(:,1).eq.7 .or. new_date_ctl(:,1).eq.8)
		var_ctl_seas=var_ctl_dtr(ind_seas_ctl,:,:)
	
		ind_seas_exp=ind((new_date_exp(:,1).eq.6 .and. new_date_exp(:,2).gt.8) \
		 .or. new_date_exp(:,1).eq.7 .or. new_date_exp(:,1).eq.8)
		var_exp_seas=var_exp_dtr(ind_seas_exp,:,:)

	  else if (j .eq. 3) then
		seas="SON"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.9 .and. new_date_ctl(:,2).gt.7) \
		 .or. new_date_ctl(:,1).eq.10 .or. new_date_ctl(:,1).eq.11)
		var_ctl_seas=var_ctl_dtr(ind_seas_ctl,:,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.9 .and. new_date_exp(:,2).gt.7) \
		 .or. new_date_exp(:,1).eq.10 .or. new_date_exp(:,1).eq.11)
		var_exp_seas=var_exp_dtr(ind_seas_exp,:,:)

	  end if
	  end if
	  end if
	  end if

	  ntim_seas=dimsizes(var_ctl_seas(:,0,0))

;-----------------------------------------------------------------------------;
; Calculate 6-daily means
;-----------------------------------------------------------------------------;
	ntim_6d=ntim_seas/6

	var_ctl_6dmean=new((/ntim_6d,nlat,nlon/),float)
	var_exp_6dmean=new((/ntim_6d,nlat,nlon/),float)
	ind_t=0

	do tt=0,ntim_seas-1,6
	   var_ctl_6dmean(ind_t,:,:)=dim_avg_n_Wrap(var_ctl_seas(tt:tt+5,:,:),0)
	   var_exp_6dmean(ind_t,:,:)=dim_avg_n_Wrap(var_exp_seas(tt:tt+5,:,:),0)
	   ind_t=ind_t+1
	end do

	delete(var_ctl_seas)
	delete(var_exp_seas)

;-----------------------------------------------------------------------------;
; Calculate mean climatology
;-----------------------------------------------------------------------------;
	ndays=ntim_6d/nyears
	var_ctl_yd=reshape(var_ctl_6dmean,(/nyears,ndays,nlat,nlon/))
	var_exp_yd=reshape(var_exp_6dmean,(/nyears,ndays,nlat,nlon/))

	var_ctl_clim=dim_avg_n(var_ctl_yd,0)
	var_exp_clim=dim_avg_n(var_exp_yd,0)

;-----------------------------------------------------------------------------;
; Calculating standard deviations
;-----------------------------------------------------------------------------;

	print("Calculating standard deviations")
	std_ctl=dim_variance_n(var_ctl_clim,0)
	std_exp=dim_variance_n(var_exp_clim,0)

	std_ctl_full=dim_variance_n(var_ctl_6dmean,0)
	std_exp_full=dim_variance_n(var_exp_6dmean,0)

	if ( j.eq.0 ) then
	omega_ctl=new((/4,nlat,nlon/),float)
	omega_exp=new((/4,nlat,nlon/),float)
	coup=new((/4,nlat,nlon/),float)
	end if

	print("Calculate Omegas and coupling")
	omega_ctl(j,:,:)=((nyears*std_ctl)-std_ctl_full)/((nyears-1) * where(std_ctl_full.eq.0,std_ctl_full@_FillValue,std_ctl_full))
	omega_exp(j,:,:)=((nyears*std_exp)-std_exp_full)/((nyears-1) * where(std_exp_full.eq.0,std_exp_full@_FillValue,std_exp_full))

	coup(j,:,:)=omega_exp(j,:,:)-omega_ctl(j,:,:)

	delete(var_ctl_clim)
	delete(var_exp_clim)
	delete(std_ctl)
	delete(std_exp)
	delete(std_ctl_full)
	delete(std_exp_full)

;-----------------------------------------------------------------------------;
; bootstrap for significance testing:
; draw with replacement, random new timeseries, do 1000 times
;-----------------------------------------------------------------------------;

	var_ctl_yd_boot=new((/nyears,ndays,nlat,nlon/),float)
	var_exp_yd_boot=new((/nyears,ndays,nlat,nlon/),float)

do boot=0,999
      tmp=new((/nyears/),integer)
      do tt=0,nyears-1
      	 tmp(tt) = rand()
      end do ;tt

      indices=round((nyears-1)/32766.0*tmp,3)

       var_ctl_yd_boot=var_ctl_yd(indices,:,:,:)
       var_exp_yd_boot=var_exp_yd(indices,:,:,:)

; Calculate mean climatology

	var_ctl_clim=dim_avg_n(var_ctl_yd_boot,0)
	var_exp_clim=dim_avg_n(var_exp_yd_boot,0)

; Calculating variance

	;print("Calculating variance")
	std_ctl=dim_variance_n(var_ctl_clim,0)
	std_exp=dim_variance_n(var_exp_clim,0)

	;rearange var_ctl_yd_boot to (boot,time,lat,lon)
	var_ctl_6daymean_boot=reshape(var_ctl_yd_boot,(/nyears*ndays,nlat,nlon/))
	var_exp_6daymean_boot=reshape(var_exp_yd_boot,(/nyears*ndays,nlat,nlon/))
	std_ctl_full=dim_variance_n(var_ctl_6daymean_boot,0)
	std_exp_full=dim_variance_n(var_exp_6daymean_boot,0)

	if ( j.eq.0 .and. boot .eq. 0 ) then
	omega_ctl_boot=new((/1000,4,nlat,nlon/),float)
	omega_exp_boot=new((/1000,4,nlat,nlon/),float)
	coup_boot=new((/1000,4,nlat,nlon/),float)
	end if

	;print("Calculate Omegas and coupling for significance testing")
	omega_ctl_boot(boot,j,:,:)=((nyears*std_ctl)-std_ctl_full)/((nyears-1) *\
			 where(std_ctl_full.eq.0,std_ctl_full@_FillValue,std_ctl_full))
	omega_exp_boot(boot,j,:,:)=((nyears*std_exp)-std_exp_full)/((nyears-1) *\
			 where(std_exp_full.eq.0,std_exp_full@_FillValue,std_exp_full))

	coup_boot(boot,j,:,:)=omega_exp_boot(boot,j,:,:)-omega_ctl_boot(boot,j,:,:)

   end do ;boot

;calculate significance
	tval = dim_avg_n(coup_boot(:,j,:,:),0) / where(dim_stddev_n(coup_boot(:,j,:,:),0).ne.0, \
	       dim_stddev_n(coup_boot(:,j,:,:),0),var_ctl@_FillValue )
	pval = student_t(tval,1000)
 	
	H=where(pval .lt. sigr,1,0)

	if ( j .eq. 0) then
   	   signif=new((/4,nlat,nlon/),integer)
	end if

	signif(j,:,:)=(/H/)


	delete(var_ctl_clim)
	delete(var_exp_clim)
	delete(var_ctl_6dmean)
	delete(var_exp_6dmean)
	delete(std_ctl)
	delete(std_exp)
	delete(std_ctl_full)
	delete(std_exp_full)
	delete(ind_seas_ctl) ;remove when updated Tmin from GC1A85
	delete(ind_seas_exp)

	end do ;season

   	signif!0                 = "time"
   	signif!1                 = "lat"
   	signif!2                 = "lon"
   	signif&lat               = lat
   	signif&lon               = lon

	omega_ctl@long_name = "~F33~W~F21~~B~" +Var+"~N~("+Runname1+")"
	omega_ctl@units = "-"
	omega_ctl!0="time"
	omega_ctl!1="lat"
	omega_ctl!2="lon"
	omega_ctl&lon = var_ctl&lon
	omega_ctl&lat = var_ctl&lat

	omega_exp@long_name = "~F33~W~F21~~B~" +Var+"~N~("+runnames(run)+")"
	omega_exp@units = "-"
	omega_exp!0="time"
	omega_exp!1="lat"
	omega_exp!2="lon"
	omega_exp&lon = var_ctl&lon
	omega_exp&lat = var_ctl&lat

	coup@long_name = "~F33~W~F21~~B~" +Var+"~N~("+runnames(run)+")-~F33~W~F21~~B~" +Var+"~N~("+Runname1+")"
	coup@units = "-"
	coup!0="time"
	coup!1="lat"
	coup!2="lon"
	coup&lon = var_ctl&lon
	coup&lat = var_ctl&lat


;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;
	outDir = systemfunc("echo $OUTDIR")+runnames(run)+"/"


	;Levels for contours
	lev_o=(/0.00, 0.10, 0.20, 0.30, 0.40, 0.50, 0.60, 0.70, 0.80, 0.90, 1.00/)
	lev_c=(/-0.12,-0.08,-0.04,0.04,0.08,0.12,0.16,0.20,0.24,0.28,0.32,0.36,0.40,0.44/)
	colorbar_o="omega_green_red"
	colorbar_c="coup_str_temp_14lev"

	Panel_Plot_sig(outDir,"omega_"+Var+"_"+Runname1,AStart,AEnd,plttype,colorbar_o,omega_ctl,signif,False,True,lev_o,True)
	Panel_Plot_sig(outDir,"aust_omega_"+Var+"_"+Runname1,AStart,AEnd,plttype,colorbar_o,omega_ctl,signif,True,True,lev_o,True)

	Panel_Plot_sig(outDir,"omega_"+Var+"_"+runnames(run),AStart,AEnd,plttype,colorbar_o,omega_exp,signif,False,True,lev_o,True)
	Panel_Plot_sig(outDir,"aust_omega_"+Var+"_"+runnames(run),AStart,AEnd,plttype,colorbar_o,omega_exp,signif,True,True,lev_o,True)

	Panel_Plot_sig(outDir,"coup_iav_"+Var,AStart,AEnd,plttype,colorbar_c,coup,signif,False,True,lev_c,True)
	Panel_Plot_sig(outDir,"aust_coup_iav_"+Var,AStart,AEnd,plttype,colorbar_c,coup,signif,True,True,lev_c,True)

;-----------------------------------------------------------------------------;
; save data to netcdf for further use
;-----------------------------------------------------------------------------;
outfile = workDir + "LUC_omega_coupstr_"+runnames(run)+ "_" +Runname1+ "_" \
	+Var+ "_" +AStart+ "-" +AEnd+ ".nc"
system("rm "+outfile)
ncdf = addfile(outfile, "c")
ncdf->omega_ctl=omega_ctl
ncdf->omega_exp=omega_exp
ncdf->coup=coup
;ncdf->omega_ctl_boot=omega_ctl_boot
;ncdf->omega_exp_boot=omega_exp_boot
ncdf->coup_boot=coup_boot
ncdf->signif=signif

;-----------------------------------------------------------------------------;
	delete(var_ctl)
	delete(var_exp)
	delete(var_ctl_dtr)
	delete(var_exp_dtr)
	delete(omega_ctl)
	delete(omega_exp)


end do ;run

end do ;variable

end ;script