;-----------------------------------------------------------------------------;
; File Name : bootstrap_move_blocks.ncl
; Creation Date : 18-02-2014
; Last Modified : Tue 18 Feb 2014 17:28:36 EST
; Created By : Ruth Lorenz
; Purpose : do bootstrapping of LUC experiment anomalies, draw with replacement,
;	moving blocks, 5yrs -> distribution estimation then
;	significance testing
;	von Storch and Zwiers, Cambridge University Press, 1999, p.94

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_seas_panel_overlay_significance_LUC.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME
Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))           ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin,PR
;-----------------------------------------------------------------------------;
if ( Region .eq. "sc" ) then
   runids = (/"uaoyc","uaoye","uaoyg","uaoyi","uaoyk","uaoym","uaoyo","uaoya"/)
   runnames = (/"001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc"/)
else if ( Region .eq. "wc" ) then
   runids = (/"uaoyd","uaoyf","uaoyh","uaoyj","uaoyl","uaoyn","uaoyp","uaoyb"/)
   runnames = (/"001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc"/)
else print("wrong region name, does not exist")
end if
end if

varname = (/"pr","tas","tasmax","tasmin","zg","psl","rss","rls","rnet","hfls","alb","cancd"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","GPH@300hPa","MSLP","SW~B~NET~N~","LW~B~NET~N~","R~B~NET~N~","LH","ALB","CAN"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","m","hPa","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","-","ms~S~-1~N~"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;

;start loop
;do v = 0,11
do v = 0,5
   do run = 0,7
   if ( v .eq. 0 ) then
      Var="PR"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="orange_to_blue_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.1,0.1,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 1 ) then
      Var="T"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 2 ) then
      Var="TX"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 3 ) then
      Var="TN"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 4 ) then
      Var="GPH"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-6,-5,-4,-3,-2,-1,-0.5,0.5,1,2,3,4,5,6/)
   else if ( v .eq. 5 ) then
      Var="MSLP"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev = (/-1.5e08,-1e08,-1.5e07,-1e07,-1.5e06,-1e06,1e06,1.5e06,1e07,1.5e07,1e08,1.5e08/)
   else if ( v .eq. 6 ) then
      Var="SW"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
   else if ( v .eq. 7 ) then
      Var="LW"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
   else if ( v .eq. 8 ) then
      Var="RN"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir  +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
  else if ( v .eq. 9 ) then
      Var="LH"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="precip_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
  else if ( v .eq. 10 ) then
      Var="ALB"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_loud"
      lev = (/-0.1,-0.08,-0.06,-0.04,-0.02,-0.01,0.01,0.02,0.04,0.06,0.08,0.1/)
  else if ( v .eq. 11 ) then
      Var="CC"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
      	 file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
	end if		     
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev =(/-0.01,-0.008,-0.006,-0.004,-0.002,-0.001,0.001,0.002,0.004,0.006,0.008,0.01/)
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;

	if ( run .eq. 0 ) then
	   nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

	   ;derive time axis
	   time_ctl = file1->time
	   time_ctl@calendar = "gregorian"
	   ctl_date = ut_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
	   lat = file1->lat
	   lon = file1->lon

	   time_exp = file2->time
	   time_exp@calendar = "gregorian"
	   exp_date = ut_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

	   ;cut into analysis period
	   ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
	   new_date_ctl=ctl_date(ind_yrs_ctl,:)

	   ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
	   new_date_exp=exp_date(ind_yrs_exp,:)

	   end if

if (Var .eq. "PR") then	;convert prec to mm/day
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig*(24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "MSLP") then	;convert mslp to hPa
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig/100
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "RN") then	;add SW and LW to Rnet
      sw_ctl = file1 ->$varname(6)$(ind_yrs_ctl,:,:)
      sw_exp = file2 ->$varname(6)$(ind_yrs_exp,:,:)
      lw_ctl = file1 ->$varname(7)$(ind_yrs_ctl,:,:)
      lw_exp = file2 ->$varname(7)$(ind_yrs_exp,:,:)
      var_ctl=sw_ctl+lw_ctl
      copy_VarMeta(sw_ctl,var_ctl)
      var_exp=sw_exp+lw_exp
      copy_VarMeta(sw_exp,var_exp)
else if (Var .eq. "GPH") then ;only read at 300hPa
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,9,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,9,:,:)
      var_ctl!1="lat"
      var_ctl!2="lon"
      var_exp!1="lat"
      var_exp!2="lon"
else
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)
end if
end if
end if
end if

      var_ctl@long_name     = longname(v)
      var_ctl@units         = units(v)

      var_exp@long_name     = longname(v)
      var_exp@units         = units(v)

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(var_ctl)
dsize_exp=dimsizes(var_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
        print("ERROR: CTL and EXP do not have same dimension, exiting")
        exit
else
        ntim=ntim_ctl
        nlat=nlat_ctl
        nlon=nlon_ctl

	delete(dsize_ctl)
	delete(dsize_exp)
        delete(ntim_ctl)
        delete(nlat_ctl)
        delete(nlon_ctl)
        delete(ntim_exp)
        delete(nlat_exp)
        delete(nlon_exp)
end if

;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;-----------------------------------------------------------------------------;
; detrend timeseries
;-----------------------------------------------------------------------------;
var_ctl_dtr=dtrend_leftdim(var_ctl,False)
var_exp_dtr=dtrend_leftdim(var_exp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
var_diff_seas_avg=new((/4,nlat,nlon/),float)

do j = 0,3
;season
        if (j .eq. 0) then
                seas="DJF"

        else if (j .eq. 1) then
                seas="MAM"
 
        else if (j .eq. 2) then
                seas="JJA"

        else if (j .eq. 3) then
                seas="SON"

        end if
        end if
        end if
        end if

var_ctl_seas=month_to_season(var_ctl_dtr,seas)
var_exp_seas=month_to_season(var_exp_dtr,seas)

var_ctl_seasavg=dim_avg_n_Wrap(var_ctl_seas,0)
var_exp_seasavg=dim_avg_n_Wrap(var_exp_seas,0)

var_diff_seas_avg(j,:,:)=var_exp_seasavg-var_ctl_seasavg
var_diff_seas_avg@long_name= longname(v)+" "+runnames(run)+"-"+Runname1
var_diff_seas_avg@units    = units(v)
var_diff_seas_avg!0="time"
var_diff_seas_avg!1="lat"
var_diff_seas_avg!2="lon"
var_diff_seas_avg&lon = var_ctl&lon
var_diff_seas_avg&lat = var_ctl&lat

dsize_seas=dimsizes(var_ctl_seas)
nyrs=dsize_seas(0)

;create 5yr blocks and
;put ctl and exp into one timeseries
ntim_5blk=nyrs/5
var_seas_5blk=new((/2,ntim_5blk,5,nlat,nlon/),float)
ind_t=0
do tt=0,ntim_5blk-1,5
   var_seas_5blk(0,ind_t,:,:,:)=var_ctl_seas(tt:tt+4,:,:)
   var_seas_5blk(1,ind_t,:,:,:)=var_exp_seas(tt:tt+4,:,:)
   ind_t=ind_t+1
end do
delete(tt)

var_seas_5blk_resh=reshape(var_seas_5blk,(/2*ntim_5blk,5,nlat,nlon/))

;bootstrap: draw with replacement, random new timeseries, do 1000 times
var_seas_boot1=new((/1000,ntim_5blk,5,nlat,nlon/),float)
var_seas_boot2=new((/1000,ntim_5blk,5,nlat,nlon/),float)

do boot=0,999
      tmp=new((/ntim_5blk/),integer)
      do tt=0,ntim_5blk-1
      	 tmp(tt) = rand()
      end do ;tt

      indices=round((ntim_5blk-1)/32766.0*tmp,3)

;      if (boot .eq. 0 ) then
;      print(tmp)
;      print(indices)
;      end if

   var_seas_boot1(boot,:,:,:,:)=var_seas_5blk_resh(indices,:,:,:)

   end do ;boot
delete(indices)

do boot=0,999
      tmp=new((/ntim_5blk/),integer)
      do tt=0,ntim_5blk-1
      	 tmp(tt) = rand()
      end do ;tt

      indices=round((ntim_5blk-1)/32766.0*tmp,3)

   var_seas_boot2(boot,:,:,:,:)=var_seas_5blk_resh(indices,:,:,:)

   end do ;boot

var_seas_boot1_resh=reshape(var_seas_boot1,(/1000*ntim_5blk*5,nlat,nlon/))
var_seas_boot2_resh=reshape(var_seas_boot2,(/1000*ntim_5blk*5,nlat,nlon/))

; apply kolmogorov-smirnoff- two-sample test
p=new((/nlat,nlon/),float)

if (Var.eq."PR") then
   pr_1_nozeros=where(var_seas_boot1_resh.gt.0.0,var_seas_boot1_resh,var_seas_boot1_resh@_FillValue)
   pr_2_nozeros=where(var_seas_boot2_resh.gt.0.0,var_seas_boot2_resh,var_seas_boot2_resh@_FillValue)

   do x=0,nlat-1
       do y=0,nlon-1
       	  p(x,y) = kolsm2_n(pr_1_nozeros(:,x,y),pr_2_nozeros(:,x,y), 0)
       end do
   end do
   delete( [/pr_1_nozeros, pr_2_nozeros/] )
else 
     do x=0,nlat-1
       do y=0,nlon-1
       	  p(x,y) = kolsm2_n(var_seas_boot1_resh(:,x,y),var_seas_boot2_resh(:,x,y), 0)
       end do
   end do
end if

 H=where(p .lt. sigr,1,0)

if ( j .eq. 0) then
   signif=new((/4,nlat,nlon/),integer)

end if

signif(j,:,:)=(/H/)

   signif!0                 = "time"
   signif!1                 = "lat"
   signif!2                 = "lon"
   signif&lat               = var_ctl&lat
   signif&lon               = var_ctl&lon

if (Var.ne."GPH" .and. Var.ne."MSLP") then
   signif(j,:,:) = where(lsm(0,0,:,:).ne.0,signif(j,:,:),signif@_FillValue)
end if

nr_sig_seas = num(signif(j,:,:).eq.1)

outDir = systemfunc("echo $OUTDIR")+runnames(run)+"/"

;save nr of signif grid points to file
filo=outDir+"bootstrp_nr_signif_gp_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_"+seas+".txt"
system("rm "+filo)
asciiwrite(filo,nr_sig_seas)

delete(nr_sig_seas)

end do ;loop over seasons

nr_sig=num(signif.eq.1)

;save nr of signif grid points to file
filo=outDir+"bootstrp_nr_signif_gp_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_all"+".txt"
system("rm "+filo)
asciiwrite(filo,nr_sig)

;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;
if (Var.ne."GPH" .and. Var.ne."MSLP") then
   Panel_Plot_sig_LUC(outDir,"diff_bstrp_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(:,:,:),signif(:,:,:),False,Region,True,lev,True)
else
   Panel_Plot_sig_LUC(outDir,"diff_bstrp_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(:,:,:),signif(:,:,:),False,Region,True,lev,False)
end if

delete(lev)
delete(var_diff_seas_avg)

end do ;run

delete(var_ctl)
delete(var_exp)
delete(ntim)
delete(nlat)
delete(nlon)
delete(var_ctl_dtr)
delete(var_exp_dtr)
delete(var_ctl_seas)
delete(var_ctl_seasavg)
delete(var_exp_seas)
delete(var_exp_seasavg)
delete(dsize_seas)
delete(var_seas_5blk)
delete(var_seas_5blk_resh)
delete(var_seas_boot1)
delete(var_seas_boot2)
delete(var_seas_boot1_resh)
delete(var_seas_boot2_resh)
delete(tval)
delete(H)
delete(signif)
end do ;loop over variables
;-----------------------------------------------------------------------------;

end
