;-----------------------------------------------------------------------------;
; File Name : tmax_AWAP_evaluation.ncl
; Creation Date : May 2013
; Last Modified : Tue 24 Sep 2013 19:59:24 EST
; Created By : Ruth Lorenz
; Purpose : Australian daily maximum temperature evaluation with AWAP data

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1") 	; RUNID
Runname1 = systemfunc("echo $RUNNAME1")	; RUNNAME

user = systemfunc("echo $USER")

if (user.eq."rzl561") then
    project = systemfunc("echo $PROJECT")
    DATAPATH="/short/" + project + "/" +user
    PATHCTL="/short/" +project+ "/" +user+ "/postproc"
else if (user.eq."z3441306") then
    DATAPATH="/srv/ccrc/data23/" +user
    PATHCTL="/srv/ccrc/data23/" +user+ "/ACCESS_output"
else
    print("Wrong USER")
    exit
end if
end if

fileDir = PATHCTL +"/"+ Runid1 + "/timeseries/"
compDir = DATAPATH + "/ACCESS_plots_workdir/"
outDir = systemfunc("echo $OUTDIR")

YStart = systemfunc("echo $AWAP_START")
YEnd = systemfunc("echo $AWAP_STOP")

;define plot type
type = systemfunc("echo $pltType")

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")

ctlfile = addfile(compDir + Runid1 + ".seasmean_tmax_tmin." + YStart + "_" + YEnd + "_Aus.nc","r")
obsfile = addfile(compDir + "AWAP_tmax_seas_mean_" + YStart + "-" + YEnd + "_regrid.nc","r")

;-----------------------------------------------------------------------------;

        tmax_K_ctl = ctlfile ->tasmax         ;max temperature rate [K]
        tmax_ctl=tmax_K_ctl-273.15  ;convert to degC
        copy_VarMeta(tmax_K_ctl,tmax_ctl)
        tmax_ctl@long_name     = Runname1+" T~B~MAX~N~"
        tmax_ctl@units         = "~S~o~N~C"

        tmax_obs = obsfile ->tmax   ;max temperature [deg C]
        tmax_obs@long_name    = "AWAP T~B~MAX~N~ regridded to UM N96"
        tmax_obs@units        = "~S~o~N~C"

print("Calculating biases")
	diff_tmax = tmax_ctl(:,:,:) - tmax_obs(:,:,:)
	copy_VarMeta(tmax_ctl(:,:,:),diff_tmax)
	diff_tmax@long_name = "T~B~MAX~N~ bias: " +Runname1+ "-AWAP"
	diff_tmax@units = "~S~o~N~C"
	diff_tmax!0 = "time"
	diff_tmax&time = tmax_ctl&time

;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;

;Levels for contours
lev_tmax  = (/-10,0,5,10,15,20,25,30,35,40,45/)              ;max temperature
lev_abtmax =  (/-8,-7,-6,-5,-4,-3,-2,-1,-0.5,0.5,1,2,3,4,5,6,7,8/)	         ;max temperature bias for Australia

colorbar_abs = "BlueGreenYellowRed"
colorbar_diff = "blue_to_red_18lev"

Panel_Plot(outDir,"aust_seas_tmax",YStart,YEnd,type,colorbar_abs,tmax_ctl(:,:,:),True,True,lev_tmax,True)
Panel_Plot(outDir,"aust_AWAP_seas_tmax",YStart,YEnd,type,colorbar_abs,tmax_obs(:,:,:),True,True,lev_tmax,True)

Panel_Plot(outDir,"austbias_tmax_AWAP",YStart,YEnd,type,colorbar_diff,diff_tmax ,True,True,lev_abtmax,True)

;---------------END----------------

end
