# plots ETCCDI indices from GCCMIP5 as spatially aggregated pdfs
# method based on Fischer and Knutti 2014 GRL

library(ncdf4)

in_dir <- "/srv/ccrc/data32/z3441306/GLACE-CMIP5/"

out_dir <- "/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/EHF/"

A.Start="2056"
A.End="2085"

#read data from netcdfs, index, all models, CTL, ExpA, ExpB
index<-list("HWA","HWM","HWN","HWF","HWD","HWT")
xaxis.min<-list(0,0,0,0,0,0)
xaxis.max<-list(80,10,25,100,150,300) #future
#xaxis.max<-list(80,10,25,100,50,300) #present
yaxis.min<-list(0.0,0.0,0.0,0.0,0.0,0.0)
yaxis.max<-list(0.13,0.6,0.3,0.08,0.05,0.01) #future
#yaxis.max<-list(0.13,0.6,0.3,0.08,0.18,0.01) #present
expname<-list("CTL","GC1A85","GC1B85")
model<-list("ACCESS","CESM","ECHAM6","GFDL","IPSL","EC-EARTH")
seas<-list("DJF","MAM","JJA","SON","ANN")
region<-list("CNA","EAS","MED","SAS","NAU","EAF","SAF","NEB")

nmod<-length(model)
nreg<-length(region)
nlat=96
nlon=96
ntim=(as.numeric(A.End)-as.numeric(A.Start)+1)

#loop over indices
for (i in 5:5){
    print (index[i])
    nmon<-1

    #loop over all regions
    for (reg in 1:nreg){
    	print(region[reg])

    	mask_file=paste("/home/z3441306/scripts/plot_scripts/areas_txt/",region[reg],".txt",sep="")
    	mask=read.table(mask_file)

    	if (region[reg]=="SAS"){
    	   maxlon<-mask[1,1]
    	   minlon<-mask[5,1]
    	   maxlat<-mask[1,2]
    	   minlat<-mask[4,2]
	   }
    	else {
		maxlon<-mask[1,1]
    		minlon<-mask[3,1]
    		maxlat<-mask[1,2]
    		minlat<-mask[2,2]
    	}

    data_seas_ctl<-array(NA,dim=c(nlon,nlat,ntim,nmod))
    data_seas_weight_ctl<-array(NA,dim=c(nlon,nlat,ntim,nmod))
    data_seas_mask_ctl<-array(NA,dim=c(nlon,nlat,ntim,nmod))

    data_seas_a<-array(NA,dim=c(nlon,nlat,ntim,nmod))
    data_seas_weight_a<-array(NA,dim=c(nlon,nlat,ntim,nmod))
    data_seas_mask_a<-array(NA,dim=c(nlon,nlat,ntim,nmod))

    data_seas_b<-array(NA,dim=c(nlon,nlat,ntim,nmod))
    data_seas_weight_b<-array(NA,dim=c(nlon,nlat,ntim,nmod))
    data_seas_mask_b<-array(NA,dim=c(nlon,nlat,ntim,nmod))

		#loop over models
    		for (mod in 1:nmod){
     
		file=paste(in_dir,model[mod],"/",expname[1],"/EHF/",model[mod],"_",expname[1],"_",A.Start,"-",A.End,"_EHFheatwaves_yearly_remapcon.nc",sep="")
      		print(file)
      		data_ctl.nc = nc_open(file)
      		#print(data_ctl.nc)

		time_units<-ncatt_get( data_ctl.nc,"time","units")
		time=ncvar_get( data_ctl.nc,"time")
		time.y<-substr(as.character(time),1,4)
		t1_ctl=min(which(as.numeric(time.y)>=A.Start))
        	t2_ctl=max(which(as.numeric(time.y)<=A.End))
		
		latitude=ncvar_get( data_ctl.nc,"lat")
		longitude=ncvar_get( data_ctl.nc,"lon")

		file=paste(in_dir,model[mod],"/",expname[2],"/EHF/",model[mod],"_",expname[2],"_",A.Start,"-",A.End,"_EHFheatwaves_yearly_remapcon.nc",sep="")
      		print(file)
      		data_a.nc = nc_open(file)
      		#print(data_a.nc)

		time_units<-ncatt_get( data_a.nc,"time","units")
		time_exp=ncvar_get( data_a.nc,"time")

		file=paste(in_dir,model[mod],"/",expname[3],"/EHF/",model[mod],"_",expname[3],"_",A.Start,"-",A.End,"_EHFheatwaves_yearly_remapcon.nc",sep="")
      		print(file)
      		data_b.nc = nc_open(file)
      		#print(data_b.nc)
		
		varname<-paste(index[[i]],"_EHF",sep="")
		print(varname)
      		data_seas_ctl[,,,mod]=ncvar_get(data_ctl.nc,varname) #read variable Annual
		data_units<-ncatt_get( data_ctl.nc,varname,"units")
      		data_seas_a[,,,mod]=ncvar_get(data_a.nc,varname) #read variable Annual
      		data_seas_b[,,,mod]=ncvar_get(data_b.nc,varname) #read variable Annual
			
		}

		#cut data into region
		data_seas_ctl[which(longitude<=minlon),,,] <- NA
		data_seas_ctl[which(longitude>=maxlon),,,] <- NA
		data_seas_ctl[,which(latitude<=minlat),,] <- NA	
		data_seas_ctl[,which(latitude>=maxlat),,] <- NA	

		data_seas_a[which(longitude<=minlon),,,] <- NA
		data_seas_a[which(longitude>=maxlon),,,] <- NA
		data_seas_a[,which(latitude<=minlat),,] <- NA	
		data_seas_a[,which(latitude>=maxlat),,] <- NA	

		data_seas_b[which(longitude<=minlon),,,] <- NA
		data_seas_b[which(longitude>=maxlon),,,] <- NA
		data_seas_b[,which(latitude<=minlat),,] <- NA	
		data_seas_b[,which(latitude>=maxlat),,] <- NA	

		#weight by latitude dependent area
		rad<-4.0*atan(1.0)/180.0
		for (lon in 1:nlon){
		    for (lat in 1:nlat){
		    	clat1 <-cos(latitude[lat]*rad)
		    	for (yr in 1:ntim){
			    data_seas_weight_ctl[lon,lat,yr,]<-data_seas_ctl[lon,lat,yr,]*clat1
			    data_seas_weight_a[lon,lat,yr,]<-data_seas_a[lon,lat,yr,]*clat1
			    data_seas_weight_b[lon,lat,yr,]<-data_seas_b[lon,lat,yr,]*clat1
			}
		    }
		}

     #mask grid points were less than half of models have data
     data_seas_mask_ctl=array(NA,dim=c(nlon,nlat,ntim,nmod))
     data_seas_mask_a=array(NA,dim=c(nlon,nlat,ntim,nmod))
     data_seas_mask_b=array(NA,dim=c(nlon,nlat,ntim,nmod))
     mask_ctl=(apply(data_seas_weight_ctl[,,1,],c(1,2),function(x) sum(is.finite(x))))
     mask_a=(apply(data_seas_weight_a[,,1,],c(1,2),function(x) sum(is.finite(x))))
     mask_b=(apply(data_seas_weight_b[,,1,],c(1,2),function(x) sum(is.finite(x))))
     for (mod in 1:6){
	     for (yr in 1:ntim){
	     	 tmp_ctl=data_seas_weight_ctl[,,yr,mod]
		 tmp_ctl[which(mask_ctl<=3)]<-NA
	     	 data_seas_mask_ctl[,,yr,mod]=tmp_ctl
	     	 tmp_a=data_seas_weight_a[,,yr,mod]
		 tmp_a[which(mask_a<=3)]<-NA
	     	 data_seas_mask_a[,,yr,mod]=tmp_a
	     	 tmp_b=data_seas_weight_b[,,yr,mod]
		 tmp_b[which(mask_b<=3)]<-NA
	     	 data_seas_mask_b[,,yr,mod]=tmp_b
	     }
     }

     #test with kolmogorov smirnoff test if pdfs different
     Atest<-ks.test(data_seas_mask_ctl[,,,],data_seas_mask_a[,,,],na.rm=T)
     Btest<-ks.test(data_seas_mask_ctl[,,,],data_seas_mask_b[,,,],na.rm=T)   
     ABtest<-ks.test(data_seas_mask_a[,,,],data_seas_mask_b[,,,],na.rm=T)

     #plot pdfs
     outfile=paste(out_dir,index[i],"_spat_agg_",region[reg],"_ANN_",A.Start,"-",A.End,"_sig.pdf", sep="")
     print(outfile)
     pdf(outfile)

     plot(density(data_seas_mask_ctl[,,,1],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=6,
			ylim=c(yaxis.min[[i]],yaxis.max[[i]]),
                        xlim=c(xaxis.min[[i]],xaxis.max[[i]]),
			xlab=paste(index[i]," [",data_units$value,"]",sep=""),ylab="Density",
                        cex.lab=1.6,cex.axis=1.6,main=paste(index[i]," ",region[reg], sep=""),cex.main=1.6)
                         lines(density(data_seas_mask_ctl[,,,2],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=5)
                        lines(density(data_seas_mask_ctl[,,,3],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=4)
                        lines(density(data_seas_mask_ctl[,,,4],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=3)
                        lines(density(data_seas_mask_ctl[,,,5],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=2)
                        lines(density(data_seas_mask_ctl[,,,6],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1)

                       	lines(density(data_seas_mask_a[,,,1],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=6)
                        lines(density(data_seas_mask_a[,,,2],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=5)
                        lines(density(data_seas_mask_a[,,,3],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=4)
                        lines(density(data_seas_mask_a[,,,4],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=3)
                        lines(density(data_seas_mask_a[,,,5],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=2)
                        lines(density(data_seas_mask_a[,,,6],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)

                       	lines(density(data_seas_mask_b[,,,1],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=6)
                        lines(density(data_seas_mask_b[,,,2],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=5)
                        lines(density(data_seas_mask_b[,,,3],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=4)
                        lines(density(data_seas_mask_b[,,,4],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=3)
                        lines(density(data_seas_mask_b[,,,5],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=2)
                        lines(density(data_seas_mask_b[,,,6],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)

			# plot full ensemble in thick
                        lines(density(data_seas_mask_ctl[,,,],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 7,lty=1)
                        lines(density(data_seas_mask_a[,,,],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 7,lty=1)
                        lines(density(data_seas_mask_b[,,,],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 7,lty=1)

                legend("topright",c("CTL","SMclim","SMtrend"),cex=1.6,bty="n",
                                col = c("grey40","red3","royalblue4"),lwd=c(5,5,5),lty = c(1,1,1))

        text(xaxis.max[[i]],yaxis.max[[i]]/2,bquote("A-CTL p="~.(signif(Atest$p.value,digits = 3))),cex=1.6,adj = c(1,0))
        text(xaxis.max[[i]],(yaxis.max[[i]]/2-yaxis.max[[i]]/10),bquote("B-CTL p="~.(signif(Btest$p.value,digits = 3))),cex=1.6,adj = c(1,0))
        text(xaxis.max[[i]],(yaxis.max[[i]]/2-2*yaxis.max[[i]]/10),bquote("A-B p="~.(signif(ABtest$p.value,digits=3))),cex=1.6,adj = c(1,0))
				
     dev.off()

     } #end reg

} #end i
