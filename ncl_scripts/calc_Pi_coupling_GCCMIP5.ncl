;-----------------------------------------------------------------------------;
; File Name : calc_Pi_coupling_GCCMIP5.ncl
; Creation Date : 07-01-2014
; Last Modified : Tue 07 Jan 2014 12:14:08 EST
; Created By : Ruth Lorenz
; Purpose : calculate Pi couplong metrics after Miralles et al. 2012 GRL

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME
Model = systemfunc("echo $MODEL") ; model

user = systemfunc("echo $USER")

ctlDir = systemfunc("echo $CTLDIR")
expDir = systemfunc("echo $EXPDIR")
outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

if (Runname1.eq."GC1A85") then
   Ctlname = "ExpA"
else if (Runname1.eq."CTL") then
   Ctlname = "CTL"
else
	print("Wrong experiment name")
end if
end if

if (Runname2.eq."GC1A85") then
   Expname = "ExpA"
else if (Runname2.eq."GC1B85") then
   Expname = "ExpB"
else
	print("Wrong experiment name")
end if
end if

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")
;temperature
file1 = addfile(ctlDir + "tas_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file2 = addfile(expDir + "tas_monthly_" +Model+ "_" +Runname2+ "_1_" \
   	 + R2Start + "-" + R2End + ".nc","r")
;latent heat flux
file3= addfile(ctlDir + "hfls_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file4 = addfile(expDir + "hfls_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")
;long-wave down
file5= addfile(ctlDir + "rlds_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file6 = addfile(expDir + "rlds_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")
;long-wave up
file7= addfile(ctlDir + "rlus_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file8 = addfile(expDir + "rlus_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")
;short-wave down
file9= addfile(ctlDir + "rsds_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file10 = addfile(expDir + "rsds_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")
;short-wave up
file11= addfile(ctlDir + "rsus_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file12 = addfile(expDir + "rsus_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")

;sea level pressure
file13= addfile(ctlDir + "psl_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")
file14 = addfile(expDir + "psl_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")
;sensible heat flux
file15= addfile(ctlDir + "hfss_monthly_" +Model+ "_" +Runname1+ "_1_" \
       + R1Start + "-" + R1End + ".nc","r")

file16 = addfile(expDir + "hfss_monthly_" +Model+ "_" +Runname2+ "_1_" \
       	+ R2Start + "-" + R2End + ".nc","r")

;-----------------------------------------------------------------------------;
; read temperature data
;-----------------------------------------------------------------------------;
;derive time axis
time_ctl = file1->time
time_ctl@calendar = "gregorian"
ctl_date = ut_calendar(time_ctl,0)

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)

temp_ctl = file1->tas(ind_yrs_ctl,:,:)        ;temperature at 1.5m

temp_ctl@long_name     = "Temperature at 1.5m"
temp_ctl@units         = "K"

;derive time axis
time_exp = file2->time
time_exp@calendar = "gregorian"
exp_date = ut_calendar(time_exp,0)

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=ctl_date(ind_yrs_exp,:)

temp_exp = file2->tas(ind_yrs_exp,:,:)        ;temperature at 1.5m

temp_exp@long_name     = "Temperature at 1.5m"
temp_exp@units         = "K"

;check if ctl and exp time periods of same length
if ( dimsizes(temp_ctl(:,0,0)) .ne. dimsizes(temp_exp(:,0,0)) .or.\
   dimsizes(temp_ctl(0,:,0)) .ne. dimsizes(temp_exp(0,:,0)) .or. \
   dimsizes(temp_ctl(0,0,:)) .ne. dimsizes(temp_exp(0,0,:))) then
	print("ERROR: CTL and EXP do not have same dimension, exiting")
	exit
end if

hfls_ctl = file3->hfls(ind_yrs_ctl,:,:)        ;latent heat flux

hfls_ctl@long_name     = "Latent heat flux"
hfls_ctl@units         = "Wm~S~-2~N~"

hfls_exp = file4->hfls(ind_yrs_exp,:,:)        ;latent heat flux

hfls_exp@long_name     = "Latent heat flux"
hfls_exp@units         ="Wm~S~-2~N~"

rlds_ctl = file5->rlds(ind_yrs_ctl,:,:)        ;Long-wave down

rlds_ctl@long_name     = "Long-wave down"
rlds_ctl@units         = "Wm~S~-2~N~"

rlds_exp = file6->rlds(ind_yrs_exp,:,:)        ;Long-wave down

rlds_exp@long_name     = "Long-wave down"
rlds_exp@units         ="Wm~S~-2~N~"

rlus_ctl = file7->rlus(ind_yrs_ctl,:,:)        ;Long-wave up

rlus_ctl@long_name     = "Long-wave up"
rlus_ctl@units         = "Wm~S~-2~N~"

rlus_exp = file8->rlus(ind_yrs_exp,:,:)        ;Long-wave up

rlus_exp@long_name     = "Long-wave up"
rlus_exp@units         ="Wm~S~-2~N~"

;short wave
rsds_ctl = file9->rsds(ind_yrs_ctl,:,:)        ;Short-wave down

rsds_ctl@long_name     = "Short-wave down"
rsds_ctl@units         = "Wm~S~-2~N~"

rsds_exp = file10->rsds(ind_yrs_exp,:,:)        ;Short-wave down

rsds_exp@long_name     = "Short-wave down"
rsds_exp@units         ="Wm~S~-2~N~"

rsus_ctl = file11->rsus(ind_yrs_ctl,:,:)        ;Short-wave up

rsus_ctl@long_name     = "Short-wave up"
rsus_ctl@units         = "Wm~S~-2~N~"

rsus_exp = file12->rsus(ind_yrs_exp,:,:)        ;Short-wave up

rsus_exp@long_name     = "Short-wave up"
rsus_exp@units         ="Wm~S~-2~N~"

;net radiation
rnet_ctl=(rsds_ctl-rsus_ctl)+(rlds_ctl-rlus_ctl)
copy_VarMeta(rsds_ctl,rnet_ctl)
rnet_ctl@long_name     = "Net radiation"
rnet_ctl@units         = "Wm~S~-2~N~"

rnet_exp=(rsds_exp-rsus_exp)+(rlds_exp-rlus_exp)
copy_VarMeta(rsds_exp,rnet_exp)
rnet_exp@long_name     = "Net radiation"
rnet_exp@units         = "Wm~S~-2~N~"

;pressure
psl_ctl = file13->psl(ind_yrs_ctl,:,:)        ;Mean sea level pressure

psl_ctl@long_name     = "Mean sea level pressure"
psl_ctl@units         = "Pa"

psl_exp = file14->psl(ind_yrs_exp,:,:)        ;Mean sea level pressure

psl_exp@long_name     = "Mean sea level pressure"
psl_exp@units         ="Pa"

;sensible heat flux
hfss_ctl = file15->hfss(ind_yrs_ctl,:,:)        ;sensible heat flux

hfss_ctl@long_name     = "Sensible heat flux"
hfss_ctl@units         = "Wm~S~-2~N~"

hfss_exp = file16->hfss(ind_yrs_exp,:,:)        ;sensible heat flux

hfss_exp@long_name     = "Sensible heat flux"
hfss_exp@units         ="Wm~S~-2~N~"

;-----------------------------------------------------------------------------;
H_ctl=rnet_ctl-hfls_ctl
copy_VarMeta(rnet_ctl,H_ctl)
H_ctl@long_name     = ""
H_ctl@units         = "Wm~S~-2~N~"

H_exp=rnet_exp-hfls_exp
copy_VarMeta(rnet_exp,H_exp)
H_exp@long_name     = ""
H_exp@units         = "Wm~S~-2~N~"

;-----------------------------------------------------------------------------;
; calculate Hp using Priestly-Taylor for Ep
;-----------------------------------------------------------------------------;
gamma_ctl=0.665*psl_ctl
gamma_exp=0.665*psl_exp

s_ctl=(4098*(0.6108*exp((17.27*(temp_ctl-273.15))/((temp_ctl-273.15)+237.3))))/((temp_ctl-273.15)+237.3)^2
s_exp=(4098*(0.6108*exp((17.27*(temp_exp-273.15))/((temp_exp-273.15)+237.3))))/((temp_exp-273.15)+237.3)^2

G_ctl=rnet_ctl-hfss_ctl-hfls_ctl
G_exp=rnet_exp-hfss_exp-hfls_exp

Ep_ctl=1.26*(s_ctl/(s_ctl+gamma_ctl)*(rnet_ctl-G_ctl))
Ep_exp=1.26*(s_exp/(s_exp+gamma_exp)*(rnet_exp-G_exp))

Hp_ctl=rnet_ctl-Ep_ctl
Hp_exp=rnet_exp-Ep_exp
;clean up
delete([/gamma_ctl,gamma_exp,s_ctl,s_exp,G_ctl,G_exp,rnet_ctl,rnet_exp,hfss_ctl,hfss_exp,hfls_ctl,hfls_exp/])

        Hp_exp!0 = "time"
        Hp_exp!1 = "lat"
        Hp_exp&lat = temp_exp&lat
        Hp_exp!2 = "lon"
        Hp_exp&lon = temp_exp&lon
        Hp_exp@_FillValue=temp_exp@_FillValue

        Hp_ctl!0 = "time"
        Hp_ctl!1 = "lat"
        Hp_ctl&lat = temp_ctl&lat
        Hp_ctl!2 = "lon"
        Hp_ctl&lon = temp_ctl&lon
        Hp_ctl@_FillValue=temp_ctl@_FillValue

;-----------------------------------------------------------------------------;
; detrend timeseries?
;-----------------------------------------------------------------------------;
temp_ctl_dtr=dtrend_leftdim(temp_ctl,False)
temp_exp_dtr=dtrend_leftdim(temp_exp,False)

H_ctl_dtr=dtrend_leftdim(H_ctl,False)
H_exp_dtr=dtrend_leftdim(H_exp,False)

Hp_ctl_dtr=dtrend_leftdim(Hp_ctl,False)
Hp_exp_dtr=dtrend_leftdim(Hp_exp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
do j = 0,3
;season
	if (j .eq. 0) then
		seas="DJF"

	else if (j .eq. 1) then
		seas="MAM"

	else if (j .eq. 2) then
		seas="JJA"

	else if (j .eq. 3) then
		seas="SON"

	end if
	end if
	end if
	end if

temp_ctl_seas=month_to_season(temp_ctl_dtr,seas)
temp_exp_seas=month_to_season(temp_exp_dtr,seas)
H_ctl_seas=month_to_season(H_ctl_dtr,seas)
H_exp_seas=month_to_season(H_exp_dtr,seas)
Hp_ctl_seas=month_to_season(Hp_ctl_dtr,seas)
Hp_exp_seas=month_to_season(Hp_exp_dtr,seas)

        temp_ctl_seas!0 = "time"
        temp_ctl_seas!1 = "lat"
        temp_ctl_seas&lat = temp_ctl&lat
        temp_ctl_seas!2 = "lon"
        temp_ctl_seas&lon = temp_ctl&lon
        temp_ctl_seas@_FillValue=temp_ctl@_FillValue

        temp_exp_seas!0 = "time"
        temp_exp_seas!1 = "lat"
        temp_exp_seas&lat = temp_exp&lat
        temp_exp_seas!2 = "lon"
        temp_exp_seas&lon = temp_exp&lon
        temp_exp_seas@_FillValue=temp_exp@_FillValue

        H_ctl_seas!0 = "time"
        H_ctl_seas!1 = "lat"
        H_ctl_seas&lat = temp_ctl&lat
        H_ctl_seas!2 = "lon"
        H_ctl_seas&lon = temp_ctl&lon
        H_ctl_seas@_FillValue=temp_ctl@_FillValue

        H_exp_seas!0 = "time"
        H_exp_seas!1 = "lat"
        H_exp_seas&lat = temp_exp&lat
        H_exp_seas!2 = "lon"
        H_exp_seas&lon = temp_exp&lon
        H_exp_seas@_FillValue=temp_exp@_FillValue

        Hp_ctl_seas!0 = "time"
        Hp_ctl_seas!1 = "lat"
        Hp_ctl_seas&lat = temp_ctl&lat
        Hp_ctl_seas!2 = "lon"
        Hp_ctl_seas&lon = temp_ctl&lon
        Hp_ctl_seas@_FillValue=temp_ctl@_FillValue

        Hp_exp_seas!0 = "time"
        Hp_exp_seas!1 = "lat"
        Hp_exp_seas&lat = temp_exp&lat
        Hp_exp_seas!2 = "lon"
        Hp_exp_seas&lon = temp_exp&lon
        Hp_exp_seas@_FillValue=temp_exp@_FillValue

;-----------------------------------------------------------------------------;
; calculate correlation between temp and H, Hp
;-----------------------------------------------------------------------------;

        if (j .eq. 0) then
                ccr_ctl_temp_H=new((/4,dimsizes(temp_ctl_seas(1,:,1)),dimsizes(temp_ctl_seas(1,1,:))/),float)
                ccr_exp_temp_H=new((/4,dimsizes(temp_exp_seas(1,:,1)),dimsizes(temp_exp_seas(1,1,:))/),float)
                ccr_ctl_temp_Hp=new((/4,dimsizes(temp_ctl_seas(1,:,1)),dimsizes(temp_ctl_seas(1,1,:))/),float)
                ccr_exp_temp_Hp=new((/4,dimsizes(temp_exp_seas(1,:,1)),dimsizes(temp_exp_seas(1,1,:))/),float)
		Pi_ctl=new((/4,dimsizes(temp_ctl_seas(1,:,1)),dimsizes(temp_ctl_seas(1,1,:))/),float)
		Pi_exp=new((/4,dimsizes(temp_exp_seas(1,:,1)),dimsizes(temp_exp_seas(1,1,:))/),float)
		diff_Pi=new((/4,dimsizes(temp_exp_seas(1,:,1)),dimsizes(temp_exp_seas(1,1,:))/),float)
        end if
        ccr_ctl_temp_H(j,:,:)=escorc(H_ctl_seas(lat|:,lon|:,time|:),temp_ctl_seas(lat|:,lon|:,time|:))
        ccr_exp_temp_H(j,:,:)=escorc(H_exp_seas(lat|:,lon|:,time|:),temp_exp_seas(lat|:,lon|:,time|:))

        ccr_ctl_temp_Hp(j,:,:)=escorc(Hp_ctl_seas(lat|:,lon|:,time|:),temp_ctl_seas(lat|:,lon|:,time|:))
        ccr_exp_temp_Hp(j,:,:)=escorc(Hp_exp_seas(lat|:,lon|:,time|:),temp_exp_seas(lat|:,lon|:,time|:))

	Pi_ctl(j,:,:)=ccr_ctl_temp_H(j,:,:)-ccr_ctl_temp_Hp(j,:,:)
	Pi_exp(j,:,:)=ccr_exp_temp_H(j,:,:)-ccr_exp_temp_Hp(j,:,:)
	diff_Pi(j,:,:)=Pi_exp(j,:,:)-Pi_ctl(j,:,:)
end do ;season


Pi_ctl@long_name = "~F33~P~F21~ (" +Ctlname+ ") " +AStart+"-"+AEnd
Pi_ctl@units = "-"
Pi_ctl!0="time"
Pi_ctl!1="lat"
Pi_ctl!2="lon"
Pi_ctl&lon = temp_ctl&lon
Pi_ctl&lat = temp_ctl&lat

Pi_exp@long_name = "~F33~P~F21~ (" +Expname+ ") " +AStart+"-"+AEnd
Pi_exp@units = "-"
Pi_exp!0="time"
Pi_exp!1="lat"
Pi_exp!2="lon"
Pi_exp&lon = temp_exp&lon
Pi_exp&lat = temp_exp&lat

diff_Pi@long_name = "~F33~P~F21~ " +Ctlname+ "-" +Expname+ " " +AStart+"-"+AEnd
diff_Pi@units = "-"
diff_Pi!0="time"
diff_Pi!1="lat"
diff_Pi!2="lon"
diff_Pi&lon = temp_exp&lon
diff_Pi&lat = temp_exp&lat

	if (Model .eq. "IPSL" ) then
	   Pi_ctl=lonFlip(Pi_ctl)
	   Pi_exp=lonFlip(Pi_exp)
	   diff_Pi=lonFlip(diff_Pi)
	end if

;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;

;Levels for contours
lev=(/0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9/)
lev_diff=(/-0.9,-0.7,-0.5,-0.3,-0.1,0.1,0.3,0.5,0.7,0.9/)
colorbar = "omega_green_red"
colorbar_diff = "blue_to_red_loud"

	Panel_Plot(outDir,"Pi_ctl",AStart,AEnd,plttype,colorbar,Pi_ctl,False,True,lev,True)
	Panel_Plot(outDir,"aust_Pi_ctl",AStart,AEnd,plttype,colorbar,Pi_ctl,True,True,lev,True)

	Panel_Plot(outDir,"Pi_exp",AStart,AEnd,plttype,colorbar,Pi_exp,False,True,lev,True)
	Panel_Plot(outDir,"aust_Pi_exp",AStart,AEnd,plttype,colorbar,Pi_exp,True,True,lev,True)

	Panel_Plot(outDir,"diff_Pi",AStart,AEnd,plttype,colorbar_diff,diff_Pi,False,True,lev_diff,True)
	Panel_Plot(outDir,"aust_diff_Pi",AStart,AEnd,plttype,colorbar_diff,diff_Pi,True,True,lev_diff,True)

;-----------------------------------------------------------------------------;
; save data to netcdf for further use
;-----------------------------------------------------------------------------;
outfile = outDir + "GCCMIP5_Pi_"+Runname2+ "_" +Runname1+ \
        "_" +AStart+ "-" +AEnd+ ".nc"
system("rm "+outfile)
ncdf = addfile(outfile, "c")
ncdf->Pi_ctl=Pi_ctl
ncdf->Pi_exp=Pi_exp

end
