#!/bin/bash
# File Name : access_plot.sh
# Creation Date : April 2013
# Last Modified : Mon 13 Oct 2014 12:36:18 EST
# Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
# Purpose : main run and set up script to plot ACCESS output

#PBS -N accessplot
#PBS -P dt6
#PBS -j oe
#PBS -l ncpus=1
#PBS -l walltime=00:05:00
#PBS -l vmem=2600mb
#PBS -wd

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID1=leuning
export RUNID2=medlyn

export Y_START=1960
export Y_before=1959
export Y_STOP=2011

export RUNNAME1=leuning
export RUNNAME2=medlyn

export pltType=pdf
# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    #export DATAPATH=/srv/ccrc/data23/$USER
    export DATAPATH=/srv/ccrc/data32/$USER
    export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output
else
    echo "Wrong USER"
    exit
fi

#export CTLDIR=$PATHCTL/$RUNID1/timeseries/
export CTLDIR=/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn/LEUNING/ENS_MEAN/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_plot without CTL input data"
    exit 1
fi

if [[ "$RUNID2" != '' ]]; then
    echo "(0) Run comparing to experiment"
    #export EXPDIR=$PATHCTL/$RUNID2/timeseries/
    export EXPDIR=/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn/MEDLYN/ENS_MEAN/
    export EXP=yes
	if [[ ! -d $EXPDIR ]]; then
	    echo "(1) $EXPDIR does not exist"
	    echo "(2) cannot run access_plot without EXP input data"
	    exit 1
	fi
fi

if [[ "$RUNID2" == '' ]]; then
    echo "(0)Run comparing to observations"
    export OBSDIR=$DATAPATH/DATASETS/ERAint_Data
    export EXP=no
    if [[ ! -d $OBSDIR ]]; then
	echo "(1) $OBSDIR does not exist"
	echo "(2) cannot run access_plot without obs input data"
	exit 1
    fi
fi

if [[ "$EXP" == "no" ]]; then
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID1/
else
#export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID2/
export OUTDIR=/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn/plots/seasonal_means/
fi

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export RES=n96
export NTILES=17
export NSOIL=6

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
if  [[ $RUNID2 != '' ]]; then
    echo "Results are compared to Jobid: " $RUNID2 " with files in Directory " $EXPDIR
else
    echo "Results are compared to Observations with files in Directory " $OBSDIR
fi

    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "Start Time:" $date
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo

# -----------------------------------------------------------------------
# get data from archive and unzip
# -----------------------------------------------------------------------
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.seasmean_tmax_tmin.${Y_START}_${Y_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasmean_tmax_tmin.${Y_START}_${Y_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasmean_tmax_tmin.${Y_START}_${Y_STOP}.nc.gz
fi

if  [[ $RUNID2 != '' ]] && [[ ! -f "$WORKDIR/$RUNID2.seasonal_means.${Y_START}_${Y_STOP}.nc" ]]; then
    cp $EXPDIR/$RUNID2.seasonal_means.${Y_START}_${Y_STOP}.nc.gz $WORKDIR
    gunzip $WORKDIR/$RUNID2.seasonal_means.${Y_START}_${Y_STOP}.nc.gz
    cp $EXPDIR/$RUNID2.seasmean_tmax_tmin.${Y_START}_${Y_STOP}.nc.gz $WORKDIR
    gunzip $WORKDIR/$RUNID2.seasmean_tmax_tmin.${Y_START}_${Y_STOP}.nc.gz
elif [[ $RUNID2 == '' ]] && [[ ! -f "$WORKDIR/ERAint_T2m_seas_mean_${Y_START}-${Y_STOP}_1deg.nc" ]]; then
    echo "Calculate seasonal means of Observations"
    cdo yseasmean $OBSDIR/ERAint_T2m_monthly_dec${Y_before}-${Y_STOP}-dec_1deg.nc $WORKDIR/ERAint_T2m_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
    cdo yseasmean $OBSDIR/ERAint_prec_monthly_dec${Y_before}-${Y_STOP}-dec_00_12UTC_step12_1deg.nc $WORKDIR/ERAint_prec_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
    cdo yseasmean $OBSDIR/ERAint_netsw_netlw_monthly_dec${Y_before}-${Y_STOP}-dec_00_12UTC_step12_div12_1deg.nc $WORKDIR/ERAint_netsw_netlw_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
    cdo yseasmean $OBSDIR/ERAint_mslp_alb_cld_monthly_dec${Y_before}-${Y_STOP}-dec_1deg.nc $WORKDIR/ERAint_mslp_alb_cld_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
    cdo yseasmean $OBSDIR/ERAint_et_sh_lh_monthly_dec${Y_before}-${Y_STOP}-dec_00_12UTC_step12_div12_1deg.nc $WORKDIR/ERAint_et_sh_lh_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
    cdo yseasmean $OBSDIR/ERAint_tmax_monthly_dec${Y_before}-${Y_STOP}-dec_1deg.nc $WORKDIR/ERAint_Tmax_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
    cdo yseasmean $OBSDIR/ERAint_tmin_monthly_dec${Y_before}-${Y_STOP}-dec_1deg.nc $WORKDIR/ERAint_Tmin_seas_mean_${Y_START}-${Y_STOP}_1deg.nc
elif [[ $RUNID2 == '' ]] && [[ ! -f "$WORKDIR/ERAint_T2m_seas_mean_${Y_START}-${Y_STOP}_regrid.nc" ]]; then
    echo "Regrid observations to model grid"
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_T2m_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_T2m_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_prec_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_prec_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_netsw_netlw_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_netsw_netlw_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_mslp_alb_cld_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_mslp_alb_cld_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_et_sh_lh_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_et_sh_lh_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_Tmax_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_Tmax_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
    cdo remapbil,$WORKDIR/$RUNID1.seasonal_means.${Y_START}_${Y_STOP}.nc $WORKDIR/ERAint_Tmin_seas_mean_${Y_START}-${Y_STOP}_1deg.nc $WORKDIR/ERAint_Tmin_seas_mean_${Y_START}-${Y_STOP}_regrid.nc
fi

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call run.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/run.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0

# -----------------------------------------------------------------------
# 
# -----------------------------------------------------------------------
