;-----------------------------------------------------------------------------;
; File Name : plot_vert_prof_diffs_ANN.ncl
; Creation Date : 24-08-2015
; Last Modified : 
; Created By : Ruth Lorenz
; Purpose : plot vertical profiles of changes in LCC regions for annual means 

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_single_overlay_significance_LUC.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
runnames = str_split(systemfunc("echo $EXPERIMENTS")," ")
print(runnames)
region = systemfunc("echo $REGION")

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")
outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

varname = (/"hur","hus","ta","ua","va","zg"/)
longname = (/"HUR","HUS","T","U-Wind","V-Wind","GPH"/)
units = (/"%"," ","K","ms~S~-1~N~","ms~S~-1~N~","m"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;start loop variables
do v = 0,5
   do run = 1,7

      file1 = addfile(workDir +runnames(0)+ ".yearly_means." \
                       + R1Start + "_" + R1End + "_ensmean.nc","r")
      file2 = addfile(workDir +runnames(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + "_ensmean.nc","r")

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;
	if ( run .eq. 1 ) then
	    nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

	    ;derive time axis
	    time_ctl = file1->time
	    time_ctl@calendar = "gregorian"
	    ctl_date = ut_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
	    lat = file1->lat
	    lon = file1->lon

	    time_exp = file2->time
	    time_exp@calendar = "gregorian"
	    exp_date = ut_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

	    ;cut into analysis period
	    ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
	    new_date_ctl=ctl_date(ind_yrs_ctl,:)

	    ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
	    new_date_exp=exp_date(ind_yrs_exp,:)
	end if

	var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:,:)
    	var_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:,:)

      	var_ctl@long_name     = longname(v)
      	var_ctl@units         = units(v)

      	var_exp@long_name     = longname(v)
      	var_exp@units         = units(v)

	;check if ctl and exp time periods of same length
	dsize_ctl=dimsizes(var_ctl)
	dsize_exp=dimsizes(var_exp)
	ntim_ctl=dsize_ctl(0)
	nlev_ctl=dsize_ctl(1)
	nlat_ctl=dsize_ctl(2)
	nlon_ctl=dsize_ctl(3)
	ntim_exp=dsize_exp(0)
	nlev_exp=dsize_exp(1)
	nlat_exp=dsize_exp(2)
	nlon_exp=dsize_exp(3)

	if ( ntim_ctl.ne.ntim_exp .or. nlev_ctl.ne.nlev_exp .or. \
	    nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
	        print("ERROR: CTL and EXP do not have same dimension, exiting")
		        exit
	else
		ntim=ntim_ctl
		nlev=nlev_ctl
        	nlat=nlat_ctl
        	nlon=nlon_ctl

        	delete(ntim_ctl)
		delete(nlev_ctl)
        	delete(nlat_ctl)
        	delete(nlon_ctl)
        	delete(ntim_exp)
		delete(nlev_exp)
        	delete(nlat_exp)
        	delete(nlon_exp)
	end if

;-----------------------------------------------------------------------------;
; Calculating differences
;-----------------------------------------------------------------------------;
	var_exp_avg=dim_avg_n(var_exp,0)
	var_ctl_avg=dim_avg_n(var_ctl,0)

	var_diff= var_exp_avg-var_ctl_avg

	var_diff@long_name= longname(v)+" "+runnames(run)+"-"+Runname1
	var_diff@units    = units(v)
	var_diff!0="lev"
	var_diff!1="lat"
	var_diff!2="lon"
	var_diff&lev = var_ctl&z12_p_level
	var_diff&lon = var_ctl&lon_1
	var_diff&lat = var_ctl&lat_1

;-----------------------------------------------------------------------------;
; calculate latitudinal average over LCC region
;-----------------------------------------------------------------------------;
	;cut data into LCC region
	latS = -13.75
   	latN = 3.75
   	lonL = 281.25
   	lonR = 320.625

	AMZ_lon=fspan(lonL,lonR,22)

	var_diff(:,{:latS-1.25},:)=var_diff@_FillValue
	var_diff(:,{latN+1.25:},:)=var_diff@_FillValue
	var_diff(:,:,{:lonL-1.875})=var_diff@_FillValue
	var_diff(:,:,{lonR+1.875:})=var_diff@_FillValue

;average across latitude
	var_diff_avg=dim_avg_n_Wrap(var_diff,1)
	;printVarSummary(var_diff_avg)

	dlev=(/1,0.00015,0.1,0.2,0.1,1/)
	minLev = (/-4.5,-0.0005,-0.65,-1.3,-0.65,-4.5/)
	maxLev = (/4.5,0.0005,0.65,1.3,0.65,4.5/)
	colors = (/"dared_to_blue_18lev","dared_to_blue_18lev","BlWhRe","BlWhRe","BlWhRe","BlWhRe"/)

;plot map to check masking
lev=(/-1.8,-1.4,-1,-0.6,-0.2,0.2,0.6,1,1.4,1.8/)
   zeros=new((/nlat,nlon/),integer)
   printVarSummary(zeros)
   printVarSummary(var_ctl)
   zeros=0
   zeros!0="lat"
   zeros!1="lon"
   zeros&lon = var_ctl&lon_1
   zeros&lat = var_ctl&lat_1

signif_single_Plot_LUC(outDir+runnames(run)+"/","diff_mask_for_vert_latAvg_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,"blue_to_red_18lev",var_diff(16,:,:),zeros,True,region,True,lev,False)

;-----------------------------------------------------------------------------;
; plot data
;-----------------------------------------------------------------------------;
	pltName = outDir +runnames(run)+ "/vert_prof_lowlev_latAvg_"+runnames(run)+"-CTL_ANN_" +varname(v)
	wks = gsn_open_wks(plttype,pltName)
	gsn_define_colormap(wks,colors(v))                ; choose colormap

  	res          = True                   ; plot mods desired

  	;res@tiMainString         =  var_diff_avg@long_name       ; title
	res@gsnMajorLonSpacing = 10
  	res@cnLevelSelectionMode = "ManualLevels"        ; manual contour levels
  	res@cnLevelSpacingF      = dlev(v)                   ; contour interval
  	res@cnMinLevelValF       = minLev(v)                  ; min level
  	res@cnMaxLevelValF       = maxLev(v)                 ; max level
  	res@cnLineLabelsOn       = True                  ; turn on line labels
  	res@cnFillOn             = True                  ; turn on color fill

;---This resource not needed in V6.1.0
  	 res@gsnSpreadColors      = True                  ; use full range of colors

;---This resource defaults to True in NCL V6.1.0
  	 res@lbLabelAutoStride    = True                  ; optimal labels
  
	 plot = gsn_csm_pres_hgt(wks,var_diff_avg(10:16,{lonL:lonR}),res)

	pltName = outDir +runnames(run)+ "/vert_prof_latAvg_"+runnames(run)+"-CTL_ANN_" +varname(v)
	wks = gsn_open_wks(plttype,pltName)
	gsn_define_colormap(wks,colors(v))                ; choose colormap
	plot = gsn_csm_pres_hgt(wks,var_diff_avg(:,{lonL:lonR}),res)

;-----------------------------------------------------------------------------;

    end do ;runs

end do ;variable
;-----------------------------------------------------------------------------;
end
