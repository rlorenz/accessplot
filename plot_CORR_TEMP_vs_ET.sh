#!/bin/bash
# File Name : plot_CORR_Temp_vs_ET.sh
# Creation Date : 04/09/2013
# Last Modified : Wed 21 May 2014 15:56:18 EST
# Created By : Ruth Lorenz
# Purpose : plot correlation of Tmax versus ET

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=uakpe
export RUNNAME=ACCESS
export DATAT=HadGHCND
export DATAR=GLEAM
export DATAE=LandFluxEVAL

export Y_START=1989
export Y_STOP=2005

export NLAT=192
export NLON=145

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/ACCESS_output/${RUNID}/timeseries/
export OBSDIR=$DATAPATH/DATASETS/
export WORKDIR=$DATAPATH/ACCESS_plots_workdir/
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID/scatter_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load nco
module load cdo
#module load R/2.15.3

# -----------------------------------------------------------------------
# copy data to workdir
# -----------------------------------------------------------------------
#cp $MODDIR/${RUNID}.monmean_tmax_tmin_hfls.${Y_START}-${Y_STOP}.nc $WORKDIR

#cp $OBSDIR/GLEAM_ETdata/GLEAM.New_n96.monmean_global_EVAP_${Y_START}-${Y_STOP}.nc $WORKDIR

#cp $OBSDIR/${DATAT}_Data/HadGHCND_TX_monmean_${Y_START}-${Y_STOP}.nc $WORKDIR

#cp $OBSDIR/${DATAE}/LandFluxEVAL.merged.89-05.monthly.all.nc $WORKDIR/LandFluxEVAL.merged.monthly.all_${Y_START}-${Y_STOP}.nc
# -----------------------------------------------------------------------
# regrid all data to coarsest resolution (coming from HadGHCND)
# -----------------------------------------------------------------------
#cdo remapbil,$WORKDIR/HadGHCND_TX_monmean_${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}.monmean_tmax_tmin_hfls.${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}.monmean_tmax_tmin_hfls.${Y_START}-${Y_STOP}_rgd.nc
#cdo remapbil,$WORKDIR/HadGHCND_TX_monmean_${Y_START}-${Y_STOP}.nc $WORKDIR/GLEAM.New_n96.monmean_global_EVAP_1984-2007.nc $WORKDIR/GLEAM.New.monmean_global_EVAP_${Y_START}-${Y_STOP}_rgd.nc
#cdo remapbil,$WORKDIR/HadGHCND_TX_monmean_${Y_START}-${Y_STOP}.nc $WORKDIR/LandFluxEVAL.merged.monthly.all_${Y_START}-${Y_STOP}.nc $WORKDIR/LandFluxEVAL.merged.monthly.all_${Y_START}-${Y_STOP}_rgd.nc
# -----------------------------------------------------------------------
# call ncl script
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_corr_tmax_vs_etGLEAM.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_corr_tmax_vs_etLandFlux.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_svdcov_tmax_vs_et.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
