#!/bin/bash
### File Name : plot_climdex_seas_GCCMIP5.sh
### Creation Date : October 2014
### Last Modified : Wed 09 Jul 2014 19:26:57 EST
### Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
### Purpose : script plotting climdex indices maps, trends, and timeseries
### for GLACE_CMIP5 multi-model ensemble
### climdex indices need to be calculated first using fortran code
### in /home/561/rzl561/scripts/fclimdex/fclimdex_readperc/

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export MODEL=ENS_MEAN

export R1_START=1954 #ctl run start
export R1_STOP=2100  #ctl run stop
export R2_START=1950 #exp run start
export R2_STOP=2100  #exp run stop
export A1_START=2021 #analysis start CTL
export A1_STOP=2040  #analysis stop CTL
export A2_START=2021 #analysis start EXP/SCEN
export A2_STOP=2040  #analysis stop EXP/SCEN

export RUNNAME1=CTL
export RUNNAME2=GC1A85

export NLON=96
export NLAT=96
# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data32/$USER/GLACE-CMIP5/
fi

export INDIR=$DATAPATH

export OUTDIR=/srv/ccrc/data32/$USER/GLACE-CMIP5_plots/$MODEL/$RUNNAME2/climdex_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------
module load gcc/4.7.2
module load ncl/6.2.0

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call climdex.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_climdex_ANN_GCCMIP5_ENSEMBLE.ncl
# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting climdex indices"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
