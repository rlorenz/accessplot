# plot scatter plots for GLACE_CMIP5 experiments
# evaporative fraction versus soil moisture in the difference models and experiments
#install.packages("hexbin")
library(ncdf)
library(hexbin)
library(lattice)
in_dir <- "/srv/ccrc/data32/z3441306/GLACE-CMIP5/"

out_dir <- "/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/"

R.Start=list("195001","195001","195001") #for CTL
R.End="210012"

A.Start="2056"
A.End="2085"

expname<-list("CTL","GC1A85","GC1B85")
#model<-list("ACCESS","CESM","ECHAM6","GFDL","IPSL")
model<-list("ACCESS","CESM","ECHAM6","GFDL","IPSL","EC-EARTH")
seas<-list("DJF","MAM","JJA","SON","ANN")
region<-list("CNA","EAS","MED","SAS","NAU","EAF","SAF","NEB")
#region<-list("CNA","MED","NAU","SAF")

xaxis.min<-list(-400,-300,-200,-200,-100,-500)
xaxis.max<-list(400,300,200,200,100,500)
#xaxis.max<-list(0,0,0,0,0,0)
yaxis.min<-list(-1,-1,-0.1,-1,-1,-1)
yaxis.max<-list(1,1,0.1,1,1,1)

nmod<-length(model)
nreg<-length(region)
ntim_A=(as.numeric(A.End)-as.numeric(A.Start)+1)*12

    #loop over all regions
    for (reg in 1:nreg){
    	print(region[reg])

    	#smdata_ctl<-array(NA,dim=c(nlon,nlat,ntim_R1,nmod))
    	#smdata_seas_ctl<-array(NA,dim=c(nlon,nlat,4,nmod))

		#loop over models
    		for (mod in 1:nmod){
		print(model[mod])
		    mask_file=paste("/home/z3441306/scripts/plot_scripts/areas_txt/",region[reg],".txt",sep="")
		    mask=read.table(mask_file)

		    if (region[reg]=="SAS"){
		       maxlon<-mask[1,1]
		       minlon<-mask[5,1]
		       maxlat<-mask[1,2]
		       minlat<-mask[4,2]
		       } else {
			    maxlon<-mask[1,1]
			    minlon<-mask[3,1]
			    maxlat<-mask[1,2]
			    minlat<-mask[2,2]
		    }
	    
		smdata<-list()
	    	ef<-list()
		    #loop over experiments to get all data
		    for (e in 1:3) {
			if (model[mod]=="GFDL"){
			   R.Start[e]<-"195101"
			} else {
			  R.Start[e]<-"195001"
			}

			file=paste(in_dir,model[mod],"/",expname[e],"/mrso_monthly_",model[mod],"_",expname[e],"_1_",R.Start[e],"-",R.End,".nc",sep="")
			print(file)
			smdata.nc = open.ncdf(file)
			#print(smdata.nc)

			time_units<-att.get.ncdf( smdata.nc,"time","units")
			base_date=as.character(lapply(strsplit(as.character(time_units$value),split=" "),"[",3))
			time=get.var.ncdf( smdata.nc,"time")
			if (model[mod]=="ECHAM6") {
			   time.y<-substr(time,1,4)
			} else {
			  time_d <- as.Date(time, format = "%j", origin = as.Date(base_date))
			  time.y<-format(time_d, "%Y")
			}  
			#print(time.y)
    
			t1=min(which(as.numeric(time.y)>=A.Start))
			t2=(as.numeric(A.End)-as.numeric(A.Start)+1)*12
			if (model[mod]=="ECHAM6"){
			   time.m<-substr(time[t1:max(which(as.numeric(time.y)<=A.End))],5,6)
			} else {
			  time.m<-format(time_d[t1:max(which(as.numeric(time.y)<=A.End))],"%m")
			}
			#print(time.m)

			latitude=get.var.ncdf( smdata.nc,"lat")
			longitude=get.var.ncdf( smdata.nc,"lon")
			if (max(longitude)>180 & minlon<0){
			   minlon<-minlon+360
			}
			if (max(longitude)>180 & maxlon<0){ 
			   maxlon<-maxlon+360
			}
			nlat<-length(latitude)
			nlon<-length(longitude)
    
			#smdata<-list(c(nlon,nlat,ntim_A),c(nlon,nlat,ntim_A),c(nlon,nlat,ntim_A))
			if (model[mod]=="EC-EARTH"){
			   smdata[[e]]=get.var.ncdf(smdata.nc,"mrso",c(1,1,1,t1),c(nlon,nlat,1,t2)) #read variable Annual
			} else {
		    	  smdata[[e]]=get.var.ncdf(smdata.nc,"mrso",c(1,1,t1),c(nlon,nlat,t2)) #read variable Annual
			}
			#take care of missing values
			smdata[[e]][which(smdata[[e]]>=1e20)] <- NA
			if ((model[mod]=="ECHAM6") | (model[mod]=="EC-EARTH")){
			   smdata[[e]][which(smdata[[e]]==0)] <- NA
			}
			#print(min(smdata[[e]],na.rm=T))
			#print(max(smdata[[e]],na.rm=T))

			file=paste(in_dir,model[mod],"/",expname[e],"/hfls_monthly_",model[mod],"_",expname[e],"_1_",R.Start[e],"-",R.End,".nc",sep="")
			hflsdata.nc = open.ncdf(file)
			hflsdata=get.var.ncdf(hflsdata.nc,"hfls",c(1,1,t1),c(nlon,nlat,t2)) #read variable Annual
			hflsdata[which(hflsdata>=1e20)] <- NA
			if (model[mod]=="ECHAM6") {
			   hflsdata=-hflsdata
			}
			file=paste(in_dir,model[mod],"/",expname[e],"/rlds_monthly_",model[mod],"_",expname[e],"_1_",R.Start[e],"-",R.End,".nc",sep="")
			rldsdata.nc = open.ncdf(file)
			rldsdata=get.var.ncdf(rldsdata.nc,"rlds",c(1,1,t1),c(nlon,nlat,t2)) #read variable Annual
			rldsdata[which(rldsdata>=1e20)] <- NA

			file=paste(in_dir,model[mod],"/",expname[e],"/rlus_monthly_",model[mod],"_",expname[e],"_1_",R.Start[e],"-",R.End,".nc",sep="")
			rlusdata.nc = open.ncdf(file)
			rlusdata=get.var.ncdf(rlusdata.nc,"rlus",c(1,1,t1),c(nlon,nlat,t2)) #read variable Annual
			rlusdata[which(rlusdata>=1e20)] <- NA
			if (model[mod]=="CESM"){	#rlus data in CESM 1000 orders of magnitude off 
			   rlusdata<-rlusdata/1000
			}

			file=paste(in_dir,model[mod],"/",expname[e],"/rsds_monthly_",model[mod],"_",expname[e],"_1_",R.Start[e],"-",R.End,".nc",sep="")
			rsdsdata.nc = open.ncdf(file)
			rsdsdata=get.var.ncdf(rsdsdata.nc,"rsds",c(1,1,t1),c(nlon,nlat,t2)) #read variable Annual
			rsdsdata[which(rsdsdata>=1e20)] <- NA

			file=paste(in_dir,model[mod],"/",expname[e],"/rsus_monthly_",model[mod],"_",expname[e],"_1_",R.Start[e],"-",R.End,".nc",sep="")
			rsusdata.nc = open.ncdf(file)
			rsusdata=get.var.ncdf(rsusdata.nc,"rsus",c(1,1,t1),c(nlon,nlat,t2)) #read variable Annual
			rsusdata[which(rsusdata>=1e20)] <- NA

			# calculate net radiation
			if (model[mod]=="EC-EARTH"){
			   rnet<-(rldsdata+rlusdata)+(rsdsdata+rsusdata)
			} else {
			  rnet<-(rldsdata-rlusdata)+(rsdsdata-rsusdata)
			}
			#print(dim(rnet))
			#print(min(rnet,na.rm=T))
			#print(max(rnet,na.rm=T))
    
			#calculate evaporative fraction
			ef[[e]] <- hflsdata/rnet

		    }
		    #calculate difference in ef and sm between exp B and A
		    sm_diff<-smdata[[3]]-smdata[[2]]
		    ef_diff<-ef[[3]]-ef[[2]]

			#split into seasons
			for (j in 1:5){ #loop over seasons
			    if (j==1){seas <- "DJF"
				ind.seas <- which((time.m=="12")|(time.m=="01")|(time.m=="02"))
				
			    }
			    if (j==2){seas <- "MAM"
				ind.seas <- which((time.m=="03")|(time.m=="04")|(time.m=="05"))
			    }
			    if (j==3){seas <- "JJA"
				ind.seas <- which((time.m=="06")|(time.m=="07")|(time.m=="08"))
			    }
			    if (j==4){seas <- "SON"
				ind.seas <- which((time.m=="09")|(time.m=="10")|(time.m=="11"))
			    }
			    if (j==5){seas <- "ANN"
			    }
			    print(seas)

			    #print(length(ind.seas))
			    #print(ind.seas)

			    if (j<=4){
			       ef_diff_seas <- ef_diff[,,ind.seas]
			       sm_diff_seas <- sm_diff[,,ind.seas]
			    } else {
			      ef_diff_seas <- ef_diff
			      sm_diff_seas <- sm_diff
			    }

			#mask positive SM changes
			#sm_diff_seas[which(sm_diff_seas>=0)]<-NA
			#remove EF outliers
			ef_diff_seas[which(ef_diff_seas>=100)]<-NA
			ef_diff_seas[which(ef_diff_seas<=-100)]<-NA

			#cut data into region
			ef_diff_seas[,which(latitude<=minlat),] <- NA
			ef_diff_seas[,which(latitude>=maxlat),] <- NA
			sm_diff_seas[,which(latitude<=minlat),] <- NA
			sm_diff_seas[,which(latitude>=maxlat),] <- NA

			if (minlon>maxlon){		       #for Mediterranean area which goes over 0 meridian
			   ef_diff_seas[which((longitude<=minlon)&(longitude>=maxlon)),,] <- NA
			   sm_diff_seas[which((longitude<=minlon)&(longitude>=maxlon)),,] <- NA
			} else {
			  ef_diff_seas[which(longitude<=minlon),,] <- NA
			  ef_diff_seas[which(longitude>=maxlon),,] <- NA
			  sm_diff_seas[which(longitude<=minlon),,] <- NA
			  sm_diff_seas[which(longitude>=maxlon),,] <- NA
			}

			#put data into vector before plotting
			sm_diff_seas_1D<-(c(sm_diff_seas))
			ef_diff_seas_1D<-(c(ef_diff_seas))

			#principal component analysis for total least squares fit
			r <- princomp( ~ sm_diff_seas_1D + ef_diff_seas_1D)
			slope <- r$loadings[2,1] / r$loadings[1,1]
			intercept <- r$center[2] - slope*r$center[1]

			# plot data fore each region, model and season
			outfile=paste(out_dir,model[mod],"/",expname[3],"/scatter_dEF_dSM_",expname[3],"-",expname[2],"_",model[mod],"_",region[reg],"_",seas,"_",A.Start,"-",A.End,".pdf", sep="")
        		pdf(outfile)
			par(mar=c(5,5,4,2) + 0.1) #increase inner margin on left side

			plot(sm_diff_seas_1D,ef_diff_seas_1D,
			xlab=expression(paste(Delta," total soil moisture")),ylab=expression(paste(Delta, " evaporative fraction")),
			#ylim=c(yaxis.min[[mod]],yaxis.max[[mod]]),
                        #xlim=c(xaxis.min[[mod]],xaxis.max[[mod]]),
			cex.lab=1.5,col="darkgray",cex.axis=1.5,
			main =paste(model[mod]," ",region[reg]," ",seas," EF vs SM", sep=""),cex.main=1.5)

			abline(v = 0, col="black",lwd = "1",lty=2)
			abline(h=0, col="black",lwd = "1",lty=2)
			abline(a = intercept,b = slope, col="black",lwd = "2")
			legend("bottomright", bty="n",col="black", lwd="2",legend="Total least squares")
			
        		dev.off()
			 
			outfile=paste(out_dir,model[mod],"/",expname[3],"/hexascatter_dEF_dSM_",expname[3],"-",expname[2],"_",model[mod],"_",region[reg],"_",seas,"_",A.Start,"-",A.End,".pdf", sep="")
        		pdf(outfile)
			par(mar=c(5,6,4,2) + 0.1) #increase inner margin on left side

 			#bin <- hexbin(ef_diff_seas_1D, sm_diff_seas_1D, xbins=200)
			#plot(bin,xlab=expression(paste(Delta," evaporative fraction"))
			#        ,ylab=expression(paste(Delta," total soil moisture")),
			#main=paste(model[mod]," ",region[reg]," ",seas," EF vs SM", sep=""))
			plot(hexbinplot(ef_diff_seas_1D~sm_diff_seas_1D,
				xlab=expression(paste(Delta," total soil moisture")),
				ylab=expression(paste(Delta," evaporative fraction")),
				cex.axis=1.5,cex.lab=1.5,
				ylim=c(yaxis.min[[mod]],yaxis.max[[mod]]),
                        	xlim=c(xaxis.min[[mod]],xaxis.max[[mod]]),
				main=paste(model[mod]," ",region[reg]," ",seas," EF vs SM", sep=""),
				cex.main=1.5,
				aspect=1,
				#legend("bottomright", bty="n",col="black", lwd="2",legend="Total least squares"),
				panel = function(...) {
             			      panel.hexbinplot(...)
             			      panel.abline(v=0, col="black",lwd = "1",lty=2)
             			      panel.abline(h=0, col="black",lwd = "1",lty=2)
				      panel.abline(a = intercept,b = slope, col="black",lwd = "2")
             			      }))
			
	        	dev.off()

			} #end loop seasons
		} #end loop models
	} #end for loop regions
