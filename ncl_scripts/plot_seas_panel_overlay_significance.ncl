;-----------------------------------------------------------------------------;
; File Name : plot_seas_panel.ncl
; Creation Date : April 2013
; Last Modified : Thu 17 Apr 2014 00:10:15 EST
; Created By : Ruth Lorenz, based on Lauren Stevens plot.ncl
; Purpose : Plot seasonal panels with overlayed significance
; procedure code
; see https://trac.nci.org.au/svn/cable/branches/Users/lxs599/umplot/plot.ncl

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

;-----------------------------------------------------------------------------;
undef("Panel_Plot_sig")	;always undef procedures, otherwise problems occur

procedure Panel_Plot_sig(Dir:string,varName:string,Y_Start:string,Y_End:string, \
			pltType:string,pltColor:string,field:numeric,\
			sig:numeric,region:logical, \
			sysFlag:logical,cnLevels:numeric,lmask:logical)

local pltName, wks, colors		;local variables should be local
begin				;alwas begin script with begin

;-----------------------------------------------------------------------------;
; Plot set-up
;-----------------------------------------------------------------------------;

  pltName = Dir + varName + "_sig_" + Y_Start + "-" + Y_End

  wks = gsn_open_wks(pltType,pltName)

printMinMax(field, True)

;----------ColorMap-----------------;

  gsn_define_colormap(wks,pltColor)

;add grey in colormaps for plotting biases and differences
if (pltColor.eq."temp_diff_18lev" .or. pltColor.eq."blue_to_red_18lev") then
	colors = gsn_retrieve_colormap(wks)     ; retrieve color map for editing. dimensioned (20,3)
 	colors(11,:) = (/ .8, .8, .8 /)         ; replace the middle color (white) with a medium gray
  	gsn_define_colormap(wks,colors)         ; redefine colormap to workstation,
					 	; color map now includes a gray
	else if (pltColor.eq."precip_diff_12lev".or.pltColor.eq."sunshine_diff_12lev") then
		colors = gsn_retrieve_colormap(wks)     ; retrieve color map for editing, dimensioned (20,3)
 		colors(8,:) = (/ .8, .8, .8 /)          ; replace the middle color (white) with a medium gray
  		gsn_define_colormap(wks,colors)         ; redefine colormap to workstation,
						  	; color map now includes a gray
	else if (pltColor.eq."blue_to_red_loud") then
		colors = gsn_retrieve_colormap(wks)    ; retrieve color map for editing. dimensioned (20,3)
 		colors(12,:) = (/ .8, .8, .8 /)     ; replace the middle color (white) with a medium gray
  		gsn_define_colormap(wks,colors)        ; redefine colormap to workstation, color map now includes a gray
	else if (pltColor.eq."dared_to_blue_18lev" .or. pltColor.eq."orange_to_blue_18lev") then
		colors = gsn_retrieve_colormap(wks)     ; retrieve color map for editing, dimensioned (15,3)
 		colors(10,:) = (/ .8, .8, .8 /)          ; replace the middle color (white) with a medium gray
  		gsn_define_colormap(wks,colors)         ; redefine colormap to workstation,
		
	end if
	end if
	end if

end if

;reverse colormap for certain variables
	if (varName.eq."diff_evap") then
		gsn_reverse_colormap(wks)
	end if

	if (varName.eq."seas_smcl_level1".or.varName.eq."seas_smcl_level2".or.varName.eq. \
		"seas_smcl_level3".or.varName.eq."seas_smcl_level4".or.varName.eq."seas_smcl_level5" \
		.or.varName.eq."seas_smcl_level6") then
		gsn_reverse_colormap(wks)
	end if

	if (varName.eq."diff_smcl_level1".or.varName.eq."diff_smcl_level2".or.varName.eq. \
		"diff_smcl_level3".or.varName.eq."diff_smcl_level4".or.varName.eq."diff_smcl_level5" \
		.or.varName.eq."diff_smcl_level6") then
		gsn_reverse_colormap(wks)
	end if

	if (varName.eq."austseas_smcl_level1".or.varName.eq."austseas_smcl_level2".or. \
		varName.eq."austseas_smcl_level3".or.varName.eq."austseas_smcl_level4".or. \
		varName.eq."austseas_smcl_level5".or.varName.eq."austseas_smcl_level6") then
		gsn_reverse_colormap(wks)
	end if

	if (varName.eq."austdiff_smcl_level1".or.varName.eq."austdiff_smcl_level2".or. \
		varName.eq."austdiff_smcl_level3".or.varName.eq."austdiff_smcl_level4".or. \
		varName.eq."austdiff_smcl_level5".or.varName.eq."austdiff_smcl_level6") then
		gsn_reverse_colormap(wks)
	end if


	if (varName.eq."seas_lw".or.varName.eq."erai_seas_lw".or.varName.eq."aust_seas_lw" \
		.or.varName.eq."erai_aust_seas_lw" .or.varName.eq."CERES_seas_lw".or. \
		varName.eq."aust_CERES_seas_lw") then
		gsn_reverse_colormap(wks)
	end if
 
;----------GeneralResources---------;

  res = True
  
  res@gsnFrame             = False           ; Don't advance the frame
  res@gsnDraw              = False           ; only for Panel plots
                                             ; switch off for individual plots
  res@gsnStringFontHeightF = 0.03            ; changes font height of all labels simultaneously

  res@cnFillOn             = True            ; Turn on contour fill.
  res@cnFillMode           = "CellFill"    ; Pixels

	if (region) then
  		res@cnRasterSmoothingOn  = True
	end if

  res@cnLineLabelsOn       = False            ; Turn off line labels.
  res@cnInfoLabelOn        = False            ; Turn off info label.
  
	;if(lmask)
  		res@cnFillDrawOrder      = "PreDraw"   ; Predraw cn so mask will appear
						       ; (if land transparent ow land will also be masked)
        ;end if

  res@cnLinesOn            = False             ; no contour lines
   
  res@gsnSpreadColors      = True              ;
  res@lbLabelBarOn         = False             ;
  res@lbLabelAutoStride    = True              ;
  res@lbLabelFontHeightF      = .03              ; increase label bar labels
  
  res@gsnLeftString        = ""                ;
  res@gsnRightString       = ""                ;


  res@cnLevelSelectionMode = "ExplicitLevels"  ; Define your own
  res@cnLevels = cnLevels 

;----------Region ------------------

	if (.not.region) then
		Area = "Global"
		latS = -60
		latN =  80 
		lonL =   0
		lonR = 360
	else
 ;		Area = "Australia"
;		latS = -45
;   		latN = -10 
;   		lonL = 110 
;   		lonR = 160
		Area = "Amazonia"
		latS = -30
   		latN = 10 
   		lonL = 275 
   		lonR = 328
  	end if 

  res@mpMinLatF  = latS
  res@mpMaxLatF  = latN
  res@mpMinLonF  = lonL
  res@mpMaxLonF  = lonR
  
	if(region) then
  		res@gsnAddCyclic = False
	end if
  res@mpCenterLonF         = 0.             ; center on 0 meridian

  if (.not.region .and. res@mpCenterLonF.eq.0.) then
	  res@mpMinLonF  = -180
  	res@mpMaxLonF  = 180
	else
  	res@mpMinLonF  = lonL
  	res@mpMaxLonF  = lonR
  end if

;----------Mask --------------------
res@mpFillOn             = True
res@mpLandFillColor      = "transparent"

;mask ocean
	if(lmask) 
  		res@mpOceanFillColor     = "white"
	end if  

;----------Titles ------------------
  
  res@tiMainOn      = False
  ;res@tiXAxisString = "Longitude"
  ;res@tiYAxisString = "Latitude"

;---------- Plot -------------------
;plot panels
 
  plot = new(4,graphic)
  dum = new(4,graphic)

;loop over season
do j=0,3
   if (j.eq.0) then    
      	  res@tmXBLabelsOn = False
   else if (j.eq.1) then
   	  res@tmYLLabelsOn = False
   else if (j.eq.2) then
   	  res@tmXBLabelsOn = True
	  res@tmYLLabelsOn = True
   else if (j.eq.3) then
   	  res@tmYLLabelsOn = False
   end if 
   end if
   end if
   end if
  	plot(j) = gsn_csm_contour_map(wks,field(j,{latS:latN},{lonL:lonR}),res)

  ;plot signifance (add polymarker if gridbox is significant) ***
  ;identify coordinates x,y where significant
    if( (max(sig(j,:,:)).eq.0) .and. (min(sig(j,:,:)).eq.0) ) then
        print("No significant grid points")
    else 

    sig_1D      = ndtooned(sig(j,:,:))
    dsizes_p = dimsizes(sig(j,:,:))
    lats=sig&lat
    lons=sig&lon
    indices_p = ind_resolve(ind(sig_1D.eq.1),dsizes_p)
    lonind=indices_p(:,1)                                
    latind=indices_p(:,0)
    lonsig=lons(lonind)
    latsig=lats(latind)

  ;; 2.2.2 add polymarkers
    gsres             = True
    ;;gsres@gsMarkerIndex = 2      ;use + sign for markers
    gsres@gsMarkerIndex = 1      ;use small filled circles
    if (.not.region) then
       gsres@gsMarkerSizeF = 0.001
    else
	gsres@gsMarkerSizeF = 0.0025
    end if
    gsres@gsMarkerColor      = 1

    gsres@tfPolyDrawOrder = "Draw"

    dum(j)=gsn_add_polymarker(wks,plot(j),lonsig,latsig,gsres)

    str1=unique_string("dum")
    plot@$str1$=dum

    delete(sig_1D)
    delete(dsizes_p)
    delete(indices_p)
    delete(lonind)
    delete(latind)
    delete(lonsig)
    delete(latsig)
    end if

end do ;seasons

;---------- Paneled Plot-------------

  panel_res                       = True
  panel_res@gsnMaximize           = True
  panel_res@gsnOrientation        = "landscape"
  panel_res@gsnPanelLabelBar      = True
  panel_res@gsnPanelFigureStrings = (/"DJF","MAM","JJA","SON"/)
  panel_res@amJust		  = "BottomLeft"
  panel_res@gsnPanelRowSpec       = True
  panel_res@pmLabelBarWidthF      = 0.8         ; makes label bar bigger
  panel_res@txString              = (field@long_name + " [" + field@units + "]")
  
  gsn_panel(wks,plot,(/2,2/),panel_res)

if(pltType .eq. "pdf")
        delete(wks)
	system("pdfcrop " + pltName + "." + pltType+ " "+ pltName + "." + pltType)
end if

end
