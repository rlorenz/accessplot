#!/bin/bash
# File Name : plot_PDFs_TX_TN_CABLEoff.sh
# Creation Date : 27/09/2013
# Last Modified : Wed 21 May 2014 15:58:43 EST
# Created By : Ruth Lorenz
# Purpose : plot PDFs of Tmax, Tmin over regions for CABLE offline output

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=out_gswp
export RUNNAME=CABLE
export DATA=HadGHCND

export Y_START=1986
export Y_STOP=1995

export NLAT=360
export NLON=150

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/CABLE_output/${RUNID}/timeseries/
export OBSDIR=/srv/ccrc/data07/z3356123/HadGHCND_nc/
export WORKDIR=$DATAPATH/CABLE_plots_workdir/
export OUTDIR=$DATAPATH/CABLE_plots/$RUNID/PDFs_figures/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.0
module load netcdf
module load nco
module load cdo
module load R

# -----------------------------------------------------------------------
# cut time to common timeperiod 1986-1995
# -----------------------------------------------------------------------
for VAR in TX TN
do
	ncks -O -d time,13149,16800 ${OBSDIR}/HadGHCND_${VAR}_1950-2011.nc $WORKDIR/${DATA}_${VAR}_${Y_START}-${Y_STOP}.nc
done

# -----------------------------------------------------------------------
# regird CABLE output to obs resolution
# -----------------------------------------------------------------------
for VAR in TX TN
do 
       cdo remapbil,$WORKDIR/${DATA}_${VAR}_${Y_START}-${Y_STOP}.nc $MODDIR/${RUNID}_${VAR}_${Y_START}-${Y_STOP}_degC.nc $WORKDIR/${RUNID}_${VAR}_${Y_START}-${Y_STOP}_rgd.nc
done
# -----------------------------------------------------------------------
# mask regions and monthly means
# -----------------------------------------------------------------------

for area in ASI AUS EUR NAM
do

	for VAR in TX TN
	do

	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $WORKDIR/${RUNID}_${VAR}_${Y_START}-${Y_STOP}_rgd.nc $WORKDIR/${area}_${RUNID}_${VAR}_${Y_START}-${Y_STOP}_rgd.nc
	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $WORKDIR/${DATA}_${VAR}_${Y_START}-${Y_STOP}.nc $WORKDIR/${area}_${DATA}_${VAR}_${Y_START}-${Y_STOP}.nc
	cdo monmean $WORKDIR/${area}_${RUNID}_${VAR}_${Y_START}-${Y_STOP}_rgd.nc $WORKDIR/${area}_${RUNID}_${VAR}_monmean_${Y_START}-${Y_STOP}_rgd.nc
	cdo monmean $WORKDIR/${area}_${DATA}_${VAR}_${Y_START}-${Y_STOP}.nc $WORKDIR/${area}_${DATA}_${VAR}_monmean_${Y_START}-${Y_STOP}.nc
	done

done

# -----------------------------------------------------------------------
# call ncl script
# -----------------------------------------------------------------------
cd $OUTDIR

#echo "Call ncl scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_pdfs_tmax_tmin.ncl 

echo "Call R script"
R --vanilla $HOME/scripts/plot_scripts/R_scripts/plot_pdf_skillscore_tmax_tmin_CABLEoff.R

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
