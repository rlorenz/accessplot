#!/bin/bash
# File Name : plot_ETCCDI_CMIP5_SREXreg.sh
# Creation Date : 05/08/2013
# Last Modified : Wed 21 May 2014 15:56:52 EST
# Created By : Ruth Lorenz
# Purpose : plot ETCCDI indices for ACCESS and CMIP5 models
#		boxplots over SREX regions

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=uakpd
export RUNNAME=ACCESS
export DATA=HADEX2

export Y_START=1951
export Y_STOP=2005

export NLAT=192
export NLON=145

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/ACCESS_output/${RUNID}/climdex_index/
export OBSDIR=$DATAPATH/DATASETS/${DATA}_Data/
export CMIPDIR=$DATAPATH/DATASETS/CMIP5_ETCCDI/
export WORKDIR=$DATAPATH/ACCESS_plots_workdir/
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID/ETCCDI_CMIP5_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.0.0
module load netcdf
module load nco
module load cdo

# -----------------------------------------------------------------------
# cut time to common timeperiod 1951-2005
# -----------------------------------------------------------------------
#for INDEX in CDD DTR #Rx5day TN10p TN90p TX10p TX90p
#do
#        ncks -d time,0,54 $MODDIR/${RUNID}_${Y_START}-2010_${INDEX}.nc $WORKDIR/${RUNID}_${Y_START}-${Y_STOP}_${INDEX}.nc
#	ncks -d time,0,54 $OBSDIR/RawData_HADEX2_${INDEX}_${Y_START}-2010_ANN_from-90to90_from-180to180.nc $WORKDIR/RawData_HADEX2_${INDEX}_${Y_START}-${Y_STOP}_ANN_from-90to90_from-180to180.nc
#done

# -----------------------------------------------------------------------
# mask SREX regions
# -----------------------------------------------------------------------

#for area in ALA AMZ CAM CAS CEU CGI CNA EAF EAS ENA MED NAS NAU NEB NEU SAF SAH SAS SAU SEA SSA TIB WAF WAS WNA WSA
#do

#	for INDEX in CDD DTR #Rx5day Tn10p Tn90p Tx10p Tx90p
#	do

#	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $WORKDIR/${RUNID}_${Y_START}-${Y_STOP}_${INDEX}.nc $WORKDIR/${area}_${RUNID}_${Y_START}-${Y_STOP}_${INDEX}.nc
#	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $WORKDIR/RawData_HADEX2_${INDEX}_${Y_START}-${Y_STOP}_ANN_from-90to90_from-180to180.nc $WORKDIR/${area}_HADEX2_${INDEX}_${Y_START}-${Y_STOP}_ANN.nc
#	done

#	for index in cdd dtr #rx5day tn10p tn90p tx10p tx90p
#	do
#		for model in ACCESS1-0 BNU-ESM CCSM4 CESM1-BGC
#		do
		
#		cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $CMIPDIR/${index}ETCCDI_yr_${model}_historical_r1i1p1_${Y_START}-${Y_STOP}.nc $WORKDIR/${area}_${index}ETCCDI_yr_${model}_r1i1p1_${Y_START}-${Y_STOP}.nc
#                ncrename -v ${index}ETCCDI,index $WORKDIR/${area}_${index}ETCCDI_yr_${model}_r1i1p1_${Y_START}-${Y_STOP}.nc

#		done
#	done
#done

# -----------------------------------------------------------------------
# call ncl script
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_CMIP5_boxplot.ncl 

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
