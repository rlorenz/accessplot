#!/bin/bash
# File Name : plot_LUC_blockbs_field_sig_ENS.sh
# Creation Date : 14/08/2014
# Last Modified : Tue 01 Sep 2015 10:06:27 AEST
# Created By : Ruth Lorenz
# Purpose : prepare plotting of LUC experiment differences
#	with significance testing, testing with block bootstrapping and test
#       field significance	

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export REGION=wc
export RUNID1=uaoyr
export RUNNAME1=CTL

export R1_START=1978 #run start RUNID1
export R1_STOP=2011  #run end RUNID1
export R2_START=1978 #run start RUNID1
export R2_STOP=2011  #run end RUNID1

export A_START=1981 #analysis start
export A_STOP=2011  #analysis end

export NENS=5 #number of ensembles

export pltType=pdf

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
    export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/$RUNID1/timeseries/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_plot without CTL input data"
    exit 1
fi

export OUTDIR=$DATAPATH/ACCESS_plots/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export siglev=0.05
export RES=n96
export NTILES=17
export NSOIL=6

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "CTL run: " $RUNID1 "with files in Directory " $CTLDIR
    echo "Experiments with files in Directories "$PATHCTL "/uaoya-p/timeseries/"
    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "Start Time:" $date
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load gcc/4.7.2
module load ncl/6.2.0
module load netcdf
module load cdo
module load R/3.0.3

for RUN in uaoya uaoyb uaoyc uaoyd uaoye uaoyf uaoyg uaoyh uaoyi uaoyj uaoyk uaoyl uaoym uaoyn uaoyo uaoyp uaoyr uaoys uaoyt uaoyw uaoyx uaoyy uaoyz vacda vacdb vacdc vacdd vacde vacdf vacdg vacdh vacdi vacdj vacdk vacdl vacdm vacdn vacdo vacdp vacdq vacdr vacds vacdt vacdu vacdv vajoa vajob vajoc vajod vajoe vajof vajog vajoh vajoi vajoj vajok vajol vajom vajon vajoo vajop vajoq vajor vajos vajot vajou vajov vacdw vacdx vacdy vacdz
do
if [[ ! -f "$WORKDIR/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc" ]]; then
        cp $PATHCTL/$RUN/timeseries/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc.gz
fi
 
#if [[ ! -f "$WORKDIR/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc" ]]; then
#        cp $PATHCTL/$RUN/timeseries/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
#        gunzip $WORKDIR/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc.gz
#fi

done

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call R scripts"
#R  --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_auto_arima.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_auto_arima_allAMZ.Rout

#R  --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_$REGION.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_noSA.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_${REGION}_noSA.Rout

#R --vanilla --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_ENS.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_${NENS}ENS_$REGION.Rout
#R --vanilla --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_ENS_noSA.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_notest_simpleTtest_${NENS}ENS_${REGION}_noSA.Rout


#R --vanilla --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_$REGION.Rout
###R --vanilla --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_fs.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_fs_sc.Rout #incl. monte carlo -> slow
#R --vanilla --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_ENS.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_${NENS}ENS_$REGION.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_ENS_noSA.R > $HOME/scripts/plot_scripts/R_scripts/LUC_stats/LUC_exp_modTtest_${NENS}ENS_$REGION_noSA.Rout

#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_ENS_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_${NENS}ENS_Wilks_121${REGION}.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_Wilks_allAMZ.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_Wilks_001GPsc.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_ENS_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_ENS_Wilks_1000bs_001GP$REGION.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_ENS_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_${NENS}ENS_Wilks_allAMZ$REGION.Rout
#wait

echo "Call ncl scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_lag1_autocorr.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_notest_Ttest.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_Ttest_noSA.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_notest_Ttest_ENS.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_Ttest_ENS_noSA.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_modTtest_field_sig.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_modTtest_field_sig_noSA.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_modTtest_ENS_field_sig.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_modTtest_ENS_field_sig_noSA.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_blockbs_field_sig.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_blockbs_field_sig_noSA.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_blockbs_ENS_field_sig.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_LUC_blockbs_ENS_field_sig_noSA.ncl

#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_land_global_stat.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_land_global_stat_ENS.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_noSAland_global_stat.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_ttest_wilc_mttest.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_noSAland_global_stat_ENS.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_noSAland_global_stat_ENS_allExp.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_nr_sig_gp_ttest_wilc_mttest_ENS.ncl
# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
