;-----------------------------------------------------------------------------;
; File Name :
; Creation Date : 05-08-2014
; Last Modified : Tue 05 Aug 2014 17:01:46 EST
; Created By : Ruth Lorenz
; Purpose : plot slope from quantile regression calculated in R
;		(../R_scripts/calc_SPI_HD_quantreg.R)

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_single_overlay_significance.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME
Model = systemfunc("echo $MODEL") ; model

user = systemfunc("echo $USER")

ctlDir = systemfunc("echo $CTLDIR")
expDir = systemfunc("echo $EXPDIR")
outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

seas = systemfunc("echo $seas")

;define plot type
plttype = systemfunc("echo $pltType")

if (Runname1.eq."GC1A85") then
   Ctlname = "ExpA"
else if (Runname1.eq."CTL") then
   Ctlname = "CTL"
else
	print("Wrong experiment name")
end if
end if

if (Runname2.eq."GC1A85") then
   Expname = "ExpA"
else if (Runname2.eq."GC1B85") then
   Expname = "ExpB"
else
	print("Wrong experiment name")
end if
end if

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")
file1 = addfile(ctlDir + "Quantile_Slope0.9_HD_SPI3_"+seas+"_"+AStart+"-"+AEnd+".nc","r")

slope = file1->Slope_90perc
slope@long_name     = "Slope 0.9 Quantile Regression SPI-HD "+seas

signif = file1->Significance
signif@long_name     = "Significance on 0.9 level"

pltColor="MPL_autumn"
lev=(/-0.5,-0.4,-0.3,-0.2,-0.1,0,0.1/)
signif_single_Plot(outDir,"slope_90perc_quantreg_SPI_HD_"+seas+"_"+Runname1,slope@long_name,Runname1,"",AStart,AEnd,plttype,pltColor,slope,"",signif,False,True,lev,True)

end
