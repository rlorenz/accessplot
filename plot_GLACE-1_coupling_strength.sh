#!/bin/bash
# File Name : plot_GLACE_coupling_strength.sh
# Creation Date : 03/12/2013
# Last Modified : Wed 21 May 2014 15:58:07 EST
# Created By : Ruth Lorenz
# Purpose : Wrapper to plot GLACE-1 coupling strength
#	calls ncl_scripts/coupling_strength_classic.ncl

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID1=uany #coupled, uany for JJA and uaod for DJF
export RUNID2=uaob     #uncoupled, uaob for JJA and uaof for DJF

export R_START=2000 #run start
export R_STOP=2000 #run stop

export RUNNAME1=CTL
export RUNNAME2=UNCOUP

export SEASON=JJA

export pltType=pdf
 
# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
    export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/$RUNID1/timeseries/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run without CTL input data"
    exit 1
fi


export EXPDIR=$PATHCTL/$RUNID2/timeseries/
if [[ ! -d $EXPDIR ]]; then
            echo "(1) $EXPDIR does not exist"
            echo "(2) cannot run without EXP input data"
	    exit 1
fi

export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID2/coupling/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
    echo "Results are compared to Jobid: " $RUNID2 " with files in Directory " $EXPDIR
    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call NCL scripts"

ncl $HOME/scripts/plot_scripts/ncl_scripts/coupling_strength_classic.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------
    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
