# plots ETCCDI indices from GCCMIP5 as spatially aggregated pdfs
# method based on Fischer and Knutti 2014 GRL

library(ncdf)

in_dir <- "/srv/ccrc/data32/z3441306/GLACE-CMIP5/"

out_dir <- "/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ENS_MEAN/climdex/"

R1.Start="1954" #for CTL
R2.Start="1950" #for EXPs
R.End="2100"

A.Start="2055"
A.End="2084"

#read data from netcdfs, index, all models, CTL, ExpA, ExpB
index<-list("TX90p","TN90p","WSDI","R95p","R10mm","CDD","CWD","TNx","TXx")
unit<-list("%","%","days","mm","days","days","days","degC","degC")
expname<-list("CTL","GC1A85","GC1B85")
model<-list("ACCESS","CESM","ECHAM6","GFDL","IPSL","EC-EARTH")
seas<-list("DJF","MAM","JJA","SON","ANN")

nexp<-length(expname)
nmod<-length(model)
nlat=96
nlon=96
ntim_A=(as.numeric(A.End)-as.numeric(A.Start)+1)
ntim_R1=(as.numeric(R.End)-as.numeric(R1.Start)+1)
ntim_R2=(as.numeric(R.End)-as.numeric(R2.Start)+1)

#loop over indices
for (i in 1:9){
    if (i <= 7){
    nmon<-1 #indices with only annual
    j<-5
    }
    else {
    nmon<-3 #indices were seasonal calculation possible
    j<-1
    }

    while ( j<=5 ){
    data_ctl<-array(NA,dim=c(nlon,nlat,ntim_R1,nmod,nmon))
    data_seas_ctl<-array(NA,dim=c(nlon,nlat,ntim_R1,nmod))
    data_seas_norm_ctl<-array(NA,dim=c(nlon,nlat,ntim_R1,nmod))
    data_seas_norm_tp_ctl<-array(NA,dim=c(nlon,nlat,ntim_A,nmod))
    data_seas_norm_weight_ctl<-array(NA,dim=c(nlon,nlat,ntim_A,nmod))

    data_a<-array(NA,dim=c(nlon,nlat,ntim_R2,nmod,nmon))
    data_seas_a<-array(NA,dim=c(nlon,nlat,ntim_R2,nmod))
    data_seas_norm_a<-array(NA,dim=c(nlon,nlat,ntim_R1,nmod))
    data_seas_norm_tp_a<-array(NA,dim=c(nlon,nlat,ntim_A,nmod))
    data_seas_norm_weight_a<-array(NA,dim=c(nlon,nlat,ntim_A,nmod))

    data_b<-array(NA,dim=c(nlon,nlat,ntim_R2,nmod,nmon))
    data_seas_b<-array(NA,dim=c(nlon,nlat,ntim_R2,nmod))
    data_seas_norm_b<-array(NA,dim=c(nlon,nlat,ntim_R1,nmod))
    data_seas_norm_tp_b<-array(NA,dim=c(nlon,nlat,ntim_A,nmod))
    data_seas_norm_weight_b<-array(NA,dim=c(nlon,nlat,ntim_A,nmod))

		#loop over models
    		for (mod in 1:nmod){
     
		file=paste(in_dir,model[mod],"/",expname[1],"/climdex_index/",model[mod],"_",expname[1],"_1_",R1.Start,"-",R.End,"_",index[i],"_maskocean_remapcon.nc",sep="")
      		print(file)
      		data_ctl.nc = open.ncdf(file)
      		print(data_ctl.nc)

		time_units<-att.get.ncdf( data_ctl.nc,"time","units")
		time=get.var.ncdf( data_ctl.nc,"time")
		time.y<-substr(as.character(time),1,4)
		t1_ctl=min(which(as.numeric(time.y)>=A.Start))
        	t2_ctl=max(which(as.numeric(time.y)<=A.End))
		
		latitude=get.var.ncdf( data_ctl.nc,"lat")
		longitude=get.var.ncdf( data_ctl.nc,"lon")

		file=paste(in_dir,model[mod],"/",expname[2],"/climdex_index/",model[mod],"_",expname[2],"_1_",R2.Start,"-",R.End,"_",index[i],"_maskocean_remapcon.nc",sep="")
      		print(file)
      		data_a.nc = open.ncdf(file)
      		print(data_a.nc)

		time_units<-att.get.ncdf( data_a.nc,"time","units")
		time_exp=get.var.ncdf( data_a.nc,"time")

		file=paste(in_dir,model[mod],"/",expname[3],"/climdex_index/",model[mod],"_",expname[3],"_1_",R2.Start,"-",R.End,"_",index[i],"_maskocean_remapcon.nc",sep="")
      		print(file)
      		data_b.nc = open.ncdf(file)
      		print(data_b.nc)

		if (i <= 8){
      		data_seas_ctl[,,,mod]=get.var.ncdf(data_ctl.nc,"Annual") #read variable Annual
      		data_seas_a[,,,mod]=get.var.ncdf(data_a.nc,"Annual") #read variable Annual
      		data_seas_b[,,,mod]=get.var.ncdf(data_b.nc,"Annual") #read variable Annual

		}
		else {
		     if (j==1){
      		     	data_ctl[,,,mod,1]=get.var.ncdf(data_ctl.nc,"December")
			data_ctl[,,,mod,2]=get.var.ncdf(data_ctl.nc,"January")
			data_ctl[,,,mod,3]=get.var.ncdf(data_ctl.nc,"February")
      		     	data_a[,,,mod,1]=get.var.ncdf(data_a.nc,"December")
			data_a[,,,mod,2]=get.var.ncdf(data_a.nc,"January")
			data_a[,,,mod,3]=get.var.ncdf(data_a.nc,"February")
      		     	data_b[,,,mod,1]=get.var.ncdf(data_b.nc,"December")
			data_b[,,,mod,2]=get.var.ncdf(data_b.nc,"January")
			data_b[,,,mod,3]=get.var.ncdf(data_b.nc,"February")
			}
			else if (j==2){
      		     	data_ctl[,,,mod,1]=get.var.ncdf(data_ctl.nc,"March")
			data_ctl[,,,mod,2]=get.var.ncdf(data_ctl.nc,"April")	
			data_ctl[,,,mod,3]=get.var.ncdf(data_ctl.nc,"May")	
      		     	data_a[,,,mod,1]=get.var.ncdf(data_a.nc,"March")
			data_a[,,,mod,2]=get.var.ncdf(data_a.nc,"April")	
			data_a[,,,mod,3]=get.var.ncdf(data_a.nc,"May")
      		     	data_b[,,,mod,1]=get.var.ncdf(data_b.nc,"March")
			data_b[,,,mod,2]=get.var.ncdf(data_b.nc,"April")	
			data_b[,,,mod,3]=get.var.ncdf(data_b.nc,"May")
			}
			else if (j==3){
      		     	data_ctl[,,,mod,1]=get.var.ncdf(data_ctl.nc,"June")
			data_ctl[,,,mod,2]=get.var.ncdf(data_ctl.nc,"July")
			data_ctl[,,,mod,3]=get.var.ncdf(data_ctl.nc,"August")
      		     	data_a[,,,mod,1]=get.var.ncdf(data_a.nc,"June")
			data_a[,,,mod,2]=get.var.ncdf(data_a.nc,"July")
			data_a[,,,mod,3]=get.var.ncdf(data_a.nc,"August")
      		     	data_b[,,,mod,1]=get.var.ncdf(data_b.nc,"June")
			data_b[,,,mod,2]=get.var.ncdf(data_b.nc,"July")
			data_b[,,,mod,3]=get.var.ncdf(data_b.nc,"August")
			}
			else if (j==4){
      		     	data_ctl[,,,mod,1]=get.var.ncdf(data_ctl.nc,"September")
			data_ctl[,,,mod,2]=get.var.ncdf(data_ctl.nc,"October")
			data_ctl[,,,mod,3]=get.var.ncdf(data_ctl.nc,"November")
	      		data_a[,,,mod,1]=get.var.ncdf(data_a.nc,"September")
			data_a[,,,mod,2]=get.var.ncdf(data_a.nc,"October")
			data_a[,,,mod,3]=get.var.ncdf(data_a.nc,"November")
	      		data_b[,,,mod,1]=get.var.ncdf(data_b.nc,"September")
			data_b[,,,mod,2]=get.var.ncdf(data_b.nc,"October")
			data_b[,,,mod,3]=get.var.ncdf(data_b.nc,"November")
			}
			else if (j==5){
      		     	data_ctl[,,,mod,1]=get.var.ncdf(data_ctl.nc,"Annual")
			data_ctl[,,,mod,2]=get.var.ncdf(data_ctl.nc,"Annual")
			data_ctl[,,,mod,3]=get.var.ncdf(data_ctl.nc,"Annual")
	      		data_a[,,,mod,1]=get.var.ncdf(data_a.nc,"Annual")
			data_a[,,,mod,2]=get.var.ncdf(data_a.nc,"Annual")
			data_a[,,,mod,3]=get.var.ncdf(data_a.nc,"Annual")
	      		data_b[,,,mod,1]=get.var.ncdf(data_b.nc,"Annual")
			data_b[,,,mod,2]=get.var.ncdf(data_b.nc,"Annual")
			data_b[,,,mod,3]=get.var.ncdf(data_b.nc,"Annual")
			}			
			data_seas_ctl[,,,mod]<-apply(data_ctl[,,,mod,],c(1:3),mean,na.rm=TRUE,names=FALSE)
			data_seas_a[,,,mod]<-apply(data_a[,,,mod,],c(1:3),mean,na.rm=TRUE,names=FALSE)
			data_seas_b[,,,mod]<-apply(data_b[,,,mod,],c(1:3),mean,na.rm=TRUE,names=FALSE)
		}

		#normalize data by mean and std of CTL from each model
		#first cut expA and expB in same length as ctl
		t_diff=as.numeric(R1.Start)-as.numeric(R2.Start)+1
		time=time_exp[t_diff:dim(time_exp)]

		data_seas_a_cut = data_seas_a[,,t_diff:dim(time_exp),mod]
		data_seas_b_cut = data_seas_b[,,t_diff:dim(time_exp),mod]

		time.y<-substr(as.character(time),1,4)
		t1_exp=min(which(as.numeric(time.y)>=A.Start))
        	t2_exp=max(which(as.numeric(time.y)<=A.End))

		ctl_mean = apply(data_seas_ctl[,,,mod],c(1:2),mean,na.rm=TRUE) #data_seas[lon,lat,t,mod]
		ctl_std = apply(data_seas_ctl[,,,mod],c(1:2),sd,na.rm=TRUE) #data_seas[lon,lat,t,mod]

		for (t in 1:ntim_R1){
		data_seas_norm_ctl[,,t,mod] = (data_seas_ctl[,,t,mod]-ctl_mean)/ctl_std
		data_seas_norm_a[,,t,mod] = (data_seas_a_cut[,,t]-ctl_mean)/ctl_std
		data_seas_norm_b[,,t,mod] = (data_seas_b_cut[,,t]-ctl_mean)/ctl_std
		}

		#cut into analysis period
		data_seas_norm_tp_ctl[,,,mod] = data_seas_norm_ctl[,,t1_ctl:t2_ctl,mod]
		data_seas_norm_tp_a[,,,mod] = data_seas_norm_a[,,t1_exp:t2_exp,mod]
		data_seas_norm_tp_b[,,,mod] = data_seas_norm_b[,,t1_exp:t2_exp,mod]

		#weight by latitude dependent area
		rad<-4.0*atan(1.0)/180.0
		for (lon in 1:nlon){
		    for (lat in 1:nlat){
		    	clat1 <-cos(latitude[lat]*rad)
		    	for (yr in 1:ntim_A){
			    data_seas_norm_weight_ctl[lon,lat,yr,mod]<-data_seas_norm_tp_ctl[lon,lat,yr,mod]*clat1
			    data_seas_norm_weight_a[lon,lat,yr,mod]<-data_seas_norm_tp_a[lon,lat,yr,mod]*clat1
			    data_seas_norm_weight_b[lon,lat,yr,mod]<-data_seas_norm_tp_b[lon,lat,yr,mod]*clat1
			}
		    }
		}
		
    		} # end mod

     #mask grid points were less than half of models have data
     data_seas_norm_mask_ctl=array(NA,dim=c(nlon,nlat,ntim_A,nmod))
     data_seas_norm_mask_a=array(NA,dim=c(nlon,nlat,ntim_A,nmod))
     data_seas_norm_mask_b=array(NA,dim=c(nlon,nlat,ntim_A,nmod))
     mask_ctl=(apply(data_seas_norm_weight_ctl[,,1,],c(1,2),function(x) sum(is.finite(x))))
     mask_a=(apply(data_seas_norm_weight_a[,,1,],c(1,2),function(x) sum(is.finite(x))))
     mask_b=(apply(data_seas_norm_weight_b[,,1,],c(1,2),function(x) sum(is.finite(x))))
     for (mod in 1:6){
	     for (yr in 1:ntim_A){
	     	 tmp_ctl=data_seas_norm_weight_ctl[,,yr,mod]
		 tmp_ctl[which(mask_ctl<=3)]<-NA
	     	 data_seas_norm_mask_ctl[,,yr,mod]=tmp_ctl
	     	 tmp_a=data_seas_norm_weight_a[,,yr,mod]
		 tmp_a[which(mask_a<=3)]<-NA
	     	 data_seas_norm_mask_a[,,yr,mod]=tmp_a
	     	 tmp_b=data_seas_norm_weight_b[,,yr,mod]
		 tmp_b[which(mask_b<=3)]<-NA
	     	 data_seas_norm_mask_b[,,yr,mod]=tmp_b
	     }
     }

     #test with kolmogorov smirnoff test if pdfs different
#     Atest<-ks.test(data_seas_norm_mask_ctl[,,,],data_seas_norm_mask_a[,,,])
#     Btest<-ks.test(data_seas_norm_mask_ctl[,,,],data_seas_norm_mask_b[,,,])   
#     ABtest<-ks.test(data_seas_norm_mask_a[,,,],data_seas_norm_mask_b[,,,])

     #plot pdfs
     outfile=paste(out_dir,index[i],"_spat_agg_",seas[j],"_",A.Start,"-",A.End,".pdf", sep="")
     print(outfile)
     pdf(outfile)

     plot(density(data_seas_norm_mask_ctl[,,,1],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1,#ylim=c(0,0.8),
                        xlim=c(-4.0,4.0),xlab="normalized index [-]",ylab="Density",
                        cex.lab=1.6,cex.axis=1.6,main=paste(index[i]," ",seas[j], sep=""),cex.main=1.6)  
                        lines(density(data_seas_norm_mask_ctl[,,,2],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_ctl[,,,3],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_ctl[,,,4],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_ctl[,,,5],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_ctl[,,,6],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 1,lty=1)

                       	lines(density(data_seas_norm_mask_a[,,,1],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_a[,,,2],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_a[,,,3],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_a[,,,4],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_a[,,,5],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_a[,,,6],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 1,lty=1)

                       	lines(density(data_seas_norm_mask_b[,,,1],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_b[,,,2],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_b[,,,3],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_b[,,,4],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_b[,,,5],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)
                        lines(density(data_seas_norm_mask_b[,,,6],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 1,lty=1)

			# plot full ensemble in thick
                        lines(density(data_seas_norm_mask_ctl[,,,],na.rm = TRUE,bw="nrd"),col = "grey40", lwd = 4,lty=1)
                        lines(density(data_seas_norm_mask_a[,,,],na.rm = TRUE,bw="nrd"),col = "red3", lwd = 4,lty=1)
                        lines(density(data_seas_norm_mask_b[,,,],na.rm = TRUE,bw="nrd"),col = "royalblue4", lwd = 4,lty=1)

                legend("topright",c("CTL","ExpA","ExpB"),cex=1.6,bty="n",
                                col = c("grey40","red3","royalblue4"),lwd=c(4,4,4),lty = c(1,1,1))

#        text(0.5,5,bquote("ExpA-CTL p.val="~.(Atest$p.value)),cex=1.6)
#        text(0.5,5,bquote("ExpB-CTL p.val="~.(Btest$p.value)),cex=1.6)
#        text(0.5,5,bquote("ExpA-ExpB p.val="~.(ABtest$p.value)),cex=1.6)
				
     dev.off()

     j<-j+1
     } #end while

    if (i <= 8){
    j<-5
    }
    else {
    j<-1
    }

} #end i