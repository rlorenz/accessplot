# Compare CTL and LUC experiments
# test significance with modified t-test and 
# walker's test and false discovery rate, and
# Livezey and Chen (1984) counting Monta Carlo test for field significance

library(ncdf)
source("/home/z3441306/scripts/plot_scripts/R_scripts/modified_t_test/modTtest_large.R")
source("/home/z3441306/scripts/plot_scripts/R_scripts/modified_t_test/modTtest_small.R")

###set-up
# input directory
file_dir <- Sys.getenv("WORKDIR")

# run id's for control run
runid_ctl<-Sys.getenv("RUNID1")
ctl<-Sys.getenv("RUNNAME1")

# strongly or weakly coupled region
region<-Sys.getenv("REGION")
print(region)

# runids for experiments depend on region
if (region == "sc"){
runid<-list("uaoyc","uaoye","uaoyg","uaoyi","uaoyk","uaoym","uaoyo","uaoya","vacdq")
runname<-list("001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc","242GP")
} else if (region == "wc"){
runid<-list("uaoyd","uaoyf","uaoyh","uaoyj","uaoyl","uaoyn","uaoyp","uaoyb","vacdt")
runname<-list("001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc","allAMZ")
} else {
  print("Wrong region")
}

# output directory
out_dir<-Sys.getenv("OUTDIR")

# significance level for t-test
siglev<-as.numeric(Sys.getenv("siglev"))

# run start and end for CTL
R1.Start<-Sys.getenv("R1_START")
R1.End<-Sys.getenv("R1_STOP")

# run start and end for experiments
R2.Start<-Sys.getenv("R2_START")
R2.End<-Sys.getenv("R2_STOP")

# start and end of analysis period
A.Start<-as.numeric(Sys.getenv("A_START"))
A.End<-as.numeric(Sys.getenv("A_STOP"))

# time data frame for analysis period
time.ind<- c(1:((A.End-A.Start+1)*12))
time.m <- rep(seq(1,12),(A.End-A.Start+1))
time.y <- c()
ind =1
for (yr in A.Start:A.End){
        time.y[ind:(ind+11)] <- rep(yr,12)
        ind = ind+12
}
time <- data.frame(time.y,time.m,time.ind)
# number of years in analysis period
nyears=A.End-A.Start+1

###read data from netcdfs
print("read data")
# first read land sea mask, determines number of lats and lons
file_lsm=paste("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc",sep="")
        print(file_lsm)
        ex.nc = open.ncdf(file_lsm)
        print(ex.nc)
        lsm=get.var.ncdf( ex.nc, "lsm") #lon,lat,surface,time
        lon=get.var.ncdf( ex.nc, "longitude")
        lat=get.var.ncdf( ex.nc, "latitude")

	dims=dim(lsm)
	nlon=dims[1]
	nlat=dims[2]

###loop over runs
print("Loop over runs")
for (run in 1:9){
    #check if output file exists, if not create it
    dir.create(file.path(out_dir,runname[run]), showWarnings = FALSE)

    ###loop over variables
    print("Loop over variables")
    for (v in 1:2){
    	if ( v==1 ){
       	   var<-"tas"
       	}
	else if (v==2){
       	    var<-"pr"
       	}

       	file_exp=paste(file_dir,runid[run],".monthly_TS.",R2.Start,"_",R2.End,".nc",sep="")
        print(file_exp)
        ex.nc = open.ncdf(file_exp)

        time_exp_units<-att.get.ncdf( ex.nc,"time","units")
        base_date_exp=as.character(lapply(strsplit(as.character(time_exp_units$value),split=" "),"[",3))
        time_exp=get.var.ncdf( ex.nc,"time")
        time_exp_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_exp))
        time_exp.y<-format(time_exp_d, "%Y")
        time_exp.m<-format(time_exp_d, "%m")

        t1=min(which(as.numeric(time_exp.y)>=A.Start))
        t2=(A.End-A.Start+1)*12

        var_exp=get.var.ncdf( ex.nc, var, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time
	unit_var<- att.get.ncdf(ex.nc, var, "units")

	file_ctl=paste(file_dir,runid_ctl,".monthly_TS.",R1.Start,"_",R1.End,".nc",sep="")
        print(file_ctl)
        ex.nc = open.ncdf(file_ctl)

        time_ctl_units<-att.get.ncdf( ex.nc,"time","units")
        base_date_ctl=as.character(lapply(strsplit(as.character(time_ctl_units$value),split=" "),"[",3))
        time_ctl=get.var.ncdf( ex.nc,"time")
        time_ctl_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_ctl))
        time_ctl.y<-format(time_ctl_d, "%Y")
        time_ctl.m<-format(time_ctl_d, "%m")

        t1=min(which(as.numeric(time_ctl.y)>=A.Start))
        t2=(A.End-A.Start+1)*12
        var_ctl=get.var.ncdf( ex.nc, var, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time

	dims=dim(var_exp)
	ntim=dims[3]

	#loop over seasons,
	var_exp_seas<-array(NA,dim=c(nlon,nlat,4,nyears))
	var_ctl_seas<-array(NA,dim=c(nlon,nlat,4,nyears))
 
	for (j in 1:4){
                print(j)
                #loop over years
                y=1
                for (yr in A.Start:A.End){
                if (j==1){seas <- "DJF"
		   if (y==1){
		      ind.seas <- time[which((time[,1]==yr)&((time[,2]==1)|(time[,2]==2))),3]
		      }
		      else{
                        ind.seas <- time[which((time[,1]==(yr-1)&(time[,2]==12))|(time[,1]==yr&((time[,2]==1)|(time[,2]==2)))),3]
			}
                }
                if (j==2){seas <- "MAM"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==3)|(time[,2]==4)|(time[,2]==5))),3]
                }
                if (j==3){seas <- "JJA"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==6)|(time[,2]==7)|(time[,2]==8))),3]
                }
                if (j==4){seas <- "SON"
                        ind.seas <- time[which((time[,1]==yr)&((time[,2]==9)|(time[,2]==10)|(time[,2]==11))),3]
                }

                #calculate seasonal means per year
                var_exp_seas[,,j,y]<-apply(var_exp[,,ind.seas],c(1:2),mean,na.rm=TRUE)
                var_ctl_seas[,,j,y]<-apply(var_ctl[,,ind.seas],c(1:2),mean,na.rm=TRUE)

		tmp1<-var_exp_seas[,,j,y]
	  	tmp1[which(lsm==0)] <- NA
	  	var_exp_seas[,,j,y] <- tmp1
	  	tmp2<-var_ctl_seas[,,j,y]
	  	tmp2[which(lsm==0)] <- NA
	  	var_ctl_seas[,,j,y] <- tmp2

	  	#mask allAMZ region
	  	ind_lat<-which(lat<=21.25&lat>=-26.25)
	  	ind_lon<-which(lon>=270&lon<=333.750)
	  	var_exp_seas[ind_lon,ind_lat,j,y] <- NA
	  	var_ctl_seas[ind_lon,ind_lat,j,y] <- NA

                y=y+1
                } #end loop years

	} #end loop seasons

	#apply modified t-test, for large samples if ne>=30, otherwise small sample
	ne<-nyears*2

	if (ne>=30){
   	   print("ne larger than 30")
   	   test<-modTtest_large(var_exp_seas,var_ctl_seas,siglev)
	}
	else {
    	     print("ne smaller than 30 -> use look up tables for small sample size")
    	     test<-modTtest_small(var_exp_seas,var_ctl_seas,siglev,ne)
	}

	var_exp_ave<-test$XAve
	var_ctl_ave<-test$YAve
	Hval_exp_ctl<-test$Hval
	Tval_exp_ctl<-test$Tval
	Pval_exp_ctl<-test$Pval

	###check field significance with Walker's test
	sig_walk<-array(NA,dim=c(nlon,nlat,4))
	p_min<-array(NA,dim=c(4))

	#total number of local tests
	K<-sum(!is.na(var_ctl_seas[,,1,1]))

	#walker criterium (Wilks, 2006)
	p_walk<-1-(1-siglev)^(1/K)

	# test if minimum p-value per season is larger than p_walk -> not significant
	for (j in 1:4){
    	    p_min[j]<-min(Pval_exp_ctl[,,j],na.rm=T)
    	    if (p_min[j]>p_walk){
       	       sig_walk[,,j]<-0
    	    }  else {
       	       sig_walk[,,j]<-Hval_exp_ctl[,,j]
    	    }
	}

	###check field significance with False Discovery Rate
	fdr<-array(0,dim=c(nlon,nlat,4))
	sig_fdr<-array(0,dim=c(nlon,nlat,4))
	p<-array(NA,dim=c(nlon*nlat))

	#put all p-values in 1D vector
	prob_1D<-(c(Pval_exp_ctl))

	# sort vecor increasing
	p_sort<-sort(prob_1D, decreasing = FALSE)

	# reject those local tests for which max[p(k)<=(siglev^(k/K)]
	for (k in 1:K){
    	    if (p_sort[k]<=(siglev^(k/K))){
       	       p[k]<-p_sort[k]
    	    } else {
      	      p[k]<-0.0
    	    }
	}
	p_fdr<-max(p,na.rm=T)

	fdr[which(Pval_exp_ctl<=p_fdr)] <- 1
	sig_fdr[which(fdr==1 & Hval_exp_ctl==1)] <- 1

	#check field significance after livezey and chen 1983
	print("check field significance with monte carlo")
	field_sig<-array(NA,dim=c(nlon,nlat,4))
	field_sig_AMZ<-array(NA,dim=c(nlon,nlat,4))
	p.test<-array(NA,dim=c(nlon,nlat,4,3000))
	for (mc in 1:3000){
    	print(mc)
	rand_4d<-array(runif(nlon*nlat*4*nyears,min(var_ctl_seas),max(var_ctl_seas)),dim=c(nlon,nlat,4,nyears))
		for (y in 1:nlon){
	    	    for (x in 1:nlat){
	    	    	for (j in 1:4){
		    	    test<-cor.test(var_ctl_seas[y,x,j,],rand_4d[y,x,j,])
		    	    p.test[y,x,j,mc]<-test$p.value
			}
	    	    }	
		}
	}
	quant<-apply(p.test[,,,],c(3),function(z) quantile(z,c(0.05),na.rm=TRUE))

	#nr of grid point where test applied (nlat*nlon-ocean-AMZ)
	nr_grdpts<-sum(!is.na(var_ctl_seas[,,1,1]))

	for (j in 1:4){
    	    tot_sig_prct<-sum(Hval_exp_ctl[,,j],na.rm=TRUE)/nr_grdpts
    	    print(tot_sig_prct)
		if ( tot_sig_prct >= quant[j]){
       	   	   field_sig[,,j]<-Hval_exp_ctl[,,j]
    		} else {
	  	   field_sig[,,j]<-0	
		}
	}

	#only over AMZ region where LUC applied
      	if (region == "sc"){
      	   reg<-"SC" 
	} else {
	   reg<-"WC"
	}

        mask_file=paste("/home/z3441306/scripts/plot_scripts/areas_txt/AMZ_",reg,".txt",sep="")
	print(mask_file)
        mask=read.table(mask_file)
        minlon<-mask[1,1]
        maxlon<-mask[3,1]
        maxlat<-mask[1,2]
        minlat<-mask[3,2]
        if (maxlon<=0){
           maxlon<-maxlon+360
        }
        if (minlon<=0){
           minlon<-minlon+360
        }
        #cut data into region
	print("Cut data into AMZ region")
        p.test[which(lon<=minlon),,,] <- NA
        p.test[which(lon>=maxlon),,,] <- NA
        p.test[,which(lat<=minlat),,] <- NA
        p.test[,which(lat>=maxlat),,] <- NA

	Hval_exp_ctl_AMZ<-Hval_exp_ctl
        Hval_exp_ctl_AMZ[which(lon<=minlon),,] <- NA
        Hval_exp_ctl_AMZ[which(lon>=maxlon),,] <- NA
        Hval_exp_ctl_AMZ[,which(lat<=minlat),] <- NA
        Hval_exp_ctl_AMZ[,which(lat>=maxlat),] <- NA

	quant_AMZ<-apply(p.test[,,,],c(3),function(z) quantile(z,c(0.05),na.rm=TRUE))
	for (j in 1:4){

    	tot_sig_prct<-sum(Hval_exp_ctl_AMZ[,,j],na.rm=T)/(((maxlat-minlat)/1.25)*((maxlon-minlon)/1.875))
    	print(tot_sig_prct)
		if ( tot_sig_prct >= quant_AMZ[j]){
       		   field_sig_AMZ[,,j]<-Hval_exp_ctl_AMZ[,,j]
    		} else {
		   field_sig_AMZ[,,j]<-0	
		}
	}

	###save data as netcdf to plot
	print("Save data to netcdf file for plotting")
	outfile<-paste(out_dir,runname[run],"/",var,"_",runname[run],"_",ctl,"_seas_modTtest_fs_",A.Start,"_",A.End,".nc",sep="")
	print(outfile)
	#define dimension
	dimLat <- dim.def.ncdf("Latitude", "degrees_north", lat)
	dimLon <- dim.def.ncdf("Longitude", "degrees_east", lon)
	dimT <- dim.def.ncdf("time","season",1:4,unlim = FALSE)

	#define variables
	FillValue<- -9999
	exp_Ave<-var.def.ncdf("exp_Ave", unit_var$value, list(dimLon,dimLat,dimT), FillValue) #,prec="double")
	ctl_Ave<-var.def.ncdf("ctl_Ave", unit_var$value, list(dimLon,dimLat,dimT), FillValue) #,prec="double")
	Hval<-var.def.ncdf("Hval","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")
	Tval<-var.def.ncdf("Tval","-",list(dimLon,dimLat,dimT), FillValue) #,prec="double")
	Pval<-var.def.ncdf("Pval","-",list(dimLon,dimLat,dimT), FillValue) #,prec="double")
	field_walk<-var.def.ncdf("FS_walk","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")
	field_fdr<-var.def.ncdf("FS_fdr","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")
	fs_glob<-var.def.ncdf("FS","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")
	fs_AMZ<-var.def.ncdf("FS_AMZ","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")

	#create netcdf
	nc <- create.ncdf( outfile, list(exp_Ave,ctl_Ave,Hval,Tval,Pval,field_walk,field_fdr,fs_glob,fs_AMZ))

	#put variables into file
	put.var.ncdf(nc, exp_Ave,var_exp_ave, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, ctl_Ave,var_ctl_ave, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, Hval,Hval_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, Tval,Tval_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, Pval,Pval_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, field_walk,sig_walk, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, field_fdr,sig_fdr, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, fs_glob,field_sig, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
	put.var.ncdf(nc, fs_AMZ,field_sig_AMZ, start = c(1, 1, 1),  count = c(nlon, nlat, 4))

	#close file
	close.ncdf(nc)

	rm(exp_Ave)
	rm(ctl_Ave)
	rm(Hval)
	rm(Tval)
	