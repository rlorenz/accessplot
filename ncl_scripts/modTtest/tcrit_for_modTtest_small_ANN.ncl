;-----------------------------------------------------------------------------;
; File Name : tcrit_for_modTtest_small.ncl
; Creation Date : 22-01-2014
; Last Modified : Fri 12 Feb 2016 00:09:31 AEDT
; Created By : Ruth Lorenz
; Purpose : evaluate tcrit for modified t-test for small samples based on
;	Zwiers and von Storch, 1995, J. Clim 

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/modTtest/create_LookupTabs.ncl"

undef("lookupTcrit_ANN")

function lookupTcrit_ANN(r1:float,siglev:numeric,nobs:numeric,nlat:numeric,\
	 mlon:numeric)

local a, b, c, tindx, alpha, stat_val, tab

begin

a=fspan(-0.35,0.95,27)
b=a-0.025
c=a+0.025
alpha=new((/27,3/),float)
alpha(:,0)=a
alpha(:,1)=b
alpha(:,2)=c

stat_val=lookupTabs(nobs)

if (siglev .eq. 0.20)
  tab=stat_val(:,0);
else if (siglev.eq.0.10)
  tab=stat_val(:,1);
else if (siglev.eq.0.05)
  tab=stat_val(:,2);
else if (siglev.eq.0.02)
  tab=stat_val(:,3);
else if (siglev.eq.0.01)
  tab=stat_val(:,4);
end if
end if
end if
end if
end if

tcrit=new((/nlat,mlon/),float)

   do lat=0,nlat-1
      do lon=0,mlon-1
      r=r1(lat,lon)
;      print(max(ind(alpha(:,1).lt.r)))
;      print(min(ind(alpha(:,2).ge.r)))
        if ( ismissing(min(ind(alpha(:,2).ge.r)) )) then
	   tcrit(lat,lon)=tcrit@_FillValue
	else if (ismissing(max(ind(alpha(:,1).lt.r)) )) then
	   tcrit(lat,lon)=tcrit@_FillValue
	else if ( (min(ind(alpha(:,2).ge.r))) .eq. (max(ind(alpha(:,1).lt.r))) ) then
   	   tindx=min( ind(alpha(:,2).ge.r) )
   	   tcrit(lat,lon)=tab(tindx)
	end if
	end if
	end if

       end do
   end do

return(tcrit)

end
