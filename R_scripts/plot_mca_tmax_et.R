#R scirpt for maximum covariance analysis of Tmax and ET
# Load libraries
library(ACWD)
library(pcaXcca)
library(ncdf)


file_dir <- "/srv/ccrc/data23/z3441306/ACCESS_plots_workdir/"
out_dir <- "/home/z3441306/scripts/plot_scripts/ACCESS_plots/uakpe/scatter_figures/"

#read data from netcdfs
file_mod=paste(file_dir,"uakpe.monmean_tmax_tmin_hfls.1984-2007_rgd.nc",sep="")
print(file_mod)
ex.nc = open.ncdf(file_mod)
print(ex.nc)
tmax_mod = get.var.ncdf( ex.nc, "tasmax")
lh_mod = get.var.ncdf( ex.nc, "hfls")
x = get.var.ncdf( ex.nc, "lon")          # coordinate variable
y = get.var.ncdf( ex.nc, "lat")          # coordinate variable
t = get.var.ncdf( ex.nc, "time")          # coordinate variable
close.ncdf(ex.nc)

file_obsT=paste(file_dir,"HadGHCND_TX_monmean_1984-2007.nc",sep="")
print(file_obsT)
ex.nc = open.ncdf(file_obsT)
print(ex.nc)
tmax_obs = get.var.ncdf( ex.nc, "TX")
tmax_obs[which(tmax_obs<=-999)] <- NA
close.ncdf(ex.nc)

file_obsET=paste(file_dir,"GLEAM.New.monmean_global_EVAP_1984-2007_rgd.nc",sep="")
print(file_obsET)
ex.nc = open.ncdf(file_obsET)
print(ex.nc)
et_obs = get.var.ncdf( ex.nc, "EVAP")
et_obs[which(et_obs>=9.96921e+36)] <- NA
et_years = get.var.ncdf( ex.nc, "years")
et_month = get.var.ncdf( ex.nc, "month")
close.ncdf(ex.nc)

#put into 3D array and  calculate et in Wm-2
dims=dim(et_obs)
lh_obs <-array(NA,dim=c(dims[1],dims[2],dims[3]*dims[4]))
t=0
        for (yr in 1:24){
                for (mt in 1:12){
                        lh_obs[,,t]=et_obs[,,mt,yr]*2.5*10^6/(24*60*60)
                t=t+1
                }
        }

#mask lh data where no temp obs
lh_obs[which(is.finite(tmax_obs)==FALSE)] <- NA

#mask model data where no temp obs
tmax_mod[which(is.finite(tmax_obs)==FALSE)] <- NA
lh_mod[which(is.finite(lh_obs)==FALSE)] <- NA

#calculate biases
tmax_bias=tmax_mod-tmax_obs
lh_bias=lh_mod-lh_obs

#assign attributes to tmax_bias and lh_bias
        start.y = 2001
        end.y = 2007
        time.ind<- c(1:((end.y-start.y+1)*12))
        time.m <- rep(seq(1,12),(end.y-start.y+1))
        time.y <- c()
        ind =1
        for (i in start.y:end.y){
                time.y[ind:(ind+11)] <- rep(i,12)
                ind = ind+12
        }
        time <- data.frame(time.y,time.m,time.ind)



        for (j in 1:4){ #loop over seasons
                if (j==1){seas <- "DJF"
                        ind.seas <- time[which((time[,2]==12)|(time[,2]==1)|(time[,2]==2)),3]
                }
                if (j==2){seas <- "MAM"
                        ind.seas <- time[which((time[,2]==3)|(time[,2]==4)|(time[,2]==5)),3]
                }
                if (j==3){seas <- "JJA"
                        ind.seas <- time[which((time[,2]==6)|(time[,2]==7)|(time[,2]==8)),3]
                }
                if (j==4){seas <- "SON"
                        ind.seas <- time[which((time[,2]==9)|(time[,2]==10)|(time[,2]==11)),3]
                }

                tmax_bias_seas = tmax_bias[,,ind.seas]
                lh_bias_seas = lh_bias[,,ind.seas]

		tmax.mat <- field2dmat(tmax_bias_seas,year="all",months="all",ylim="all",xlim="all")
		lh.mat <- field2dmat(lh_bias_seas,year="all",months="all",ylim="all",xlim="all")
	
		# Calculate the MCA  
		dat.mca.seas <- mca(left=tmax.mat,right=lh.mat)

	} #end j

#save data to netcdf
# NetCDF output from dat.mca
dim1 = dim.def.ncdf( "LON","longitude", as.double(x))
dim2 = dim.def.ncdf( "LAT","latitude", as.double(y))
# define the EMPTY (dat.mca) netcdf variable
varz = var.def.ncdf("mca","-", list(dim1,dim2), -1,
          longname="Maximum covariance analysis")
# associate the netcdf variable with a netcdf file   
filo = paste(out_dir,"data_maxcovvar_tmax_lh.nc",sep="")
nc.ex = create.ncdf( filo, varz )
put.var.ncdf(nc.ex, varz, dat.mca)
close.ncdf(nc.ex)

