;-----------------------------------------------------------------------------;
; File Name : plot_vert_prof_diffs_ANN.ncl
; Creation Date : 24-08-2015
; Last Modified : 
; Created By : Ruth Lorenz
; Purpose : plot moisture convergence in LUC experiemnts
; 	   in LCC regions for annual means 

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
runnames = str_split(systemfunc("echo $EXPERIMENTS")," ")
print(runnames)
region = systemfunc("echo $REGION")

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")
outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;start loop runs
do run = 1,7
      file1 = addfile(workDir +runnames(0)+ ".yearly_means." \
                       + R1Start + "_" + R1End + "_ensmean.nc","r")
      file2 = addfile(workDir +runnames(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + "_ensmean.nc","r")

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;
	if ( run .eq. 1 ) then
	    nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

	    ;derive time axis
	    time_ctl = file1->time
	    time_ctl@calendar = "gregorian"
	    ctl_date = ut_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
	    lat = file1->lat
	    lon = file1->lon

	    time_exp = file2->time
	    time_exp@calendar = "gregorian"
	    exp_date = ut_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

	    ;cut into analysis period
	    ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
	    new_date_ctl=ctl_date(ind_yrs_ctl,:)

	    ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
	    new_date_exp=exp_date(ind_yrs_exp,:)
	end if

	ua_ctl = file1 ->ua(ind_yrs_ctl,:,:,:)	;eastward wind on plev in ms-1
    	ua_exp = file2 ->ua(ind_yrs_exp,:,:,:)

	va_ctl = file1 ->va(ind_yrs_ctl,:,:,:)	;northward wind on plev in ms-1
    	va_exp = file2 ->va(ind_yrs_exp,:,:,:)

	lat_uv = file1 ->lat_1
	lon_uv = file1 ->lon_1

	sphum_ctl = file1 ->hus(ind_yrs_ctl,:,:,:)	;specififc humidity on plev
	sphum_exp = file2 ->hus(ind_yrs_exp,:,:,:)

	t_ctl = file1 ->ta(ind_yrs_ctl,:,:,:)	;temperature on 17 plev in K
	t_exp = file2 ->ta(ind_yrs_exp,:,:,:)	;temperature on 17 plev in K

	psfc_ctl = file1 ->ps(ind_yrs_ctl,:,:)	; surface pressure in Pa
	psfc_exp = file2 ->ps(ind_yrs_exp,:,:)	; surface pressure in Pa
	lat_p = file1 ->lat
	lon_p = file1 ->lon

	plev = file1 ->z12_p_level	;pressure on 17 levels in Pa =  kg/(m s2)

	;check if ctl and exp time periods of same length
	dsize_ctl=dimsizes(ua_ctl)
	dsize_exp=dimsizes(ua_exp)
	ntim_ctl=dsize_ctl(0)
	nlev_ctl=dsize_ctl(1)
	nlat_ctl=dsize_ctl(2)
	nlon_ctl=dsize_ctl(3)
	ntim_exp=dsize_exp(0)
	nlev_exp=dsize_exp(1)
	nlat_exp=dsize_exp(2)
	nlon_exp=dsize_exp(3)

	if ( ntim_ctl.ne.ntim_exp .or. nlev_ctl.ne.nlev_exp .or. \
	    nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
	        print("ERROR: CTL and EXP do not have same dimension, exiting")
		        exit
	else
		ntim=ntim_ctl
		nlev=nlev_ctl
        	nlat=nlat_ctl
        	nlon=nlon_ctl

        	delete(ntim_ctl)
		delete(nlev_ctl)
        	delete(nlat_ctl)
        	delete(nlon_ctl)
        	delete(ntim_exp)
		delete(nlev_exp)
        	delete(nlat_exp)
        	delete(nlon_exp)
	end if

;-----------------------------------------------------------------------------;
; Calculating Moisture Flux Transport -> advection of q across N,E,S,W boundaries
;    adv = -( u*(dq/dx)+v*(dq/dy) )    ; advection term
;    MFT = -1/g[advect*dS]*dP	       ;dS=path length in horizontal
;     	   			       ;dP=path length in vertical
;-----------------------------------------------------------------------------;

; Calculate advection
  	q_grad_lon_ctl = sphum_ctl
	q_grad_lat_ctl = sphum_ctl
	q_ctl = sphum_ctl
  	q_grad_lon_exp = sphum_exp
	q_grad_lat_exp = sphum_exp
	q_exp = sphum_exp

	gradsf(sphum_ctl,q_grad_lon_ctl,q_grad_lat_ctl)
	gradsf(sphum_exp,q_grad_lon_exp,q_grad_lat_exp)

  	adv_ctl = ua_ctl
	adv_ctl = ua_ctl*q_grad_lon_ctl + va_ctl*q_grad_lat_ctl
	
  	adv_exp = ua_exp
	adv_exp = ua_exp*q_grad_lon_exp + va_exp*q_grad_lat_exp
	

; Calculate vertically integrated moisture flux transport
  	ptop = min(plev)
	dp_ctl   = dpres_plevel_Wrap(plev, psfc_ctl, ptop, 0)
	dp_exp   = dpres_plevel_Wrap(plev, psfc_exp, ptop, 0)
	;printVarSummary(plev)
	;printVarSummary(dp_ctl)

	; regrid adv to "normal" grid (192x145)
	adv_ctl_reg = linint2_Wrap(lon_uv,lat_uv,adv_ctl,False,lon_p,lat_p,0)
	adv_exp_reg = linint2_Wrap(lon_uv,lat_uv,adv_exp,False,lon_p,lat_p,0)

	advdp_ctl = adv_ctl_reg*dp_ctl
	advdp_exp = adv_exp_reg*dp_exp

	g = 9.81                ; gravity      [ m/s2 ]
  	adv_int_ctl = 1/g*dim_sum_n_Wrap(advdp_ctl,1)	;units [kg/(m^2 s)]=mm s-1
	adv_int_exp = 1/g*dim_sum_n_Wrap(advdp_exp,1)

	delete(advdp_ctl)
	delete(advdp_exp)

; average over time
	adv_int_exp_avg=dim_avg_n(adv_int_exp,0)
	adv_int_ctl_avg=dim_avg_n(adv_int_ctl,0)

	adv_int_exp_avg!0="lat"
	adv_int_exp_avg!1="lon"
	adv_int_exp_avg&lon = lon_p
	adv_int_exp_avg&lat = lat_p

	adv_int_ctl_avg!0="lat"
	adv_int_ctl_avg!1="lon"
	adv_int_ctl_avg&lon = lon_p
	adv_int_ctl_avg&lat = lat_p
	printVarSummary(adv_int_exp_avg)
	printVarSummary(adv_int_ctl_avg)

;-----------------------------------------------------------------------------;
; Calculate flux over boundaries
;-----------------------------------------------------------------------------;
	if (region .eq. "wc") then
		    lon1_dom=281.25
		    lon2_dom=300.0
		    lat1_dom=3.75
		    lat2_dom=-8.75
	else if (region .eq. "sc") then
		    lon1_dom=301.875
		    lon2_dom=320.625
		    lat1_dom=-1.25
		    lat2_dom=-13.75
	end if
	end if

	if (runnames(run) .eq. "allAMZ") then
           lonL_dom=278.0
           lonR_dom=325.0
           latN_dom=15.625
           latS_dom=-18.125	   
	end if
	
	;find indices
	ind_lon_dom=ind(lon.ge.lon1_dom .and. lon.le.lon2_dom)
	ind_lat_dom=ind(lat.le.lat1_dom .and. lat.ge.lat2_dom)
    	lon_dom = lon(ind_lon_dom)
    	lat_dom = lat(ind_lat_dom)

	if  (runnames(run) .eq. "001GPwc" .or. runnames(run) .eq. "001GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(5)
              lat_gd_ch_dom=lat_dom(5)
	      ind_lon_ch_dom=ind(lon.eq.lon_gd_ch_dom)
              ind_lat_ch_dom=ind(lat.eq.lat_gd_ch_dom)
	else if (runnames(run) .eq. "009GPwc" .or. runnames(run) .eq. "009GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(4:6)
              lat_gd_ch_dom=lat_dom(4:6)
	else if (runnames(run) .eq. "025GPwc" .or. runnames(run) .eq. "025GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(3:7)
              lat_gd_ch_dom=lat_dom(3:7)
	else if (runnames(run) .eq. "081GPwc" .or. runnames(run) .eq. "081GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(1:9)
              lat_gd_ch_dom=lat_dom(1:9)
	else if (runnames(run) .eq. "121GPwc" .or. runnames(run) .eq. "121GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(0:10)
              lat_gd_ch_dom=lat_dom(0:10)
	else if (runnames(run) .eq. "242GP") then

	      lonL_dom=280.3125
	      lonR_dom=321.5625
	      latN_dom=4.375
	      latS_dom=-14.375

	end if
	end if
	end if
	end if
	end if
	end if

	if (runnames(run) .eq. "242GP" .or. runnames(run) .eq. "allAMZ") then
	   ; North
	   adv_int_exp_nb = dim_sum_n_Wrap(adv_int_exp_avg({latN_dom},{lonL_dom:lonR_dom}),0)
	   adv_int_ctl_nb = dim_sum_n_Wrap(adv_int_ctl_avg({latN_dom},{lonL_dom:lonR_dom}),0)
	   ; East
	   adv_int_exp_eb = dim_sum_n_Wrap(adv_int_exp_avg({latS_dom:latN_dom},{lonR_dom}),0)
	   adv_int_ctl_eb = dim_sum_n_Wrap(adv_int_ctl_avg({latS_dom:latN_dom},{lonR_dom}),0)
	   ; South
	   adv_int_exp_sb = dim_sum_n_Wrap(adv_int_exp_avg({latS_dom},{lonL_dom:lonR_dom}),0)
	   adv_int_ctl_sb = dim_sum_n_Wrap(adv_int_ctl_avg({latS_dom},{lonL_dom:lonR_dom}),0)
	   ; West
	   adv_int_exp_wb = dim_sum_n_Wrap(adv_int_exp_avg({latS_dom:latN_dom},{lonL_dom}),0)
	   adv_int_ctl_wb = dim_sum_n_Wrap(adv_int_ctl_avg({latS_dom:latN_dom},{lonL_dom}),0)
	else
	   ; North
	   adv_int_exp_nb = dim_sum_n_Wrap(adv_int_exp_avg({max(lat_gd_ch_dom)},{min(lon_gd_ch_dom):max(lon_gd_ch_dom)}),0)
	   adv_int_ctl_nb = dim_sum_n_Wrap(adv_int_ctl_avg({max(lat_gd_ch_dom)},{min(lon_gd_ch_dom):max(lon_gd_ch_dom)}),0)
	   ; East
	   adv_int_exp_eb = dim_sum_n_Wrap(adv_int_exp_avg({min(lat_gd_ch_dom):max(lat_gd_ch_dom)},{max(lon_gd_ch_dom)}),0)
	   adv_int_ctl_eb = dim_sum_n_Wrap(adv_int_ctl_avg({min(lat_gd_ch_dom):max(lat_gd_ch_dom)},{max(lon_gd_ch_dom)}),0)
	   ; South
	   adv_int_exp_sb = dim_sum_n_Wrap(adv_int_exp_avg({min(lat_gd_ch_dom)},{min(lon_gd_ch_dom):max(lon_gd_ch_dom)}),0)
	   adv_int_ctl_sb = dim_sum_n_Wrap(adv_int_ctl_avg({min(lat_gd_ch_dom)},{min(lon_gd_ch_dom):max(lon_gd_ch_dom)}),0)
	   ; West
	   adv_int_exp_wb = dim_sum_n_Wrap(adv_int_exp_avg({min(lat_gd_ch_dom):max(lat_gd_ch_dom)},{min(lon_gd_ch_dom)}),0)
	   adv_int_ctl_wb = dim_sum_n_Wrap(adv_int_ctl_avg({min(lat_gd_ch_dom):max(lat_gd_ch_dom)},{min(lon_gd_ch_dom)}),0)
	end if
	     
;-----------------------------------------------------------------------------;
; Calculating differences
;-----------------------------------------------------------------------------;

; calculate differences and attach Coordinates
	nb_diff= (adv_int_exp_nb-adv_int_ctl_nb)
	eb_diff= (adv_int_exp_eb-adv_int_ctl_eb)
	sb_diff= (adv_int_exp_sb-adv_int_ctl_sb)
	wb_diff= (adv_int_exp_wb-adv_int_ctl_wb)

	nb_diff@long_name= "MFT North "+runnames(run)+"-"+Runname1
	nb_diff@units    = "kg s~S~-1~N~"
	printMinMax(nb_diff,True)

	eb_diff@long_name= "MFT East "+runnames(run)+"-"+Runname1
	eb_diff@units    = "kg s~S~-1~N~"

	sb_diff@long_name= "MFS East "+runnames(run)+"-"+Runname1
	sb_diff@units    = "kg s~S~-1~N~"

	wb_diff@long_name= "MFT West "+runnames(run)+"-"+Runname1
	wb_diff@units    = "kg s~S~-1~N~"

	; put data into one array
	data = new((/4/),float)
	data(0)=nb_diff
	data(1)=eb_diff
	data(2)=sb_diff
	data(3)=wb_diff

;-----------------------------------------------------------------------------;
; plot data
;-----------------------------------------------------------------------------;
        pltName = outDir +runnames(run)+ "/horiz_moist_flux_"+runnames(run)+"-CTL_ANN"
        wks = gsn_open_wks(plttype,pltName)

	res          = True                   ; plot mods desired

	res@gsnXYBarChart = True            ; Create bar plot
     	res@gsnXYBarChartBarWidth = 0.2           ; change bar widths
	res@gsnYRefLine           = 0.              ; reference line
	res@gsnXYBarChartColors = (/"black"/)

     	res@tmXBMode          = "Explicit"         ; explicit labels
     	res@tmXBValues        = (/0,1,2,3/)
     	res@tmXBLabels = (/"North","East","South","West"/)

	res@trYMinF = -7e-5
	res@trYMaxF = 7e-5

	res@tiXAxisString = ""
	res@tiYAxisString = "Difference in Moisture Flux ["+nb_diff@units+"]"

 	plot = gsn_csm_xy (wks,ispan(0,3,1),data,res)

;-----------------------------------------------------------------------------;
if (runnames(run) .ne. "242GP" .and. runnames(run) .ne. "allAMZ") then
   delete(lon_gd_ch_dom)
   delete(lat_gd_ch_dom)
end if
end do ;runs

;-----------------------------------------------------------------------------;
end
