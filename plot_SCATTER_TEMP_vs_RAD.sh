#!/bin/bash
# File Name : plot_PDFs_TX_TN_TMN.sh
# Creation Date : 20/08/2013
# Last Modified : Wed 21 May 2014 15:59:15 EST
# Created By : Ruth Lorenz
# Purpose : plot scatter of Tmax, Tmin and Tmean versus SW, LW and Rnet

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=uakpe
export RUNNAME=ACCESS
export DATAT=HadGHCND
export DATAR=CERES

export Y_START=2001
export Y_STOP=2009

export NLAT=192
export NLON=145

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/ACCESS_output/${RUNID}/timeseries/
export OBSDIR=$DATAPATH/DATASETS/
export WORKDIR=$DATAPATH/ACCESS_plots_workdir/
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID/scatter_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.0
module load netcdf
module load nco
module load cdo
module load R/2.15.3

# -----------------------------------------------------------------------
# cut time to common timeperiod 2001-2009
# -----------------------------------------------------------------------
#cp $MODDIR/${RUNID}.monthly_TS.1951_2011.nc.gz $WORKDIR
#gunzip $WORKDIR/${RUNID}.monthly_TS.1951_2011.nc.gz

#cp $MODDIR/${RUNID}.monmean_tmax_tmin.1951_2011.nc.gz $WORKDIR
#gunzip $WORKDIR/${RUNID}.monmean_tmax_tmin.1951_2011.nc.gz

#ncks -O -d time,600,707 $WORKDIR/${RUNID}.monthly_TS.1951_2011.nc $WORKDIR/${RUNID}.monthly_TS.${Y_START}-${Y_STOP}.nc
#ncks -O -d time,600,707 $WORKDIR/${RUNID}.monmean_tmax_tmin.1951_2011.nc $WORKDIR/${RUNID}.monmean_tmax_tmin.${Y_START}-${Y_STOP}.nc
#ncks -O -d time,601,708 $OBSDIR/${DATAT}_Data/HadGHCND_TN_monmean_195012-201111.nc $WORKDIR/HadGHCND_TN_${Y_START}-${Y_STOP}.nc
#ncks -O -d time,601,708 $OBSDIR/${DATAT}_Data/HadGHCND_TX_monmean_195012-201111.nc $WORKDIR/HadGHCND_TX_${Y_START}-${Y_STOP}.nc
#ncks -O -d time,10,117 $OBSDIR/${DATAR}_Data/CERES_EBAF-Surface_Ed2.7_Subset_200003-201006.nc $WORKDIR/CERES_EBAF-Surface_Ed2.7_${Y_START}-${Y_STOP}.nc

#extract variables needed into one file for model
#	ncks -O -v rss,rls,rlds $WORKDIR/${RUNID}.monthly_TS.${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}.monthly_TS_tasmax_tasmin_rss_rls_rlds.${Y_START}-${Y_STOP}.nc #overwrite file if already exists
#	ncks -A -v tasmax,tasmin $WORKDIR/${RUNID}.monmean_tmax_tmin.${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}.monthly_TS_tasmax_tasmin_rss_rls_rlds.${Y_START}-${Y_STOP}.nc #append to file
#
# -----------------------------------------------------------------------
# regrid all data to coarsest resolution (coming from HadGHCND)
# -----------------------------------------------------------------------
#cdo remapbil,$WORKDIR/HadGHCND_TX_${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}.monthly_TS_tasmax_tasmin_rss_rls_rlds.${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}.monthly_TS_tasmax_tasmin_rss_rls_rlds.${Y_START}-${Y_STOP}_rgd.nc
#cdo remapbil,$WORKDIR/HadGHCND_TX_${Y_START}-${Y_STOP}.nc $WORKDIR/CERES_EBAF-Surface_Ed2.7_${Y_START}-${Y_STOP}.nc $WORKDIR/CERES_EBAF-Surface_Ed2.7_${Y_START}-${Y_STOP}_rgd.nc

#extract variables needed into one file for obs
#	ncks -O -v sfc_net_sw_all_mon,sfc_lw_down_all_mon,sfc_net_tot_all_mon $WORKDIR/CERES_EBAF-Surface_Ed2.7_${Y_START}-${Y_STOP}_rgd.nc $WORKDIR/obs_monthly_TS_TX_TN_sw_lwdown_rnet_${Y_START}-${Y_STOP}_rgd.nc
#	ncks -A -v TX $WORKDIR/HadGHCND_TX_${Y_START}-${Y_STOP}.nc $WORKDIR/obs_monthly_TS_TX_TN_sw_lwdown_rnet_${Y_START}-${Y_STOP}_rgd.nc
#	ncks -A -v TN $WORKDIR/HadGHCND_TN_${Y_START}-${Y_STOP}.nc $WORKDIR/obs_monthly_TS_TX_TN_sw_lwdown_rnet_${Y_START}-${Y_STOP}_rgd.nc

# -----------------------------------------------------------------------
# mask regions
# -----------------------------------------------------------------------

#for area in ASI AUS EUR NAM
#do

#	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $WORKDIR/${RUNID}.monthly_TS_tasmax_tasmin_rss_rls_rlds.${Y_START}-${Y_STOP}_rgd.nc $WORKDIR/${area}_${RUNID}_monthly_temp_rad_${Y_START}-${Y_STOP}.nc
#	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $WORKDIR/obs_monthly_TS_TX_TN_sw_lwdown_rnet_${Y_START}-${Y_STOP}_rgd.nc $WORKDIR/${area}_obs_monthly_temp_rad_${Y_START}-${Y_STOP}.nc
#done

# -----------------------------------------------------------------------
# call ncl script
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_scatter_tmax_tmin_vs_rad.ncl 
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_corr_tmax_tmin_vs_rad.ncl

#echo"Call R script"
#R --vanilla < $HOME/scripts/plot_scripts/R_scripts/plot_density_scatter_tmax_tmin_vs_rad.R
#R --vanilla < $HOME/scripts/plot_scripts/R_scripts/plot_pdf_skillscore_tmax_tmin.R

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
