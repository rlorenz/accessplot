# Compare CTL and LUC experiments ensembles
# test significance with simple t-test
#install.packages("abind",lib="/home/z3441306/scripts/plot_scripts/R_scripts/libraries_zip/abind_1.4-0.tar.gz", repos = NULL, type="source")

library(ncdf)
#library(abind,lib.loc="/home/z3441306/R_libs/")

source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/walkers_test_package.R")
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/false_discovery_rate_package.R")

#set-up
file_dir <- Sys.getenv("WORKDIR")
nens <- Sys.getenv("NENS")

if (nens == 3){
   runid_ctl<-list("uaoyr","uaoys","uaoyt")
} else if (nens == 5){
  runid_ctl<-list("uaoyr","uaoys","uaoyt","vajoa","vajob")
} else {
  print("Wrong number of ensembles")
}
ctl<-"CTL"

region<-Sys.getenv("REGION")

out_dir<-Sys.getenv("OUTDIR")
print(out_dir)

siglev<-as.numeric(Sys.getenv("siglev")) #significance level for t-test

R1.Start<-Sys.getenv("R1_START")
R1.End<-Sys.getenv("R1_STOP")

R2.Start<-Sys.getenv("R2_START")
R2.End<-Sys.getenv("R2_STOP")

A.Start<-as.numeric(Sys.getenv("A_START"))
A.End<-as.numeric(Sys.getenv("A_STOP"))

time.ind<- c(1:((A.End-A.Start+1)*12))
time.m <- rep(seq(1,12),(A.End-A.Start+1))
time.y <- c()
ind =1
for (year in A.Start:A.End){
        time.y[ind:(ind+11)] <- rep(year,12)
        ind = ind+12
}
time <- data.frame(time.y,time.m,time.ind)

#read data from netcdfs
print("read data")
file_lsm=paste("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc",sep="")
        print(file_lsm)
        ex.nc = open.ncdf(file_lsm)
        print(ex.nc)
        lsm=get.var.ncdf( ex.nc, "lsm") #lon,lat,surface,time
        lon=get.var.ncdf( ex.nc, "longitude")
        lat=get.var.ncdf( ex.nc, "latitude")

dims=dim(lsm)
nlon=dims[1]
nlat=dims[2]

nyears=A.End-A.Start+1

runs<-list("121GP","001GP","009GP","025GP","081GP","242GP","allAMZ")
nruns<-length(runs)

#loop over runs
for (run in 1:nruns){
    if (run < 6){
       runname<-paste(runs[run],region,sep="")
    } else {
      runname<-paste(runs[run],sep="")
    }  
    if (nens == 3){
       if (runname=="121GPsc"){
       	  runid<-list("uaoya","uaoyw","uaoyx")
       } else if (runname=="121GPwc"){
       	 runid<-list("uaoyb","uaoyy","uaoyz")
       } else if (runname=="001GPsc"){
       	 runid<-list("uaoyc","vacda","vacdb")
       } else if (runname=="001GPwc"){
       	 runid<-list("uaoyd","vacdc","vacdd")
       } else if (runname=="009GPsc"){
       	 runid<-list("uaoyi","vacde","vacdf")
       } else if (runname=="009GPwc"){
       	 runid<-list("uaoyj","vacdg","vacdh")
       } else if (runname=="025GPsc"){
       	 runid<-list("uaoyk","vacdi","vacdj")
       } else if (runname=="025GPwc"){
       	 runid<-list("uaoyl","vacdk","vacdl")
       } else if (runname=="081GPsc"){
       	 runid<-list("uaoyo","vacdm","vacdn")
       } else if (runname=="081GPwc"){
       	 runid<-list("uaoyp","vacdo","vacdp")
       } else if (runname=="242GP"){
       	 runid<-list("vacdq","vacdr","vacds")
       } else if (runname=="allAMZ"){
       	 runid<-list("vacdt","vacdu","vacdv")
       }

    }  else if (nens == 5){

       if (runname=="121GPsc"){
       	  runid<-list("uaoya","uaoyw","uaoyx","vajou","vajov")
       } else if (runname=="121GPwc"){
       	 runid<-list("uaoyb","uaoyy","uaoyz","vajos","vajot")
       } else if (runname=="001GPsc"){
       	 runid<-list("uaoyc","vacda","vacdb","vajoe","vajof")
       } else if (runname=="001GPwc"){
       	 runid<-list("uaoyd","vacdc","vacdd","vajoc","vajod")
       } else if (runname=="009GPsc"){
       	 runid<-list("uaoyi","vacde","vacdf","vajoi","vajoj")
       } else if (runname=="009GPwc"){
       	 runid<-list("uaoyj","vacdg","vacdh","vajog","vajoh")
       } else if (runname=="025GPsc"){
       	 runid<-list("uaoyk","vacdi","vacdj","vajom","vajon")
       } else if (runname=="025GPwc"){
       	 runid<-list("uaoyl","vacdk","vacdl","vajok","vajol")
       } else if (runname=="081GPsc"){
       	 runid<-list("uaoyo","vacdm","vacdn","vajoq","vajor")
       } else if (runname=="081GPwc"){
       	 runid<-list("uaoyp","vacdo","vacdp","vajoo","vajop")
       } else if (runname=="242GP"){
       	 runid<-list("vacdq","vacdr","vacds","vacdy","vacdz")
       } else if (runname=="allAMZ"){
       	 runid<-list("vacdt","vacdu","vacdv","vacdw","vacdx")
       }
   }
   nens<-length(runid) #nr of ensembles

    #check if output file exists, if not create it
    dir.create(file.path(out_dir,runname), showWarnings = FALSE)

   #loop over variables
   for (v in 1:2){
       if ( v==1 ){
       	  variable<-"tas"
       	  }
	  else if (v==2){
       	       variable<-"pr"
       	  }

	  #loop over all ensembles and read all data
	  var_ctl <- array(NA,dim=c(nlon,nlat,nyears*12,nens))
	  var_exp <- array(NA,dim=c(nlon,nlat,nyears*12,nens))
	  
	  for (ens in 1:nens){
 	      file_exp=paste(file_dir,runid[ens],".monthly_TS.",R2.Start,"_",R2.End,".nc",sep="")
              print(file_exp)
              ex.nc = open.ncdf(file_exp)

              time_exp_units<-att.get.ncdf( ex.nc,"time","units")
              base_date_exp=as.character(lapply(strsplit(as.character(time_exp_units$value),split=" "),"[",3))
              time_exp=get.var.ncdf( ex.nc,"time")
              time_exp_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_exp))
              time_exp.y<-format(time_exp_d, "%Y")
              time_exp.m<-format(time_exp_d, "%m")

              t1=min(which(as.numeric(time_exp.y)>=A.Start))
	      t2=(A.End-A.Start+1)*12

              var_exp[,,,ens]=get.var.ncdf( ex.nc, variable, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time
	      unit_var<- att.get.ncdf(ex.nc, variable, "units")

	      file_ctl=paste(file_dir,runid_ctl[ens],".monthly_TS.",R1.Start,"_",R1.End,".nc",sep="")
              print(file_ctl)
              ex.nc = open.ncdf(file_ctl)

              time_ctl_units<-att.get.ncdf( ex.nc,"time","units")
              base_date_ctl=as.character(lapply(strsplit(as.character(time_ctl_units$value),split=" "),"[",3))
              time_ctl=get.var.ncdf( ex.nc,"time")
              time_ctl_d <- as.Date(time_exp, format = "%j", origin = as.Date(base_date_ctl))
              time_ctl.y<-format(time_ctl_d, "%Y")
              time_ctl.m<-format(time_ctl_d, "%m")

              t1=min(which(as.numeric(time_ctl.y)>=A.Start))
              t2=(A.End-A.Start+1)*12
              var_ctl[,,,ens]=get.var.ncdf( ex.nc, variable, c(1,1,t1), c(nlon, nlat,t2)) #lon,lat,time

	      } #end loop ensembles

	      dims=dim(var_exp)
	      ntim=dims[3]

	      #average over ensembles
	      var_exp_ens<-apply(var_exp[,,,],c(1:3),mean,na.rm=T)
	      var_ctl_ens<-apply(var_ctl[,,,],c(1:3),mean,na.rm=T)

	      #loop over seasons,
	      var_exp_seas<-array(NA,dim=c(nlon,nlat,nyears,4))
	      var_ctl_seas<-array(NA,dim=c(nlon,nlat,nyears,4))
	      anom_seas<-array(NA,dim=c(nlon,nlat,nyears,4))
	      anom_ave<-array(NA,dim=c(nlon,nlat,4))

 	      var_exp_ens_re<-array(NA,dim=c(nlon,nlat,4,nyears))
 	      var_ctl_ens_re<-array(NA,dim=c(nlon,nlat,4,nyears))

 	      var_exp_ave<-array(NA,dim=c(nlon,nlat,4))
 	      var_ctl_ave<-array(NA,dim=c(nlon,nlat,4))
	      Tval_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
	      Pval_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
	      Hval_exp_ctl<-array(NA,dim=c(nlon,nlat,4))

	      Tval_wilk_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
	      Pval_wilk_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
	      Hval_wilk_exp_ctl<-array(NA,dim=c(nlon,nlat,4))

	      Tval_ks_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
	      Pval_ks_exp_ctl<-array(NA,dim=c(nlon,nlat,4))
	      Hval_ks_exp_ctl<-array(NA,dim=c(nlon,nlat,4))

	      for (j in 1:4){
              print(j)
              #loop over years
              yr=1
		    for (year in A.Start:A.End){
		    if (j==1){seas <- "DJF"
		       if (yr==1){
			  ind.seas <- time[which((time[,1]==year)&((time[,2]==1)|(time[,2]==2))),3]
		       }
		       else{
		       ind.seas <- time[which((time[,1]==(year-1)&(time[,2]==12))|(time[,1]==year&((time[,2]==1)|(time[,2]==2)))),3]
		       }
		    }
		    if (j==2){seas <- "MAM"
		       ind.seas <- time[which((time[,1]==year)&((time[,2]==3)|(time[,2]==4)|(time[,2]==5))),3]
		    }
		    if (j==3){seas <- "JJA"
		       ind.seas <- time[which((time[,1]==year)&((time[,2]==6)|(time[,2]==7)|(time[,2]==8))),3]
		    }
		    if (j==4){seas <- "SON"
		       ind.seas <- time[which((time[,1]==year)&((time[,2]==9)|(time[,2]==10)|(time[,2]==11))),3]
		    }

		    #calculate seasonal means per year
		    var_exp_seas[,,yr,j]<-apply(var_exp_ens[,,ind.seas],c(1:2),mean,na.rm=TRUE)
		    var_ctl_seas[,,yr,j]<-apply(var_ctl_ens[,,ind.seas],c(1:2),mean,na.rm=TRUE)
		    anom_seas[,,yr,j]<-var_exp_seas[,,yr,j]-var_ctl_seas[,,yr,j]

		    #mask ocean gridpoints
		    tmp1<-anom_seas[,,yr,j]
		    tmp1[which(lsm==0)] <- NA
		    anom_seas[,,yr,j] <- tmp1

                    #mask very high and very low latitudes
                    ind_lat<-which(lat<=-60.00|lat>=80.00)
                    anom_seas[,ind_lat,yr,j] <- NA

		    yr=yr+1
		    } #end loop years

		    var_exp_ave[,,j]<-apply(var_exp_seas[,,,j],c(1:2),mean,na.rm=TRUE)
		    var_ctl_ave[,,j]<-apply(var_ctl_seas[,,,j],c(1:2),mean,na.rm=TRUE)
		    anom_ave[,,j]<-apply(anom_seas[,,,j],c(1:2),mean,na.rm=TRUE)

		for (y in 1:nlat){
		    for (x in 1:nlon){
			if (all(is.na(anom_seas[x,y,j,]))) {
			   Tval_exp_ctl[x,y,j]<-NA
			   Pval_exp_ctl[x,y,j]<-NA
			   Hval_exp_ctl[x,y,j]<-NA

			   Tval_wilk_exp_ctl[x,y,j]<-NA
			   Pval_wilk_exp_ctl[x,y,j]<-NA
			   Hval_wilk_exp_ctl[x,y,j]<-NA

			   Tval_ks_exp_ctl[x,y,j]<-NA
			   Pval_ks_exp_ctl[x,y,j]<-NA
			   Hval_ks_exp_ctl[x,y,j]<-NA
			   }
			else {

			     #apply  t-test
			     test<-t.test(anom_seas[x,y,,j],mu=0,alternative = c("two.sided"),conf.level=1.0-siglev)
			     Tval_exp_ctl[x,y,j]<-test$statistic
			     Pval_exp_ctl[x,y,j]<-test$p.value

			     if (Pval_exp_ctl[x,y,j]<siglev){
			     Hval_exp_ctl[x,y,j]<-1
			     } else {
			     Hval_exp_ctl[x,y,j]<-0
			     }

			     #apply  wilcoxon rank sum test
			     wilk<-wilcox.test(anom_seas[x,y,,j],mu=0,alternative = c("two.sided"),conf.level=1.0-siglev)
			     Tval_wilk_exp_ctl[x,y,j]<-wilk$statistic
			     Pval_wilk_exp_ctl[x,y,j]<-wilk$p.value

			     if (Pval_wilk_exp_ctl[x,y,j]<siglev){
			     Hval_wilk_exp_ctl[x,y,j]<-1
			     } else {
			     Hval_wilk_exp_ctl[x,y,j]<-0
			     }

			     #apply kolmogorov-smirnov-test
			     ks<-ks.test(anom_seas[x,y,,j],"pnorm",alternative = c("two.sided"))
			     Tval_ks_exp_ctl[x,y,j]<-ks$statistic
			     Pval_ks_exp_ctl[x,y,j]<-ks$p.value

			     if (Pval_ks_exp_ctl[x,y,j]<siglev){
			     Hval_ks_exp_ctl[x,y,j]<-1
			     } else {
			     Hval_ks_exp_ctl[x,y,j]<-0
			     }
			}
		    } #end loop x
		} #end loop y

		} #end loop seasons
		
           	###check field significance with Walker's test
            	#total number of local tests
            	K<-sum(!is.na(anom_seas[,,1,1]))
                print(K)
           	sig_walk<-walkerTest(Pval_exp_ctl,siglev,K)

        	###check field significance with False Discovery Rate
	        sig_fdr<-fdrTest(Pval_exp_ctl,siglev,K)

    #save data as netcdf to plot
    outfile<-paste(out_dir,runname,"/",variable,"_",runname,"_",ctl,"_seas_Ttest_1samp_",nens,"ENS_",A.Start,"_",A.End,".nc",sep="")
    print(outfile)
    #define dimension
    dimLat <- dim.def.ncdf("Latitude", "degrees_north", lat)
    dimLon <- dim.def.ncdf("Longitude", "degrees_east", lon)
    dimT <- dim.def.ncdf("time","season",1:4,unlim = FALSE)

    #define variables
    FillValue<- -9999
    exp_Ave<-var.def.ncdf("exp_Ave", unit_var$value, list(dimLon,dimLat,dimT), FillValue,prec="double")
    ctl_Ave<-var.def.ncdf("ctl_Ave", unit_var$value, list(dimLon,dimLat,dimT), FillValue,prec="double")
    anom_Ave<-var.def.ncdf("anom_Ave", unit_var$value, list(dimLon,dimLat,dimT), FillValue,prec="double")
    Hval<-var.def.ncdf("Hval","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Tval<-var.def.ncdf("Tval","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Pval<-var.def.ncdf("Pval","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    field_walk<-var.def.ncdf("FS_walk","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")
    field_fdr<-var.def.ncdf("FS_fdr","-",list(dimLon,dimLat,dimT), FillValue) #,prec="integer")

    Hval_wilk<-var.def.ncdf("Hval_wilk","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Tval_wilk<-var.def.ncdf("Tval_wilk","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Pval_wilk<-var.def.ncdf("Pval_wilk","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Hval_ks<-var.def.ncdf("Hval_ks","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Tval_ks<-var.def.ncdf("Tval_ks","-",list(dimLon,dimLat,dimT), FillValue,prec="double")
    Pval_ks<-var.def.ncdf("Pval_ks","-",list(dimLon,dimLat,dimT), FillValue,prec="double")

    #create netcdf
    nc <- create.ncdf( outfile, list(exp_Ave,ctl_Ave,anom_Ave,Hval,Tval,Pval,field_walk,field_fdr,Hval_wilk,Tval_wilk,Pval_wilk,Hval_ks,Tval_ks,Pval_ks))

    #put variables into file
    put.var.ncdf(nc, exp_Ave,var_exp_ave, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, ctl_Ave,var_ctl_ave, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, anom_Ave,anom_ave, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Hval,Hval_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Tval,Tval_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Pval,Pval_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, field_walk,sig_walk$h.value, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, field_fdr,sig_fdr$h.value, start = c(1, 1, 1),  count = c(nlon, nlat, 4))

    put.var.ncdf(nc, Hval_wilk,Hval_wilk_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Tval_wilk,Tval_wilk_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Pval_wilk,Pval_wilk_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Hval_ks,Hval_ks_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Tval_ks,Tval_ks_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))
    put.var.ncdf(nc, Pval_ks,Pval_ks_exp_ctl, start = c(1, 1, 1),  count = c(nlon, nlat, 4))

    #close file
    close.ncdf(nc)

    rm(exp_Ave)
    rm(ctl_Ave)
    rm(anom_Ave)
    rm(Hval)
    rm(Tval)
    rm(Pval)
    rm(Hval_wilk)
    rm(Tval_wilk)
    rm(Pval_wilk)
    rm(Hval_ks)
    rm(Tval_ks)
    rm(Pval_ks)

    } #end loop over variables
} # end loop over runs
