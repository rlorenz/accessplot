#!/bin/bash
### File Name : plot_climdex_seas_EXP-CTL.sh
### Creation Date : September 2014
### Last Modified : Mon 13 Oct 2014 14:45:53 EST
### Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
### Purpose : script plotting climdex indices maps, trends, and timeseries
### climdex indices need to be calculated first using fortran code
### in /home/561/rzl561/scripts/fclimdex/fclimdex_nc/
### to compare two model runs, CTL and EXP

#PBS -N climdex_plot_seas
#PBS -P dt6
#PBS -j oe
#PBS -l ncpus=1
#PBS -l walltime=00:40:00
#PBS -l vmem=2600mb
#PBS -wd

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export MODEL=ACCESS

export R1_START=1959 #ctl run start
export R1_STOP=2011  #ctl run stop
export R2_START=1959 #exp run start
export R2_STOP=2011  #exp run stop
export A1_START=1960 #analysis start CTL
export A1_STOP=2011  #analysis stop CTL
export A2_START=1960 #analysis start EXP/SCEN
export A2_STOP=2011  #analysis stop EXP/SCEN

export RUNNAME1=LEUNING
export RUNNAME2=MEDLYN

export NLAT=145
export NLON=192

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data32/$USER/
fi

export EXPDIR=$DATAPATH/ACCESS-Leuning-Medlyn/MEDLYN/ENS_MEAN/climdex_index/
export CTLDIR=$DATAPATH/ACCESS-Leuning-Medlyn/LEUNING/ENS_MEAN/climdex_index/
export OUTDIR=$DATAPATH/ACCESS-Leuning-Medlyn/plots/climdex_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load gcc/4.7.2
module load ncl/6.2.0
module load netcdf
module load nco
module load cdo

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call climdex.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/climdex_EXP_CTL.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting climdex indices"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
