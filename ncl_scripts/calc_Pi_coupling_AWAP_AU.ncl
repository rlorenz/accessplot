;-----------------------------------------------------------------------------;
; File Name : calc_Pi_coupling_AWAP_AU.ncl
; Creation Date : 16-03-2015
; Last Modified : Mon 16 Mar 2015 14:43:49 AEDT
; Created By : Ruth Lorenz
; Purpose : calculate Pi soil moisture-temperature coupling (Miralles et al 2012 GRL)
;		from AWAP data over Australia

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

user = systemfunc("echo $USER")

dataDir = "/srv/ccrc/data11/z3381484/AWAP-ALL/month-means-CF-comply/"
outDir = "/srv/ccrc/data23/z3441306/DATASETS/AWAP_Data/Pi_coupling/"

Start = 1900
End = 2011
AStart = "2000"
AEnd = "2011"

;define plot type
plttype = "pdf"

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")
;max. temperature TempMax [degC]
file_tmax = addfile(dataDir + "month_mean_TempMax.nc","r")

;min. temperature TempMin [degC]
file_tmin = addfile(dataDir + "month_mean_TempMin.nc","r")

;radiation SolarMJ [MJ/m^2/d]
file_rad = addfile(dataDir + "month_mean_SolarMJ.nc","r")

;total Evaporation FWE [m/d]
file_ET = addfile(dataDir + "month_mean_FWE.nc","r")

;total Evaporation FWPT [m/d]
file_ETP = addfile(dataDir + "month_mean_FWPT.nc","r")

;derive time axis
time = file_tmax->time
time@calendar = "gregorian"
date = ut_calendar(time,0)

;cut into analysis period
ind_yrs=ind(date(:,0).ge.AStart .and. date(:,0).le.AEnd)
new_date=date(ind_yrs,:)

temp_max = file_tmax->TempMax(ind_yrs,:,:)        ;maximum temperature
printVarSummary(temp_max)
temp_min = file_tmin->TempMin(ind_yrs,:,:)        ;minimum temperature

rad_orig = file_rad->SolarMJ(ind_yrs,:,:)	  ;MJ m-2 day-1 -> Wm-2, 1W=1J/s
rad = (rad_orig*10^6)/(60*60*24)
copy_VarMeta(rad_orig,rad)

et_orig = file_ET->FWE(ind_yrs,:,:)		;m day-1 -> Wm-2
lh = et_orig*10^3*2.5*10^6/(24*60*60)		;ET[kg m**-2 s**-1]=LH/lambda=LE/(2.5*10^6[J kg**-1])
copy_VarMeta(et_orig,lh)			;[kg m**-2 s**-1]*24*60*60= mm/day

etp_orig = file_ETP->FWPT(ind_yrs,:,:)
lhp = etp_orig*10^3*2.5*10^6/(24*60*60)
copy_VarMeta(etp_orig,lhp)

;-----------------------------------------------------------------------------;
; calculate mean T, H and Hp
;-----------------------------------------------------------------------------;
temp = (temp_max+temp_min)/2+273.15	; calculate mean tempearture and convert to K
copy_VarMeta(temp_max,temp)
temp@units         = "K"

H = rad - lh
copy_VarMeta(rad,H)
H@long_name     = ""
H@units         = "Wm~S~-2~N~"

Hp = rad - lhp
copy_VarMeta(rad,Hp)
Hp@long_name     = ""
Hp@units         = "Wm~S~-2~N~"
;Hp!0 = "time"
;Hp!1 = "lat"
;Hp&lat = temp_max&lat
;Hp!2 = "lon"
;Hp&lon = temp_max&lon
;Hp@_FillValue=temp_max@_FillValue

;-----------------------------------------------------------------------------;
; detrend timeseries?
;-----------------------------------------------------------------------------;
temp_dtr=dtrend_leftdim(temp,False)
H_dtr=dtrend_leftdim(H,False)
Hp_dtr=dtrend_leftdim(Hp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
do j = 0,3
;season
	if (j .eq. 0) then
		seas="DJF"

	else if (j .eq. 1) then
		seas="MAM"

	else if (j .eq. 2) then
		seas="JJA"

	else if (j .eq. 3) then
		seas="SON"

	end if
	end if
	end if
	end if

	temp_seas=month_to_season(temp_dtr,seas)
	H_seas=month_to_season(H_dtr,seas)
	Hp_seas=month_to_season(Hp_dtr,seas)

        temp_seas!0 = "time"
        temp_seas!1 = "lat"
        temp_seas&lat = temp_max&lat
        temp_seas!2 = "lon"
        temp_seas&lon = temp_max&lon
        temp_seas@_FillValue=temp_max@_FillValue
        H_seas!0 = "time"
        H_seas!1 = "lat"
        H_seas&lat = temp_max&lat
        H_seas!2 = "lon"
        H_seas&lon = temp_max&lon
        H_seas@_FillValue=temp_max@_FillValue
        Hp_seas!0 = "time"
        Hp_seas!1 = "lat"
        Hp_seas&lat = temp_max&lat
        Hp_seas!2 = "lon"
        Hp_seas&lon = temp_max&lon
        Hp_seas@_FillValue=temp_max@_FillValue
;-----------------------------------------------------------------------------;
; calculate correlation between temp and H, Hp
;-----------------------------------------------------------------------------;

        if (j .eq. 0) then
                ccr_temp_H=new((/4,dimsizes(temp_seas(1,:,1)),dimsizes(temp_seas(1,1,:))/),float)
                ccr_temp_Hp=new((/4,dimsizes(temp_seas(1,:,1)),dimsizes(temp_seas(1,1,:))/),float)
		Pi=new((/4,dimsizes(temp_seas(1,:,1)),dimsizes(temp_seas(1,1,:))/),float)
        end if
        ccr_temp_H(j,:,:)=escorc(H_seas(lat|:,lon|:,time|:),temp_seas(lat|:,lon|:,time|:))
        ccr_temp_Hp(j,:,:)=escorc(Hp_seas(lat|:,lon|:,time|:),temp_seas(lat|:,lon|:,time|:))
	Pi(j,:,:)=ccr_temp_H(j,:,:)-ccr_temp_Hp(j,:,:)
end do ;season


Pi@long_name = "~F33~P~F21~ (AWAP) " +AStart+"-"+AEnd
Pi@units = "-"
Pi!0="time"
Pi!1="lat"
Pi!2="lon"
Pi&lon = temp_max&lon
Pi&lat = temp_max&lat

;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;

;Levels for contours
lev=(/0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4/)
colorbar = "omega_green_red"
Panel_Plot(outDir,"Pi_coupling",AStart,AEnd,plttype,colorbar,Pi,True,True,lev,True)

;-----------------------------------------------------------------------------;
; save data to netcdf for further use
;-----------------------------------------------------------------------------;
outfile = outDir + "AWAP_Pi_" +AStart+ "-" +AEnd+ ".nc"
system("rm "+outfile)
ncdf = addfile(outfile, "c")
ncdf->Pi=Pi

end
