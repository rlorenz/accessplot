#!/bin/bash
# File Name : ens_mean_LUC.sh
# Creation Date : 05/08/2015
# Last Modified : Thu 05 Nov 2015 22:25:48 AEDT
# Created By : Ruth Lorenz
# Purpose : calculate ensemble means over multiple ACCESS runs

###-------------------------------------------------------

###-------------------------------------------------------
module load netcdf
module load nco
module load cdo

experiment=$1
variables="pr,prc1,prc2,tas,rss,rls,hfls,hfss,alb,ps,hur,hus,hus_tl,pfull,ta,ta_tl,ts,tcld,ua,va,wap,zg,bldepth,blhf"
#for experiment in CTLLUC 001GPwc 009GPwc 025GPwc 081GPwc 121GPwc 242GP allAMZ
#do
for RUN in uaoya uaoyb uaoyc uaoyd uaoye uaoyf uaoyg uaoyh uaoyi uaoyj uaoyk uaoyl uaoym uaoyn uaoyo uaoyp uaoyr uaoys uaoyt uaoyw uaoyx uaoyy uaoyz vacda vacdb vacdc vacdd vacde vacdf vacdg vacdh vacdi vacdj vacdk vacdl vacdm vacdn vacdo vacdp vacdq vacdr vacds vacdt vacdu vacdv vajoa vajob vajoc vajod vajoe vajof vajog vajoh vajoi vajoj vajok vajol vajom vajon vajoo vajop vajoq vajor vajos vajot vajou vajov vacdw vacdx vacdy vacdz
do
	cdo yearavg $WORKDIR/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$RUN.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc
done

	if [[ $experiment == CTLLUC ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyr.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoys.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyt.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoa.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajob.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyr.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoys.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyt.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoa.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajob.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi

	if [[ $experiment == 001GPwc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyd.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdc.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdd.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoc.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajod.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A  $WORKDIR/uaoyd.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdc.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdd.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoc.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajod.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
	if [[ $experiment == 001GPsc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyc.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacda.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdb.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoe.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajof.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyc.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacda.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdb.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoe.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajof.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi

	if [[ $experiment == 009GPwc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyj.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdg.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdh.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajog.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoh.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	   ncea -A $WORKDIR/uaoyj.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdg.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdh.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajog.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoh.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
	if [[ $experiment == 009GPsc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyi.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacde.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdf.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoi.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoj.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyi.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacde.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdf.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoi.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoj.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi

	if [[ $experiment == 025GPwc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyl.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdk.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdl.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajok.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajol.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyl.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdk.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdl.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajok.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajol.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
	if [[ $experiment == 025GPsc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyk.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdj.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdj.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajom.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajon.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyk.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdj.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdj.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajom.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajon.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi

	if [[ $experiment == 081GPwc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyp.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdo.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdp.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoo.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajop.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyp.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdo.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdp.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoo.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajop.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
	if [[ $experiment == 081GPsc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyo.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdm.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdn.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoq.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajor.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyo.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdm.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdn.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajoq.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajor.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi

	if [[ $experiment == 121GPwc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoyb.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyy.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyz.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajos.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajot.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoyb.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyy.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyz.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajos.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajot.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
	if [[ $experiment == 121GPsc ]]; then
	    ncea -O -v $variables $WORKDIR/uaoya.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyw.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyx.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajou.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vajov.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/uaoya.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyw.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/uaoyx.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajou.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vajov.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi

	if [[ $experiment == 242GP ]]; then
	    ncea -O -v $variables $WORKDIR/vacdq.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdr.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacds.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdy.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdz.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/vacdq.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdr.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacds.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdy.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdz.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
	if [[ $experiment == allAMZ ]]; then
	    ncea -O -v $variables $WORKDIR/vacdt.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdu.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdv.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdw.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdx.yearly_means.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	    ncea -A $WORKDIR/vacdt.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdu.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdv.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdw.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/vacdx.yearly_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR/$experiment.yearly_means.${R2_START}_${R2_STOP}_ensmean.nc
	fi
#done 
