calc_poolSampVar <-
function(x,y,xAve,yAve){
# Calculate pooled sample Variance
# for modified t-test for large samples (ne>=30)
# Zwiers and von Storch, 1995, J.Clim, Eq. 13 (two sample case)

xnyrs<-length(x)
ynyrs<-length(y)
xsumsq<-0
ysumsq<-0
for (yr in 1:xnyrs){
    xsumsq<-xsumsq+((x[yr]-xAve)^2)
}
for (yr in 1:ynyrs){
    ysumsq<-ysumsq+((y[yr]-yAve)^2)
}	    
sp2<-((xnyrs-1)/xnyrs*xsumsq+(ynyrs-1)/ynyrs*ysumsq)/(xnyrs+ynyrs-2)

return(sp2)
}
