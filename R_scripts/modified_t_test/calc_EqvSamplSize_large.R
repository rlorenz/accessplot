# Calculate equivalent sample size 
# for Modified t-test for large samples (ne>=30)
# Zwiers and von Storch, 1995, J.Clim, Eq. 10

calc_EqvSamplSize_large <- function(x,y,r1){

dimsX<-dim(x)
xnyrs<-dimsX[4]

dimsY<-dim(y)
ynyrs<-dimsY[4]

if ( dimsX[3] != dimsY[3]){
   stop("Error: X and Y have different number of months")
} else if ( dimsX[2] != dimsY[2]){
   stop("Error: X and Y have different number of latitudes")
} else if ( dimsX[1] != dimsY[1]){
   stop("Error: X and Y have different number of longitudes")
} else{
	nmon<-dimsX[3]
	nlat<-dimsX[2]
	nlon<-dimsX[1]
}

xne_p<-array(NA,dim=c(nlon,nlat,nmon,1))
yne_p<-array(NA,dim=c(nlon,nlat,nmon,1))
ne<-array(NA,dim=c(nlon,nlat,nmon,1))
#V<-array(1,dim=c(nlon,nlat,nmon,1))
if ((xnyrs>50) & (any(abs(r1)!=1.0))){
	xne<-xnyrs*((1-r1)/(1+r1))
} else {
	den<-array(0,dim=c(nlon,nlat,nmon,1))
	for (n in 1:xnyrs-1){
		if (!(all(is.na(x[,,,n])))){
		den<-den+(1-n/xnyrs)*r1^n
		} 
	}
	xne<-xnyrs/(1+2*den)
#	xne<-xnyrs/V
}
if ((ynyrs>50) && (abs(r1)!=1.0)){
	yne<-ynyrs*((1-r1)/(1+r1))
} else {
        den<-array(0,dim=c(nlon,nlat,nmon,1))
        for (n in 1:ynyrs-1){
		if (!(all(is.na(y[,,,n])))){
                den<-den+(1-n/ynyrs)*r1^n
		}
        }
        yne<-ynyrs/(1+2*den)
#	yne<-ynyrs/V
}
for (mt in 1:nmon){
    for (lat in 1:nlat){
    	for (lon in 1:nlon){
	    if (all(is.na(x[lon,lat,mt,]))) {
	    xne_p[lon,lat,mt,1]<-NA
	    yne_p[lon,lat,mt,1]<-NA
	    ne[lon,lat,mt,1]<-NA
	    }
	    else {
		if (r1[lon,lat,mt,1]==0){
			xne_p[lon,lat,mt,1]<-xnyrs
			yne_p[lon,lat,mt,1]<-ynyrs
		} else {
	    		if (xne[lon,lat,mt,1] <= 2){
	       			xne_p[lon,lat,mt,1]<-2
	    		} else if ((xne[lon,lat,mt,1]>2) && (xne[lon,lat,mt,1]<=xnyrs)){
	       			xne_p[lon,lat,mt,1]<-xne[lon,lat,mt,1]
	   		} else {
	       			xne_p[lon,lat,mt,1]<-xnyrs
	    		}

	    		if (yne[lon,lat,mt,1] <= 2){
	       			yne_p[lon,lat,mt,1]<-2
	    		} else if ((yne[lon,lat,mt,1]>2) && (yne[lon,lat,mt,1]<=ynyrs)){
	       			yne_p[lon,lat,mt,1]<-yne[lon,lat,mt,1]
	    		} else {
	       			yne_p[lon,lat,mt,1]<-ynyrs
	    		}
		}
	}
	}# end lon
    }#end lat
}#end mt
ne<-(sqrt(1/xne_p+1/yne_p))
#ne<-(1/sqrt(xne_p)+1/sqrt(yne_p))
return(list(Xne_p=xne_p,Yne_p=yne_p,NE=ne))
}
