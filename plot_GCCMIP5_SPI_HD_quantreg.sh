#!/bin/bash
# File Name : plot_GLACE_coupling_strength.sh
# Creation Date : 03/12/2013
# Last Modified : Fri 23 May 2014 09:34:29 EST
# Created By : Ruth Lorenz
# Purpose : Wrapper to plot GLACE-CMIP5 coupling strength
#	calls ncl_scripts/plot_SPI_HD_quantreg_slope_GCCMIP5.ncl

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export MODEL=ACCESS
export RUNID1=CTL
export RUNID2=GC1A85

export R1_START=195001 #run 1 start
export R1_STOP=210012  #run 1 stop
export R2_START=195001 #run 1 start
export R2_STOP=210012  #run 1 stop
export A_START=1981 #analysis start
export A_STOP=2000  #analysis stop

export seas=JJA

export RUNNAME1=CTL
export RUNNAME2=GC1A85

export pltType=pdf
export masko=no #set to yes for ocean masked ensemble mean files

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data32/$USER
    export PATHCTL=/srv/ccrc/data32/$USER/GLACE-CMIP5_plots/$MODEL
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/coupling/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run without CTL input data"
    exit 1
fi


export EXPDIR=$PATHCTL/coupling/
if [[ ! -d $EXPDIR ]]; then
            echo "(1) $EXPDIR does not exist"
            echo "(2) cannot run without EXP input data"
	    exit 1
fi

export OUTDIR=$DATAPATH/GLACE-CMIP5_plots/$MODEL/coupling/
if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

#export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

#if [[ ! -d "$WORKDIR" ]]; then
# mkdir -p $WORKDIR
# echo "(0)     Making Directory $WORKDIR"
#fi

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR
#R --vanilla --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/calc_SPI_HD_quantreg.R
echo "Call NCL scripts"
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_SPI_HD_seas_quantreg_slope_GCCMIP5.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------
    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
