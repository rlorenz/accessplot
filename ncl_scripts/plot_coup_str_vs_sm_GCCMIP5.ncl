;-----------------------------------------------------------------------------;
; File Name : plot_coup_str_vs_sm_GCMIP5.ncl
; Creation Date : 19-03-2014
; Last Modified : Wed 19 Mar 2014 12:11:17 EST
; Created By : Ruth Lorenz
; Purpose : plot coupling versus soil moisture for GCCMIP5

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

user = systemfunc("echo $USER")
DATAPATH="/srv/ccrc/data32/" +user+ "/GLACE-CMIP5/"

Model = "ACCESS"
RStart="195001" ;run start
REnd="210012" 	;run end
AStart="2081" 	;analysis start, coupling needs to be calculated for this timeperiod
AEnd="2100" 	;analysis end ,  before running this script

Runname1 = "CTL" ; RUNNAME
Runname2 = "GC1A85" ; RUNNAME
Runname3 = "GC1B85" ; RUNNAME

inDir1 = DATAPATH + Model +"/"+ Runname2+"/"
inDir2 = DATAPATH + Model +"/"+ Runname3+"/"

inDir3 = "/home/z3441306/scripts/plot_scripts/GLACE-CMIP5/" + Model + "/" + \
        Runname2 + "/coupling/"
inDir4 = "/home/z3441306/scripts/plot_scripts/GLACE-CMIP5/" + Model + "/" + \
        Runname3 + "/coupling/"

outDir = "/home/z3441306/scripts/plot_scripts/GLACE-CMIP5/" + Model + "/" + \
       	   "/coupling/"

system("if ! test -d " + outDir +" ; then mkdir -p " + outDir + " ; fi")

;define plot type
pltType = "pdf"

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;

print("reading NetCDFs")
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

file1 = addfile(inDir1 +"mrsos_monthly_"+Model+"_"+ \
	 Runname2+"_1_"+RStart+"-"+REnd+".nc","r")
file2 = addfile(inDir2 +"mrsos_monthly_"+Model+"_"+ \
	 Runname3+"_1_"+RStart+"-"+REnd+".nc","r")

;derive time axis
time_A = file1->time
if ( Model .eq. "CESM") then
   time_A@calendar = "no_leap"
else if (Model .eq. "IPSL" .or. Model .eq. "GFDL") then
   time_A@calendar = "365_day"
else
   time_A@calendar = "gregorian"
end if
end if
A_date = cd_calendar(time_A,0) ;(:,0)->years, (:,1)->months, (:,2)->days

time_B = file1->time
if ( Model .eq. "CESM") then
   time_B@calendar = "no_leap"
else if (Model .eq. "IPSL" .or. Model .eq. "GFDL") then
   time_B@calendar = "365_day"
else
   time_B@calendar = "gregorian"
end if
end if
B_date = cd_calendar(time_B,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_A=ind(A_date(:,0).ge.AStart .and. A_date(:,0).le.AEnd)
new_date_A=A_date(ind_yrs_A,:)

ind_yrs_B=ind(A_date(:,0).ge.AStart .and. A_date(:,0).le.AEnd)
new_date_B=B_date(ind_yrs_B,:)

sm_A = file1 ->mrsos(ind_yrs_A,:,:)
sm_B = file2 ->mrsos(ind_yrs_B,:,:)


;check if ctl and exp time periods of same length
dsize_A=dimsizes(sm_A)
dsize_B=dimsizes(sm_B)
ntim_A=dsize_A(0)
nlat_A=dsize_A(1)
nlon_A=dsize_A(2)
ntim_B=dsize_B(0)
nlat_B=dsize_B(1)
nlon_B=dsize_B(2)

if ( ntim_A.ne.ntim_B .or. nlat_A.ne.nlat_B .or. nlon_A.ne.nlon_B ) then
        print("ERROR: ExpA and ExpB do not have same dimension, exiting")
        exit
else
        ntim=ntim_A
        nlat=nlat_A
        nlon=nlon_A

        delete(ntim_A)
        delete(nlat_A)
        delete(nlon_A)
        delete(ntim_B)
        delete(nlat_B)
        delete(nlon_B)
end if
;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin
;-----------------------------------------------------------------------------;

;start loop
do v = 0,2
   if ( v .eq. 0 ) then
      Var="T"
   else if ( v .eq. 1 ) then
      Var="TX"
   else if ( v .eq. 2 ) then
      Var="TN"

   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;

do j = 0,3
	if (j .eq. 0) then
		seas="DJF"
	else if (j .eq. 1) then
		seas="MAM"
	else if (j .eq. 2) then
		seas="JJA"
	else if (j .eq. 3) then
		seas="SON"
	end if
	end if
	end if
	end if

	sm_A_seas = month_to_season (sm_A, seas)
	sm_B_seas = month_to_season (sm_B, seas)

	sm_A_seasavg = dim_avg_n_Wrap(sm_A_seas,0)
	sm_B_seasavg = dim_avg_n_Wrap(sm_B_seas,0)


;-----------------------------------------------------------------------------;
; Read netcdfs with saved coupling strength
;-----------------------------------------------------------------------------;

;read coupling stength
file3 = addfile(inDir3 + "GCCMIP5_omega_coupstr_" + Runname2 + "_" + Runname1+ "_" +\
       Var + "_" + AStart + "-" + AEnd+ ".nc","r")
file4 = addfile(inDir4 + "GCCMIP5_omega_coupstr_" + Runname3 + "_" + Runname1+ "_" +\
       Var + "_" + AStart + "-" + AEnd+ ".nc","r")

      coup_A = file3 ->coup(j,:,:)    ;coupling
      coup_B = file4 ->coup(j,:,:)    ;coupling

      ;mask ocean points
      coup_A=where(lsm(0,0,:,:).ne.0,coup_A,coup_A@_FillValue)
      coup_B=where(lsm(0,0,:,:).ne.0,coup_B,coup_B@_FillValue)

      ;mask point where coupling <0
      coup_A=where(coup_A.gt.0,coup_A,coup_A@_FillValue)
      coup_B=where(coup_B.gt.0,coup_B,coup_B@_FillValue)

      ;mask NH in DJF and SH in JJA
      if ( j.eq.2) then
      	 coup_A({:10},{:})=coup_A@_FillValue
      	 coup_A({60:},{:})=coup_A@_FillValue
      	 coup_B({:10},{:})=coup_B@_FillValue
      	 coup_B({60:},{:})=coup_B@_FillValue
      else if (j.eq.0) then
      	 coup_A({0:},{:})=coup_A@_FillValue
      	 coup_A({:-60},{:})=coup_A@_FillValue
      	 coup_B({0:},{:})=coup_B@_FillValue
      	 coup_B({:-60},{:})=coup_B@_FillValue
      else
      	 coup_A({10:},{:})=coup_A@_FillValue
      	 coup_A({:-60},{:})=coup_A@_FillValue
      	 coup_B({10:},{:})=coup_B@_FillValue
      	 coup_B({:-60},{:})=coup_B@_FillValue
      end if
      end if

;-----------------------------------------------------------------------------;
; put soil moisture data into 11 bins
;-----------------------------------------------------------------------------;

intervals = (/ 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55/)
if (j.eq.0) then
   sm_bin_A_avg=new((/4,11/),"float")
   sm_bin_B_avg=new((/4,11/),"float")
end if

sm_bin_A_resh=new((/11,nlat*nlon/),"float")
sm_bin_B_resh=new((/11,nlat*nlon/),"float")

sm_bin_A=mask(coup_A,sm_A_seasavg.gt.intervals(0),False)
sm_bin_A_resh(0,:)=reshape(sm_bin_A,(/nlat*nlon/))
sm_bin_A_avg(j,0)=avg(sm_bin_A_resh(0,:))

sm_bin_B=mask(coup_B,sm_B_seasavg.gt.intervals(0),False)
sm_bin_B_resh(0,:)=reshape(sm_bin_B,(/nlat*nlon/))
sm_bin_B_avg(j,0)=avg(sm_bin_B_resh(0,:))

do mm=1,9
   sm_bin_A=mask(coup_A,sm_A_seasavg.gt.intervals(mm),False)
   sm_bin_A=mask(coup_A,sm_A_seasavg.le.intervals(mm-1),False)
   sm_bin_A_resh(mm,:)=reshape(sm_bin_A,(/nlat*nlon/))
   sm_bin_A_avg(j,mm)=avg(sm_bin_A_resh(mm,:))

   sm_bin_B=mask(coup_B,sm_B_seasavg.gt.intervals(mm),False)
   sm_bin_B=mask(coup_B,sm_B_seasavg.le.intervals(mm-1),False)
   sm_bin_B_resh(mm,:)=reshape(sm_bin_B,(/nlat*nlon/))
   sm_bin_B_avg(j,mm)=avg(sm_bin_B_resh(mm,:))
end do

sm_bin_A=mask(coup_A,sm_A_seasavg.le.intervals(9),False)
sm_bin_A_resh(10,:)=reshape(sm_bin_A,(/nlat*nlon/))
sm_bin_A_avg(j,10)=avg(sm_bin_A_resh(10,:))

sm_bin_B=mask(coup_B,sm_B_seasavg.le.intervals(9),False)
sm_bin_B_resh(10,:)=reshape(sm_bin_B,(/nlat*nlon/))
sm_bin_B_avg(j,10)=avg(sm_bin_B_resh(10,:))

;calculate confidence interval
if (j.eq.0) then
yhighci_A=new((/4,11/),"float")
ylowci_A=new((/4,11/),"float")
yhighci_B=new((/4,11/),"float")
ylowci_B=new((/4,11/),"float")
end if
do mm=0,10
   tmpN_A = num(.not.ismissing(sm_bin_A_resh(mm,:)))
   tmpN_A@_FillValue = -2147483647
   tmpN_B = num(.not.ismissing(sm_bin_B_resh(mm,:)))
   tmpN_B@_FillValue = -2147483647
   tmpSD_A = stddev(sm_bin_A_resh(mm,:))
   tmpSD_B = stddev(sm_bin_B_resh(mm,:))
   yhighci_A(j,mm) = sm_bin_A_avg(j,mm) + ( 1.96 * (tmpSD_A / where(tmpN_A.ne.0,sqrt(tmpN_A),tmpN_A@_FillValue)) )
   ylowci_A(j,mm) = sm_bin_A_avg(j,mm) - ( 1.96 * (tmpSD_A / where(tmpN_A.ne.0,sqrt(tmpN_A),tmpN_A@_FillValue)) )

   yhighci_B(j,mm) = sm_bin_B_avg(j,mm) + ( 1.96 * (tmpSD_B / where(tmpN_B.ne.0,sqrt(tmpN_B),tmpN_B@_FillValue)) )
   ylowci_B(j,mm) = sm_bin_B_avg(j,mm) - ( 1.96 * (tmpSD_B / where(tmpN_B.ne.0,sqrt(tmpN_B),tmpN_B@_FillValue)) )

   delete(tmpN_A)
   delete(tmpN_B)
   delete(tmpSD_A)
   delete(tmpSD_B)
end do

delete(sm_bin_A_resh)
delete(sm_bin_B_resh)

     end do;season

;-----------------------------------------------------------------------------;
; plot data
;-----------------------------------------------------------------------------;
pltName = outDir + "GCCMIP5_coup_vs_sm_mean_"+Model+"_" +Var+"_allSeas_"+AStart+"-"+AEnd

wks = gsn_open_wks(pltType,pltName)

  colors = (/"white","black","orangered2","navyblue","turquoise","slateblue",\
  	 "royalblue","goldenrod","firebrick","red4","indianred3"/)
  gsn_define_colormap(wks, colors) 

  res = True
  res@gsnDraw = False
  res@gsnFrame = False

  res@tiMainOn = True
  res@tiMainString = Var+" "+AStart+"-"+AEnd
  res@trXMinF = 5
  res@trXMaxF = 55
  res@gsnMaximize   =True
  res@vpHeightF = 0.4
  res@vpWidthF = 1.0
  res@vpXF     = 0.1                   ; start plot at x ndc coord

  res@tmXBOn = True
  res@tiXAxisString = "Soil Moisture Mean"
  res@tmXTOn = False
  res@tmYROn = False
  res@trYMinF = -0.05
  res@trYMaxF = 0.4
  res@xyLineColors = (/ "navyblue", "turquoise", "slateblue","royalblue"/)

  res@gsnYRefLine = 0.0
  res@gsnYRefLineColor = (/"black"/)
  res@gsnYRefLineDashPattern = (/2/)

  res@tiYAxisOn = True
  res@tmYLOn = True
  res@tiYAxisString = "Mean  ~F33~D~F33~W"

  res@xyDashPatterns = (/0,0,0,0/)
  res@xyLineThicknesses = (/2,2,2,2/)

  plot1 = gsn_csm_xy(wks,intervals,sm_bin_A_avg,res)

  res@xyLineColors = (/ "orangered2", "firebrick", "goldenrod","indianred3"/)
  plot2 = gsn_csm_xy(wks,intervals,sm_bin_B_avg,res)

  overlay(plot1,plot2)

  ; Add the confidence intervals
  gsres = True
  gsres@gsFillOpacityF = 0.15
  gsres@tfPolyDrawOrder   = "Predraw"

  yfill = (/ "navyblue", "turquoise", "slateblue","royalblue","orangered2", "firebrick", "goldenrod","indianred3"/)
  xb2 = new((/22/),"float")
  A_fill = new((/4,22/),"float")
  B_fill = new((/4,22/),"float")
  do tt = 0,10
    xb2(tt) = intervals(tt)
    xb2(tt+11) = intervals(10-tt)
    A_fill(0,tt) = yhighci_A(0,tt)
    A_fill(0,tt+11) = ylowci_A(0,10-tt)
    A_fill(1,tt) = yhighci_A(1,tt)
    A_fill(1,tt+11) = ylowci_A(1,10-tt)
    A_fill(2,tt) = yhighci_A(2,tt)
    A_fill(2,tt+11) = ylowci_A(2,10-tt)
    A_fill(3,tt) = yhighci_A(3,tt)
    A_fill(3,tt+11) = ylowci_A(3,10-tt)

    B_fill(0,tt) = yhighci_B(0,tt)
    B_fill(0,tt+11) = ylowci_B(0,10-tt)
    B_fill(1,tt) = yhighci_B(1,tt)
    B_fill(1,tt+11) = ylowci_B(1,10-tt)
    B_fill(2,tt) = yhighci_B(2,tt)
    B_fill(2,tt+11) = ylowci_B(2,10-tt)
    B_fill(3,tt) = yhighci_B(3,tt)
    B_fill(3,tt+11) = ylowci_B(3,10-tt)
  end do

    gsres@gsFillColor = yfill(0)
    dummy1 = gsn_add_polygon(wks,plot1,xb2,A_fill(0,:),gsres)
    gsres@gsFillColor = yfill(1)
    dummy2 = gsn_add_polygon(wks,plot1,xb2,A_fill(1,:),gsres)
    gsres@gsFillColor = yfill(2)
    dummy3 = gsn_add_polygon(wks,plot1,xb2,A_fill(2,:),gsres)
    gsres@gsFillColor = yfill(3)
    dummy4 = gsn_add_polygon(wks,plot1,xb2,A_fill(3,:),gsres)
    gsres@gsFillColor = yfill(4)
    dummy5 = gsn_add_polygon(wks,plot1,xb2,B_fill(0,:),gsres)
    gsres@gsFillColor = yfill(5)
    dummy6 = gsn_add_polygon(wks,plot1,xb2,B_fill(1,:),gsres)
    gsres@gsFillColor = yfill(6)
    dummy7 = gsn_add_polygon(wks,plot1,xb2,B_fill(2,:),gsres)
    gsres@gsFillColor = yfill(7)
    dummy8 = gsn_add_polygon(wks,plot1,xb2,B_fill(3,:),gsres)

; Attach a legend
  lgres1                    = True
  lgres1@lgLineColors       = (/"navyblue", "turquoise", "slateblue",\
  			   "royalblue"/)
  lgres1@lgItemType         = "Lines"        ; show lines only (default)
  lgres1@lgLabelFontHeightF = .1            ; legend label font
  lgres1@vpWidthF           = 0.2           ; width of legend (NDC)
  lgres1@vpHeightF          = 0.12           ; height of legend (NDC)
  lgres1@lgPerimOn          = False
  lgres1@lgMonoDashIndex    = True
  lgres1@lgDashIndex        = 0
  lgres1@lgLineThicknessF   = 7.0        ;thickness value for all Legend item lines

  legend1 = gsn_create_legend (wks, 4, (/"  A DJF","  A MAM","  A JJA","  A SON"/),lgres1)

  lgres2                    = True
  lgres2@lgLineColors       = (/"orangered2", "firebrick", "goldenrod","indianred3"/)
  lgres2@lgItemType         = "Lines"        ; show lines only (default)
  lgres2@lgLabelFontHeightF = .1            ; legend label font
  lgres2@vpWidthF           = 0.2           ; width of legend (NDC)
  lgres2@vpHeightF          = 0.12           ; height of legend (NDC)
  lgres2@lgPerimOn          = False
  lgres2@lgMonoDashIndex    = True
  lgres2@lgDashIndex        = 0
  lgres2@lgLineThicknessF   = 7.0        ;thickness value for all Legend item lines

  legend2 = gsn_create_legend (wks, 4, (/"  B DJF","  B MAM","  B JJA","  B SON"/),lgres2)

; Use gsn_add_annotation to attach this legend to our existing plot.
; This way, if we resize the plot, the legend will stay with the
; plot and be resized automatically.
;
; Point (0,0) is the dead center of the plot. Point (0,.5) is center,
; flush bottom. Point (0.5,0.5) is flush bottom, flush right.
;
  amres1                  = True
  amres1@amSide = "Top"
  amres1@amJust           = "TopRight"    ; Use top right corner of box
  amres1@amParallelPosF   = 0.2              ; Move legend to right
  amres1@amOrthogonalPosF = 0.5             ; Move legend up.
                                            ; for determining its location.

  annoid = gsn_add_annotation(plot1,legend1,amres1)  ; add legend to plot

  amres2                  = True
  amres2@amSide = "Top"
  amres2@amJust           = "TopRight"    ; Use top right corner of box
  amres2@amParallelPosF   = 0.5              ; Move legend to right
  amres2@amOrthogonalPosF = 0.5             ; Move legend up.
                                            ; for determining its location.

  annoid = gsn_add_annotation(plot1,legend2,amres2)  ; add legend to plot

  draw(plot1)
  frame(wks)

if(pltType .eq. "pdf")
        delete(wks)
        system("pdfcrop " + pltName + "." + pltType+ " " + pltName + "." + pltType)
end if

;-----------------------------------------------------------------------------;
;clean up
delete(res)
delete(lgres1)
delete(lgres2)
delete(amres1)
delete(amres2)
 
end do; variable

end
