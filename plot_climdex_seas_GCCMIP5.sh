#!/bin/bash
### File Name : plot_climdex_seas_GCCMIP5.sh
### Creation Date : December 2013
### Last Modified : Thu 23 Oct 2014 17:52:38 AEDT
### Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
### Purpose : script plotting climdex indices maps, trends, and timeseries
### climdex indices need to be calculated first using fortran code
### in /home/561/rzl561/scripts/fclimdex/fclimdex_nc/

#PBS -N climdex_plot_seas
#PBS -P dt6
#PBS -j oe
#PBS -l ncpus=1
#PBS -l walltime=00:40:00
#PBS -l vmem=2600mb
#PBS -wd

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export MODEL=ENS_MEAN  #ENS_MEAN
#export RUNID1=uamoa
#export RUNID2=uamoh

export R1_START=1954 #ctl run start
export R1_STOP=2100  #ctl run stop
export R2_START=1950 #exp run start
export R2_STOP=2100  #exp run stop
export A1_START=2021 #analysis start CTL
export A1_STOP=2040  #analysis stop CTL
export A2_START=2021 #analysis start EXP/SCEN
export A2_STOP=2040  #analysis stop EXP/SCEN

export RUNNAME1=CTL
export RUNNAME2=GC1A85

export NLAT=180 #ACCESS:145, CESM:192, EC-EARTH:160, ECHAM6:96, GFDL:90, IPSL:96, HadGEM2: ENS_MEAN: 180
export NLON=360 #ACCESS:192, CESM:288, EC-EARTH:320, ECHAM6:192, GFDL:144, IPSL:96, HadGEM2: ENS_MEAN: 360

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data32/$USER/GLACE-CMIP5/$MODEL
fi

export CTLDIR=$DATAPATH/$RUNNAME1/climdex_index/
export EXPDIR=$DATAPATH/$RUNNAME2/climdex_index/
export OUTDIR=/srv/ccrc/data32/$USER/GLACE-CMIP5_plots/$MODEL/$RUNNAME2/climdex_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load nco
module load cdo

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call climdex.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/climdex_GCCMIP5.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting climdex indices"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
