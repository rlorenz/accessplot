#!/bin/bash
 
#PBS -N LUC_blockbs
#PBS -l nodes=1:ppn=1
#PBS -l vmem=96gb
#PBS -l walltime=50:00:00
#PBS -j oe
#PBS -M r.lorenz@unsw.edu.au
#PBS -m ae
 
cd $PBS_O_WORKDIR

### add modules
module add R/3.1.1

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export REGION=wc
export RUNID1=uaoyr
export RUNNAME1=CTL

export R1_START=1978 #run start RUNID1
export R1_STOP=2011  #run end RUNID1
export R2_START=1978 #run start RUNID1
export R2_STOP=2011  #run end RUNID1

export A_START=1981 #analysis start
export A_STOP=2011  #analysis end

export NENS=5 #number of ensembles

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

export DATAPATH=/srv/ccrc/data23/$USER
export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output

export CTLDIR=$PATHCTL/$RUNID1/timeseries/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_plot without CTL input data"
    exit 1
fi

export OUTDIR=$DATAPATH/ACCESS_plots/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

for RUN in uaoya uaoyb uaoyc uaoyd uaoye uaoyf uaoyg uaoyh uaoyi uaoyj uaoyk uaoyl uaoym uaoyn uaoyo uaoyp uaoyr uaoys uaoyt uaoyw uaoyx uaoyy uaoyz vacda vacdb vacdc vacdd vacde vacdf vacdg vacdh vacdi vacdj vacdk vacdl vacdm vacdn vacdo vacdp vacdq vacdr vacds vacdt vacdu vacdv vajoa vajob vajoc vajod vajoe vajof vajog vajoh vajoi vajoj vajok vajol vajom vajon vajoo vajop vajoq vajor vajos vajot vajou vajov vacdw vacdx vacdy vacdz
do
    if [[ ! -f "$WORKDIR/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc" ]]; then
        cp $PATHCTL/$RUN/timeseries/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc.gz
    fi
done

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export siglev=0.05
# -----------------------------------------------------------------------
# call R scripts
# -----------------------------------------------------------------------
cd $OUTDIR
echo "Call R scripts"
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_Wilks_005${REGION}.Rout

R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_ENS_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_noSA_${NENS}ENS_Wilks_009GP${REGION}.Rout
#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_ENS_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_${NENS}ENS_Wilks_242${REGION}.Rout

#R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_ENS_Wilks.R > $HOME/scripts/plot_scripts/R_scripts/block_bootstrap/bootstrap_LUC_ttest_${NENS}ENS_Wilks_allAMZ.Rout
