;-----------------------------------------------------------------------------;
; File Name : seas_ann_variability_diff.ncl
; Creation Date : 17-03-2015
; Last Modified : Tue 17 Mar 2015 16:54:25 AEDT
; Created By : Ruth Lorenz
; Purpose : plot difference in intra-seasonal variability between CTL and 
;		GCCMIP5 experiments for reviewer

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME
Model = systemfunc("echo $MODEL") ; model

user = systemfunc("echo $USER")

ctlDir = systemfunc("echo $CTLDIR")
expDir = systemfunc("echo $EXPDIR")
outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

if (Runname1.eq."GC1A85") then
   Ctlname = "ExpA"
else if (Runname1.eq."CTL") then
   Ctlname = "CTL"
else
	print("Wrong experiment name")
end if
end if

if (Runname2.eq."GC1A85") then
   Expname = "ExpA"
else if (Runname2.eq."GC1B85") then
   Expname = "ExpB"
else
	print("Wrong experiment name")
end if
end if

;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin
;-----------------------------------------------------------------------------;
varname = (/"tas","tasmax","tasmin","pr"/)
longname = (/"Temperature at 1.5m","daily maximum Temperature","daily minimum Temperature","Total Precipitation"/)

;start loop
do v = 0,3
   if ( v .eq. 0 ) then
      Var="T"
   else if ( v .eq. 1 ) then
      Var="TX"
   else if ( v .eq. 2 ) then
      Var="TN"
   else if ( v .eq. 3 ) then
      Var="PR"

   end if
   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")
if ( Model .eq. "CESM" ) then
file1 = addfile(ctlDir + varname(v) + "_daily_CCSM4_" +Runname1+ \
      	"_1_195501-" + R1End + ".nc","r")
else
file1 = addfile(ctlDir + varname(v) + "_daily_" +Model+ "_" +Runname1+ "_1_" \
      		       + R1Start + "-" + R1End + ".nc","r")
end if
file2 = addfile(expDir + varname(v) + "_daily_" +Model+ "_" +Runname2+ "_1_" \
      		       + R2Start + "-" + R2End + ".nc","r")

;-----------------------------------------------------------------------------;
; read temperature data
;-----------------------------------------------------------------------------;

nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

;derive time axis
time_ctl = file1->time
if ( Model .eq. "CESM") then
   time_ctl@calendar = "no_leap"
else if (Model .eq. "IPSL" .or. Model .eq. "GFDL") then
   time_ctl@calendar = "365_day"
else
   time_ctl@calendar = "gregorian"
end if
end if
ctl_date = cd_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days

time_exp = file2->time
if ( Model .eq. "CESM" .or. Model .eq. "IPSL" ) then
   time_exp@calendar = "365_day"
else if (Model .eq. "GFDL") then
   time_exp@calendar = "no_leap"
else
   time_exp@calendar = "gregorian"
end if
end if
exp_date = cd_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)
nyrs = stringtoint(AEnd)-stringtoint(AStart)+1

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=exp_date(ind_yrs_exp,:)

      temp_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)        ;temperature at 1.5m
      temp_ctl@long_name     = longname(v)
      temp_ctl@units         = "K"

      temp_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)        ;temperature at 1.5m
      temp_exp@long_name     = longname(v)
      temp_exp@units         = "K"

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(temp_ctl)
dsize_exp=dimsizes(temp_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
	print("ERROR: CTL and EXP do not have same dimension, exiting")
	exit
else
	ntim=ntim_ctl
	nlat=nlat_ctl
	nlon=nlon_ctl

	delete(ntim_ctl)
	delete(nlat_ctl)
	delete(nlon_ctl)
	delete(ntim_exp)
	delete(nlat_exp)
	delete(nlon_exp)
end if

;-----------------------------------------------------------------------------;
; detrend timeseries
;-----------------------------------------------------------------------------;
temp_ctl_dtr=dtrend_leftdim(temp_ctl,False)
temp_exp_dtr=dtrend_leftdim(temp_exp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
	do j = 0,3
	;season, choose indices for months of season
	  if (j .eq. 0) then
		seas="DJF"

		ind_seas_ctl=ind(new_date_ctl(:,1).eq.1 .or. (new_date_ctl(:,1).eq.2 .and. new_date_ctl(:,2).ne.29 )\
		 .or. (new_date_ctl(:,1).eq.12) )
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)
		date_seas_ctl=new_date_ctl(ind_seas_ctl,:)

		ind_seas_exp=ind(new_date_exp(:,1).eq.1 .or. (new_date_exp(:,1).eq.2 .and. new_date_ctl(:,2).ne.29)\
		 .or. (new_date_exp(:,1).eq.12))
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)
		date_seas_exp=new_date_exp(ind_seas_exp,:)
	  else if (j .eq. 1) then
		seas="MAM"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.3) \
		 .or. new_date_ctl(:,1).eq.4 .or.new_date_ctl(:,1).eq.5 )
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)
		date_seas_ctl=new_date_ctl(ind_seas_ctl,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.3) \
		 .or. new_date_exp(:,1).eq.4 .or. new_date_exp(:,1).eq.5)
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)
		date_seas_exp=new_date_exp(ind_seas_exp,:)
	  else if (j .eq. 2) then
		seas="JJA"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.6) \
		 .or. new_date_ctl(:,1).eq.7 .or. new_date_ctl(:,1).eq.8)
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)
		date_seas_ctl=new_date_ctl(ind_seas_ctl,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.6) \
		 .or. new_date_exp(:,1).eq.7 .or. new_date_exp(:,1).eq.8)
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)
		date_seas_exp=new_date_exp(ind_seas_exp,:)
	  else if (j .eq. 3) then
		seas="SON"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.9) \
		 .or. new_date_ctl(:,1).eq.10 .or. new_date_ctl(:,1).eq.11)
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)
		date_seas_ctl=new_date_ctl(ind_seas_ctl,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.9) \
		 .or. new_date_exp(:,1).eq.10 .or. new_date_exp(:,1).eq.11)
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)
		date_seas_exp=new_date_exp(ind_seas_exp,:)

	  end if
	  end if
	  end if
	  end if

	  ntim_seas=dimsizes(temp_ctl_seas(:,0,0))
	  
	  ;calculate intra-seasonal variability for each year
	  seas_var_ctl=new((/nyrs,nlat,nlon/),float)
	  seas_var_exp=new((/nyrs,nlat,nlon/),float)
	  y=0
	  do yr=stringtointeger(AStart),stringtointeger(AEnd)
	     ind_yr_ctl=ind(date_seas_ctl(:,0).eq.yr)
	     seas_var_ctl(y,:,:)=dim_variance_n(temp_ctl_seas(ind_yr_ctl,:,:),0)

	     ind_yr_exp=ind(date_seas_exp(:,0).eq.yr)
	     seas_var_exp(y,:,:)=dim_variance_n(temp_exp_seas(ind_yr_exp,:,:),0)

	     y=y+1
	     delete(ind_yr_ctl)
	     delete(ind_yr_exp)
	  end do
	  
	  if (j.eq.0)
	     seas_var_ctl_avg=new((/4,nlat,nlon/),float)
	     seas_var_exp_avg=new((/4,nlat,nlon/),float)
	     diff_seas_var=new((/4,nlat,nlon/),float)
	  end if

	  seas_var_ctl_avg(j,:,:)=dim_avg_n(seas_var_ctl,0)
	  seas_var_exp_avg(j,:,:)=dim_avg_n(seas_var_exp,0)
	  diff_seas_var(j,:,:)=seas_var_exp_avg(j,:,:)-seas_var_ctl_avg(j,:,:)

	  delete(ind_seas_ctl)
	  delete(ind_seas_exp)
	  delete(temp_ctl_seas)
	  delete(temp_exp_seas)
	  delete(date_seas_ctl)
	  delete(date_seas_exp)
	end do ;season

	diff_seas_var@long_name ="Intra-seasonal variability "+Expname+ "-"+Ctlname+" " +AStart+"-"+AEnd
	diff_seas_var@units = "-"
	diff_seas_var!0="time"
	diff_seas_var!1="lat"
	diff_seas_var!2="lon"
	diff_seas_var&lon = temp_ctl&lon
	diff_seas_var&lat = temp_ctl&lat

	colorbar="dared_to_blue_18lev"
	printMinMax(diff_seas_var,True)
	lev=(/-5.5,-4.5,-3.5,-2.5,-1.5,-0.5,0.5,1.5,2.5,3.5,4.5,5.5/)
	Panel_Plot(outDir,"diff_seas_var_"+Var,AStart,AEnd,plttype,colorbar,diff_seas_var,False,True,lev,True)

end do ;variable
end ;script
