#!/bin/bash
### File Name : plot_climdex_seas.sh
### Creation Date : June 2013
### Last Modified : Wed 21 May 2014 15:55:12 EST
### Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
### Purpose : script plotting climdex indices maps, trends, and timeseries
### climdex indices need to be calculated first using fortran code
### in /home/561/rzl561/scripts/fclimdex/fclimdex_nc/

#PBS -N climdex_plot_seas
#PBS -P dt6
#PBS -j oe
#PBS -l ncpus=1
#PBS -l walltime=00:40:00
#PBS -l vmem=2600mb
#PBS -wd

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=uakpe
export RUNNAME=ACCESS
export DATA=HADEX2

export Y_START=1951
export Y_STOP=2010

export NLAT=73
#NLON different for HADEX2 and GHCNDEX, HADEX2=96, GHCNDEX=144
if [[ "$DATA" == "HADEX2" ]]; then
	export NLON=96
elif [[ "$DATA" == "GHCNDEX" ]]; then
	export NLON=144
fi

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER/
fi

export MODDIR=$DATAPATH/ACCESS_output/$RUNID/climdex_index/
export OBSDIR=$DATAPATH/DATASETS/${DATA}_Data/
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID/climdex_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.0.0
module load netcdf
module load nco
module load cdo

# -----------------------------------------------------------------------
# if look at seasonal values: calculate seasonal values for Obs first
# -----------------------------------------------------------------------
# TXx,TNx -> look for max
if [[ ! -f "$OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O -y max $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXx_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
if [[ ! -f "$OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O -y max $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNx_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
# TXn,TNn -> look for min
if [[ ! -f "RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O -y min $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O -y min $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O -y min $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O -y min $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TXn_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
if [[ ! -f "RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O -y min $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O -y min $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O -y min $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O -y min $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_TNn_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
# Rx1day, Rx5day -> look for max
if [[ ! -f "$OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx1day_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
if [[ ! -f "$OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O -y max $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_Rx5day_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
# DTR -> seasonal mean 
if [[ ! -f "$OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc" ]]; then
	ncea -O $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_DEC_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_JAN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_FEB_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_DJF_from-90to90_from-180to180.nc

	ncea -O $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_MAR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_APR_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_MAY_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_MAM_from-90to90_from-180to180.nc

	ncea -O $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_JUN_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_JUL_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_AUG_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_JJA_from-90to90_from-180to180.nc

	ncea -O $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_SEP_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_OCT_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_NOV_from-90to90_from-180to180.nc \
	    $OBSDIR/RawData_${DATA}_DTR_${Y_START}-${Y_STOP}_SON_from-90to90_from-180to180.nc
fi
# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call climdex.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/climdex.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting climdex indices"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
