;-----------------------------------------------------------------------------;
; File Name : TX_HadGHCND_evaluation.ncl
; Creation Date : July 2013
; Last Modified : Mon 19 May 2014 17:53:07 EST
; Created By : Ruth Lorenz
; Purpose : daily maximum temperature evaluation with HadGHCND data

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1") 	; RUNID
Runname1 = systemfunc("echo $RUNNAME1")	; RUNNAME

user = systemfunc("echo $USER")

if (user.eq."rzl561") then
    project = systemfunc("echo $PROJECT")
    DATAPATH="/short/" + project + "/" +user
    PATHCTL="/short/" +project+ "/" +user+ "/postproc"
else if (user.eq."z3441306") then
    DATAPATH="/srv/ccrc/data23/" +user
    PATHCTL="/srv/ccrc/data23/" +user+ "/ACCESS_output"
else
    print("Wrong USER")
    exit
end if
end if

fileDir = PATHCTL +"/"+ Runid1 + "/timeseries/"
compDir = DATAPATH + "/ACCESS_plots_workdir/"
outDir = systemfunc("echo $OUTDIR")

YStart = systemfunc("echo $HadG_START")
YEnd = systemfunc("echo $HadG_STOP")

;define plot type
type = systemfunc("echo $pltType")

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")

ctlfile = addfile(compDir + Runid1 + ".seasmean_tmax_tmin." + YStart + "_" + YEnd + ".nc","r")
ctlfile_rgd = addfile(compDir + Runid1 + ".seasmean_tmax_tmin." + YStart + "-" + YEnd + "_regrid.nc","r")
obsfile = addfile(compDir + "HadGHCND_TX_seas_mean_" + YStart + "-" + YEnd + ".nc","r")

;-----------------------------------------------------------------------------;

        tmax_K_ctl = ctlfile ->tasmax         ;max temperature rate [K]
        tmax_ctl=tmax_K_ctl-273.15  ;convert to degC
        copy_VarMeta(tmax_K_ctl,tmax_ctl)
        tmax_ctl@long_name     = Runname1+" T~B~MAX~N~"
        tmax_ctl@units         = "~S~o~N~C"


        tmax_K_ctl_rgd = ctlfile_rgd ->tasmax         ;max temperature rate [K]
	tmax_ctl_rgd=tmax_K_ctl_rgd-273.15  ;convert to degC
        copy_VarMeta(tmax_K_ctl_rgd,tmax_ctl_rgd)
        tmax_ctl_rgd@long_name     = Runname1+" T~B~MAX~N~"
        tmax_ctl_rgd@units         = "~S~o~N~C"

        tmax_K_obs = obsfile ->TX   ;max temperature [K]
        tmax_obs = tmax_K_obs-273.15  ;convert to degC
        copy_VarMeta(tmax_K_obs,tmax_obs)
        tmax_obs@long_name    = "HadGHCND T~B~MAX~N~"
        tmax_obs@units        = "~S~o~N~C"

	;change gri from -180-180 to 0-360
	tmax_obs=lonPivot(tmax_obs,0)
	tmax_ctl_rgd=lonPivot(tmax_ctl_rgd,0)
    
print("Calculating biases")
	diff_tmax = tmax_ctl_rgd(:,:,:) - tmax_obs(:,:,:)
	copy_VarMeta(tmax_ctl_rgd(:,:,:),diff_tmax)
	diff_tmax@long_name = "T~B~MAX~N~ bias: " +Runname1+ "-HadGHCND"
	diff_tmax@units = "~S~o~N~C"
	diff_tmax!0 = "time"
	diff_tmax&time = tmax_ctl&time

;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;

;Levels for contours
lev_tmax  = (/-10,0,5,10,15,20,25,30,35,40,45/)              ;max temperature
lev_btmax = (/-8,-7,-6,-5,-4,-3,-2,-1,1,2,3,4,5,6,7,8/)  ;max temperature bias

colorbar_abs = "BlueGreenYellowRed"
colorbar_diff = "blue_to_red_18lev"

Panel_Plot(outDir,"seas_tmax",YStart,YEnd,type,colorbar_abs,tmax_ctl(:,:,:),False,True,lev_tmax,True)
Panel_Plot(outDir,"HadGHCND_seas_tmax",YStart,YEnd,type,colorbar_abs,tmax_obs(:,:,:),False,True,lev_tmax,True)

Panel_Plot(outDir,"bias_tmax_HadGHCND",YStart,YEnd,type,colorbar_diff,diff_tmax,False,True,lev_btmax,True)

Panel_Plot(outDir,"reg_seas_tmax",YStart,YEnd,type,colorbar_abs,tmax_ctl(:,:,:),True,True,lev_tmax,True)
Panel_Plot(outDir,"reg_HadGHCND_seas_tmax",YStart,YEnd,type,colorbar_abs,tmax_obs(:,:,:),True,True,lev_tmax,True)

Panel_Plot(outDir,"regbias_tmax_HadGHCND",YStart,YEnd,type,colorbar_diff,diff_tmax,True,True,lev_btmax,True)

;---------------END----------------

end
