;-----------------------------------------------------------------------------;
; File Name :
; Creation Date : 14-08-2014
; Last Modified : Mon 30 Mar 2015 22:52:22 AEDT
; Created By : Ruth Lorenz
; Purpose : plot difference between CTL and LUC experiments, use data from
;	all ensembles and output modTtest from R and test for field significance

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_seas_panel_overlay_significance_LUC.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_single_overlay_significance_LUC.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;
Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME

Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $OUTDIR")

outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))       ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over runs and variables
;-----------------------------------------------------------------------------;
if ( Region .eq. "sc" ) then
   runnames = (/"001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc","242GP"/)
else if ( Region .eq. "wc" ) then
   runnames = (/"001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc","allAMZ"/)
else print("wrong region name, does not exist")
end if
end if

varname = (/"pr","tas","tasmax","tasmin","zg","psl","rss","rls","rnet","hfls","alb","cancd"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","GPH@300hPa","MSLP","SW~B~NET~N~","LW~B~NET~N~","R~B~NET~N~","LH","ALB","CAN"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","m","hPa","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","-","ms~S~-1~N~"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;start loop
do v = 0,1
   do run = 0,8
      print("reading NetCDFs")
      file1 = addfile(workDir+runnames(run)+"/" +varname(v)+"_anomaly_"+runnames(run)+  \
               "_noSA80N-60S_signif_0.05lev_1000bs_Wilks_1samp_" + AStart + "-" + AEnd + ".nc","r")
   if ( v .eq. 0 ) then
      Var="PR"
      pltColor="orange_to_blue_18lev"
      lev=(/-2,-1.5,-1,-0.6,-0.2,0.2,0.6,1,1.5,2/)
   else if ( v .eq. 1 ) then
      Var="T"
      pltColor="blue_to_red_18lev"
      lev=(/-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2/)
   end if
   end if

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;

if (Var .eq. "PR") then	;convert prec to mm/day
   var=varname(v)+" Anomaly"
      var_diff_orig = file1 ->$var$
      var_diff=var_diff_orig*(24*60*60)
      copy_VarMeta(var_diff_orig,var_diff)

      signif    = doubletoint(file1 ->signif)
      fs  = doubletofloat(file1 ->fs)

else
   var=varname(v)+" Anomaly"
      var_diff = file1 ->$var$

      signif  = doubletoint(file1 ->signif)
      fs  = doubletofloat(file1 ->fs)
end if

dsize=dimsizes(var_diff)

ntim=dsize(0)
nlat=dsize(1)
nlon=dsize(2)


var_diff@long_name     = longname(v)+" "+runnames(run)+"-"+"CTL"
var_diff@units         = units(v)
var_diff!1="lat"
var_diff!2="lon"
var_diff&lon = lsm&longitude
var_diff&lat = lsm&latitude

signif!1="lat"
signif!2="lon"
signif&lon = lsm&longitude
signif&lat = lsm&latitude

;-----------------------------------------------------------------------------;
; count number of significant grid points over land and save to .txt for each season
;-----------------------------------------------------------------------------;
	signif_land_fs=new((/4,nlat,nlon/),float)
	nr_sig_seas_noSA=new((/4/),float)
	nr_sig_seas_fs=new((/4/),float)

	do j=0,3
	   nr_sig_seas_noSA(j) = num(signif(j,:,:).eq.1)

	   if ( fs(j).eq.1) then
	   signif_land_fs(j,:,:)=signif(j,:,:)
	   else
   	   signif_land_fs(j,:,:)=0
	   end if
   	   nr_sig_seas_fs(j) = num(signif_land_fs(j,:,:).eq.1)

	end do

	outDir = systemfunc("echo $OUTDIR")+runnames(run) +"/"

	;save nr of signif grid points to file

	filo=outDir+"nr_signif_gp_noSAland80N-60S_blockbs_1samp_"+runnames(run)+"-"+Runname1+"_"+varname(v)+".txt"
	system("rm "+filo)
	asciiwrite(filo,nr_sig_seas_noSA)

	filo=outDir+"nr_signif_gp_noSAland80N-60S_blockbs_1samp_fs_"+runnames(run)+"-"+Runname1+"_"+varname(v)+".txt"
	system("rm "+filo)
	asciiwrite(filo,nr_sig_seas_fs)

	delete(nr_sig_seas_noSA)
	delete(nr_sig_seas_fs)

signif_land_fs!1="lat"
signif_land_fs!2="lon"
signif_land_fs&lon = lsm&longitude
signif_land_fs&lat = lsm&latitude

;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;
	;No for field significance
	Panel_Plot_sig_LUC(outDir+"/","diff_blockbs_1samp_noSA80N-60S_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(:,:,:),signif(:,:,:),False,Region,True,lev,True)
	signif_single_Plot_LUC(outDir+"/","diff_blockbs_1samp_noSA80N-60S_JJA_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(2,:,:),signif(2,:,:),False,Region,True,lev,True)

	;test for field significance
	Panel_Plot_sig_LUC(outDir+"/","diff_blockbs_1samp_fs_noSA80N-60S_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(:,:,:),signif_land_fs(:,:,:),False,Region,True,lev,True)
	signif_single_Plot_LUC(outDir+"/","diff_blockbs_1samp_fs_noSA80N-60S_JJA_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(2,:,:),signif_land_fs(2,:,:),False,Region,True,lev,True)

	delete(lev)
	delete(var_diff)

	delete(signif_land_fs)
	delete(signif)

  end do ;run

end do ;loop over variables
;-----------------------------------------------------------------------------;
end
