;-----------------------------------------------------------------------------;
; File Name : plot_nr_sig_gp_land_global_stat.ncl
; Creation Date : 30-10-2014
; Last Modified : mer. 03 juin 2015 01:43:00 AEST
; Created By : Ruth Lorenz
; Purpose : plot number of significant grid points for different statistical
;		tests and all LUC experiments	

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;
Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME

Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $OUTDIR")

outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")
Nens = systemfunc("echo $NENS")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))       ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; read changed area of each experiment 
;-----------------------------------------------------------------------------;
indir = workDir + "LUC/"
file1 = addfile(indir+"LUC_area_changed_per_run_all.nc","r")

area_diff = file1->area_diff ; has dimension 2 regions, 8 experiments
;choose 5 experiments with ensembles
area_diff_ens = new((/2,7/),float)
area_diff_ens(:,0) = area_diff(:,0)
area_diff_ens(:,1) = area_diff(:,3)
area_diff_ens(:,2) = area_diff(:,4)
area_diff_ens(:,3) = area_diff(:,6)
area_diff_ens(:,4) = area_diff(:,7)
area_diff_ens(:,5) = area_diff(:,8)
area_diff_ens(:,6) = area_diff(:,9)


;-----------------------------------------------------------------------------;
; loop over runs and variables
;-----------------------------------------------------------------------------;
Runnames=(/"001GP","009GP","025GP","081GP","121GP","242GP","allAMZ"/)

varname = (/"pr","tas"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~"/)

;start loop
do v = 0,1

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
	do j=0,3
	if ( j.eq.0) then
	   seas="DJF"
	else if (j.eq.1) then
	     seas="MAM"
	else if (j.eq.2) then
	     seas="JJA"
	else if (j.eq.3) then
	     seas="SON"
	else if (j.eq.4) then
	     seas="ANN"
	end if     
	end if 
	end if 
	end if 
	end if 
	print("seas= "+seas)
;-----------------------------------------------------------------------------;
; loop over strongly and weakly coupled region
;-----------------------------------------------------------------------------;

	do c=0,1
   	   if (c .eq. 0) then
      	   coup="sc"
	   coup_long = "strongly"
      	   else
      	   coup="wc"
	   coup_long = "weakly"
      	   end if
	   print("coup= "+coup_long)

		   ;start loop over experiments
		   do e = 0,6
		      if (e .gt. 4) then
		      	 coup=""
			 coup_long = ""
		      end if
;-----------------------------------------------------------------------------;
; read nr of significant grid points
;-----------------------------------------------------------------------------;
			if (e.eq.0 .and. c.eq.0) then
		   	   sig_diff_ttest=new((/4,2,7/),integer)
                           sig_diff_Ttest_fs=new((/4,2,7/),integer)
		   	   sig_diff_modTtest=new((/4,2,7/),integer)
		   	   sig_diff_modTtest_fs=new((/4,2,7/),integer)
		   	   sig_diff_wilk=new((/4,2,7/),integer)
		   	   sig_diff_wilk_fs=new((/4,2,7/),integer)
		   	end if
			
			inDir = workDir + Runnames(e)+coup+"/"
			sig_diff_ttest(:,c,e) = asciiread(inDir+"nr_signif_gp_noSAland80N-60S_ttest_1samp_"+Nens+"ENS_"\
				      +Runnames(e)+coup+"-"+Runname1+"_"+varname(v)\
				      +".txt",-1, "integer")
			sig_diff_modTtest(:,c,e) = asciiread(inDir+\
				      "nr_signif_gp_noSAland80N-60S_modTtest_1samp_"+Nens+"ENS_"+Runnames(e)+coup+"-"+\
				      Runname1+"_"+varname(v)+".txt",-1, "integer")
			sig_diff_wilk(:,c,e) = asciiread(inDir+"nr_signif_gp_noSAland80N-60S_wilktest_1samp_"+Nens+"ENS_"\
				      +Runnames(e)+coup+"-"+Runname1+"_"+varname(v)\
				      +".txt",-1, "integer")
                        file0 = addfile(workDir+Runnames(e)+coup+"/" +varname(v)+"_"+Runnames(e)+coup+  \
                              "_"+Runname1+"_seas_Ttest_1samp_"+Nens+"ENS_noSA80N-60S_" + AStart + "_" + AEnd + ".nc","r")
                        fs_walk = floattoint(file0->FS_walk)
                        sig_diff_Ttest_fs(0,c,e) = sum(fs_walk(0,:,:))
                        sig_diff_Ttest_fs(1,c,e) = sum(fs_walk(1,:,:))
                        sig_diff_Ttest_fs(2,c,e) = sum(fs_walk(2,:,:))
                        sig_diff_Ttest_fs(3,c,e) = sum(fs_walk(3,:,:))
                        fs_wilk_walk = floattoint(file0->FS_wilk_walk)
                        sig_diff_wilk_fs(0,c,e) = sum(fs_wilk_walk(0,:,:))
                        sig_diff_wilk_fs(1,c,e) = sum(fs_wilk_walk(1,:,:))
                        sig_diff_wilk_fs(2,c,e) = sum(fs_wilk_walk(2,:,:))
                        sig_diff_wilk_fs(3,c,e) = sum(fs_wilk_walk(3,:,:))

			file1 = addfile(workDir+Runnames(e)+coup+"/" +varname(v)+"_"+Runnames(e)+coup+  \
                     	      "_"+Runname1+"_seas_modTtest_1samp_"+Nens+"ENS_fs_noSA80N-60S_" + AStart + "_" + AEnd + ".nc","r")
		     	fs_walk = floattoint(file1->FS_walk)
			sig_diff_modTtest_fs(0,c,e) = sum(fs_walk(0,:,:))
			sig_diff_modTtest_fs(1,c,e) = sum(fs_walk(1,:,:))
			sig_diff_modTtest_fs(2,c,e) = sum(fs_walk(2,:,:))
			sig_diff_modTtest_fs(3,c,e) = sum(fs_walk(3,:,:))

		   end do; loop over exps

	end do ;loop over regions
;print(sig_diff_modTtest_fs)
;print(sig_diff_wilk_fs)

;-----------------------------------------------------------------------------;
; plot strongly weakly region separate but with linear Y-axis
;-----------------------------------------------------------------------------;
pltname=outDir + "LUC/nr_sig_gp_ttest_wilc_mttest_1samp_fs_"+Nens+"ENS_"+varname(v)+"_"+seas

wks = gsn_open_wks (plttype,pltname)

res		= True
res@gsnMaximize = True

res@gsnDraw     = False      ; Will draw later, after overlaying
res@gsnFrame    = False      ; all plots

res@xyLineThicknessF= 6.0
if (j.eq.4) then
	res@tiMainString = longname(v)
else
	res@tiMainString = longname(v) +" "+seas
end if
res@tiXAxisString = "Area of changed land cover [km~S~2~N~]"
res@tiYAxisString = "Nr. of stat. sig. grid points"

res@tiMainFontHeightF = 0.035
res@tiXAxisFontHeightF = 0.025
res@tiYAxisFontHeightF = 0.025
res@tmXBLabelFontHeightF = 0.025
res@tmYLLabelFontHeightF = 0.025
res@trXMinF = 0
res@trYMinF = 0
res@trXMaxF = 8900000
if (v.eq.0) then
   res@trYMaxF = 1000
else
   res@trYMaxF = 1000
end if

    res@tmLabelAutoStride = True
    res@tmXBLabelAngleF = 60

res@gsnYRefLine = 350
res@gsnYRefLineDashPattern = 1

res@xyDashPattern = 0
res@xyLineColors = (/"palegreen3"/)

plot1 = gsn_csm_xy(wks,area_diff_ens(0,:),sig_diff_ttest(j,0,:),res)

res@xyDashPattern = 16
res@xyLineColors = (/"seagreen4"/)

plot2 = gsn_csm_xy(wks,area_diff_ens(1,:),sig_diff_ttest(j,1,:),res)

res@gsMarkerSizeF    = 0.08
res@gsMarkerIndex = 1
res@gsMarkerColor = (/"palegreen3"/)
plot11 = gsn_add_polymarker(wks, plot1, area_diff_ens(0,:),sig_diff_Ttest_fs(j,0,:),res)

res@gsMarkerColor = (/"seagreen4"/)
plot12 = gsn_add_polymarker(wks, plot1, area_diff_ens(1,:),sig_diff_Ttest_fs(j,1,:),res)

res@xyDashPattern = 0
res@xyLineColors = (/"lightblue"/)

plot3 = gsn_csm_xy(wks,area_diff_ens(0,:),sig_diff_modTtest(j,0,:),res)

res@xyDashPattern = 16
res@xyLineColors = (/"royalblue"/)

plot4 = gsn_csm_xy(wks,area_diff_ens(1,:),sig_diff_modTtest(j,1,:),res)

res@gsMarkerSizeF    = 0.08
res@gsMarkerIndex = 1
res@gsMarkerColor = (/"lightblue"/)
plot7 = gsn_add_polymarker(wks, plot1, area_diff_ens(0,:),sig_diff_modTtest_fs(j,0,:),res)

res@gsMarkerColor = (/"royalblue"/)
plot8 = gsn_add_polymarker(wks, plot1, area_diff_ens(1,:),sig_diff_modTtest_fs(j,1,:),res)

res@xyDashPattern = 0
res@xyLineColors = (/"orange"/)

plot5 = gsn_csm_xy(wks,area_diff_ens(0,:),sig_diff_wilk(j,0,:),res)

res@xyDashPattern = 16
res@xyLineColors = (/"firebrick"/)

plot6 = gsn_csm_xy(wks,area_diff_ens(1,:),sig_diff_wilk(j,1,:),res)

res@gsMarkerColor = (/"orange"/)
plot9 = gsn_add_polymarker(wks, plot1, area_diff_ens(0,:),sig_diff_wilk_fs(j,0,:),res)

res@gsMarkerColor = (/"firebrick"/)
plot10 = gsn_add_polymarker(wks, plot1, area_diff_ens(1,:),sig_diff_wilk_fs(j,1,:),res)

;---Overlay one plot on the other, so they become one plot.
overlay(plot1,plot2)
overlay(plot1,plot3)
overlay(plot1,plot4)
overlay(plot1,plot5)
overlay(plot1,plot6)

; Attach a legend
  lgres                    = True
  lgres@lgLineColors       = (/"seagreen4","palegreen3","royalblue","lightblue","firebrick","orange","black","black"/)
  lgres@lgItemType         = "Lines"        ; show lines only (default)
  lgres@lgMonoDashIndex	= False
  lgres@lgDashIndexes	= (/16,0,16,0,16,0,16,0/)
  lgres@lgLabelFontHeightF = .2            ; legend label font
  lgres@vpWidthF           = 0.2          ; width of legend (NDC)
  lgres@vpHeightF          = 0.3           ; height of legend (NDC)
  lgres@lgPerimOn              = False            ; remove the box perimeter
  lgres@lgLineThicknessF   = 6.0        ;thickness value for all Legend item lines

  legend = gsn_create_legend (wks, 8, (/"  Ttest","  Ttest","  mTtest","  mTtest","  wilcox","  wilcox","  weakly ","  strongly"/),lgres)

; Use gsn_add_annotation to attach this legend to our existing plot.
; This way, if we resize the plot, the legend will stay with the
; plot and be resized automatically.
;
; Point (0,0) is the dead center of the plot. Point (0,.5) is center,
; flush bottom. Point (0.5,0.5) is flush bottom, flush right.
;
  amres                  = True
;  amres@amJust           = "BottomRight"    ; Use bottom right corner of box
  amres@amParallelPosF   = 0.33              ; Move legend to right
  amres@amOrthogonalPosF = -.28              ; Move legend down.
                                            ; for determining its location.

  annoid = gsn_add_annotation(plot3,legend,amres)  ; add legend to plot

draw(plot1)            ; This will draw all plots
frame(wks)

if (plttype .eq. "pdf")
        delete(wks)
        system("pdfcrop " + pltname + "." + plttype+ " "+ pltname + "." + plttype)
end if
delete(res)
delete(lgres)
delete(amres)

;-----------------------------------------------------------------------------;
; plot number of significant gridpoints indicating field significance
;-----------------------------------------------------------------------------;

pltname=outDir + "LUC/nr_sig_gp_ttest_wilc_mttest_1samp_fs_"+Nens+"ENS_"+varname(v)+"_"+seas+"_nolegend"

wks = gsn_open_wks (plttype,pltname)

res		= True
res@gsnMaximize = True

res@gsnDraw     = False      ; Will draw later, after overlaying
res@gsnFrame    = False      ; all plots

res@xyLineThicknessF= 6.0
if (j.eq.4) then
	res@tiMainString = longname(v)
else
	res@tiMainString = longname(v) +" "+seas
end if
res@tiXAxisString = "Area of changed land cover [km~S~2~N~]"
res@tiYAxisString = "Nr. of stat. sig. grid points"

res@tiMainFontHeightF = 0.035
res@tiXAxisFontHeightF = 0.025
res@tiYAxisFontHeightF = 0.025
res@tmXBLabelFontHeightF = 0.025
res@tmYLLabelFontHeightF = 0.025
res@trXMinF = 0
res@trYMinF = 0
res@trXMaxF = 8900000
if (v.eq.0) then
   res@trYMaxF = 1000
else
   res@trYMaxF = 1000
end if

    res@tmLabelAutoStride = True
    res@tmXBLabelAngleF = 60

res@gsnYRefLine = 350
res@gsnYRefLineDashPattern = 1

res@xyDashPattern = 0
res@xyLineColors = (/"palegreen3"/)

plot1 = gsn_csm_xy(wks,area_diff_ens(0,:),sig_diff_ttest(j,0,:),res)

res@xyDashPattern = 16
res@xyLineColors = (/"seagreen4"/)

plot2 = gsn_csm_xy(wks,area_diff_ens(1,:),sig_diff_ttest(j,1,:),res)

res@gsMarkerSizeF    = 0.08
res@gsMarkerIndex = 1
res@gsMarkerColor = (/"palegreen3"/)
plot11 = gsn_add_polymarker(wks, plot1, area_diff_ens(0,:),sig_diff_Ttest_fs(j,0,:),res)

res@gsMarkerColor = (/"seagreen4"/)
plot12 = gsn_add_polymarker(wks, plot1, area_diff_ens(1,:),sig_diff_Ttest_fs(j,1,:),res)

res@xyDashPattern = 0
res@xyLineColors = (/"lightblue"/)

plot3 = gsn_csm_xy(wks,area_diff_ens(0,:),sig_diff_modTtest(j,0,:),res)

res@xyDashPattern = 16
res@xyLineColors = (/"royalblue"/)

plot4 = gsn_csm_xy(wks,area_diff_ens(1,:),sig_diff_modTtest(j,1,:),res)

res@gsMarkerSizeF    = 0.08
res@gsMarkerIndex = 1
res@gsMarkerColor = (/"lightblue"/)
plot7 = gsn_add_polymarker(wks, plot1, area_diff_ens(0,:),sig_diff_modTtest_fs(j,0,:),res)

res@gsMarkerColor = (/"royalblue"/)
plot8 = gsn_add_polymarker(wks, plot1, area_diff_ens(1,:),sig_diff_modTtest_fs(j,1,:),res)

res@xyDashPattern = 0
res@xyLineColors = (/"orange"/)

plot5 = gsn_csm_xy(wks,area_diff_ens(0,:),sig_diff_wilk(j,0,:),res)

res@xyDashPattern = 16
res@xyLineColors = (/"firebrick"/)

plot6 = gsn_csm_xy(wks,area_diff_ens(1,:),sig_diff_wilk(j,1,:),res)

res@gsMarkerColor = (/"orange"/)
plot9 = gsn_add_polymarker(wks, plot1, area_diff_ens(0,:),sig_diff_wilk_fs(j,0,:),res)

res@gsMarkerColor = (/"firebrick"/)
plot10 = gsn_add_polymarker(wks, plot1, area_diff_ens(1,:),sig_diff_wilk_fs(j,1,:),res)

;---Overlay one plot on the other, so they become one plot.
overlay(plot1,plot2)
overlay(plot1,plot3)
overlay(plot1,plot4)
overlay(plot1,plot5)
overlay(plot1,plot6)

; Attach a legend
  lgres                    = True
  lgres@lgLineColors       = (/"seagreen4","palegreen3","royalblue","lightblue","firebrick","orange","black","black"/)
  lgres@lgItemType         = "Lines"        ; show lines only (default)
  lgres@lgMonoDashIndex	= False
  lgres@lgDashIndexes	= (/16,0,16,0,16,0,16,0/)
  lgres@lgLabelFontHeightF = .2            ; legend label font
  lgres@vpWidthF           = 0.2          ; width of legend (NDC)
  lgres@vpHeightF          = 0.25           ; height of legend (NDC)
  lgres@lgPerimOn              = False            ; remove the box perimeter
  lgres@lgLineThicknessF   = 6.0        ;thickness value for all Legend item lines

  legend = gsn_create_legend (wks, 8, (/"  Ttest","  Ttest","  mTtest","  mTtest","  wilcox","  wilcox","  weakly ","  strongly"/),lgres)

; Use gsn_add_annotation to attach this legend to our existing plot.
; This way, if we resize the plot, the legend will stay with the
; plot and be resized automatically.
;
; Point (0,0) is the dead center of the plot. Point (0,.5) is center,
; flush bottom. Point (0.5,0.5) is flush bottom, flush right.
;
  amres                  = True
;  amres@amJust           = "BottomRight"    ; Use bottom right corner of box
  amres@amParallelPosF   = -0.32              ; Move legend to right
  amres@amOrthogonalPosF = -.30              ; Move legend down.
                                            ; for determining its location.

;  annoid = gsn_add_annotation(plot1,legend,amres)  ; add legend to plot

draw(plot1)            ; This will draw all plots
frame(wks)

if (plttype .eq. "pdf")
        delete(wks)
        system("pdfcrop " + pltname + "." + plttype+ " "+ pltname + "." + plttype)
end if

delete(sig_diff_ttest)
delete(sig_diff_modTtest)
delete(sig_diff_wilk)
delete(sig_diff_modTtest_fs)
delete(sig_diff_wilk_fs)

delete(res)
delete(lgres)
delete(amres)
;-----------------------------------------------------------------------------;

	end do ;loop over seasons

end do ; loop over variables
;-----------------------------------------------------------------------------;
end
