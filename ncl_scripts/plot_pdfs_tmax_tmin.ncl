;-----------------------------------------------------------------------------;
; File Name : plot_pdfs_tmax_tmin_tmean.ncl
; Creation Date : 19-08-2013
; Last Modified : Mon 19 Aug 2013 20:04:51 EST
; Created By : Ruth Lorenz
; Purpose : plot probability density functions of maximum, minimum
;		temperature for different regions on globe with good obs data:
;		ASI (Asia), AUS (Australia), EUR (Europe), NAM (North America)
;		defined in ../areas_txt/*.txt

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin

Runid = systemfunc("echo $RUNID")
Runname = systemfunc("echo $RUNNAME")
dataSet = systemfunc("echo $DATA")

YStart = systemfunc("echo $Y_START")
YEnd = systemfunc("echo $Y_STOP")

inputdir = systemfunc("echo $WORKDIR")
outdir = systemfunc("echo $OUTDIR")

nlat = stringtointeger(systemfunc("echo $NLAT"))
nlon = stringtointeger(systemfunc("echo $NLON"))

plttype = systemfunc("echo $pltType")

;-----------------------------------------------------------------------------;
; Read data, loop over regions
;-----------------------------------------------------------------------------;
; read land mask
landmask_file = addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
land_mask = landmask_file->lsm(0,0,:,:)

do a = 0,3

	if (a .eq. 0) then
		area =  "ASI"
	else if (a .eq. 1) then
		area = "AUS"
	else if (a .eq. 2) then
		area = "EUR"
	else if (a .eq. 3) then
		area = "NAM"
	end if
	end if
	end if
	end if

; inputfiles
	tmax_mod_file = addfile(inputdir + area +"_" + Runid + "_TX_1951-2012.nc","r")
	tmin_mod_file = addfile(inputdir + area +"_" + Runid + "_TN_1951-2012.nc","r")

        tmax_obs_file = addfile(inputdir + area +"_" + dataSet + "_TX_1950-2011.nc","r")
        tmin_obs_file = addfile(inputdir + area +"_" + dataSet + "_TN_1950-2011.nc","r")

;time arrays
	time_mod = tmax_mod_file->time
	time_mod@calendar = "gregorian"
	yyyymmdd_mod = ut_calendar(time_mod,-2)
 	delete(time_mod)

	time_obs = tmax_obs_file->time
	yyyymmdd_obs = ut_calendar(time_obs,-2)
	delete(time_obs)

; read variables for time 1951-2011
	ymStrt = stringtoint(YStart+"0101")
	ymLast = stringtoint(YEnd+"1231")
   	ntStrt_mod  = ind(yyyymmdd_mod.eq.ymStrt)            ; index start
   	ntLast_mod  = ind(yyyymmdd_mod.eq.ymLast)            ; index last
 	
	tmax_mod = tmax_mod_file->Tmax(ntStrt_mod:ntLast_mod,:,:)
        tmin_mod = tmin_mod_file->Tmin(ntStrt_mod:ntLast_mod,:,:)

	time_mod = tmax_mod_file->time(ntStrt_mod:ntLast_mod)
	time_mod@calendar = "gregorian"
	new_time_mod = ut_calendar(time_mod,0)
	month_mod   = floattointeger( new_time_mod(:,1) )

   	ntStrt_obs  = ind(yyyymmdd_obs.eq.ymStrt)            ; index start
   	ntLast_obs  = ind(yyyymmdd_obs.eq.ymLast)              ; index last 	
        tmax_obs = tmax_obs_file->TX(ntStrt_obs:ntLast_obs,:,:)
        tmin_obs = tmin_obs_file->TN(ntStrt_obs:ntLast_obs,:,:)

	time_obs = tmax_obs_file->time(ntStrt_obs:ntLast_obs)
	new_time_obs = ut_calendar(time_obs,0)
	month_obs   = floattointeger( new_time_obs(:,1) )

;check if time arrays the same
	dsize_mod = dimsizes(tmax_mod)
	dsize_obs = dimsizes(tmax_obs)
	if (dsize_mod(0) .ne. dsize_obs(0)) then
		print ("Warning: Arrays of model and obs are not the same size.")
	end if

;-----------------------------------------------------------------------------;
; mask ocean values in model
;-----------------------------------------------------------------------------;

	ntim = dsize_mod(0)

        tmax_mod_mask = tmax_mod
        tmin_mod_mask = tmin_mod
 
	do t=0,ntim-1
		tmax_mod_mask(t,:,:) = mask(rm_single_dims(tmax_mod(t,:,:)),land_mask,1)	; Return the values of tmax where land_mask=1
		tmin_mod_mask(t,:,:) = mask(rm_single_dims(tmin_mod(t,:,:)),land_mask,1)   	; Return the values of tmin where land_mask=1
	end do
;printVarSummary(tmax_mod_mask)
;printVarSummary(tmin_mod_mask)

;-----------------------------------------------------------------------------;
; choose data over seasons
;-----------------------------------------------------------------------------;
	do j = 0,3 	;loop over seasons

		if (j .eq. 0) then
			season="DJF"
			print("Season " +season)
			seas_mod = ind(month_mod.eq.12 .or. month_mod.eq.1 .or. month_mod.eq.2)
			printVarSummary(seas_mod)
			tmax_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmax_mod_mask(0,:,0)),dimsizes(tmax_mod_mask(0,0,:))/),float)
			tmax_mod_seas = tmax_mod_mask(seas_mod,:,:)

			tmin_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmin_mod_mask(0,:,0)),dimsizes(tmin_mod_mask(0,0,:))/),float)
			tmin_mod_seas = tmin_mod_mask(seas_mod,:,:)

			seas_obs = ind(month_obs.eq.12 .or. month_obs.eq.1 .or. month_obs.eq.2)

			tmax_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmax_obs(0,:,0)),dimsizes(tmax_obs(0,0,:))/),float)
			tmax_obs_seas = tmax_obs(seas_obs,:,:)

			tmin_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmin_obs(0,:,0)),dimsizes(tmin_obs(0,0,:))/),float)
			tmin_obs_seas = tmin_obs(seas_obs,:,:)

		else if (j .eq. 1) then
			season="MAM"
			print("Season " +season)
			seas_mod = ind(month_mod.eq.3 .or. month_mod.eq.4 .or. month_mod.eq.5)

			tmax_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmax_mod_mask(0,:,0)),dimsizes(tmax_mod_mask(0,0,:))/),float)
			tmax_mod_seas = tmax_mod_mask(seas_mod,:,:)

			tmin_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmin_mod_mask(0,:,0)),dimsizes(tmin_mod_mask(0,0,:))/),float)
			tmin_mod_seas = tmin_mod_mask(seas_mod,:,:)

			seas_obs = ind(month_obs.eq.3 .or. month_obs.eq.4 .or. month_obs.eq.5)

			tmax_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmax_obs(0,:,0)),dimsizes(tmax_obs(0,0,:))/),float)
			tmax_obs_seas = tmax_obs(seas_obs,:,:)

			tmin_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmin_obs(0,:,0)),dimsizes(tmin_obs(0,0,:))/),float)
			tmin_obs_seas = tmin_obs(seas_obs,:,:)

		else if (j .eq. 2) then
			season="JJA"
			print("Season " +season)
			seas_mod = ind(month_mod.eq.6 .or. month_mod.eq.7 .or. month_mod.eq.8)

			tmax_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmax_mod_mask(0,:,0)),dimsizes(tmax_mod_mask(0,0,:))/),float)
			tmax_mod_seas = tmax_mod_mask(seas_mod,:,:)

			tmin_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmin_mod_mask(0,:,0)),dimsizes(tmin_mod_mask(0,0,:))/),float)
			tmin_mod_seas = tmin_mod_mask(seas_mod,:,:)

			seas_obs = ind(month_obs.eq.6 .or. month_obs.eq.7 .or. month_obs.eq.8)

			tmax_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmax_obs(0,:,0)),dimsizes(tmax_obs(0,0,:))/),float)
			tmax_obs_seas = tmax_obs(seas_obs,:,:)

			tmin_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmin_obs(0,:,0)),dimsizes(tmin_obs(0,0,:))/),float)
			tmin_obs_seas = tmin_obs(seas_obs,:,:)

		else if (j .eq. 3) then
			season="SON"
			print("Season " +season)
			seas_mod = ind(month_mod.eq.9 .or. month_mod.eq.10 .or. month_mod.eq.11)

			tmax_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmax_mod_mask(0,:,0)),dimsizes(tmax_mod_mask(0,0,:))/),float)
			tmax_mod_seas = tmax_mod_mask(seas_mod,:,:)

			tmin_mod_seas = new((/dimsizes(seas_mod),dimsizes(tmin_mod_mask(0,:,0)),dimsizes(tmin_mod_mask(0,0,:))/),float)
			tmin_mod_seas = tmin_mod_mask(seas_mod,:,:)

			seas_obs = ind(month_obs.eq.9 .or. month_obs.eq.10 .or. month_obs.eq.11)

			tmax_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmax_obs(0,:,0)),dimsizes(tmax_obs(0,0,:))/),float)
			tmax_obs_seas = tmax_obs(seas_obs,:,:)

			tmin_obs_seas = new((/dimsizes(seas_obs),dimsizes(tmin_obs(0,:,0)),dimsizes(tmin_obs(0,0,:))/),float)
			tmin_obs_seas = tmin_obs(seas_obs,:,:)

		end if
		end if
		end if
		end if

	tmax_mod_seas!0 = "time"
	tmax_mod_seas!1 = "lat"
	tmax_mod_seas&lat = tmax_mod&lat
	tmax_mod_seas!2 = "lon"
	tmax_mod_seas&lon = tmax_mod&lon

	tmin_mod_seas!0 = "time"
	tmin_mod_seas!1 = "lat"
	tmin_mod_seas&lat = tmin_mod&lat
	tmin_mod_seas!2 = "lon"
	tmin_mod_seas&lon = tmin_mod&lon

	tmax_obs_seas!0 = "time"
	tmax_obs_seas!1 = "lat"
	tmax_obs_seas&lat = tmax_obs&lat
	tmax_obs_seas!2 = "lon"
	tmax_obs_seas&lon = tmax_obs&lon

	tmin_obs_seas!0 = "time"
	tmin_obs_seas!1 = "lat"
	tmin_obs_seas&lat = tmin_obs&lat
	tmin_obs_seas!2 = "lon"
	tmin_obs_seas&lon = tmin_obs&lon

;-----------------------------------------------------------------------------;
; calculate probability density functions
;-----------------------------------------------------------------------------;

	tmax_mod_p = pdfx(tmax_mod_seas, 150, False)
	printVarSummary(tmax_mod_p)
	tmin_mod_p = pdfx(tmin_mod_seas, 150, False)
	printVarSummary(tmin_mod_p)

        tmax_obs_p = pdfx(tmax_obs_seas, 150, False)
 	printVarSummary(tmax_obs_p)
        tmin_obs_p = pdfx(tmin_obs_seas, 150, False)
 	printVarSummary(tmin_obs_p)

;-----------------------------------------------------------------------------;
; plot pdfs
;-----------------------------------------------------------------------------;
  nVar    = 4
  nBin    = tmax_mod_p@nbins          ; retrieve the number of bins

  xx      = new ( (/nVar, nBin/), typeof(tmax_mod_p))

  xx(0,:) = tmax_mod_p@bin_center     ; assign appropriate "x" axis values
  xx(1,:) = tmin_mod_p@bin_center
  xx(2,:) = tmax_obs_p@bin_center     ; assign appropriate "x" axis values
  xx(3,:) = tmin_obs_p@bin_center
 
  yy      = new ( (/nVar, nBin/), typeof(tmax_mod_p))
  yy(0,:) = (/ tmax_mod_p /)
  yy(1,:) = (/ tmin_mod_p /)
  yy(2,:) = (/ tmax_obs_p /)
  yy(3,:) = (/ tmin_obs_p /)

  pltName = outdir + area + "_PDF_TX_TN_" +season+ "_" + YStart+ "-" +YEnd

  wks  = gsn_open_wks (plttype,pltName)

  colors = (/"white","black","deepskyblue","deepskyblue4","palegreen","palegreen4",\
             "coral","coral4","slateblue1","slateblue4"/)

  gsn_define_colormap(wks, colors)

  res  = True
  res@xyLineThicknesses        = (/3.0,3.0,3.0,3.0/)        
  res@xyLineColors             = (/"coral4","deepskyblue4","coral","deepskyblue"/)  
  res@xyDashPatterns           = (/0,1,2,3/)              ; dash patterns
  res@tiYAxisString            = "PDF (%)"
  res@tiXAxisString            = "Tmax/Tmin [degC]"

  res@gsnXYBarChart            = True              ; Create bar plot
  res@gsnXYBarChartOutlineOnly = True

  res@pmLegendDisplayMode    = "Always"            ; turn on legend
  res@pmLegendSide           = "Top"               ; Change location of 
  res@pmLegendParallelPosF   = .26                 ; move units right
  res@pmLegendOrthogonalPosF = -0.35                ; move units down
  res@pmLegendWidthF         = 0.125               ; Change width and
  res@pmLegendHeightF        = 0.15                ; height of legend.
  res@lgPerimOn              = False                ; turn off/on box around
  res@lgLabelFontHeightF     = .02                ; label font height
  res@xyExplicitLegendLabels = (/"Tmax ACCESS","Tmin ACCESS","Tmax HadGHCND","Tmin HadGHCND"/)  ; create explicit labels

  res@tiMainString           = "PDFs for " +season+ " in region " +area
  plot = gsn_csm_xy (wks, xx, yy, res)

;-----------------------------------------------------------------------------;
; calculate skill score from Perkins et al. 2007, J.Clim
;-----------------------------------------------------------------------------;
;preassign Skill score variables to fill in scores for all areas in first loop
	if (a .eq. 0) then
		S_tmax = new((/4,4/),double)
 		S_tmin = new((/4,4/),double)
	end if

;put model and obs data into one array

	tmax_p = new((/2,nBin/),typeof(tmax_mod_p))
	tmax_p(0,:) = (/(tmax_mod_p/100*tmax_mod_p@bin_spacing)/)
	tmax_p(1,:) = (/(tmax_obs_p/100*tmax_obs_p@bin_spacing)/)

	tmin_p = new((/2,nBin/),typeof(tmin_mod_p))
	tmin_p(0,:) = (/(tmin_mod_p/100*tmin_mod_p@bin_spacing)/)
	tmin_p(1,:) = (/(tmin_obs_p/100*tmin_obs_p@bin_spacing)/)

;calculate skill score for tmax, tmin and tmean
	print(j)
	print(a)
	S_tmax(j,a) = sum(dim_min_n(tmax_p,0))
	S_tmin(j,a) = sum(dim_min_n(tmin_p,0))

printMinMax(S_tmax,True)
printMinMax(S_tmin,True)

;-----------------------------------------------------------------------------;
; end loops and clean up
;-----------------------------------------------------------------------------;
	delete(seas_mod)
	delete(seas_obs)
	delete(tmax_mod_seas)
	delete(tmin_mod_seas)
	delete(tmax_obs_seas)
	delete(tmin_obs_seas)
	delete(tmax_mod_p)
	delete(tmin_mod_p)
	delete(tmax_obs_p)
	delete(tmin_obs_p)
	delete(xx)
	delete(yy)

	end do ;j loop over seasons

 	delete(time_mod)
 	delete(time_obs)

end do ;a loop over areas

;-----------------------------------------------------------------------------;
; plot skill score from Perkins et al. 2007, J.Clim
;-----------------------------------------------------------------------------;
;Tmax
wks = gsn_open_wks(plttype,outdir +"skill_score_PDF_TX_areas")

  sres                       = True                 ; plot mods desired         
  sres@gsnFrame              = False                ; don't advance frame yet
  sres@gsnDraw 		     = True
  sres@gsnXYBarChart         = True                 ; turn on bar chart
  sres@gsnXYBarChartBarWidth = 0.2                 ; change bar widths

  sres@vpWidthF = 0.7
  sres@vpHeightF = 0.5
  sres@vpXF = .15 

  sres@trYMinF               = 0.0                    ; bring bars down to zero
  sres@trYMaxF               = 1.0                    ;
  sres@trXMinF               = 0.4                    ; adds space on either end
  sres@trXMaxF               = 4.6                    ; of the 1st and last bars
 
  sres@tmXBMode		= "Explicit"
  sres@tmXBValues	= (/1,2,3,4/)
  sres@tmXBLabels	= (/"DJF", "MAM", "JJA", "SON"/)

  sres@tiMainString          = "Skill score for Tmax PDFs"
  sres@tiYAxisString = "skill score"

  sres@gsnXYBarChartColors = (/"darkorange3"/)
  plot1 = gsn_csm_xy(wks,fspan(.7,3.7,4),S_tmax(:,0),sres)                  ; create plot
  sres@gsnXYBarChartColors = (/"light sea green"/)
  plot2 = gsn_csm_xy(wks,fspan(.9,3.9,4),S_tmax(:,1),sres)                  ; create plot
  sres@gsnXYBarChartColors = (/"royal blue"/)
  plot3 = gsn_csm_xy(wks,fspan(1.1,4.1,4),S_tmax(:,2),sres)                  ; create plot
  sres@gsnXYBarChartColors = (/"darkorchid4"/)
  plot4 = gsn_csm_xy(wks,fspan(1.3,4.3,4),S_tmax(:,3),sres)                  ; create plot
 
     lbres                    = True          ; labelbar only resources
     lbres@vpWidthF           = 0.3           ; labelbar width
     lbres@vpHeightF          = 0.1           ; labelbar height
     lbres@lbBoxMajorExtentF  = 0.36          ; puts space between color boxes
     lbres@lbFillColors       = (/"darkorange3","light sea green"/)
     lbres@lbMonoFillPattern  = True          ; Solid fill pattern
     lbres@lbLabelFontHeightF = 0.025         ; font height. default is small
     lbres@lbLabelJust        = "CenterLeft"  ; left justify labels
     lbres@lbPerimOn          = False
     lbres@lgPerimColor 	 = "white"
     labels = (/"ASI","AUS"/)
     gsn_labelbar_ndc(wks,2,labels,0.52,0.23,lbres)	; draw right labelbar column
	
     lbres@lbFillColors       = (/"royal blue","darkorchid4"/)
     labels = (/"EUR","NAM"/)
     gsn_labelbar_ndc(wks,2,labels,0.17,0.23,lbres)	; draw left labelbar column
     frame(wks)

delete(wks)

;Tmin
   wks = gsn_open_wks(plttype,outdir +"skill_score_PDF_TN_areas")

  sres                       = True                 ; plot mods desired 
  sres@gsnFrame              = False                ; don't advance frame yet

  sres@tiMainString          = "Skill score for Tmin PDFs"

  sres@gsnXYBarChartColors = (/"darkorange3"/)
  plot1 = gsn_csm_xy(wks,fspan(.7,3.7,4),S_tmin(:,0),sres)                  ; create plot
  sres@gsnXYBarChartColors = (/"light sea green"/)
  plot2 = gsn_csm_xy(wks,fspan(.9,3.9,4),S_tmin(:,1),sres)                  ; create plot
  sres@gsnXYBarChartColors = (/"royal blue"/)
  plot3 = gsn_csm_xy(wks,fspan(1.1,4.1,4),S_tmin(:,2),sres)                  ; create plot
  sres@gsnXYBarChartColors = (/"darkorchid4"/)
  plot4 = gsn_csm_xy(wks,fspan(1.3,4.3,4),S_tmin(:,3),sres)                  ; create plot

  lbres@lbFillColors       = (/"darkorange3","light sea green"/)
  labels = (/"ASI","AUS"/)
  gsn_labelbar_ndc(wks,2,labels,0.52,0.23,lbres)	; draw right labelbar column

  lbres@lbFillColors       = (/"royal blue","darkorchid4"/)
  labels = (/"EUR","NAM"/)
  gsn_labelbar_ndc(wks,2,labels,0.17,0.23,lbres)	; draw left labelbar column
  frame(wks)

end
