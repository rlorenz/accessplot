#!/bin/bash
# File Name : access_eval.sh
# Creation Date : July 2013
# Last Modified : Wed 03 Dec 2014 10:25:34 AEDT
# Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
# Purpose : main run and set up script to plot ACCESS output
#	evaluation globally and Australia, different datasets
#	global temp: ERAint (1979-2011), HadGHCND (1950-2011),
#	global prec: GPCP (1979-2012)
#	gloabl ET: GLEAM (1984-2007)
#	Australia temp and prec: AWAP (1900-2011)->use 1951-2011
#	Datasets hosted @ccrc-servers-> run there
#	Datasets exist for certain time periods only, not very flexible!

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID1=30d
export R_START=1957
export R_STOP=2005

export ERAi_START=1980
export ERAi_STOP=2011

export HadG_START=1951
export HadG_STOP=2011

export GPCP_START=1980
export GPCP_STOP=2012

export GLEAM_START=1984
export GLEAM_STOP=2007

export AWAP_START=1951
export AWAP_STOP=2011

export CERES_START=2001
export CERES_STOP=2009

export FLUX_START=1990
export FLUX_STOP=2005

export ISCCP_START=1985
export ISCCP_STOP=2007

export RUNNAME1=30d

export pltType=pdf
echo "(0) plot type is $pltType"

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
    #export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output
    #export PATHCTL=/srv/ccrc/data44/$USER/ACCESS-Leuning-Medlyn/MEDLYN/ENS_MEAN/
    #export PATHCTL=/srv/ccrc/data44/$USER/ACCESS_newHyd/$RUNID1/timeseries/
    export PATHCTL=/srv/ccrc/data44/$USER/ACCESS_LAI_MA/$RUNID1/
else
    echo "Wrong USER"
    exit
fi

#export CTLDIR=$PATHCTL/$RUNID1/timeseries/
export CTLDIR=$PATHCTL
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_eval without CTL input data"
    exit 1
fi

echo "(0)Run comparing to observations"
export OBSDIR=$DATAPATH/DATASETS/
export EXP=no
    if [[ ! -d $OBSDIR ]]; then
	echo "(1) $OBSDIR does not exist"
	echo "(2) cannot run access_plot without obs input data"
	exit 1
    fi

export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID1/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------
export siglev=0.05
export RES=n96
export NTILES=17
export NSOIL=6

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
    echo "Results are compared to Observations with files in Directory " $OBSDIR

    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "Start Time:" $date
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo
module load nco

# -----------------------------------------------------------------------
# seasonal mean and regridding of obs
# -----------------------------------------------------------------------
if [[ ! -f "$WORKDIR/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc" ]]; then
	#cp $CTLDIR/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc.gz $WORKDIR
        cp $CTLDIR/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR
	#gunzip $WORKDIR/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.monmean_tmax_tmin.${R_START}_${R_STOP}.nc" ]]; then
	#cp $CTLDIR/$RUNID1.monmean_tmax_tmin.${R_START}_${R_STOP}.nc.gz $WORKDIR
        cp $CTLDIR/$RUNID1.monmean_tmax_tmin.${R_START}_${R_STOP}.nc $WORKDIR
	#gunzip $WORKDIR/$RUNID1.monmean_tmax_tmin.${R_START}_${R_STOP}.nc.gz
fi

if [[ ! -f "$WORKDIR/ERAint_T2m_monthly_${ERAi_START}-${ERAi_STOP}_1deg.nc" ]]; then
	echo "Copy Observations to Workdir"
	cp $OBSDIR/ERAint_Data/ERAint_T2m_monthly_dec1979-${ERAi_STOP}-dec_1deg.nc $WORKDIR/ERAint_T2m_monthly_${ERAi_START}-${ERAi_STOP}_1deg.nc
	cp $OBSDIR/GPCP_Data/precip_mon_mean_197912-${GPCP_STOP}11.nc $WORKDIR/GPCP_prec_monmean_${GPCP_START}-${GPCP_STOP}_25deg.nc
	cp $OBSDIR/HadGHCND_Data/HadGHCND_TX_monmean_195012-${HadG_STOP}11.nc $WORKDIR/HadGHCND_TX_monmean_${HadG_START}-${HadG_STOP}.nc
	cp $OBSDIR/HadGHCND_Data/HadGHCND_TN_monmean_195012-${HadG_STOP}11.nc $WORKDIR/HadGHCND_TN_monmean_${HadG_START}-${HadG_STOP}.nc
	cp $OBSDIR/CERES_Data/CERES_EBAF-Surface_Ed2.7_Subset_200012-200911.nc $WORKDIR/CERES_monthly_${CERES_START}-${CERES_STOP}.nc
	cp $OBSDIR/LandFluxEVAL/LandFluxEVAL.merged.8912-0511.monthly.all.nc $WORKDIR/LandFluxEVAL.merged.monthly.all_${FLUX_START}-${FLUX_STOP}.nc
	cp $OBSDIR/ISCCP_data/CAM_ISCCP_198412-200711.nc $WORKDIR/CAM_ISCCP_${ISCCP_START}-${ISCCP_STOP}.nc
	cp $OBSDIR/ISCCP_data/CAH_ISCCP_198412-200711.nc $WORKDIR/CAH_ISCCP_${ISCCP_START}-${ISCCP_STOP}.nc
	cp $OBSDIR/ISCCP_data/CAL_ISCCP_198412-200711.nc $WORKDIR/CAL_ISCCP_${ISCCP_START}-${ISCCP_STOP}.nc
fi

#always regrid from higher to lower resolution -> obs GPCP and HadGHCND on lower res than model
if [[ ! -f "$WORKDIR/ERAint_T2m_monthly_${ERAi_START}-${ERAi_STOP}_regrid.nc" ]]; then
	echo "Regrid observations and model to same grid"
	cdo remapbil,${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR/ERAint_T2m_monthly_${ERAi_START}-${ERAi_STOP}_1deg.nc $WORKDIR/ERAint_T2m_monthly_${ERAi_START}-${ERAi_STOP}_regrid.nc
fi

if [[ ! -f "$WORKDIR/CERES_monthly_${CERES_START}-${CERES_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR/CERES_monthly_${CERES_START}-${CERES_STOP}.nc $WORKDIR/CERES_monthly_${CERES_START}-${CERES_STOP}_regrid.nc
fi

if [[ ! -f "$WORKDIR/LandFluxEVAL.merged.monthly.all_${FLUX_START}-${FLUX_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR/LandFluxEVAL.merged.monthly.all_${FLUX_START}-${FLUX_STOP}.nc $WORKDIR/LandFluxEVAL.merged.monthly.all_${FLUX_START}-${FLUX_STOP}_regrid.nc
fi

if [[ ! -f "$WORKDIR/CAM_ISCCP_${ISCCP_START}-${ISCCP_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR/CAM_ISCCP_${ISCCP_START}-${ISCCP_STOP}.nc $WORKDIR/CAM_ISCCP_${ISCCP_START}-${ISCCP_STOP}_regrid.nc
	cdo remapbil,${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR/CAH_ISCCP_${ISCCP_START}-${ISCCP_STOP}.nc $WORKDIR/CAH_ISCCP_${ISCCP_START}-${ISCCP_STOP}_regrid.nc
	cdo remapbil,${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc $WORKDIR/CAL_ISCCP_${ISCCP_START}-${ISCCP_STOP}.nc $WORKDIR/CAL_ISCCP_${ISCCP_START}-${ISCCP_STOP}_regrid.nc
fi

if [[ ! -f "$WORKDIR/$RUNID1.monthly_TS.${R_START}-${R_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/GPCP_prec_monmean_${GPCP_START}-${GPCP_STOP}_25deg.nc ${WORKDIR}/$RUNID1.monthly_TS.${R_START}_${R_STOP}.nc  $WORKDIR/$RUNID1.monthly_TS.${R_START}-${R_STOP}_regrid.nc
fi
if [[ ! -f "$WORKDIR/$RUNID1.monmean_tmax_tmin.${R_START}-${R_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/HadGHCND_TX_monmean_${HadG_START}-${HadG_STOP}.nc ${WORKDIR}/$RUNID1.monmean_tmax_tmin.${R_START}_${R_STOP}.nc $WORKDIR/$RUNID1.monmean_tmax_tmin.${R_START}-${R_STOP}_regrid.nc
fi

#copy GLEAM data into workdir
#if [[ ! -f "$WORKDIR/GLEAM.New.monmean_global_EVAP_${GLEAM_START}-${GLEAM_STOP}_rgd.nc" ]]; then
#	cp $OBSDIR/GLEAM_ETdata/GLEAM.New.monmean_global_EVAP_${GLEAM_START}-${GLEAM_STOP}.nc ${WORKDIR}/
#fi

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_diff_significance_eval.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0

# -----------------------------------------------------------------------
# 
# -----------------------------------------------------------------------
