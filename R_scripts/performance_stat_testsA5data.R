# test code R, test if t-test, modified t-test test on correct
# significance level (for 5% level, if test performed 10000 times ~500
# false positives)
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/modTtest_3dData.R")
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/move_block_bootstrap_test_package.R")

fp_t<-array(NA,dim=c(17))
fp_modT<-array(NA,dim=c(17))
fp_modTsmall<-array(NA,dim=c(17))
fp_mbbA1<-array(NA,dim=c(17))
fp_mbbA2<-array(NA,dim=c(17))
ac1<-seq(from = 0, to = 0.8, by = 0.05)
ac2<-0.3
ac3<-0.2
ac4<-0.1
ac5<-0.5
#print(ac)
a<-1

for (a in 1:17){
autocorr<-c(ac1[a],ac2,ac3,ac4,ac5)
######################
#student t-test
######################
print("t-test")
p_val<-array(NA,dim=c(10000))
k<-array(NA,dim=c(10000))

for (p in 1:10000){
	#norm<-rnorm(62)
	#x<-sample(norm,31,replace=T)
	#y<-sample(norm,31,replace=T)
	x<-arima.sim(list(ar = autocorr),n=31,rand.gen=rnorm,sd=0.1,mean=0)
	#y<-arima.sim(list(ar = autocorr),n=31,rand.gen=rnorm)
	test<-t.test(x,mu=0,alternative = c("two.sided"),conf.level=0.95)
	p_val[p]<-test$p.value
}
k[which(p_val<0.05)] <- 1
#print(sum(k,na.rm=T))
fp_t[a]<-(sum(k,na.rm=T))/100

################
#modified t-test
################
print("modified-test")
p_val<-array(NA,dim=c(2,5000))
k<-array(NA,dim=c(10000))
x<-array(NA,dim=c(2,5000,31))
y<-array(NA,dim=c(2,5000,31))
for (h in 1:2){
	for (p in 1:5000){
			#norm<-rnorm(62)
			#x[h,p,]<-sample(norm,31,replace=T)
			#y[h,p,]<-sample(norm,31,replace=T)
			#create timeseries with AR(2) correlation
			x[h,p,]<-arima.sim(list(ar = autocorr),n=31,rand.gen=rnorm,sd=0.1,mean=0)
			#y[h,p,]<-arima.sim(list(ar = autocorr),n=31,rand.gen=rnorm)
	}
}
test<-modTtest3d(x,mu=0,alternative = c("two.sided"),conf.level=0.95)
p_val<-test$p.value
k[which(p_val<0.05)] <- 1
print(sum(k,na.rm=T))
fp_modT[a]<-(sum(k,na.rm=T))/100

#p_val<-array(NA,dim=c(2,5000))
#k<-array(NA,dim=c(10000))
#test<-modTtest3d(x[,,1:15],mu=0,alternative = c("two.sided"),conf.level=0.95)
#p_val<-test$p.value
#k[which(p_val<0.05)] <- 1
#print(sum(k,na.rm=T))
#fp_modTsmall[a]<-(sum(k,na.rm=T))/100

############################
#moving blocks bootstrapping
############################*
print("moving blocks bootstarp")
p_val<-array(NA,dim=c(2,500))
k<-array(NA,dim=c(1000))
siglev<-0.05

mbbA1 <- mbbTest(x,mu=0,model="AR1",siglev=siglev,nb=100,verbose=TRUE)
print(mbbA1)
signifA1<-mbbA1$h.value

mbbA2 <- mbbTest(x,mu=0,model="AR2",siglev=siglev,nb=100,verbose=TRUE)
print(mbbA2)
signifA2<-mbbA2$h.value

fp_mbbA1[a]<-sum(signifA1,na.rm=T)/100
fp_mbbA2[a]<-sum(signifA2,na.rm=T)/100

#plus one in index counter 
a<- a+1
}

#plot data
out_dir <- "/srv/ccrc/data23/z3441306/ACCESS_plots/LUC/"
outfile=paste(out_dir,"performance_stat_test_random_art_A5data_test.pdf", sep="")
print(outfile)
pdf(outfile)

par(mar=c(5,5,4,2) + 0.1) #increase inner margin on left side

plot(fp_t,ac1,type="n",
	ylim=c(0,50),xlim=c(-0.9,0.9),xlab="lag-1 autocorrelation",ylab="Percentage of false positives",
	cex.lab=1.6,cex.axis=1.6,main="Artificial AR(5) data, 1 sample tests",cex.main=1.6)
        abline(h=5, lwd = 2)
	lines(ac1,fp_t,col = "red3", lwd = 4,lty=1)
	lines(ac1,fp_modT,col = "green4", lwd = 4,lty=2)
	#lines(ac1,fp_modTsmall,col = "aquamarine4", lwd = 4,lty=3)
	lines(ac1,fp_mbbA1,col = "darkorchid4", lwd = 4,lty=4)
	lines(ac1,fp_mbbA2,col = "royalblue4", lwd = 4,lty=4)
	legend("topleft",c("Ttest","modTtest","mbbA1","mbbA2","5% level"),cex=1.6,bty="n",
                       col = c("red3","green4","darkorchid4","royalblue4","black"),lwd=c(4,4,4,4,2),lty = c(1,2,4,5,1))

	text(xaxis.max[[i]],yaxis.max[[i]]/1.4,bquote("AR2="~.(ar2)),cex=1.6,adj = c(1,0))
	text(xaxis.max[[i]],(yaxis.max[[i]]/1.4-yaxis.max[[i]]/10),bquote("AR3="~.(ar3)),cex=1.6,adj = c(1,0))
	text(xaxis.max[[i]],(yaxis.max[[i]]/1.4-2*yaxis.max[[i]]/10),bquote("AR4="~.(ar4)),cex=1.6,adj = c(1,0))
	text(xaxis.max[[i]],(yaxis.max[[i]]/1.4-4*yaxis.max[[i]]/10),bquote("AR5="~.(ar5)),cex=1.6,adj = c(1,0))
dev.off()
