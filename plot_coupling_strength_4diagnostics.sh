#!/bin/bash
# File Name : plot_GLACE_coupling_strength.sh
# Creation Date : 03/12/2013
# Last Modified : Wed 21 May 2014 15:56:36 EST
# Created By : Ruth Lorenz
# Purpose : Wrapper to plot GLACE-CMIP5 coupling strength
#	calls ncl_scripts/coupling_strength_temp.ncl

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export MODEL=ACCESS
export RUNID1=CTL
export RUNID2=GC1A85

export R1_START=195001 #run 1 start
export R1_STOP=210012  #run 1 stop
export R2_START=195001 #run 1 start
export R2_STOP=210012  #run 1 stop
export A_START=1981 #analysis start
export A_STOP=2000  #analysis stop

export RUNNAME1=CTL
export RUNNAME2=GC1A85

export pltType=pdf

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "z3441306" ]]; then
    export PATHCTL=/home/$USER/scripts/plot_scripts
else
    echo "Wrong USER"
    exit
fi

export GC1_JJA_DIR=$PATHCTL/ACCESS_plots/uaob/coupling/
if [[ ! -d $GC1_JJA_DIR ]]; then
            echo "(1) $GC1_JJA_DIR does not exist"
            echo "(2) cannot run without input data"
	    exit 1
fi
export GC1_DJF_DIR=$PATHCTL/ACCESS_plots/uaof/coupling/
if [[ ! -d $GC1_DJF_DIR ]]; then
            echo "(1) $GC1_DJF_DIR does not exist"
            echo "(2) cannot run without input data"
	    exit 1
fi

export GCCMIP5_DIR=$PATHCTL/GLACE-CMIP5/$MODEL/$RUNNAME2/coupling/
if [[ ! -d $GCCMIP5_DIR ]]; then
            echo "(1) $GCCMIP5_DIR does not exist"
            echo "(2) cannot run without input data"
	    exit 1
fi

export OUTDIR=$DATAPATH/GLACE-CMIP5_plots/$MODEL/$RUNNAME2/coupling/
if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Data from folders " $GC1_JJA_DIR ", " $GC1_DJF_DIR "and "$GCCMIP5_DIR "are used"
    echo "The output directory is " $OUTDIR 
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call NCL scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_scatter_4_coup_str_diagnostics.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_scatter_4_coup_str_diagnostics_Tmax.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_scatter_coup_str_T_TX.ncl
# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------
    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
