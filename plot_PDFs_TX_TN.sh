#!/bin/bash
# File Name : plot_PDFs_TX_TN_TMN.sh
# Creation Date : 05/08/2013
# Last Modified : Fri 04 Dec 2015 16:20:55 AEDT
# Created By : Ruth Lorenz
# Purpose : plot PDFs of Tmax, Tmin over regions

##-----------------------
# function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=uakpe
export RUNNAME=ACCESS
export DATA=HadGHCND

export Y_START=1951	#start of analysis period
export Y_STOP=2011	#end of analysis period
export R_START=1951	#start year of model run
export R_STOP=2012	#end year of model run
export D_START=1950	#start year of obs data
export D_STOP=2011	#end year of obs data

export NLAT=192
export NLON=145

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/ACCESS_output/${RUNID}/timeseries/
export OBSDIR=/srv/ccrc/data07/z3356123/HadGHCND_nc/
export WORKDIR=$DATAPATH/ACCESS_plots_workdir/
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID/PDFs_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# write data into txt file read by R-script
# -----------------------------------------------------------------------
echo $WORKDIR > INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $OUTDIR >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $Y_START >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $Y_STOP >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $R_START >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $R_STOP >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $D_START >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $D_STOP >> INPUT_plot_pdf_skillscore_tmax_tmin.txt
echo $RUNID >> INPUT_plot_pdf_skillscore_tmax_tmin.txt

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.0
module load netcdf
module load nco
module load cdo
module load R

# -----------------------------------------------------------------------
# cut time to common timeperiod 1951-2011
# -----------------------------------------------------------------------
#count number of leapyears in analysis period: NOT TESTED
#year=${Y_START}
#nr_leap=0
#     while [  $year -le ${Y_STOP} ]; do
#	let nr_leap=nr_leap+$(leapyr)
#	let year=year+1
#	done
#find indices NOT TESTED
#let t1=(${R_START}-${Y_START})*365			#0
#let t2=(${Y_STOP}-${Y_START}+1)*365+${nr_leap}-1+t2	#22279
#let t3=(${D_START}-${Y_START})*365			#365
#let t4=(${Y_STOP}-${Y_START}+1)*365+${nr_leap}-1+t3	#
#for VAR in TX TN
#do
#        ncks -O -d time,${t1},${t2} $MODDIR/${RUNID}_${VAR}_${R_START}-${R_STOP}_degC.nc $WORKDIR/${RUNID}_${VAR}_${Y_START}-${Y_STOP}.nc
#       ncks -O -d time,${t3},${t4} ${OBSDIR}/HadGHCND_${VAR}_${D_START}-${D_STOP}.nc $WORKDIR/${DATA}_${VAR}_${Y_START}-${Y_STOP}.nc
#done

# -----------------------------------------------------------------------
# mask regions and monthly means
# -----------------------------------------------------------------------

for area in ASI AUS EUR NAM
do

	for VAR in TX TN
	do

	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $MODDIR/${RUNID}_${VAR}_${R_START}-${R_STOP}_degC.nc $WORKDIR/${area}_${RUNID}_${VAR}_${R_START}-${R_STOP}.nc
	cdo maskregion,$HOME/scripts/plot_scripts/areas_txt/${area}.txt $OBSDIR/${DATA}_${VAR}_${D_START}-${D_STOP}.nc $WORKDIR/${area}_${DATA}_${VAR}_${D_START}-${D_STOP}.nc
	cdo monmean $WORKDIR/${area}_${RUNID}_${VAR}_${R_START}-${R_STOP}.nc $WORKDIR/${area}_${RUNID}_${VAR}_monmean_${R_START}-${R_STOP}.nc
	cdo monmean $WORKDIR/${area}_${DATA}_${VAR}_${D_START}-${D_STOP}.nc $WORKDIR/${area}_${DATA}_${VAR}_monmean_${D_START}-${D_STOP}.nc
	done

done

# -----------------------------------------------------------------------
# call ncl and R script
# -----------------------------------------------------------------------
cd $OUTDIR

#echo "Call ncl scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_pdfs_tmax_tmin.ncl 

echo "Call R script"
R CMD BATCH $HOME/scripts/plot_scripts/R_scripts/plot_pdf_skillscore_tmax_tmin.R
# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
