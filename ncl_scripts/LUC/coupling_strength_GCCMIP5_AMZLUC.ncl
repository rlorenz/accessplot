;-----------------------------------------------------------------------------;
; File Name : coupling_strength_GCCMIP5_AMZLUC.ncl
; Creation Date : July 2014
; Last Modified : 
; Created By : Ruth Lorenz
; Purpose : calculate and plot coupling strength from GLACE-CMIP5 experiments
; over Amazonia with indicated LUC regions

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runname1 = "CTL" ; RUNNAME
Runname2 = "GC1B85" ; RUNNAME
Model = "ACCESS" ; model

ctlDir = "/srv/ccrc/data32/z3441306/GLACE-CMIP5/"+Model+"/"+Runname1+"/"
expDir = "/srv/ccrc/data32/z3441306/GLACE-CMIP5/"+Model+"/"+Runname2+"/"
outDir = "/srv/ccrc/data23/z3441306/ACCESS_plots/LUC/"

R1Start = "195001"
R1End = "210012"
R2Start = "195001"
R2End = "210012"
AStart = "1981"
AEnd = "2011"

;define plot type
plttype = "pdf"

if (Runname1.eq."GC1A85") then
   Ctlname = "ExpA"
else if (Runname1.eq."CTL") then
   Ctlname = "CTL"
else
	print("Wrong experiment name")
end if
end if

if (Runname2.eq."GC1A85") then
   Expname = "ExpA"
else if (Runname2.eq."GC1B85") then
   Expname = "ExpB"
else
	print("Wrong experiment name")
end if
end if

;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin
;-----------------------------------------------------------------------------;
varname = (/"tas","tasmax","tasmin","pr"/)
longname = (/"Temperature at 1.5m","daily maximum Temperature","daily minimum Temperature","Total Precipitation"/)

;start loop
do v = 0,3
   if ( v .eq. 0 ) then
      Var="T"
   else if ( v .eq. 1 ) then
      Var="TX"
   else if ( v .eq. 2 ) then
      Var="TN"
   else if ( v .eq. 3 ) then
      Var="PR"

   end if
   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")
if ( Model .eq. "CESM" ) then
file1 = addfile(ctlDir + varname(v) + "_daily_CCSM4_" +Runname1+ \
      	"_1_195501-" + R1End + ".nc","r")
else
file1 = addfile(ctlDir + varname(v) + "_daily_" +Model+ "_" +Runname1+ "_1_" \
      		       + R1Start + "-" + R1End + ".nc","r")
end if
file2 = addfile(expDir + varname(v) + "_daily_" +Model+ "_" +Runname2+ "_1_" \
      		       + R2Start + "-" + R2End + ".nc","r")

;-----------------------------------------------------------------------------;
; read temperature data
;-----------------------------------------------------------------------------;

nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

;derive time axis
time_ctl = file1->time
if ( Model .eq. "CESM") then
   time_ctl@calendar = "no_leap"
else if (Model .eq. "IPSL" .or. Model .eq. "GFDL") then
   time_ctl@calendar = "365_day"
else
   time_ctl@calendar = "gregorian"
end if
end if
ctl_date = cd_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days

time_exp = file2->time
if ( Model .eq. "CESM" .or. Model .eq. "IPSL" ) then
   time_exp@calendar = "365_day"
else if (Model .eq. "GFDL") then
   time_exp@calendar = "no_leap"
else
   time_exp@calendar = "gregorian"
end if
end if
exp_date = cd_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=exp_date(ind_yrs_exp,:)

      temp_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)        ;temperature at 1.5m
      temp_ctl@long_name     = longname(v)
      temp_ctl@units         = "K"

      temp_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)        ;temperature at 1.5m
      temp_exp@long_name     = longname(v)
      temp_exp@units         = "K"

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(temp_ctl)
dsize_exp=dimsizes(temp_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
	print("ERROR: CTL and EXP do not have same dimension, exiting")
	exit
else
	ntim=ntim_ctl
	nlat=nlat_ctl
	nlon=nlon_ctl

	delete(ntim_ctl)
	delete(nlat_ctl)
	delete(nlon_ctl)
	delete(ntim_exp)
	delete(nlat_exp)
	delete(nlon_exp)
end if

;-----------------------------------------------------------------------------;
; detrend timeseries
;-----------------------------------------------------------------------------;
temp_ctl_dtr=dtrend_leftdim(temp_ctl,False)
temp_exp_dtr=dtrend_leftdim(temp_exp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
	do j = 0,3
	;season, choose indices for months of season, remove first 8(JJA,MAM)/7(SON)/6(DJF) days per season
	; in accordance with Koster et al 2006 and Seneviratne et al 2006
	  if (j .eq. 0) then
		seas="DJF"

		ind_seas_ctl=ind(new_date_ctl(:,1).eq.1 .or. (new_date_ctl(:,1).eq.2 .and. new_date_ctl(:,2).ne.29 )\
		 .or. (new_date_ctl(:,1).eq.12 .and. new_date_ctl(:,2).gt.6) )
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)

		ind_seas_exp=ind(new_date_exp(:,1).eq.1 .or. (new_date_exp(:,1).eq.2 .and. new_date_ctl(:,2).ne.29)\
		 .or. (new_date_exp(:,1).eq.12 .and. new_date_exp(:,2).gt.6))
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)

	  else if (j .eq. 1) then
		seas="MAM"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.3 .and. new_date_ctl(:,2).gt.8) \
		 .or. new_date_ctl(:,1).eq.4 .or.new_date_ctl(:,1).eq.5 )
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.3 .and. new_date_exp(:,2).gt.8) \
		 .or. new_date_exp(:,1).eq.4 .or. new_date_exp(:,1).eq.5)
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)

	  else if (j .eq. 2) then
		seas="JJA"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.6 .and. new_date_ctl(:,2).gt.8) \
		 .or. new_date_ctl(:,1).eq.7 .or. new_date_ctl(:,1).eq.8)
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)
	
		ind_seas_exp=ind((new_date_exp(:,1).eq.6 .and. new_date_exp(:,2).gt.8) \
		 .or. new_date_exp(:,1).eq.7 .or. new_date_exp(:,1).eq.8)
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)

	  else if (j .eq. 3) then
		seas="SON"

		ind_seas_ctl=ind((new_date_ctl(:,1).eq.9 .and. new_date_ctl(:,2).gt.7) \
		 .or. new_date_ctl(:,1).eq.10 .or. new_date_ctl(:,1).eq.11)
		temp_ctl_seas=temp_ctl_dtr(ind_seas_ctl,:,:)

		ind_seas_exp=ind((new_date_exp(:,1).eq.9 .and. new_date_exp(:,2).gt.7) \
		 .or. new_date_exp(:,1).eq.10 .or. new_date_exp(:,1).eq.11)
		temp_exp_seas=temp_exp_dtr(ind_seas_exp,:,:)

	  end if
	  end if
	  end if
	  end if

	  ntim_seas=dimsizes(temp_ctl_seas(:,0,0))

;-----------------------------------------------------------------------------;
; Calculate 6-daily means
;-----------------------------------------------------------------------------;
	ntim_6d=ntim_seas/6

	temp_ctl_6dmean=new((/ntim_6d,nlat,nlon/),float)
	temp_exp_6dmean=new((/ntim_6d,nlat,nlon/),float)
	ind_t=0

	do tt=0,ntim_seas-1,6
	   temp_ctl_6dmean(ind_t,:,:)=dim_avg_n_Wrap(temp_ctl_seas(tt:tt+5,:,:),0)
	   temp_exp_6dmean(ind_t,:,:)=dim_avg_n_Wrap(temp_exp_seas(tt:tt+5,:,:),0)
	   ind_t=ind_t+1
	end do

	delete(temp_ctl_seas)
	delete(temp_exp_seas)

;-----------------------------------------------------------------------------;
; Calculate mean climatology
;-----------------------------------------------------------------------------;
	ndays=ntim_6d/nyears
	temp_ctl_yd=reshape(temp_ctl_6dmean,(/nyears,ndays,nlat,nlon/))
	temp_exp_yd=reshape(temp_exp_6dmean,(/nyears,ndays,nlat,nlon/))

	temp_ctl_clim=dim_avg_n(temp_ctl_yd,0)
	temp_exp_clim=dim_avg_n(temp_exp_yd,0)

;-----------------------------------------------------------------------------;
; Calculating standard deviations
;-----------------------------------------------------------------------------;

	print("Calculating standard deviations")
	std_ctl=dim_stddev_n(temp_ctl_clim,0)
	std_exp=dim_stddev_n(temp_exp_clim,0)

	std_ctl_full=dim_stddev_n(temp_ctl_6dmean,0)
	std_exp_full=dim_stddev_n(temp_exp_6dmean,0)

	if ( j.eq.0 ) then
	omega_ctl=new((/4,nlat,nlon/),float)
	omega_exp=new((/4,nlat,nlon/),float)
	coup=new((/4,nlat,nlon/),float)
	end if

	print("Calculate Omegas and coupling")
	omega_ctl(j,:,:)=((nyears*std_ctl)-std_ctl_full)/((nyears-1) * where(std_ctl_full.eq.0,std_ctl_full@_FillValue,std_ctl_full))
	omega_exp(j,:,:)=((nyears*std_exp)-std_exp_full)/((nyears-1) * where(std_exp_full.eq.0,std_exp_full@_FillValue,std_exp_full))

	coup(j,:,:)=omega_exp(j,:,:)-omega_ctl(j,:,:)

	delete(temp_ctl_clim)
	delete(temp_exp_clim)
	delete(temp_ctl_6dmean)
	delete(temp_exp_6dmean)
	delete(std_ctl)
	delete(std_exp)
	delete(std_ctl_full)
	delete(std_exp_full)
	delete(ind_seas_ctl) ;remove when updated Tmin from GC1A85
	delete(ind_seas_exp)

	end do ;season

	omega_ctl@long_name = "~F33~W~F21~~B~" +Var+"~N~ "+Ctlname+" " +AStart+"-"+AEnd
	omega_ctl@units = "-"
	omega_ctl!0="time"
	omega_ctl!1="lat"
	omega_ctl!2="lon"
	omega_ctl&lon = temp_ctl&lon
	omega_ctl&lat = temp_ctl&lat

	omega_exp@long_name = "~F33~W~F21~~B~" +Var+"~N~ "+Expname+" " +AStart+"-"+AEnd
	omega_exp@units = "-"
	omega_exp!0="time"
	omega_exp!1="lat"
	omega_exp!2="lon"
	omega_exp&lon = temp_ctl&lon
	omega_exp&lat = temp_ctl&lat

	coup@long_name = "~F33~D~F33~W~F21~~B~"+Var+"~N~ "+Expname+ " " +AStart+"-"+AEnd
	coup@units = "-"
	coup!0="time"
	coup!1="lat"
	coup!2="lon"
	coup&lon = temp_ctl&lon
	coup&lat = temp_ctl&lat

	if (Model .eq. "IPSL" ) then
	   omega_ctl=lonFlip(omega_ctl)
	   omega_exp=lonFlip(omega_exp)
	   coup=lonFlip(coup)
	end if
;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;
	;Levels for contours
        if (Var.eq."PR") then
        cnLevels=(/-0.10,-0.05,0.03,0.06,0.09,0.12,0.15,0.18/)
        pltColor = "coup_str_pr_8lev"
        else
	cnLevels=(/-0.12,-0.08,-0.04,0.04,0.08,0.12,0.16,0.20,0.24,0.28,0.32,0.36,0.40,0.44/)
	pltColor="coup_str_temp_14lev"
	end if

region = True

;-----------------------------------------------------------------------------;
; Plot set-up
;-----------------------------------------------------------------------------;

  pltName = outDir + "coupstr_GCCMIP5_AMZ_"+Runname2+ "_" +Runname1+ "_" \
	+Var+ "_" +AStart+ "-" +AEnd

  wks = gsn_open_wks(plttype,pltName)

;----------ColorMap-----------------;

  gsn_define_colormap(wks,pltColor)

;----------GeneralResources---------;

  res = True

  res@gsnFrame             = False           ; Don't advance the frame
  res@gsnDraw              = False           ; only for Panel plots
                                             ; switch off for individual plots
  res@gsnStringFontHeightF = 0.03            ; changes font height of all labels simultaneously

  res@cnFillOn             = True            ; Turn on contour fill.
  res@cnFillMode           = "CellFill"    ; Pixels

        if (region) then
                res@cnRasterSmoothingOn  = True
        end if

  res@cnLineLabelsOn       = False            ; Turn off line labels.
  res@cnInfoLabelOn        = False            ; Turn off info label.

  res@cnFillDrawOrder      = "PreDraw"   ; Predraw cn so mask will appear
                                                       ; (if land transparent ow land will also be masked)

  res@cnLinesOn            = False             ; no contour lines

  res@gsnSpreadColors      = True              ;
  res@lbLabelBarOn         = False             ;
  res@lbLabelAutoStride    = True              ;

  res@gsnLeftString        = ""                ;
  res@gsnRightString       = ""                ;


  res@cnLevelSelectionMode = "ExplicitLevels"  ; Define your own
  res@cnLevels = cnLevels
;----------Region ------------------

        if (.not.region) then
                Area = "Global"
                latS = -60
                latN =  80
                lonL =   0
                lonR = 360
        else
 ;              Area = "Australia"
;               latS = -45
;               latN = -10
;               lonL = 110
;               lonR = 160
                Area = "Amazonia"
                latS = -30
                latN = 10
                lonL = 275
                lonR = 328
        end if

  res@mpMinLatF  = latS
  res@mpMaxLatF  = latN
  res@mpMinLonF  = lonL
  res@mpMaxLonF  = lonR

        if(region) then
                res@gsnAddCyclic = False
        end if
  res@mpCenterLonF         = -180.             ; center on dateline

;----------Mask --------------------
res@mpFillOn             = True
res@mpLandFillColor      = "transparent"

;mask ocean
                res@mpOceanFillColor     = "white"
;----------Titles ------------------

  res@tiMainOn      = False
  ;res@tiXAxisString = "Longitude"
  ;res@tiYAxisString = "Latitude"

;---------- Plot -------------------
;plot panels

  plot = new(4,graphic)
  dum = new(4,graphic)

;loop over season
do j=0,3
   if (j.eq.0) then
          res@tmXBLabelsOn = False
          res@tmYLLabelFontHeightF = 0.015
   else if (j.eq.1) then
          res@tmYLLabelsOn = False
          res@tmXBLabelFontHeightF = 0.015
   else if (j.eq.2) then
          res@tmXBLabelsOn = True
          res@tmYLLabelsOn = True
   res@tmXBLabelFontHeightF = 0.015
   res@tmYLLabelFontHeightF = 0.015

   else if (j.eq.3) then
          res@tmYLLabelsOn = False
          res@tmXBLabelFontHeightF = 0.015
   end if
   end if
   end if
   end if


        plot(j) = gsn_csm_contour_map(wks,coup(j,{latS:latN},{lonL:lonR}),res)

;regions to be plotted
areadir = "/home/z3441306/scripts/plot_scripts/areas_txt/"

area1 = "AMZ_WC"
area2 = "AMZ_SC"

   area2_corners = asciiread(areadir+ area2+ ".txt",(/4,2/),"float")
   xl=(/area2_corners(0,0),area2_corners(1,0)/)
   yl=(/area2_corners(0,1),area2_corners(1,1)/)
   pkres=True
   pkres@gsLineThicknessF=2.5
   pkres@gsLineColor=1
   str1 = unique_string("polyline")
   plot@$str1$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

   delete(xl)
   delete(yl)
   xl=(/area2_corners(1,0),area2_corners(2,0)/)
   yl=(/area2_corners(1,1),area2_corners(2,1)/)
   str2 = unique_string("polyline")
   plot@$str2$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

   delete(xl)
   delete(yl)
   xl=(/area2_corners(2,0),area2_corners(3,0)/)
   yl=(/area2_corners(2,1),area2_corners(3,1)/)
   str3 = unique_string("polyline")
   plot@$str3$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

   delete(xl)
   delete(yl)
   xl=(/area2_corners(3,0),area2_corners(0,0)/)
   yl=(/area2_corners(3,1),area2_corners(0,1)/)
   str4 = unique_string("polyline")
   plot@$str4$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

   delete(xl)
   delete(yl)

 ;AMZ_WC
 area1_corners = asciiread(areadir+ area1+ ".txt",(/4,2/),"float")
 xl=(/area1_corners(0,0),area1_corners(1,0)/)    ;(/90,145/)
 yl=(/area1_corners(0,1),area1_corners(1,1)/)    ;(/60,60/)
 pkres=True
 pkres@gsLineThicknessF=2.5
 pkres@gsLineColor=1
 str5 = unique_string("polyline")
 plot@$str5$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

 delete(xl)
 delete(yl)
 xl=(/area1_corners(1,0),area1_corners(2,0)/)    ;(/145,145/)
 yl=(/area1_corners(1,1),area1_corners(2,1)/)    ;(/60,20/)
 str6 = unique_string("polyline")
 plot@$str6$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

 delete(xl)
 delete(yl)
 xl=(/area1_corners(2,0),area1_corners(3,0)/)    ;(/145,90/)
 yl=(/area1_corners(2,1),area1_corners(3,1)/)    ;(/20,20/)
 str7 = unique_string("polyline")
 plot@$str7$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

 delete(xl)
 delete(yl)
 xl=(/area1_corners(3,0),area1_corners(0,0)/)    ;(/90,90/)
 yl=(/area1_corners(3,1),area1_corners(0,1)/)    ;(/20,60/)
 str8 = unique_string("polyline")
 plot@$str8$=gsn_add_polyline(wks,plot(j),xl,yl,pkres)

end do ;seasons

;---------- Paneled Plot-------------

  panel_res                       = True
  panel_res@gsnMaximize           = True
  panel_res@gsnOrientation        = "landscape"
  panel_res@gsnPanelLabelBar      = True
  panel_res@lbAutoManage          = False
  panel_res@lbLabelFontHeightF    = 0.02
  panel_res@gsnPanelFigureStrings = (/"DJF","MAM","JJA","SON"/)
  panel_res@gsnPanelFigureStringsFontHeightF = 0.015
  panel_res@gsnPanelRowSpec       = True
  panel_res@pmLabelBarWidthF      = 0.9         ; makes label bar bigger
  panel_res@txString              = (coup@long_name + " [" + coup@units + "]")
  panel_res@txFontHeightF         = 0.025

  gsn_panel(wks,plot,(/2,2/),panel_res)

if(plttype .eq. "pdf")
        delete(wks)
        system("pdfcrop " + pltName + "." + plttype+ " "+ pltName + "." + plttype)
end if

;-----------------------------------------------------------------------------;
; save data to netcdf for further use
;-----------------------------------------------------------------------------;
outfile = outDir + "GCCMIP5_omega_coupstr_"+Runname2+ "_" +Runname1+ "_" \
        +Var+ "_" +AStart+ "-" +AEnd+ ".nc"
system("rm "+outfile)
ncdf = addfile(outfile, "c")
ncdf->omega_ctl=omega_ctl
ncdf->omega_exp=omega_exp
ncdf->coup=coup

;-----------------------------------------------------------------------------;
	delete(temp_ctl)
	delete(temp_exp)
	delete(temp_ctl_dtr)
	delete(temp_exp_dtr)
	delete(omega_ctl)
	delete(omega_exp)
	delete(time_ctl)
	delete(time_exp)
	delete(ind_yrs_ctl)
	delete(ind_yrs_exp)
	delete(exp_date)
	delete(ctl_date)
	delete(new_date_ctl)
	delete(new_date_exp)
	delete(cnLevels)
	delete(res@cnLevels)
	delete(pltColor)

end do ;variable
end ;script