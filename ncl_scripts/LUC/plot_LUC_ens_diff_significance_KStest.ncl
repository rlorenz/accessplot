;-----------------------------------------------------------------------------;
; File Name : plot_LUC_diff_significance_KStest.ncl
; Creation Date : 23-01-2014
; Last Modified : Thu 15 May 2014 15:05:28 EST
; Created By : Ruth Lorenz
; Purpose : Plot differences of LUC experiments with significance

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_seas_panel_overlay_significance_LUC.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_single_overlay_significance_LUC.ncl"
begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = "ctl_ens_lcc"     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME

Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))           ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin,PR
;-----------------------------------------------------------------------------;
if ( Region .eq. "sc" ) then
   runids = (/"121sc_ens_lcc"/)
   runnames = (/"121GPsc_ens"/)
else if ( Region .eq. "wc" ) then
   runids = (/"121wc_ens_lcc"/)
   runnames = (/"121GPwc_ens"/)
else print("wrong region name, does not exist")
end if
end if

varname = (/"pr","tas","tasmax","tasmin","zg","psl","rss","rls","rnet","hfls","alb","cancd"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","GPH@300hPa","MSLP","SW~B~NET~N~","LW~B~NET~N~","R~B~NET~N~","LH","ALB","CAN"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","m","hPa","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","-","ms~S~-1~N~"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;start loop
do v = 0,11
   run = 0
   if ( v .eq. 0 ) then
      Var="PR"
      print("reading NetCDFs")

      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="orange_to_blue_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.6,-0.2,0.2,0.6,1,1.5,2,2.5,3/)
   else if ( v .eq. 1 ) then
      Var="T"
      print("reading NetCDFs")

      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 2 ) then
      Var="TX"
      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")

      file2 = addfile(workDir +runids(run)+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 3 ) then
      Var="TN"
      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")

      file2 = addfile(workDir +runids(run)+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 4 ) then
      Var="GPH"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
         file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
        end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-10,-8,-6,-4,-2,-1,-0.5,0.5,1,2,4,6,8,10/)
   else if ( v .eq. 5 ) then
      Var="MSLP"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
         file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
        end if
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev = (/-1.5e08,-1e08,-1.5e07,-1e07,-1.5e06,-1e06,1e06,1.5e06,1e07,1.5e07,1e08,1.5e08/)
   else if ( v .eq. 6 ) then
      Var="SW"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
   else if ( v .eq. 7 ) then
      Var="LW"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
   else if ( v .eq. 8 ) then
      Var="RN"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir  +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
  else if ( v .eq. 9 ) then
      Var="LH"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="precip_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
  else if ( v .eq. 10 ) then
      Var="ALB"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_loud"
      lev = (/-0.1,-0.08,-0.06,-0.04,-0.02,-0.01,0.01,0.02,0.04,0.06,0.08,0.1/)
  else if ( v .eq. 11 ) then
      Var="CC"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monthly_TS." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".monthly_TS." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev =(/-0.01,-0.008,-0.006,-0.004,-0.002,-0.001,0.001,0.002,0.004,0.006,0.008,0.01/)
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;
if ( run .eq. 0 ) then
nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

;derive time axis
time_ctl = file1->time
time_ctl@calendar = "gregorian"
ctl_date = ut_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
lat = file1->lat
lon = file1->lon

time_exp = file2->time
time_exp@calendar = "gregorian"
exp_date = ut_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=exp_date(ind_yrs_exp,:)
end if

if (Var .eq. "PR") then	;convert prec to mm/day
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig*(24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "MSLP") then  ;convert mslp to hPa
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig/100
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "RN") then	;add SW and LW to Rnet
      sw_ctl = file1 ->$varname(6)$(ind_yrs_ctl,:,:)
      sw_exp = file2 ->$varname(6)$(ind_yrs_exp,:,:)
      lw_ctl = file1 ->$varname(7)$(ind_yrs_ctl,:,:)
      lw_exp = file2 ->$varname(7)$(ind_yrs_exp,:,:)
      var_ctl=sw_ctl+lw_ctl
      copy_VarMeta(sw_ctl,var_ctl)
      var_exp=sw_exp+lw_exp
      copy_VarMeta(sw_exp,var_exp)
else if (Var .eq. "GPH") then ;only read at 300hPa
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,9,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,9,:,:)
      var_ctl!1="lat"
      var_ctl!2="lon"
      var_exp!1="lat"
      var_exp!2="lon"
else
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)
end if
end if
end if
end if

      var_ctl@long_name     = longname(v)
      var_ctl@units         = units(v)

      var_exp@long_name     = longname(v)
      var_exp@units         = units(v)

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(var_ctl)
dsize_exp=dimsizes(var_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
        print("ERROR: CTL and EXP do not have same dimension, exiting")
        exit
else
        ntim=ntim_ctl
        nlat=nlat_ctl
        nlon=nlon_ctl

        delete(ntim_ctl)
        delete(nlat_ctl)
        delete(nlon_ctl)
        delete(ntim_exp)
        delete(nlat_exp)
        delete(nlon_exp)
end if

;-----------------------------------------------------------------------------;
; detrend timeseries
;-----------------------------------------------------------------------------;
var_ctl_dtr=dtrend_leftdim(var_ctl,False)
var_exp_dtr=dtrend_leftdim(var_exp,False)

;-----------------------------------------------------------------------------;
; loop over seasons
;-----------------------------------------------------------------------------;
do j = 0,4
;season
        if (j .eq. 0) then
                seas="DJF"

                ind_seas_ctl=ind(new_date_ctl(:,1).eq.1 .or. \
                        new_date_ctl(:,1).eq.2 .or. new_date_ctl(:,1).eq.12)
                var_ctl_seas=(/var_ctl_dtr(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.1 .or. \
                        new_date_exp(:,1).eq.2 .or. new_date_exp(:,1).eq.12)
                var_exp_seas=(/var_exp_dtr(ind_seas_exp,:,:)/)

        else if (j .eq. 1) then
                seas="MAM"

                ind_seas_ctl=ind(new_date_ctl(:,1).eq.3 .or. \
                        new_date_ctl(:,1).eq.4 .or. new_date_ctl(:,1).eq.5)
                var_ctl_seas=(/var_ctl_dtr(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.3 .or. \
                        new_date_exp(:,1).eq.4 .or. new_date_exp(:,1).eq.5)
                var_exp_seas=(/var_exp_dtr(ind_seas_exp,:,:)/)

        else if (j .eq. 2) then
                seas="JJA"
                ind_seas_ctl=ind(new_date_ctl(:,1).eq.6 .or. \
                        new_date_ctl(:,1).eq.7 .or. new_date_ctl(:,1).eq.8)
                var_ctl_seas=(/var_ctl_dtr(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.6 .or. \
                        new_date_exp(:,1).eq.7 .or. new_date_exp(:,1).eq.8)
                var_exp_seas=(/var_exp_dtr(ind_seas_exp,:,:)/)

        else if (j .eq. 3) then
                seas="SON"

                ind_seas_ctl=ind(new_date_ctl(:,1).eq.9 .or. \
                        new_date_ctl(:,1).eq.10 .or. new_date_ctl(:,1).eq.11)
                var_ctl_seas=(/var_ctl_dtr(ind_seas_ctl,:,:)/)

                ind_seas_exp=ind(new_date_exp(:,1).eq.9 .or. \
                        new_date_exp(:,1).eq.10 .or. new_date_exp(:,1).eq.11)
                var_exp_seas=(/var_exp_dtr(ind_seas_exp,:,:)/)
        else if (j .eq. 4) then
                seas="ANN"
                var_ctl_seas=(/var_ctl_dtr/)
                var_exp_seas=(/var_exp_dtr/)

        end if
        end if
        end if
        end if
        end if

var_ctl_seas!0="time"
var_ctl_seas!1="lat"
var_ctl_seas!2="lon"
var_ctl_seas&lon = var_ctl&lon
var_ctl_seas&lat = var_ctl&lat

var_exp_seas!0="time"
var_exp_seas!1="lat"
var_exp_seas!2="lon"
var_exp_seas&lon = var_exp&lon
var_exp_seas&lat = var_exp&lat

;-----------------------------------------------------------------------------;
; Calculating seasonal means
;-----------------------------------------------------------------------------;
var_ctl_seas_avg=dim_avg_n_Wrap(var_ctl_seas,0)
var_exp_seas_avg=dim_avg_n_Wrap(var_exp_seas,0)

;-----------------------------------------------------------------------------;
; Calculating differences
;-----------------------------------------------------------------------------;
if ( j .eq. 0) then
   var_diff_seas_avg=new((/5,nlat,nlon/),float)
end if

var_diff_seas_avg(j,:,:)= var_exp_seas_avg-var_ctl_seas_avg

var_diff_seas_avg@long_name= longname(v)+" "+runnames(run)+"-"+Runname1
var_diff_seas_avg@units    = units(v)
var_diff_seas_avg!0="time"
var_diff_seas_avg!1="lat"
var_diff_seas_avg!2="lon"
var_diff_seas_avg&lon = var_ctl&lon
var_diff_seas_avg&lat = var_ctl&lat

;-----------------------------------------------------------------------------;
; apply kolmogorov-smirnoff- two-sample test
;-----------------------------------------------------------------------------;
p=new((/nlat,nlon/),float)

if (Var.eq."PR") then
   pr_ctl_nozeros=where(var_ctl_seas.gt.0.0,var_ctl_seas,var_ctl_seas@_FillValue)
   pr_exp_nozeros=where(var_exp_seas.gt.0.0,var_exp_seas,var_exp_seas@_FillValue)

   do x=0,nlat-1
       do y=0,nlon-1
       	  p(x,y) = kolsm2_n(pr_ctl_nozeros(:,x,y),pr_exp_nozeros(:,x,y), 0)
       end do
   end do
   delete( [/pr_ctl_nozeros, pr_exp_nozeros/] )
else 
     do x=0,nlat-1
       do y=0,nlon-1
       	  p(x,y) = kolsm2_n(var_ctl_seas(:,x,y),var_exp_seas(:,x,y), 0)
       end do
   end do
end if

H=where(p .lt. sigr,1,0)

if ( j .eq. 0) then
   signif=new((/5,nlat,nlon/),integer)
   signif_land=new((/5,nlat,nlon/),integer)
end if

signif(j,:,:)=(/H/)

   signif!0                 = "time"
   signif!1                 = "lat"
   signif!2                 = "lon"
   signif&lat               = var_ctl&lat
   signif&lon               = var_ctl&lon
   signif@lat   = signif&lat
   signif@lon   = signif&lon

if (Var.ne."GPH" .and. Var.ne."MSLP") then
print("mask ocean")
signif_land(j,:,:)=where(lsm(0,0,:,:).ne.0,signif(j,:,:),signif@_FillValue)
else
signif_land(j,:,:)=signif(j,:,:)
end if

nr_sig_seas = num(signif_land(j,:,:).eq.1)

;-----------------------------------------------------------------------------;
; save data
;-----------------------------------------------------------------------------;
outDir = systemfunc("echo $OUTDIR")+runnames(run) +"/"

;save nr of signif grid points to file
filo=outDir+"nr_signif_KS_gp_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_"+seas+".txt"
system("rm "+filo)
asciiwrite(filo,nr_sig_seas)

delete(nr_sig_seas)

;just region in Amazonia where change applied
if (j.eq.0) then
   signif_AMZ=new((/5,nlat,nlon/),integer)
   signif_AMZ!0         = "time"
   signif_AMZ!1             = "lat"
   signif_AMZ!2             = "lon"
   signif_AMZ&lat           = var_ctl&lat
   signif_AMZ&lon           = var_ctl&lon
end if
if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_AMZ(j,:,:)=where(lsm(0,0,:,:).ne.0,signif(j,:,:),signif@_FillValue)
else
signif_AMZ(j,:,:)=signif(j,:,:)
end if

if (Region .eq. "sc") then
	latS = -13.75
   	latN = -1.25
   	lonL = 301.875 
   	lonR = 320.625
 else if ( Region .eq. "wc") then
	latS = -8.75
   	latN = 3.75
   	lonL = 281.25
   	lonR = 300
else
 print("Wrong region")
end if
end if

signif_AMZ(j,{:latS-1.25},:)=signif_AMZ@_FillValue
signif_AMZ(j,{latN+1.25:},:)=signif_AMZ@_FillValue
signif_AMZ(j,:,{:lonL-1.875})=signif_AMZ@_FillValue
signif_AMZ(j,:,{lonR+1.875:})=signif_AMZ@_FillValue

nr_sig_seas_AMZ = num(signif_AMZ(j,:,:).eq.1)

;save nr of signif grid points to file
filo_AMZ=outDir+"nr_signif_KS_gp_AMZ_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_"+seas+".txt"
system("rm "+filo_AMZ)
asciiwrite(filo_AMZ,nr_sig_seas_AMZ)

delete(nr_sig_seas_AMZ)

delete(var_ctl_seas)
delete(var_exp_seas)
end do ;season

;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;
if (Var.ne."GPH" .and. Var.ne."MSLP") then
Panel_Plot_sig_LUC(outDir,"diff_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(0:3,:,:),signif(0:3,:,:),False,Region,True,lev,True)

Panel_Plot_sig_LUC(outDir,"amazdiff_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(0:3,:,:),signif(0:3,:,:),True,Region,True,lev,True)
else
Panel_Plot_sig_LUC(outDir,"diff_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(0:3,:,:),signif(0:3,:,:),False,Region,True,lev,False)

Panel_Plot_sig_LUC(outDir,"amazdiff_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(0:3,:,:),signif(0:3,:,:),True,Region,True,lev,False)
end if

if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_single_Plot_LUC(outDir,"diff_ANN_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(4,:,:),signif(4,:,:),False,Region,True,lev,True)

signif_single_Plot_LUC(outDir,"amazdiff_ANN_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(4,:,:),signif(4,:,:),True,Region,True,lev,True)
else
signif_single_Plot_LUC(outDir,"diff_ANN_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(4,:,:),signif(4,:,:),False,Region,True,lev,False)

signif_single_Plot_LUC(outDir,"amazdiff_ANN_KStest_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff_seas_avg(4,:,:),signif(4,:,:),True,Region,True,lev,False)
end if

delete(lev)
delete(var_diff_seas_avg)

delete(var_ctl)
delete(var_exp)
delete(var_ctl_dtr)
delete(var_exp_dtr)
delete(var_ctl_seas_avg)
delete(var_exp_seas_avg)

delete(p)
delete(H)
delete(signif)
delete(signif_AMZ)
delete(signif_land)

end do ;loop over variables
;-----------------------------------------------------------------------------;
end
