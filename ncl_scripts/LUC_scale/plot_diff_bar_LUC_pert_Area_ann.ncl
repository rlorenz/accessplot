;-----------------------------------------------------------------------------;
; File Name : plot_diff_bar_LUC_pert_Area_ann.ncl
; Creation Date : 13-09-2015
; Last Modified : Fri 20 Nov 2015 16:55:09 AEDT
; Created By : Ruth Lorenz
; Purpose : plot difference between CTL and LUC experiments,
;	use output modTtest from R, average over perturbed area
;	plot as bar plots, all exp in 1 graph and save data

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_single_overlay_significance_LUC.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;
Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
runnames = str_split(systemfunc("echo $EXPERIMENTS")," ")
Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $OUTDIR")

outDir = systemfunc("echo $OUTDIR")
system("if ! test -d " + outDir + "barplots  ; then mkdir -p " + outDir + "barplots ; fi")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))       ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over runs and variables
;-----------------------------------------------------------------------------;

varname = (/"pr","tas","tasmax","tasmin","rss","rls","hfls","hfss","rnet","prc1","alb","ta_tl","ts"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","SW~B~NET~N~","LW~B~NET~N~","LH","SH","R~B~NET~N~","P~B~CON~N~","Albedo","T~B~z0~N~","T~B~skin~N~"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","mm day~S~-1~N~","-","~S~o~N~C","~S~o~N~C"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm
lat = lsm_file->latitude
lon = lsm_file->longitude

nvar = dimsizes(varname)
nrun = dimsizes(runnames)

var_diff_avg=new((/nrun-1/),float)

;start loop
do v = 0,nvar-1
   do run = 1,nrun-1
      		print("reading NetCDFs")
      		file1 = addfile(workDir+runnames(run)+"/" +varname(v)+"_"+runnames(run)+  \
                     "_"+Runname1+"_ann_modTtest_" + AStart + "_" + AEnd + ".nc","r")

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;

	if (varname(v) .eq. "pr" .or. varname(v) .eq."prc1") then	;convert prec to mm/day
	      var_ctl_orig = file1 ->ctl_Ave
	      var_ctl=var_ctl_orig*(24*60*60)
	      copy_VarMeta(var_ctl_orig,var_ctl)

	      var_exp_orig = file1 ->exp_Ave
	      var_exp=var_exp_orig*(24*60*60)
	      copy_VarMeta(var_exp_orig,var_exp)
	else
	      var_ctl = file1 ->ctl_Ave
	      var_exp = file1 ->exp_Ave
	end if

	  signif  = floattoint(file1 ->Hval)
	  signif_Walk = floattoint(file1 ->FS_walk)
	  signif_FDR = floattoint(file1 ->FS_fdr)     

	var_ctl@long_name     = longname(v)
	;var_ctl@units         = units(v)

	var_exp@long_name     = longname(v)
	;var_exp@units         = units(v)


	;printVarSummary(var_ctl)
	;printVarSummary(var_exp)

	;check if ctl and exp time periods of same length
	dsize_ctl=dimsizes(var_ctl)
	dsize_exp=dimsizes(var_exp)

	nlat_ctl=dsize_ctl(0)
	nlon_ctl=dsize_ctl(1)

	nlat_exp=dsize_exp(0)
	nlon_exp=dsize_exp(1)

	if (  nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
		print("ERROR: CTL and EXP do not have same dimension, exiting")
		exit
	else
		nlat=nlat_ctl
		nlon=nlon_ctl

		delete(nlat_ctl)
		delete(nlon_ctl)
		delete(nlat_exp)
		delete(nlon_exp)
	end if

	;calculate difference
	var_diff= doubletofloat(var_exp-var_ctl)

	var_diff@long_name     = longname(v)+" "+runnames(run)+"-"+"CTL"
	var_diff@units         = units(v)
	var_diff!0="lat"
	var_diff!1="lon"
	var_diff&lon = lsm&longitude
	var_diff&lat = lsm&latitude

	signif!0="lat"
	signif!1="lon"
	signif&lon = lsm&longitude
	signif&lat = lsm&latitude

	;if (v .ne. 10) then
	signif_Walk!0                 = "lat"
	signif_Walk!1                 = "lon"
	signif_Walk&lat               = lsm&latitude
	signif_Walk&lon               = lsm&longitude

	signif_FDR!0                 = "lat"
	signif_FDR!1                 = "lon"
	signif_FDR&lat               = lsm&latitude
	signif_FDR&lon               = lsm&longitude
	;end if

	;mask ocean and insignificant grid points
	var_diff_tmp=where(lsm(0,0,:,:).ne.0,var_diff,var_diff@_FillValue)
    	var_diff_landsig=where(signif_FDR.ne.0,var_diff_tmp,var_diff_tmp@_FillValue)
	copy_VarMeta(var_diff,var_diff_landsig)

;-----------------------------------------------------------------------------;
; calculate average over perturbed region
;-----------------------------------------------------------------------------;
	
	if (Region .eq. "wc") then
		    lon1_dom=281.25
		    lon2_dom=300.0
		    lat1_dom=3.75
		    lat2_dom=-8.75
	else if (Region .eq. "sc") then
		    lon1_dom=301.875
		    lon2_dom=320.625
		    lat1_dom=-1.25
		    lat2_dom=-13.75
	end if
	end if

	if (runnames(run) .eq. "allAMZ") then
           lon1_dom=278.0
           lon2_dom=325.0
           lat1_dom=15.625
           lat2_dom=-18.125	   
	end if
	
	;find indices
	ind_lon_dom=ind(lon.ge.lon1_dom .and. lon.le.lon2_dom)
	ind_lat_dom=ind(lat.le.lat1_dom .and. lat.ge.lat2_dom)
    	lon_dom = lon(ind_lon_dom)
    	lat_dom = lat(ind_lat_dom)

	if  (runnames(run) .eq. "001GPwc" .or. runnames(run) .eq. "001GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(5)
              lat_gd_ch_dom=lat_dom(5)
	      ind_lon_ch_dom=ind(lon.eq.lon_gd_ch_dom)
              ind_lat_ch_dom=ind(lat.eq.lat_gd_ch_dom)
	else if (runnames(run) .eq. "009GPwc" .or. runnames(run) .eq. "009GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(4:6)
              lat_gd_ch_dom=lat_dom(4:6)
	else if (runnames(run) .eq. "025GPwc" .or. runnames(run) .eq. "025GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(3:7)
              lat_gd_ch_dom=lat_dom(3:7)
	else if (runnames(run) .eq. "081GPwc" .or. runnames(run) .eq. "081GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(1:9)
              lat_gd_ch_dom=lat_dom(1:9)
	else if (runnames(run) .eq. "121GPwc" .or. runnames(run) .eq. "121GPsc") then
              ;find gridpt indices in global grid
              lon_gd_ch_dom=lon_dom(0:10)
              lat_gd_ch_dom=lat_dom(0:10)
	      print(lon_gd_ch_dom)
	      print(lat_gd_ch_dom)
	else if (runnames(run) .eq. "242GP") then

	      lon1_dom=280.3125
	      lon2_dom=300.9375
	      lat1_dom=4.375
	      lat2_dom=-9.375

	      lon3_dom=300.9375
	      lon4_dom=321.5625
	      lat3_dom=-0.625
	      lat4_dom=-14.375

	end if
	end if
	end if
	end if
	end if
	end if

	;---Start with data all missing
    	data_mask = new(dimsizes(var_diff),typeof(var_diff),var_diff@_FillValue)
    	copy_VarMeta(var_diff,data_mask)

	;approximate index values that contain Amazonian region
	; This will make our gc_inout loop below go almost 4x
	; faster, because we're not checking every single
	; lat/lon point to see if it's within the area of
	; interest.
	;
	min_amz_lat = -25.0
	max_amz_lat = 20.0
	min_amz_lon = 275.0
	max_amz_lon = 330.0

	ilt_mn = ind(min_amz_lat.gt.lat)
	ilt_mx = ind(max_amz_lat.lt.lat)
	iln_mn = ind(min_amz_lon.gt.lon)
	iln_mx = ind(max_amz_lon.lt.lon)
	ilt1   = ilt_mn(dimsizes(ilt_mn)-1)    ; Start of lat box
	iln1   = iln_mn(dimsizes(iln_mn)-1)    ; Start of lon box
	ilt2   = ilt_mx(0)                     ; End of lat box
	iln2   = iln_mx(0)                     ; End of lon box

	;---Put data back in the areas that we don't want masked.
	if (run .eq. 1) then
	   data_mask(ind_lat_ch_dom,ind_lon_ch_dom) =  var_diff_landsig(ind_lat_ch_dom,ind_lon_ch_dom)
	   ;data_mask(ind_lat_ch_dom,ind_lon_ch_dom) =  var_diff(ind_lat_ch_dom,ind_lon_ch_dom)
	else
	    do ilt=ilt1,ilt2
	      do iln=iln1,iln2
	      	 if (runnames(run) .eq. "242GP") then
			if(gc_inout(lat(ilt),lon(iln),(/lat1_dom,lat1_dom,lat3_dom,lat3_dom,lat4_dom,lat4_dom,lat2_dom,lat2_dom/),(/lon1_dom,lon2_dom,lon3_dom,lon4_dom,lon4_dom,lon3_dom,lon2_dom,lon1_dom/))) then
		  	data_mask(ilt,iln) = var_diff_landsig(ilt,iln)
		  	;data_mask(ilt,iln) = var_diff(ilt,iln)
			end if
		 else if (runnames(run) .eq. "allAMZ") then
		      if(gc_inout(lat(ilt),lon(iln),(/lat1_dom,lat1_dom,lat2_dom,lat2_dom/),(/lon1_dom,lon2_dom,lon2_dom,lon1_dom/))) then
		      	data_mask(ilt,iln) = var_diff_landsig(ilt,iln)
		  	;data_mask(ilt,iln) = var_diff(ilt,iln)
			end if
		 else
			if(gc_inout(lat(ilt),lon(iln),(/min(lat_gd_ch_dom)-0.625,min(lat_gd_ch_dom)-0.625,max(lat_gd_ch_dom)+0.625,max(lat_gd_ch_dom)+0.625/),(/min(lon_gd_ch_dom)-0.9375,max(lon_gd_ch_dom)+0.9375,max(lon_gd_ch_dom)+0.9375,min(lon_gd_ch_dom)-0.9375/))) then
		  	data_mask(ilt,iln) = var_diff_landsig(ilt,iln)
		  	;data_mask(ilt,iln) = var_diff(ilt,iln)
			end if
		 end if
		 end if
	      end do
	    end do
	end if
	
	var_diff_avg(run-1)=wgt_areaave(data_mask,1,1,0)
	
;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;

	if (v .le. 5)
	   lev=(/-1.8,-1.4,-1,-0.6,-0.2,0.2,0.6,1,1.4,1.8/)
	else 
	     lev=(/-11,-9,-7,-5,-3,-1,1,3,5,7,9,11/)
	end if

    signif_single_Plot_LUC(outDir+runnames(run)+"/","diff_mask_for_bar_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,"blue_to_red_loud",data_mask(:,:),signif_FDR(:,:),True,Region,True,lev,False)

   delete(var_diff)

    delete(signif)
    delete(var_exp)
    delete(var_ctl)
    if (runnames(run) .ne."242GP" .and. runnames(run) .ne. "allAMZ") then
       delete(lon_gd_ch_dom)
       delete(lat_gd_ch_dom)
    end if
    delete(ind_lon_dom)
    delete(ind_lat_dom)
    delete(lon_dom)
    delete(lat_dom)

  end do ;run

  delete(lev)
  ;printVarSummary(var_diff_avg)
  ;printMinMax(var_diff_avg,True)
;-----------------------------------------------------------------------------;
; plot bar chart with all experiemnts
;-----------------------------------------------------------------------------;
        pltName = outDir + "barplots/diff_bar_pert_area_"+Region+"_" +varname(v) +"_ANN"
        wks = gsn_open_wks(plttype,pltName)

        res          = True                   ; plot mods desired

        res@gsnXYBarChart = True            ; Create bar plot
        res@gsnXYBarChartBarWidth = 0.2           ; change bar widths
        res@gsnYRefLine           = 0.              ; reference line
        res@gsnXYBarChartColors = (/"black"/)

        res@tmXBMode          = "Explicit"         ; explicit labels
        res@tmXBValues        = (/0,1,2,3,4,5,6/)
        res@tmXBLabels = (/"1GP","9GP","25GP","81GP","121GP","242GP","allAMZ"/)

	if ( varname(v) .eq. "alb" .or. varname(v) .eq. "hfss" .or. varname(v) .eq. "hfls" .or. varname(v) .eq. "rss" .or. varname(v) .eq. "rls" .or. varname(v) .eq. "rnet") then
           res@trYMinF = -10.0
           res@trYMaxF =   0.5
	else if ( varname(v) .eq. "tas" .or. varname(v) .eq. "tasmax" .or. varname(v) .eq. "tasmin") then
	   res@trYMinF = -1.2
           res@trYMaxF =   1.0	
	else if ( varname(v) .eq. "ta_tl" .or. varname(v) .eq. "ts") then
	   res@trYMinF = -0.5
           res@trYMaxF =   1.5	
	else
	   res@trYMinF = -1.5
           res@trYMaxF =   0.1	
	end if
	end if
	end if

	;res@gsnYRefLine           = 0.0             ; create a reference line 
  
        res@tiXAxisString = "Runnames, difference to CTL"
        res@tiYAxisString = longname(v)+ " ["+units(v)+"]"

        plot = gsn_csm_xy (wks,ispan(0,6,1),var_diff_avg,res)

; save result per variable for output
       runs = (/0,1,2,3,4,5,6/)
       if (varname(v) .eq. "alb") then
       	  alb = var_diff_avg
     	  alb@long_name = longname(v)  
	  alb@units = units(v)
	  printVarSummary(alb)
	  alb!0 = "run"
	  alb&run = runs
	else if (varname(v) .eq. "rnet") then
	     rnet = var_diff_avg
	     rnet@long_name = longname(v)  
	     rnet@units = units(v)
	     rnet!0 = "run"
	     rnet&run = runs
	else if (varname(v) .eq. "rss") then
	     rss = var_diff_avg
	     rss@long_name = longname(v)  
	     rss@units = units(v)
	     rss!0 = "run"
	     rss&run = runs
	else if (varname(v) .eq. "rls") then
	     rls = var_diff_avg
	     rls@long_name = longname(v)  
	     rls@units = units(v)
	     rls!0 = "run"
	     rls&run = runs
	else if (varname(v) .eq. "hfls") then
	     hfls = var_diff_avg
	     hfls@long_name = longname(v)  
	     hfls@units = units(v)
	     hfls!0 = "run"
	     hfls&run = runs
	else if (varname(v) .eq. "hfss") then
	     hfss = var_diff_avg
	     hfss@long_name = longname(v)  
	     hfss@units = units(v)
	     hfss!0 = "run"
	     hfss&run = runs
	else if (varname(v) .eq. "ta_tl") then
	     ta_tl = var_diff_avg
	     ta_tl@long_name = longname(v)  
	     ta_tl@units = units(v)
	     ta_tl!0 = "run"
	     ta_tl&run = runs
	else if (varname(v) .eq. "pr") then
	     pr = var_diff_avg
	     pr@long_name = longname(v)  
	     pr@units = units(v)
	     pr!0 = "run"
	     pr&run = runs
	end if
	end if
	end if
	end if
	end if
	end if
	end if
	end if

end do ;loop over variables

;===================================================================
;save data into data file for further plotting
;===================================================================
print("Save data to netcdf")
outfile = outDir +"data_LUC_pert_Area_"+Region+"_ann.nc"
print(outfile)
system("rm "+outfile)
ncdf = addfile(outfile, "c")

;===================================================================
; create global attributes of the file (optional)
;===================================================================
fAtt               = True            ; assign file attributes
fAtt@title         = "netcdf written from ncl for LUC changes in perturbed areas for barplots, "
fAtt@source_file   =  "plot_diff_bar_LUC_pert_Area_ann.ncl"
fAtt@Conventions   = "None"
fAtt@creation_date = systemfunc ("date")
fileattdef( ncdf, fAtt )            ; copy file attributes

;===================================================================
; output variables directly; NCL will call appropriate functions
; to write the meta data associated with each variable
;===================================================================
ncdf->ta_tl  =  ta_tl                                  
ncdf->pr  =  pr                                  
ncdf->alb = alb
ncdf->rnet = rnet
ncdf->rss = rss
ncdf->rls = rls
ncdf->hfss = hfss
ncdf->hfls = hfls

;-----------------------------------------------------------------------------;
end
