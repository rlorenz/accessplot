#!/bin/bash
# File Name : plot_LUX_exp_significance.sh
# Creation Date : 22/01/2014
# Last Modified : Wed 21 May 2014 15:58:26 EST
# Created By : Ruth Lorenz
# Purpose : prepare plotting of LUC experiment differences
#	with significance testing	

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID1=uaoyr
export RUNNAME1=CTL

#export REGION=sc

export R1_START=1978 #run start RUNID1
export R1_STOP=2011  #run end RUNID1
export R2_START=1978 #run start RUNID1
export R2_STOP=2011  #run end RUNID1

export A_START=1981 #analysis start
export A_STOP=2011  #analysis end

export pltType=pdf

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
    export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/$RUNID1/timeseries/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_plot without CTL input data"
    exit 1
fi

export OUTDIR=$DATAPATH/ACCESS_plots/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export siglev=0.05
export RES=n96
export NTILES=17
export NSOIL=6

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "CTL run: " $RUNID1 "with files in Directory " $CTLDIR
    echo "Experiments with files in Directories "$PATHCTL "/uaoya-p/timeseries/"
    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "Start Time:" $date
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------
module load gcc/4.7.2
module load ncl/6.2.0
module load netcdf
module load cdo
#module load R/3.0.3


if [[ ! -f "$WORKDIR/$RUNID1.yearly_means.${R1_START}_${R1_STOP}.nc" ]]; then
        cp $CTLDIR/$RUNID1.yearly_means.${R1_START}_${R1_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUNID1.yearly_means.${R1_START}_${R1_STOP}.nc.gz
fi

for RUN in uaoya uaoyb uaoyc uaoyd uaoye uaoyf uaoyg uaoyh uaoyi uaoyj uaoyk uaoyl uaoym uaoyn uaoyo uaoyp vacdq vacdt

do

    if [[ ! -f "$WORKDIR/$RUN.yearly_means.${R1_START}_${R1_STOP}.nc" ]]; then
        cp $PATHCTL/$RUN/timeseries/$RUN.yearly_means.${R1_START}_${R1_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUN.yearly_means.${R1_START}_${R1_STOP}.nc.gz
    fi

done

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"

ncl $HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_P-E_latAvg_allExp_ANN.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
