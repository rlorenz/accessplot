;-----------------------------------------------------------------------------;
; File Name : plot_LUC_diff_significance_ANN.ncl
; Creation Date : 23-01-2014
; Last Modified : Thu 17 Apr 2014 01:12:30 EST
; Created By : Ruth Lorenz
; Purpose : Plot differences of LUC experiments with significance for annual means

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/modTtest_small_ANN.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_single_overlay_significance_LUC.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME
Runid2 = systemfunc("echo $RUNID2")     ; RUNID
Runname2 = systemfunc("echo $RUNNAME2") ; RUNNAME
Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $WORKDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))           ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over  Tmean,Tmax,Tmin,PR
;-----------------------------------------------------------------------------;
if ( Region .eq. "sc" ) then
   runids = (/"uaoyc","uaoye","uaoyg","uaoyi","uaoyk","uaoym","uaoyo","uaoya"/)
   runnames = (/"001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc"/)
else if ( Region .eq. "wc" ) then
   runids = (/"uaoyd","uaoyf","uaoyh","uaoyj","uaoyl","uaoyn","uaoyp","uaoyb"/)
   runnames = (/"001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc"/)
else print("wrong region name, does not exist")
end if
end if

varname = (/"pr","tas","tasmax","tasmin","rss","rls","rnet","hfls","alb", \
	"cancd","zg","psl"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","SW~B~NET~N~",\
	 "LW~B~NET~N~","R~B~NET~N~","LH","ALB","CAN","GPH@300hPa","MSLP"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","Wm~S~-2~N~",\
      "Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","-","ms~S~-1~N~","m","hPa"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;start loop
do v = 1,11
   do run = 0,7
   if ( v .eq. 0 ) then
      Var="PR"
      print("reading NetCDFs")

      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="orange_to_blue_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.6,-0.2,0.2,0.6,1,1.5,2,2.5,3/)
   else if ( v .eq. 1 ) then
      Var="T"
      print("reading NetCDFs")

      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
  else if ( v .eq. 2 ) then
      Var="TX"
      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")

      file2 = addfile(workDir +runids(run)+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 3 ) then
      Var="TN"
      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".monmean_tmax_tmin." \
                       + R1Start + "_" + R1End + ".nc","r")

      file2 = addfile(workDir +runids(run)+ ".monmean_tmax_tmin." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-3,-2.5,-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2,2.5,3/)
   else if ( v .eq. 4 ) then
      Var="SW"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
   else if ( v .eq. 5 ) then
      Var="LW"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
   else if ( v .eq. 6 ) then
      Var="RN"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir  +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="sunshine_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
  else if ( v .eq. 7 ) then
      Var="LH"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="precip_diff_12lev"
      lev = (/-20,-15,-10,-7.5,-5,-2.5,2.5,5,7.5,10,15,20/)
  else if ( v .eq. 8 ) then
      Var="ALB"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_loud"
      lev = (/-0.1,-0.08,-0.06,-0.04,-0.02,-0.01,0.01,0.02,0.04,0.06,0.08,0.1/)
  else if ( v .eq. 9 ) then
      Var="CC"

      print("reading NetCDFs")
      file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev =(/-0.01,-0.008,-0.006,-0.004,-0.002,-0.001,0.001,0.002,0.004,0.006,0.008,0.01/)
   else if ( v .eq. 10 ) then
      Var="GPH"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
         file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
        end if
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev=(/-10,-8,-6,-4,-2,-1,-0.5,0.5,1,2,4,6,8,10/)
   else if ( v .eq. 11 ) then
      Var="MSLP"
      print("reading NetCDFs")
      if ( run .eq. 0 ) then
         file1 = addfile(workDir +Runid1+ ".yearly_means." \
                       + R1Start + "_" + R1End + ".nc","r")
        end if
      file2 = addfile(workDir +runids(run)+ ".yearly_means." \
                       + R2Start + "_" + R2End + ".nc","r")
      pltColor="blue_to_red_18lev"
      lev = (/-1.5e08,-1e08,-1.5e07,-1e07,-1.5e06,-1e06,1e06,\
      	  1.5e06,1e07,1.5e07,1e08,1.5e08/)

   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if
   end if

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;
if ( run .eq. 0 ) then
nyears=stringtointeger(AEnd)-stringtointeger(AStart)+1

;derive time axis
time_ctl = file1->time
time_ctl@calendar = "gregorian"
ctl_date = ut_calendar(time_ctl,0) ;(:,0)->years, (:,1)->months, (:,2)->days
lat = file1->lat
lon = file1->lon

time_exp = file2->time
time_exp@calendar = "gregorian"
exp_date = ut_calendar(time_exp,0) ;(:,0)->years, (:,1)->months, (:,2)->days

;cut into analysis period
ind_yrs_ctl=ind(ctl_date(:,0).ge.AStart .and. ctl_date(:,0).le.AEnd)
new_date_ctl=ctl_date(ind_yrs_ctl,:)

ind_yrs_exp=ind(exp_date(:,0).ge.AStart .and. exp_date(:,0).le.AEnd)
new_date_exp=exp_date(ind_yrs_exp,:)
end if

if (Var .eq. "PR") then	;convert prec to mm/day
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig*(24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "MSLP") then  ;convert mslp to hPa
      var_ctl_orig = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_ctl=var_ctl_orig/100
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file2 ->$varname(v)$(ind_yrs_exp,:,:)
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)
else if (Var .eq. "RN") then	;add SW and LW to Rnet
      sw_ctl = file1 ->$varname(4)$(ind_yrs_ctl,:,:)
      sw_exp = file2 ->$varname(4)$(ind_yrs_exp,:,:)
      lw_ctl = file1 ->$varname(5)$(ind_yrs_ctl,:,:)
      lw_exp = file2 ->$varname(5)$(ind_yrs_exp,:,:)
      var_ctl=sw_ctl+lw_ctl
      copy_VarMeta(sw_ctl,var_ctl)
      var_exp=sw_exp+lw_exp
      copy_VarMeta(sw_exp,var_exp)
else if (Var .eq. "GPH") then ;only read at 300hPa
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,9,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,9,:,:)
      var_ctl!1="lat"
      var_ctl!2="lon"
      var_exp!1="lat"
      var_exp!2="lon"
else
      var_ctl = file1 ->$varname(v)$(ind_yrs_ctl,:,:)
      var_exp = file2 ->$varname(v)$(ind_yrs_exp,:,:)
end if
end if
end if
end if

if (Var .eq. "TX" .or. Var .eq. "TN") then ;calculate yearly means from monthly means
   var_ctl_tmp=month_to_annual(var_ctl, 1)
   var_exp_tmp=month_to_annual(var_exp, 1)
   delete([/var_ctl,var_exp/])
   var_ctl=var_ctl_tmp
   var_exp=var_exp_tmp
   var_ctl!0="time"
   var_exp!0="time"
end if

      var_ctl@long_name     = longname(v)
      var_ctl@units         = units(v)

      var_exp@long_name     = longname(v)
      var_exp@units         = units(v)

;check if ctl and exp time periods of same length
dsize_ctl=dimsizes(var_ctl)
dsize_exp=dimsizes(var_exp)
ntim_ctl=dsize_ctl(0)
nlat_ctl=dsize_ctl(1)
nlon_ctl=dsize_ctl(2)
ntim_exp=dsize_exp(0)
nlat_exp=dsize_exp(1)
nlon_exp=dsize_exp(2)

if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
        print("ERROR: CTL and EXP do not have same dimension, exiting")
        exit
else
        ntim=ntim_ctl
        nlat=nlat_ctl
        nlon=nlon_ctl

        delete(ntim_ctl)
        delete(nlat_ctl)
        delete(nlon_ctl)
        delete(ntim_exp)
        delete(nlat_exp)
        delete(nlon_exp)
end if

;-----------------------------------------------------------------------------;
; Calculating differences
;-----------------------------------------------------------------------------;
var_exp_avg=dim_avg_n(var_exp,0)
var_ctl_avg=dim_avg_n(var_ctl,0)

var_diff= var_exp_avg-var_ctl_avg

var_diff@long_name= longname(v)+" "+runnames(run)+"-"+Runname1
var_diff@units    = units(v)
var_diff!0="lat"
var_diff!1="lon"
var_diff&lon = var_ctl&lon
var_diff&lat = var_ctl&lat

;-----------------------------------------------------------------------------;
; Test if differences statistically significant with modified t-test
;-----------------------------------------------------------------------------;

 ctl_tmp = var_ctl(lat|:,lon|:,time|:)	;reorder but do it only once [temporary]
 exp_tmp = var_exp(lat|:,lon|:,time|:)
    
 ctlAve = dim_avg (ctl_tmp)              ; calculate means at each grid point 
 expAve = dim_avg (exp_tmp)
 ctlVar = dim_variance (ctl_tmp)         ; calculate variances
 expVar = dim_variance (exp_tmp)

 ctlEqv = equiv_sample_size(ctl_tmp, sigr,0)
 expEqv = equiv_sample_size(exp_tmp, sigr,0)

; if N<30 we need to use modified t-test for small nr. (look up tables)
;use the cosine of the latitides for weight
  rad    = 4.0*atan(1.0)/180.0
  clat1   = cos(var_ctl&lat*rad)

  AvgctlEqv = wgt_areaave (ctlEqv, clat1, 1., 0)
  AvgexpEqv = wgt_areaave (expEqv, clat1, 1., 0) 

  VarAvgctl = wgt_areaave (ctlVar, clat1, 1., 0)
  VarAvgexp = wgt_areaave (expVar, clat1, 1., 0)

if ( AvgctlEqv .gt. 30 ) .and. ( AvgexpEqv .gt. 30 )
   probf=ftest(VarAvgctl,AvgctlEqv,VarAvgexp,AvgexpEqv,0)
   if ( probf .ge. 0.1 ) then
      iflag = False
   else
	iflag = True  
   end if 

   prob = ttest(ctlAve,ctlVar,AvgctlEqv,expAve,expVar,AvgexpEqv,iflag,False) 
   H=where(prob .lt. sigr,1,0)

else
   test_small=modTtest_small_ANN(var_ctl,var_exp,sigr,ntim)
   H=test_small(0,:,:)
   prob=test_small(1,:,:)
end if

   signif=new((/nlat,nlon/),typeof(H))
   signif_land=new((/nlat,nlon/),typeof(H))
   signif_Walk=new((/nlat,nlon/),typeof(H))
   signif_FDR=new((/nlat,nlon/),typeof(H))

signif(:,:)=(/H/)

   signif!0		    = "lat"
   signif!1		    = "lon"
   signif&lat		    = var_ctl&lat
   signif&lon		    = var_ctl&lon
   signif@lat	= signif&lat
   signif@lon	= signif&lon

if (Var.ne."GPH" .and. Var.ne."MSLP") then
print("mask ocean")
signif_land(:,:)=where(lsm(0,0,:,:).ne.0,signif(:,:),signif@_FillValue)
else
signif_land(:,:)=signif(:,:)
end if

nr_sig_seas = num(signif_land(:,:).eq.1)

;test for field significance with Walker-test
K=nlat*nlon
p_walk=1-(1-sigr)^(1/K)
p_min=min(prob)

if ( p_min .ge. p_walk) then
   signif_Walk(:,:)=0
else
   signif_Walk(:,:)=(/signif_land(:,:)/)
end if
   signif_Walk!0                 = "lat"
   signif_Walk!1                 = "lon"
   signif_Walk&lat               = var_ctl&lat
   signif_Walk&lon               = var_ctl&lon

;False Discovery Rate
pj=new((/nlat*nlon/),float)
prob_1D=ndtooned(prob)
p_sort= dim_pqsort(prob_1D,2)

do k=0,K-1
	if (p_sort(k).le.(sigr*(k/K))) then
		pj(k)=prob_1D(k)
	else
		pj(k)=pj@_FillValue
	end if
end do

p_FDR = max(pj)
;print(p_FDR)
;printMinMax(prob,True)
signif_FDR(:,:)=where(prob.le.p_FDR,signif_land(:,:),signif_land@_FillValue)
   signif_FDR!0                 = "lat"
   signif_FDR!1                 = "lon"
   signif_FDR&lat               = var_ctl&lat
   signif_FDR&lon               = var_ctl&lon

outDir = systemfunc("echo $OUTDIR")+runnames(run) +"/"

;just region in Amazonia where change applied
   signif_AMZ=new((/nlat,nlon/),typeof(H))
   signif_AMZ!0             = "lat"
   signif_AMZ!1             = "lon"
   signif_AMZ&lat           = var_ctl&lat
   signif_AMZ&lon           = var_ctl&lon

if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_AMZ(:,:)=where(lsm(0,0,:,:).ne.0,signif(:,:),signif@_FillValue)
else
signif_AMZ(:,:)=signif(:,:)
end if

if (Region .eq. "sc") then
	latS = -13.75
   	latN = -1.25
   	lonL = 301.875 
   	lonR = 320.625
 else if ( Region .eq. "wc") then
	latS = -8.75
   	latN = 3.75
   	lonL = 281.25
   	lonR = 300
else
 print("Wrong region")
end if
end if

signif_AMZ({:latS-1.25},:)=signif_AMZ@_FillValue
signif_AMZ({latN+1.25:},:)=signif_AMZ@_FillValue
signif_AMZ(:,{:lonL-1.875})=signif_AMZ@_FillValue
signif_AMZ(:,{lonR+1.875:})=signif_AMZ@_FillValue

;global minus region in Amazonia where change applied
   signif_SAminusAMZ=new((/nlat,nlon/),typeof(H))
   signif_SAminusAMZ!0             = "lat"
   signif_SAminusAMZ!1             = "lon"
   signif_SAminusAMZ&lat           = var_ctl&lat
   signif_SAminusAMZ&lon           = var_ctl&lon

if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_SAminusAMZ(:,:)=where(lsm(0,0,:,:).ne.0,signif(:,:),signif@_FillValue)
else
signif_SAminusAMZ(:,:)=signif(:,:)
end if

;mask domain
ind_lat=ind(signif_SAminusAMZ&lat.ge.latS .and. signif_SAminusAMZ&lat.le.latN)
ind_lon=ind(signif_SAminusAMZ&lon.ge.lonL .and. signif_SAminusAMZ&lon.le.lonR)
signif_SAminusAMZ(ind_lat,ind_lon)=signif_SAminusAMZ@_FillValue
;mask outside of South America
signif_SAminusAMZ({:-30-1.25},:)=signif_SAminusAMZ@_FillValue
signif_SAminusAMZ({10+1.25:},:)=signif_SAminusAMZ@_FillValue
signif_SAminusAMZ(:,{:275-1.875})=signif_SAminusAMZ@_FillValue
signif_SAminusAMZ(:,{328+1.875:})=signif_SAminusAMZ@_FillValue

nr_sig=num(signif_land.eq.1)
nr_sig_Walk=num(signif_Walk.eq.1)
nr_sig_FDR=num(signif_FDR.eq.1)
nr_sig_AMZ=num(signif_AMZ.eq.1)
nr_sig_SAminusAMZ=num(signif_SAminusAMZ.eq.1)

;save nr of signif grid points to file
filo=outDir+"nr_signif_gp_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_ANN"+".txt"
system("rm "+filo)
asciiwrite(filo,nr_sig)

filo=outDir+"nr_signif_gp_Walk_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_ANN"+".txt"
system("rm "+filo)
asciiwrite(filo,nr_sig_Walk)

filo=outDir+"nr_signif_gp_FDR_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_ANN"+".txt"
system("rm "+filo)
asciiwrite(filo,nr_sig_FDR)

filo_AMZ=outDir+"nr_signif_gp_AMZ_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_ANN"+".txt"
system("rm "+filo_AMZ)
asciiwrite(filo_AMZ,nr_sig_AMZ)

filo_SAminusAMZ=outDir+"nr_signif_gp_SAminusAMZ_"+runnames(run)+"-"+Runname1+"_"+varname(v)+"_ANN"+".txt"
system("rm "+filo_SAminusAMZ)
asciiwrite(filo_SAminusAMZ,nr_sig_SAminusAMZ)

;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;
if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_single_Plot_LUC(outDir,"diff_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif,False,Region,True,lev,True)

signif_single_Plot_LUC(outDir,"amazdiff_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif,True,Region,True,lev,True)
else
signif_single_Plot_LUC(outDir,"diff_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif,False,Region,True,lev,False)

signif_single_Plot_LUC(outDir,"amazdiff_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif,True,Region,True,lev,False)
end if

;Walker test for field significance
if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_single_Plot_LUC(outDir,"diff_Walk_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_Walk,False,Region,True,lev,True)

signif_single_Plot_LUC(outDir,"amazdiff_Walk_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_Walk,True,Region,True,lev,True)
else
signif_single_Plot_LUC(outDir,"diff_Walk_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_Walk,False,Region,True,lev,False)

signif_single_Plot_LUC(outDir,"amazdiff_Walk_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_Walk,True,Region,True,lev,False)
end if

;False discovery rate
if (Var.ne."GPH" .and. Var.ne."MSLP") then
signif_single_Plot_LUC(outDir,"diff_FDR_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_FDR,False,Region,True,lev,True)

signif_single_Plot_LUC(outDir,"amazdiff_FDR_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_FDR,True,Region,True,lev,True)
else
signif_single_Plot_LUC(outDir,"diff_FDR_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_FDR,False,Region,True,lev,False)

signif_single_Plot_LUC(outDir,"amazdiff_FDR_ANN_"+runnames(run)+"-"+Runname1+"_"+varname(v),AStart,AEnd,plttype,pltColor,var_diff,signif_FDR,True,Region,True,lev,False)
end if

delete(lev)
delete(var_ctl)
delete(var_exp)
delete(var_diff)

end do ;run
delete([/time_ctl,ctl_date,time_exp,exp_date/])
delete([/ind_yrs_ctl,new_date_ctl,ind_yrs_exp,new_date_exp/])
delete(var_ctl_avg)
delete(var_exp_avg)
delete(ctl_tmp)
delete(exp_tmp)
delete(ctlAve)
delete(expAve)
delete(ctlVar)
delete(expVar)
delete(ctlEqv)
delete(expEqv)
delete(clat1)

delete(VarAvgctl)
delete(VarAvgexp)
if ( AvgctlEqv .gt. 30 ) .and. ( AvgexpEqv .gt. 30 ) then
   delete(probf)
else
   delete(test_small)
end if
delete(prob)
delete(AvgctlEqv)
delete(AvgexpEqv)
delete(H)
delete(signif)
delete(signif_AMZ)
delete(signif_SAminusAMZ)
delete(signif_land)
delete(signif_Walk)
delete(signif_FDR)
delete(pj)
delete(prob_1D)
delete(p_sort)
end do ;loop over variables
;-----------------------------------------------------------------------------;
end
