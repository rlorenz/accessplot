#!/bin/bash
# File Name : access_eval.sh
# Creation Date : July 2013
# Last Modified : Wed 21 May 2014 15:53:33 EST
# Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
# Purpose : main run and set up script to plot ACCESS output
#	evaluation globally and Australia, different datasets
#	global temp: ERAint (1979-2011), HadGHCND (1950-2011),
#	global prec: GPCP (1979-2012)
#	gloabl ET: GLEAM (1984-2007)
#	Australia temp and prec: AWAP (1900-2011)->use 1951-2011
#	Datasets hosted @ccrc-servers-> run there
#	Datasets exist for certain time periods only, not very flexible!

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID1=uakpe

export ERAi_START=1980
export ERAi_STOP=2011

export HadG_START=1951
export HadG_STOP=2011

export GPCP_START=1980
export GPCP_STOP=2012

export GLEAM_START=1984
export GLEAM_STOP=2007

export AWAP_START=1951
export AWAP_STOP=2011

export CERES_START=2001
export CERES_STOP=2009

export RUNNAME1=ACCESS

export pltType=pdf
echo "(0) plot type is $pltType"

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
    export PATHCTL=/srv/ccrc/data23/$USER/ACCESS_output
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/$RUNID1/timeseries/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_eval without CTL input data"
    exit 1
fi

echo "(0)Run comparing to observations"
export OBSDIR=$DATAPATH/DATASETS/
export EXP=no
    if [[ ! -d $OBSDIR ]]; then
	echo "(1) $OBSDIR does not exist"
	echo "(2) cannot run access_plot without obs input data"
	exit 1
    fi

export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID1/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------
export siglev=0.05
export RES=n96
export NTILES=17
export NSOIL=6

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
    echo "Results are compared to Observations with files in Directory " $OBSDIR

    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "Start Time:" $date
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.0.0
module load netcdf
module load cdo
module load nco

# -----------------------------------------------------------------------
# seasonal mean and regridding of obs
# -----------------------------------------------------------------------
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${ERAi_START}_${ERAi_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${ERAi_START}_${ERAi_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${ERAi_START}_${ERAi_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${HadG_START}_${HadG_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${HadG_START}_${HadG_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${HadG_START}_${HadG_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${GPCP_START}_${GPCP_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${GPCP_START}_${GPCP_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${GPCP_START}_${GPCP_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${GLEAM_START}_${GLEAM_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${GLEAM_START}_${GLEAM_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${GLEAM_START}_${GLEAM_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${CERES_START}_${CERES_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${CERES_START}_${CERES_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${CERES_START}_${CERES_STOP}.nc.gz
fi

#cut pre, tmax and tmin for Australia only for AWAP comparison
if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${AWAP_START}_${AWAP_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasonal_means.${AWAP_START}_${AWAP_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasonal_means.${AWAP_START}_${AWAP_STOP}.nc.gz
	ncks -O -d lon,112.0,156.25 ${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}.nc ${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}_Aus.nc
	ncks -O -d lat,-44.5,-10.0 ${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}_Aus.nc ${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}_Aus.nc
fi
if [[ ! -f "$WORKDIR/$RUNID1.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}.nc" ]]; then
	cp $CTLDIR/$RUNID1.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}.nc.gz $WORKDIR
	gunzip $WORKDIR/$RUNID1.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}.nc.gz
	ncks -O -d lon,112.0,156.25 ${WORKDIR}/${RUNID1}.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}.nc ${WORKDIR}/${RUNID1}.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}_Aus.nc
	ncks -O -d lat,-44.5,-10.0 ${WORKDIR}/${RUNID1}.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}_Aus.nc ${WORKDIR}/${RUNID1}.seasmean_tmax_tmin.${AWAP_START}_${AWAP_STOP}_Aus.nc
fi

if [[ ! -f "$WORKDIR/ERAint_T2m_seas_mean_${ERAi_START}-${ERAi_STOP}_1deg.nc" ]]; then
	echo "Calculate seasonal means of Observations"
	cdo yseasmean $OBSDIR/ERAint_Data/ERAint_T2m_monthly_dec1979-${ERAi_STOP}-dec_1deg.nc $WORKDIR/ERAint_T2m_seas_mean_${ERAi_START}-${ERAi_STOP}_1deg.nc
	cdo yseasmean $OBSDIR/GPCP_Data/precip_mon_mean_197912-${GPCP_STOP}11.nc $WORKDIR/GPCP_prec_seas_mean_${GPCP_START}-${GPCP_STOP}_25deg.nc
	cdo yseasmean $OBSDIR/HadGHCND_Data/HadGHCND_TX_monmean_195012-${HadG_STOP}11.nc $WORKDIR/HadGHCND_TX_seas_mean_${HadG_START}-${HadG_STOP}.nc
	cdo yseasmean $OBSDIR/HadGHCND_Data/HadGHCND_TN_monmean_195012-${HadG_STOP}11.nc $WORKDIR/HadGHCND_TN_seas_mean_${HadG_START}-${HadG_STOP}.nc
	cdo yseasmean $OBSDIR/CERES_Data/CERES_EBAF-Surface_Ed2.7_Subset_200012-200911.nc $WORKDIR/CERES_rad_seas_mean_${CERES_START}-${CERES_STOP}.nc
fi

#always regrid from higher to lower resolution -> obs GPCP and HadGHCND on lower res than model
if [[ ! -f "$WORKDIR/ERAint_T2m_seas_mean_${ERAi_START}-${ERAi_STOP}_regrid.nc" ]]; then
	echo "Regrid observations and model to same grid"
	cdo remapbil,${WORKDIR}/$RUNID1.seasonal_means.${AWAP_START}_${AWAP_STOP}.nc $WORKDIR/ERAint_T2m_seas_mean_${ERAi_START}-${ERAi_STOP}_1deg.nc $WORKDIR/ERAint_T2m_seas_mean_${ERAi_START}-${ERAi_STOP}_regrid.nc
fi
if [[ ! -f "$WORKDIR/AWAP_pre_seas_mean_${AWAP_START}-${AWAP_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}_Aus.nc $OBSDIR/AWAP_Data/AWAP_pre_seas_mean_${AWAP_START}-${AWAP_STOP}_5km.nc $WORKDIR/AWAP_pre_seas_mean_${AWAP_START}-${AWAP_STOP}_regrid.nc
	cdo remapbil,${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}_Aus.nc $OBSDIR/AWAP_Data/AWAP_tmax_seas_mean_${AWAP_START}-${AWAP_STOP}_5km.nc $WORKDIR/AWAP_tmax_seas_mean_${AWAP_START}-${AWAP_STOP}_regrid.nc
	cdo remapbil,${WORKDIR}/${RUNID1}.seasonal_means.${AWAP_START}_${AWAP_STOP}_Aus.nc $OBSDIR/AWAP_Data/AWAP_tmin_seas_mean_${AWAP_START}-${AWAP_STOP}_5km.nc $WORKDIR/AWAP_tmin_seas_mean_${AWAP_START}-${AWAP_STOP}_regrid.nc
fi
if [[ ! -f "$WORKDIR/CERES_rad_seas_mean_${CERES_START}-${CERES_STOP}_regrid.nc" ]]; then
	cdo remapbil,${WORKDIR}/$RUNID1.seasonal_means.${CERES_START}_${CERES_STOP}.nc $WORKDIR/CERES_rad_seas_mean_${CERES_START}-${CERES_STOP}.nc $WORKDIR/CERES_rad_seas_mean_${CERES_START}-${CERES_STOP}_regrid.nc
fi

if [[ ! -f "$WORKDIR/$RUNID1.seasonal_means.${GPCP_START}-${GPCP_STOP}_regrid.nc" ]]; then
	cdo remapbil,$WORKDIR/GPCP_prec_seas_mean_${GPCP_START}-${GPCP_STOP}_25deg.nc ${WORKDIR}/$RUNID1.seasonal_means.${GPCP_START}_${GPCP_STOP}.nc  $WORKDIR/$RUNID1.seasonal_means.${GPCP_START}-${GPCP_STOP}_regrid.nc
elif [[ ! -f "$WORKDIR/$RUNID1.seasmean_tmax_tmin.${HadG_START}-${HadG_STOP}_regrid.nc" ]]; then
	cdo remapbil,$WORKDIR/HadGHCND_TX_seas_mean_${HadG_START}-${HadG_STOP}.nc ${WORKDIR}/$RUNID1.seasmean_tmax_tmin.${HadG_START}_${HadG_STOP}.nc $WORKDIR/$RUNID1.seasmean_tmax_tmin.${HadG_START}-${HadG_STOP}_regrid.nc
fi

#copy GLEAM data into workdir
if [[ ! -f "$WORKDIR/GLEAM.New_n96.seas_mean_global_EVAP_${GLEAM_START}-${GLEAM_STOP}.nc" ]]; then
	cp $OBSDIR/GLEAM_ETdata/GLEAM.New_n96.seas_mean_global_EVAP_${GLEAM_START}-${GLEAM_STOP}.nc ${WORKDIR}/
fi

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
ncl $HOME/scripts/plot_scripts/ncl_scripts/Prec_GPCP_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/ET_GLEAM_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/TX_HadGHCND_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/TN_HadGHCND_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/Prec_AWAP_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/Tmax_AWAP_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/Tmin_AWAP_evaluation.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/Rad_CERES_evaluation.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0

# -----------------------------------------------------------------------
# 
# -----------------------------------------------------------------------
