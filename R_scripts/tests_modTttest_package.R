# test code R, test if modified t-test test on correct
# significance level (for 5% level, if test performed 10000 times ~500
# false positives) and if package functions are working properly
source("/home/z3441306/scripts/plot_scripts/R_scripts/modified_t_test/modTtest_package/modTtest.R")
source("/home/z3441306/scripts/plot_scripts/R_scripts/tests_for_autocorr_fields/modTtest_3dData.R")

fp_modT<-array(NA,dim=c(17))

#ac<-seq(from = -0.8, to = 0.8, by = 0.1)
ac<-seq(from = -0.3, to = 0.8, by = 0.1)
print(ac)
a<-1

#for (autocorr in seq(from = -0.8, to = 0.8, by = 0.1)){
for (autocorr in seq(from = -0.3, to = 0.8, by = 0.1)){
print(autocorr)

p_val<-array(NA,dim=c(10000))
p_val2<-array(NA,dim=c(10000))
h_val<-array(NA,dim=c(10000))
k<-array(NA,dim=c(10000))

    for (p in 1:10000){
	    #norm<-rnorm(62)
	    #x<-sample(norm,31,replace=T)
	    #y<-sample(norm,31,replace=T)
	    x<-arima.sim(list(ar =autocorr),n=31,rand.gen=rnorm)
	    y<-arima.sim(list(ar =autocorr),n=31,rand.gen=rnorm)
	    test<-modTtest(x,y,alternative = c("two.sided"),conf.level=0.95)
	    test2<-t.test(x,y,alternative = c("two.sided"),conf.level=0.95)
	    p_val[p]<-test$p.value
	    p_val2[p]<-test2$p.value
	    t_stat<-test$statistic
	    confint<-test$conf.int
	    if (!is.na(confint[1])) {
		if ((confint[1]<0) && (confint[2]>0)) {
			    h_val[p]<-0
		    } else {
			    h_val[p]<-1
		    }
		} else { h_val[p]<-NA
	    }
    }
k[which(p_val<0.05)] <- 1
msg<-paste("Nr sig modTtest pval:",sum(k,na.rm=T),sep='')
print(msg)

k[which(h_val==1)] <- 1
msg<-paste("Nr sig modTtest hval:",sum(k,na.rm=T),sep='')
print(msg)

k[which(p_val2<0.05)] <- 1
msg<-paste("Nr sig Ttest pval:",sum(k,na.rm=T),sep='')
print(msg)

fp_modT[a]<-(sum(k,na.rm=T))/100

p_val<-array(NA,dim=c(2,5000,1))
h_val<-array(NA,dim=c(2,5000))
k<-array(NA,dim=c(10000,1))
x<-array(NA,dim=c(2,5000,1,14))
y<-array(NA,dim=c(2,5000,1,14))
for (h in 1:2){
	for (p in 1:5000){
		for (j in 1:1){
			#norm<-rnorm(62)
			#x[h,p,j,]<-sample(norm,31,replace=T)
			#y[h,p,j,]<-sample(norm,31,replace=T)
			#create timeseries with AR(1) correlation
			x[h,p,j,]<-arima.sim(list(ar =autocorr),n=14,rand.gen=rnorm)
			y[h,p,j,]<-arima.sim(list(ar =autocorr),n=14,rand.gen=rnorm)
		}
	}
}
test3d<-modTtest_3d(x,y,alternative = c("two.sided"),conf.level=0.95)
k[which(test3d$p.value<0.05)] <- 1
msg<-paste("Nr sig modTtest3d pval:",sum(k,na.rm=T),sep='')
print(msg)

confint<-test3d$conf.int
for (h in 1:2) {
	for (p in 1:5000) {    
	    if (!is.na(confint[1,h,p])) {
		if ((confint[1,h,p]<0) && (confint[2,h,p]>0)) {
			    h_val[h,p]<-0
		    } else {
			    h_val[h,p]<-1
		    }
		} else { h_val[h,p]<-NA
	    }
	}
}
k[which(h_val==1)] <- 1
msg<-paste("Nr sig modTtest3d hval:",sum(k,na.rm=T),sep='')
print(msg)

#plus one in index counter 
a<- a+1
}
