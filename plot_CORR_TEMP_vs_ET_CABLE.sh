#!/bin/bash
# File Name : plot_CORR_Temp_vs_ET.sh
# Creation Date : 04/09/2013
# Last Modified : Wed 21 May 2014 15:55:59 EST
# Created By : Ruth Lorenz
# Purpose : plot correlation of Tmax versus ET

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=out_gswp
export RUNNAME=CABLE2
export DATAT=HadGHCND
export DATAR=GLEAM

export Y_START=1986
export Y_STOP=1995

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/CABLE_output/${RUNID}/timeseries/
export OBSDIR=$DATAPATH/DATASETS/
export WORKDIR=$DATAPATH/CABLE_plots_workdir/
export OUTDIR=$DATAPATH/CABLE_plots/$RUNID/scatter_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.0
module load netcdf
module load nco
module load cdo

# -----------------------------------------------------------------------
# copy data to workdir
# -----------------------------------------------------------------------
cp $MODDIR/${RUNID}_monmean_TS_${Y_START}-${Y_STOP}.nc $WORKDIR

cp $OBSDIR/GLEAM_ETdata/GLEAM.New.monmean_global_EVAP_${Y_START}-${Y_STOP}_rgd.nc $WORKDIR

cp $OBSDIR/${DATAT}_Data/HadGHCND_TX_monmean_${Y_START}-${Y_STOP}.nc $WORKDIR

# -----------------------------------------------------------------------
# regrid all data to coarsest resolution (coming from HadGHCND)
# -----------------------------------------------------------------------
cdo remapbil,$WORKDIR/HadGHCND_TX_monmean_${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}_monmean_TS_${Y_START}-${Y_STOP}.nc $WORKDIR/${RUNID}_monmean_TS_${Y_START}-${Y_STOP}_rgd.nc

# -----------------------------------------------------------------------
# call ncl script
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_corr_tmax_vs_et_CABLE.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
