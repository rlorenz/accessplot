;-----------------------------------------------------------------------------;
; File Name : plot_LUC_notest_Ttest.ncl
; Creation Date : 26-09-2014
; Last Modified : Mon 17 Nov 2014 11:45:31 AEDT
; Created By : Ruth Lorenz
; Purpose : plot difference between CTL and LUC experiments,
; 	   output Ttest from R

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_seas_panel_overlay_significance_LUC.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/LUC/plot_single_overlay_significance_LUC.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;
Runid1 = systemfunc("echo $RUNID1")     ; RUNID
Runname1 = systemfunc("echo $RUNNAME1") ; RUNNAME

Region =  systemfunc("echo $REGION") ; Region, sc or wc

user = systemfunc("echo $USER")

workDir = systemfunc("echo $OUTDIR")

outDir = systemfunc("echo $OUTDIR")

R1Start = systemfunc("echo $R1_START")
R1End = systemfunc("echo $R1_STOP")
R2Start = systemfunc("echo $R2_START")
R2End = systemfunc("echo $R2_STOP")
AStart = systemfunc("echo $A_START")
AEnd = systemfunc("echo $A_STOP")

;define plot type
plttype = systemfunc("echo $pltType")

sigr = stringtofloat(systemfunc("echo $siglev"))       ; critical sig lvl for r

;-----------------------------------------------------------------------------;
; loop over runs and variables
;-----------------------------------------------------------------------------;
if ( Region .eq. "sc" ) then
   runnames = (/"001GPsc","003GPsc","005GPsc","009GPsc","025GPsc","049GPsc","081GPsc","121GPsc","242GP"/)
else if ( Region .eq. "wc" ) then
   runnames = (/"001GPwc","003GPwc","005GPwc","009GPwc","025GPwc","049GPwc","081GPwc","121GPwc","allAMZ"/)
else print("wrong region name, does not exist")
end if
end if

varname = (/"pr","tas","tasmax","tasmin","zg","psl","rss","rls","rnet","hfls","alb","cancd"/)
longname = (/"P~B~TOT~N~","T~B~1.5m~N~","T~B~MAX~N~","T~B~MIN~N~","GPH@300hPa","MSLP","SW~B~NET~N~","LW~B~NET~N~","R~B~NET~N~","LH","ALB","CAN"/)
units = (/"mm day~S~-1~N~","~S~o~N~C","~S~o~N~C","~S~o~N~C","m","hPa","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","Wm~S~-2~N~","-","ms~S~-1~N~"/)

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
; read land sea mask
lsm_file=addfile("/srv/ccrc/data23/z3441306/ACCESS_input_data/land_sea_mask_accessV2.nc","r")
lsm = lsm_file->lsm

;start loop
do v = 0,1
   do run = 0,8
      print("reading NetCDFs")
      file1 = addfile(workDir+runnames(run)+"/" +varname(v)+"_"+runnames(run)+  \
                     "_"+Runname1+"_seas_Ttest_1samp_" + AStart + "_" + AEnd + ".nc","r")

   if ( v .eq. 0 ) then
      Var="PR"
      pltColor="orange_to_blue_18lev"
      lev=(/-2,-1.5,-1,-0.6,-0.2,0.2,0.6,1,1.5,2/)
   else if ( v .eq. 1 ) then
      Var="T"
      pltColor="blue_to_red_18lev"
      lev=(/-2,-1.5,-1,-0.5,-0.25,0.25,0.5,1,1.5,2/)

   end if
   end if

;-----------------------------------------------------------------------------;
; read data
;-----------------------------------------------------------------------------;

   if (Var .eq. "PR") then	;convert prec to mm/day
      var_ctl_orig = file1 ->ctl_Seas
      var_ctl=var_ctl_orig*(24*60*60)
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file1 ->exp_Seas
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)

 
   else if (Var .eq. "MSLP") then  ;convert mslp to hPa
      var_ctl_orig = file1 ->ctl_Seas
      var_ctl=var_ctl_orig/100
      copy_VarMeta(var_ctl_orig,var_ctl)

      var_exp_orig = file1 ->exp_Seas
      var_exp=var_exp_orig*(24*60*60)
      copy_VarMeta(var_exp_orig,var_exp)

   else
      var_ctl = file1 ->ctl_Seas
      var_exp = file1 ->exp_Seas

   end if
   end if

      signif    = doubletoint(file1 ->Hval)
      signif_wilc = doubletoint(file1 ->Hval_wilk)

   var_ctl@long_name     = longname(v)
   var_ctl@units         = units(v)

   var_exp@long_name     = longname(v)
   var_exp@units         = units(v)

   printVarSummary(var_ctl)
   printVarSummary(var_exp)

   ;check if ctl and exp time periods of same length
   dsize_ctl=dimsizes(var_ctl)
   dsize_exp=dimsizes(var_exp)
   ntim_ctl=dsize_ctl(0)
   nlat_ctl=dsize_ctl(1)
   nlon_ctl=dsize_ctl(2)
   ntim_exp=dsize_exp(0)
   nlat_exp=dsize_exp(1)
   nlon_exp=dsize_exp(2)

   if ( ntim_ctl.ne.ntim_exp .or. nlat_ctl.ne.nlat_exp .or. nlon_ctl.ne.nlon_exp ) then
        print("ERROR: CTL and EXP do not have same dimension, exiting")
        exit
   else
        ntim=ntim_ctl
        nlat=nlat_ctl
        nlon=nlon_ctl

        delete(ntim_ctl)
        delete(nlat_ctl)
        delete(nlon_ctl)
        delete(ntim_exp)
        delete(nlat_exp)
        delete(nlon_exp)
   end if

   ;calculate difference
   var_diff= doubletofloat(var_exp-var_ctl)


   var_diff@long_name     = longname(v)+" "+runnames(run)+"-"+"CTL"
   var_diff@units         = units(v)
   var_diff!1="lat"
   var_diff!2="lon"
   var_diff&lon = lsm&longitude
   var_diff&lat = lsm&latitude

   signif!1="lat"
   signif!2="lon"
   signif&lon = lsm&longitude
   signif&lat = lsm&latitude

   signif_wilc!1="lat"
   signif_wilc!2="lon"
   signif_wilc&lon = lsm&longitude
   signif_wilc&lat = lsm&latitude

   zeros=new((/4,nlat,nlon/),integer)
   zeros=0
   zeros!1="lat"
   zeros!2="lon"
   zeros&lon = lsm&longitude
   zeros&lat = lsm&latitude

;-----------------------------------------------------------------------------;
; count number of significant grid points over land and save to .txt for each season
;-----------------------------------------------------------------------------;
	signif_land=new((/4,nlat,nlon/),integer)
	signif_noSA=new((/4,nlat,nlon/),integer)
	nr_sig_seas=new((/4/),float)
	nr_sig_seas_noSA=new((/4/),float)

	;mask inside of South America
	signif_noSA=signif
	signif_noSA(:,{-60:10},:)=signif@_FillValue
	signif_noSA(:,:,{275:328})=signif@_FillValue

	do j=0,3
   	   signif_land(j,:,:)=where(lsm(0,0,:,:).ne.0,signif(j,:,:),signif@_FillValue)
   	   nr_sig_seas(j) = num(signif_land(j,:,:).eq.1)

	   signif_noSA(j,:,:)=where(lsm(0,0,:,:).ne.0,signif(j,:,:),signif@_FillValue)
	   nr_sig_seas_noSA(j) = num(signif_noSA(j,:,:).eq.1)

	end do

	outDir = systemfunc("echo $OUTDIR")+runnames(run) +"/"

	;save nr of signif grid points to file
	filo=outDir+"nr_signif_gp_land_ttest_1samp_"+runnames(run)+"-"+Runname1+"_"+varname(v)+".txt"
	system("rm "+filo)
	asciiwrite(filo,nr_sig_seas)

	filo=outDir+"nr_signif_gp_noSAland_ttest_1samp_"+runnames(run)+"-"+Runname1+"_"+varname(v)+".txt"
	system("rm "+filo)
	asciiwrite(filo,nr_sig_seas_noSA)

	delete(nr_sig_seas)
	delete(nr_sig_seas_noSA)

;-----------------------------------------------------------------------------;
; plot
;-----------------------------------------------------------------------------;
	;No statistical test
	Panel_Plot_sig_LUC(outDir+"/","diff_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(:,:,:),zeros,False,Region,True,lev,True)
	signif_single_Plot_LUC(outDir+"/","diff_JJA_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(2,:,:),zeros,False,Region,True,lev,True)

	;simple ttest
	Panel_Plot_sig_LUC(outDir+"/","diff_ttest_1samp_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(:,:,:),signif(:,:,:),False,Region,True,lev,True)
	signif_single_Plot_LUC(outDir+"/","diff_ttest_1samp_JJA_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(2,:,:),signif(2,:,:),False,Region,True,lev,True)

	;wilcoxon signed rank test
	Panel_Plot_sig_LUC(outDir+"/","diff_wilcox_1samp_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(:,:,:),signif_wilc(:,:,:),False,Region,True,lev,True)
	signif_single_Plot_LUC(outDir+"/","diff_wilcox_1samp_JJA_"+runnames(run)+"-"+Runname1+"_"+varname(v),runnames(run),AStart,AEnd,plttype,pltColor,var_diff(2,:,:),signif_wilc(2,:,:),False,Region,True,lev,True)

	delete(lev)
	delete(var_diff)

  end do ;run

  delete(var_exp)
  delete(var_ctl)

end do ;loop over variables
;-----------------------------------------------------------------------------;
end
