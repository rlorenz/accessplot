;-----------------------------------------------------------------------------;
; File Name : climdex.ncl
; Creation Date : January 2014
; Last Modified : Thu Jan 09 16:47:37 2013
; Created By : Ruth Lorenz
; Purpose : climdex plotting script for GLACE-CMIP5

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;

load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
;load "$HOME/scripts/plot_scripts/ncl_scripts/plot_climdex_all_months.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_climdex_seas_GCCMIP5.ncl"

begin

;-----------------------------------------------------------------------------;
; define variables passed to plotting routines
;-----------------------------------------------------------------------------;
  model = systemfunc("echo $MODEL")

  rna1 = systemfunc("echo $RUNNAME1")
  rna2 = systemfunc("echo $RUNNAME2")

  ctldir = systemfunc("echo $CTLDIR")
  expdir = systemfunc("echo $EXPDIR")

  outdir = systemfunc("echo $OUTDIR")

  r1start = systemfunc("echo $R1_START") ;run length
  r1end = systemfunc("echo $R1_STOP")
  r2start = systemfunc("echo $R2_START") ;run length
  r2end = systemfunc("echo $R2_STOP")

  a1start = systemfunc("echo $A1_START")			;analysis period 1 start string
  starta1 = stringtointeger(systemfunc("echo $A1_START"))	;analysis period 1 start numeric

  a1end = systemfunc("echo $A1_STOP")	     			;analysis period 1 end string
  enda1 = stringtointeger(systemfunc("echo $A1_STOP"))		;analysis period 1 end numeric

  a2start = systemfunc("echo $A2_START")			;analysis period 2 start string
  starta2 = stringtointeger(systemfunc("echo $A2_START"))	;analysis period 2 start numeric

  a2end = systemfunc("echo $A2_STOP")	   			;analysis period 2 end string
  enda2 = stringtointeger(systemfunc("echo $A2_STOP"))		;analysis period 2 end numeric

  nlat = stringtointeger(systemfunc("echo $NLAT"))
  nlon = stringtointeger(systemfunc("echo $NLON"))

  plttype = systemfunc("echo $pltType")

;------------------Call plot_climdex_all_months.ncl--------------------

print("Call plot climdex file")
plot_climdex_seas_GCCMIP5(model,expdir,rna2,ctldir,rna1,r1start,r1end,r2start,r2end,a1start,starta1,a1end,enda1,a2start,starta2,a2end,enda2,nlat,nlon,plttype,outdir)

end
