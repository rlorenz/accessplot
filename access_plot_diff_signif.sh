#!/bin/bash
# File Name : access_plot_diff_signif.sh
# Creation Date : 22/01/2014
# Last Modified : Fri 06 Nov 2015 20:22:06 AEDT
# Created By : Ruth Lorenz
# Purpose : prepare plotting of differences in experiments or to observations
#	with significance testing	

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID1=vachb
export RUNID2=vache

export R1_START=1979 #run start RUNID1
export R1_STOP=1983 #run end RUNID1
export R2_START=1979 #run start RUNID1
export R2_STOP=1983 #run end RUNID1

export A_START=1979 #analysis start
export A_STOP=1983 #analysis end

export RUNNAME1=CTL14
export RUNNAME2=LAIpPFT

export pltType=pdf

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
    export PATHCTL=/short/$PROJECT/$USER/postproc
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data23/$USER
    #export DATAPATH=/srv/ccrc/data32/$USER
    export PATHCTL=/srv/ccrc/data44/$USER/ACCESS1_4
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/$RUNID1/timeseries/
#export CTLDIR=/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn/LEUNING/ENS_MEAN/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run access_plot without CTL input data"
    exit 1
fi

echo "(0) Run comparing to experiment"
#export EXPDIR=$PATHCTL/$RUNID2/timeseries/
#export EXPDIR=/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn/MEDLYN/ENS_MEAN/
export EXPDIR=/srv/ccrc/data44/z3441306/ACCESS1_4/$RUNID2/timeseries/
export EXP=yes
if [[ ! -d $EXPDIR ]]; then
     echo "(1) $EXPDIR does not exist"
     echo "(2) cannot run access_plot without EXP input data"
     exit 1
fi

export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID2/
#export OUTDIR=/srv/ccrc/data32/z3441306/ACCESS-Leuning-Medlyn/plots/seasonal_means/
if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export siglev=0.05
export RES=n96
export NTILES=17
export NSOIL=6

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
if  [[ $RUNID2 != '' ]]; then
    echo "Results are compared to Jobid: " $RUNID2 " with files in Directory " $EXPDIR
else
    echo "Results are compared to Observations with files in Directory " $OBSDIR
fi

    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "Start Time:" $date
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load R/3.0.3
module load netcdf
module load cdo

if [[ ! -f "$WORKDIR/$RUNID1.monthly_TS.${R1_START}_${R1_STOP}.nc" ]]; then
        cp $CTLDIR/$RUNID1.monthly_TS.${R1_START}_${R1_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUNID1.monthly_TS.${R1_START}_${R1_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID1.monmean_tmax_tmin.${R1_START}_${R1_STOP}.nc" ]]; then
        cp $CTLDIR/$RUNID1.monmean_tmax_tmin.${R1_START}_${R1_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUNID1.monmean_tmax_tmin.${R1_START}_${R1_STOP}.nc.gz
fi

if [[ ! -f "$WORKDIR/$RUNID2.monthly_TS.${R2_START}_${R2_STOP}.nc" ]]; then
        cp $EXPDIR/$RUNID2.monthly_TS.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        #cp $EXPDIR/$RUNID2.monthly_TS.${R2_START}_${R2_STOP}.nc $WORKDIR
        gunzip $WORKDIR/$RUNID2.monthly_TS.${R2_START}_${R2_STOP}.nc.gz
fi
if [[ ! -f "$WORKDIR/$RUNID2.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc" ]]; then
        cp $EXPDIR/$RUNID2.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        #cp $EXPDIR/$RUNID2.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc $WORKDIR
        gunzip $WORKDIR/$RUNID2.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc.gz
fi

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call plot_LUC_diff_significance.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_diff_significance.ncl

#call R-script to save data to netcdf
echo "R-script"
R --no-restore --no-save < $HOME/scripts/plot_scripts/R_scripts/ACCESS_exp_modTtest.R > $HOME/scripts/plot_scripts/R_scripts/ACCESS_exp_modTtest.Rout

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
