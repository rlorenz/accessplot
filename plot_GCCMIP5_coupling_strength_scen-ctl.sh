#!/bin/bash
# File Name : plot_GLACE_coupling_strength_scen-ctl.sh
# Creation Date : 03/12/2013
# Last Modified : Fri 03 Oct 2014 17:01:48 EST
# Created By : Ruth Lorenz
# Purpose : Wrapper to plot GLACE-CMIP5 coupling strength
#	for different time periods. Need to calculate coupling strength
#       for both time periods first

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------
export MODEL=ACCESS
export RUNID1=GC1A85
export RUNID2=GC1A85

export RUNNAME1=FFUT
export RUNNAME2=FFUT

export R1_START=195001 #run 1 start
export R1_STOP=210012  #run 1 stop
export R2_START=195001 #run 1 start
export R2_STOP=210012  #run 1 stop
export A1_START=2021 #analysis start
export A1_STOP=2040  #analysis stop
export A2_START=2081 #analysis start
export A2_STOP=2100  #analysis stop

export pltType=pdf
 
# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------

if [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data32/$USER
    export PATHCTL=/srv/ccrc/data32/$USER/GLACE-CMIP5/$MODEL
else
    echo "Wrong USER"
    exit
fi

export CTLDIR=$PATHCTL/$RUNID1/
if [[ ! -d "$CTLDIR" ]]; then
    echo "(0) $CTLDIR does not exist"
    exit 1
fi

ctldir=`ls -l $CTLDIR | awk '{print $5}'`

if [[ "$ctldir" == "0" ]]; then
    echo "(0) No data in $CTLDIR"
    echo "(0) cannot run without CTL input data"
    exit 1
fi


export EXPDIR=$PATHCTL/$RUNID2/
if [[ ! -d $EXPDIR ]]; then
            echo "(1) $EXPDIR does not exist"
            echo "(2) cannot run without EXP input data"
	    exit 1
fi

export OUTDIR=$DATAPATH/GLACE-CMIP5_plots/$MODEL/$RUNID1/coupling/
if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

#export WORKDIR=$DATAPATH/ACCESS_plots_workdir/

#if [[ ! -d "$WORKDIR" ]]; then
# mkdir -p $WORKDIR
# echo "(0)     Making Directory $WORKDIR"
#fi

# -----------------------------------------------------------------------
# Output information
# -----------------------------------------------------------------------

    echo "The parameters of this run have been set as:"
    echo "Jobid: " $RUNID1 "with files in Directory " $CTLDIR
    echo "Results are compared to Jobid: " $RUNID2 " with files in Directory " $EXPDIR
    echo "The working directory is " $WORKDIR
    echo "The output directory is " $OUTDIR 
    echo " "

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call NCL scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/variance_analysis_temp_GCCMIP5_scen-ctl.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_coupling_strength_GCCMIP5_scen-ctl.ncl

#ncl $HOME/scripts/plot_scripts/ncl_scripts/corr_ET_temp_GCCMIP5_scen-ctl.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/calc_Pi_coupling_GCCMIP5_scen-ctl.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_two-legged_coupling_dtrend_GCCMIP5_scen-ctl.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------
    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
