;-----------------------------------------------------------------------------;
; File Name : var_smc.ncl
; Creation Date : May 2013
; Last Modified : Tue 13 Aug 2013 09:00:58 EST
; Created By : Ruth Lorenz
; Purpose : Variable soil moisture content, not compared to observations

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$HOME/scripts/plot_scripts/ncl_scripts/plot_seas_panel.ncl"

undef("smc_ncl")		;always undef procedure, otherwise problems may occur
procedure smc_ncl(fileDir:string,Runid1:string,Runname1:string,compDir:string, \
			Runid2:string,Runname2:string,YStart:string,YEnd:string, \
			nTiles:numeric,nPft:numeric,nSoil:numeric,nRes:string, \
			NX:numeric,NY:numeric,XPE:string,workDir:string,outDir:string, \
			plttype:string)

;local variables should be local
local ctlfile, expfile, colorbar_abs, colorbar_diff, \
	smcl_ctl, smcl_exp, diff_smcl, lev_sm, lev_bsm

begin

;-----------------------------------------------------------------------------;
; Reading Data
;-----------------------------------------------------------------------------;

print("reading NetCDFs")
	ctlfile = addfile(workDir + Runid1 + ".seasonal_means." + YStart + "_" + YEnd + ".nc","r")
if (XPE.eq."yes") then
	expfile = addfile(workDir + Runid2 + ".seasonal_means." + YStart + "_" + YEnd + ".nc","r")
end if

;soil moisture
slayers = (/0.022,0.058,0.154,0.409,1.085,2.872/) ; = 4.6m

smcl_ctl = ctlfile ->mrsos	; soil moisture content in a layer, kgm-2

do i=0,nSoil-1,1
	smcl_ctl(:,i,:,:)       = smcl_ctl(:,i,:,:) / (slayers(i))
end do

smcl_ctl@units           = "kgm-3"
smcl_ctl@long_name = "Soil moisture in each (of 6) levels"

if (XPE.eq."yes") then
	smcl_exp = expfile ->mrsos	; soil moisture content in a layer, kgm-2

	do i=0,nSoil-1,1
		smcl_exp(:,i,:,:)       = smcl_exp(:,i,:,:) / (slayers(i))
	end do

	smcl_exp@units           = "kgm-3"
	smcl_exp@long_name = "Soil moisture in each (of 6) levels"
end if

;-----------------------------------------------------------------------------;
; Calculating differences/biases
;-----------------------------------------------------------------------------;
if (XPE.eq."yes") then
	print("Calculating biases")
	diff_smcl = smcl_exp(:,:,:) - smcl_ctl(:,:,:)
	copy_VarMeta(smcl_ctl(:,:,:),diff_smcl)
	diff_smcl@long_name = "Soil moisture difference: " +Runname2+ "-"+Runname1
	diff_smcl@units = "kgm-3"
	diff_smcl!0 = "time"
	diff_smcl&time = smcl_ctl&time
	
end if

;-----------------------------------------------------------------------------;
; Plotting
;-----------------------------------------------------------------------------;

lev_sm = ispan(0,500,50)
lev_bsm = (/-100,-80,-60,-40,-20,-10,10,20,40,60,80,100/)

colorbar_abs = "light_blue_to_dark_blue_12lev"
colorbar_diff = "brown_to_blue_12lev"

;global plots
print("Plot seasons global")

if (XPE.eq."no") then
	do n = 0,nSoil-1
		f = n + 1
		smcl_ctl@long_name = "Soil moisture in each (of "+nSoil+") levels: Level "+f
		smfilename = "seas_smcl_level"+f
		Panel_Plot(outDir,smfilename,YStart,YEnd,plttype,colorbar_abs,smcl_ctl(:,n,:,:),False,True,lev_sm,True)
	end do

else if  (XPE.eq."yes") then
	do n = 0,nSoil-1
		f = n + 1
		smcl_exp@long_name = "Soil moisture in each (of "+nSoil+") levels: Level "+f
		smfilename = "seas_smcl_level"+f
		Panel_Plot(outDir,smfilename,YStart,YEnd,plttype,colorbar_abs,smcl_exp(:,n,:,:),False,True,lev_sm,True)
	end do

else
	print("XPE not defined")
end if
end if

if  (XPE.eq."yes") then
	print("Plot biases global")
	do n = 0,nSoil-1
		f = n + 1
		diff_smcl@long_name = "Difference in soil moisture in each (of "+nSoil+") levels: Level "+f
		smfilename = "diff_smcl_level"+f
		Panel_Plot(outDir,smfilename,YStart,YEnd,plttype,colorbar_diff,diff_smcl(:,n,:,:),False,True,lev_bsm,True)
	end do

end if

;Australian plots
print("Plots seasons AU")

if (XPE.eq."no") then
	do n = 0,nSoil-1
		f = n + 1
		smcl_ctl@long_name = "Soil moisture in each (of "+nSoil+") levels: Level "+f
		smfilename = "austseas_smcl_level"+f
		Panel_Plot(outDir,smfilename,YStart,YEnd,plttype,colorbar_abs,smcl_ctl(:,n,:,:),True,True,lev_sm,True)
	end do

else if  (XPE.eq."yes") then
	do n = 0,nSoil-1
		f = n + 1
		smcl_exp@long_name = "Soil moisture in each (of "+nSoil+") levels: Level "+f
		smfilename = "austseas_smcl_level"+f
		Panel_Plot(outDir,smfilename,YStart,YEnd,plttype,colorbar_abs,smcl_exp(:,n,:,:),True,True,lev_sm,True)
	end do

else
	print("XPE not defined")
end if
end if

if  (XPE.eq."yes") then
	print("Plot biases AU")
	do n = 0,nSoil-1
		f = n + 1
		diff_smcl@long_name = "Difference in soil moisture in each (of "+nSoil+") levels: Level "+f
		smfilename = "austdiff_smcl_level"+f
		Panel_Plot(outDir,smfilename,YStart,YEnd,plttype,colorbar_diff,diff_smcl(:,n,:,:),True,True,lev_bsm,True)
	end do

end if

;---------------END----------------

end
