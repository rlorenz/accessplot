#!/bin/bash
# File Name : plot_climdex.sh
# Creation Date : June 2013
# Last Modified : Wed 21 May 2014 15:55:38 EST
# Created By : Ruth Lorenz, email:r.lorenz@unsw.edu.au
# Purpose : script plotting climdex indices maps, trends, and timeseries
# climdex indices need to be calculated first using fortran code
# in /home/561/rzl561/scripts/fclimdex/fclimdex_nc/

#PBS -N climdex_plot
#PBS -P dt6
#PBS -j oe
#PBS -l ncpus=1
#PBS -l walltime=06:30:00
#PBS -l vmem=2600mb
#PBS -wd

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export RUNID=uaiuc
export RUNNAME=ACCESS1.3
export DATA=HADEX2

export Y_START=1952
export Y_STOP=2007

export NLAT=73
export NLON=96

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
	export DATAPATH=/short/dt6/rzl561
elif [[ "$USER" == "z3441306" ]]; then
	export DATAPATH=/srv/ccrc/data23/$USER
fi

export MODDIR=$DATAPATH/climdex_index/
export OBSDIR=$DATAPATH/DATASETS/${DATA}_Data/
export OUTDIR=$DATAPATH/ACCESS_plots/$RUNID/climdex_figures/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.0.0
module load netcdf
module load cdo
module load nco

# -----------------------------------------------------------------------
# call ncl scripts
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call climdex.ncl"
ncl $HOME/scripts/plot_scripts/ncl_scripts/climdex.ncl

# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting climdex indices"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
