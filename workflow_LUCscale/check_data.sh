#!/bin/bash
# File Name : check_data.sh
# Creation Date : 03 August 2015
# Last Modified : Thu 05 Nov 2015 22:31:27 AEDT
# Created By : Ruth Lorenz
# Purpose : check if all data for analysis is copied into
#		workdir and unzipped

###-------------------------------------------------------

# create outdir if does not exist yet
if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi

# create workdir if does not exist yet
if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi


# check data for all runs
echo "(1) check data availability"
for RUN in uaoya uaoyb uaoyc uaoyd uaoye uaoyf uaoyg uaoyh uaoyi uaoyj uaoyk uaoyl uaoym uaoyn uaoyo uaoyp uaoyr uaoys uaoyt uaoyw uaoyx uaoyy uaoyz vacda vacdb vacdc vacdd vacde vacdf vacdg vacdh vacdi vacdj vacdk vacdl vacdm vacdn vacdo vacdp vacdq vacdr vacds vacdt vacdu vacdv vajoa vajob vajoc vajod vajoe vajof vajog vajoh vajoi vajoj vajok vajol vajom vajon vajoo vajop vajoq vajor vajos vajot vajou vajov vacdw vacdx vacdy vacdz
do
    if [[ ! -f "$WORKDIR/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc" ]]; then
        echo "(2) Copying data"
	cp $PATHCTL/$RUN/timeseries/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUN.monthly_TS.${R2_START}_${R2_STOP}.nc.gz
    fi
    if [[ ! -f "$WORKDIR/$RUN.yearly_means.${R2_START}_${R2_STOP}.nc" ]]; then
        echo "(2) Copying data"
	cp $PATHCTL/$RUN/timeseries/$RUN.yearly_means.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUN.yearly_means.${R2_START}_${R2_STOP}.nc.gz
    fi
    if [[ ! -f "$WORKDIR/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc" ]]; then
        echo "(2) Copying data"
        cp $PATHCTL/$RUN/timeseries/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc.gz $WORKDIR
        gunzip $WORKDIR/$RUN.monmean_tmax_tmin.${R2_START}_${R2_STOP}.nc.gz
    fi
done
