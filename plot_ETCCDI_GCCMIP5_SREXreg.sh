#!/bin/bash
# File Name : plot_ETCCDI_GCCMIP5_SREXreg.sh
# Creation Date : 12/03/2014
# Last Modified : Wed 21 May 2014 15:57:12 EST
# Created By : Ruth Lorenz
# Purpose : plot ETCCDI indices for GLACE-CMIP5 models
#		boxplots over SREX regions

### -----------------------------------------------------------------------
### General settings
### -----------------------------------------------------------------------

export R1_START=1954 #run 1 start
export R1_STOP=2100  #run 1 stop
export R2_START=1950 #run 1 start
export R2_STOP=2100  #run 1 stop
export A_START=2021 #analysis start
export A_STOP=2040  #analysis stop
export A2_START=2081 #analysis start if 2 timeperiods
export A2_STOP=2100  #analysis stop if 2 timeperiods

export RUNNAME1=CTL
export RUNNAME2=GC1A85
export RUNNAME3=GC1B85

export pltType=pdf

# -----------------------------------------------------------------------
# specify paths
# -----------------------------------------------------------------------
if [[ "$USER" == "rzl561" ]]; then
    export DATAPATH=/short/$PROJECT/$USER
elif [[ "$USER" == "z3441306" ]]; then
    export DATAPATH=/srv/ccrc/data32/$USER
fi

export INDIR=$DATAPATH/GLACE-CMIP5/
export AREA_DIR=$HOME/scripts/plot_scripts/areas_txt/
export WORKDIR=$DATAPATH/ACCESS_plots_workdir/
export OUTDIR=$DATAPATH/GLACE-CMIP5_plots/ENS_MEAN/climdex/

if [[ ! -d "$OUTDIR" ]]; then
 mkdir -p $OUTDIR
 echo "(0)     Making Directory $OUTDIR"
fi
if [[ ! -d "$WORKDIR" ]]; then
 mkdir -p $WORKDIR
 echo "(0)     Making Directory $WORKDIR"
fi

# -----------------------------------------------------------------------
# other set-up
# -----------------------------------------------------------------------

export pltType=pdf

# -----------------------------------------------------------------------
# load modules needed
# -----------------------------------------------------------------------

module load ncl/6.1.2
module load netcdf
module load cdo
# -----------------------------------------------------------------------
# call ncl script
# -----------------------------------------------------------------------
cd $OUTDIR

echo "Call ncl scripts"
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_GCCMIP5_boxplot.ncl 
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_GCCMIP5_boxplot_models_6reg.ncl 
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_GCCMIP5_boxplot_models_6reg_seas.ncl 
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_diffs_GCCMIP5_boxplot_models_6reg.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_diffs_GCCMIP5_boxplot_models_6reg_seas.ncl
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_diffs_time_GCCMIP5_boxplot_models_6reg.ncl 
#ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_diffs_time_GCCMIP5_boxplot_models_6reg_seas.ncl
ncl $HOME/scripts/plot_scripts/ncl_scripts/plot_ETCCDI_diffs_time_GCCMIP5_boxplot_models_4reg_per_seas.ncl
# -----------------------------------------------------------------------
# finishing and cleaning up
# -----------------------------------------------------------------------

    echo "(0) Finished plotting"

date=`date '+%Y-%m-%d-%H-%M-%S'`
    echo "End Time:" $date
    echo " "

exit 0
