;-----------------------------------------------------------------------------;
; File Name : plot_topography_subregions.ncl
; Creation Date : 20-08-2013
; Last Modified : Mon 12 May 2014 09:28:25 EST
; Created By : Ruth Lorenz
; Purpose : script to plot subregions on a map with underlying topography
;		Hardcoded to be run at storm servers

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin

;-----------------------------------------------------------------------------;
; set-up, define input/outputpaths
;-----------------------------------------------------------------------------;

inputdir ="/srv/ccrc/data23/z3441306/ACCESS_input_data/"
outputdir="/srv/ccrc/data23/z3441306/ACCESS_plots/LUC/"
system("if ! test -d " + outputdir +" ; then mkdir " + outputdir + " ; fi")

;regions to be plotted
areadir = "/home/z3441306/scripts/plot_scripts/areas_txt/"

area1 = "AMZ_WC"
area2 = "AMZ_SC"
area3 = "allAMZ"

;other set-up
region = False
lmask = True
pltType = "pdf"

;-----------------------------------------------------------------------------;
; Read files
;-----------------------------------------------------------------------------;

f0 = addfile(inputdir+ "cable_vegfracN96.2005.anc.nc","r")

;gridlat = f0->latitude
;gridlon = f0->longitude
pfts	= f0->field1391 ;evergreen broadleaf

pft1	= pfts(:,1,:,:) ;evergreen broadleaf
pft2	= pfts(:,3,:,:) ;decidous broadleaf

pft=pft1+pft2
pft@long_name="LCC regions and fraction of trees"
pft@units="fraction"
pft!0                 = "time"
pft!1                 = "lat"
pft!2                 = "lon"
pft&lat               = pfts&lat
pft&lon               = pfts&lon

;read corners of subdomains
area1_corners = asciiread(areadir+ area1+ ".txt",(/4,2/),"float")
area2_corners = asciiread(areadir+ area2+ ".txt",(/4,2/),"float")

;-----------------------------------------------------------------------------;
; Plot set-up
;-----------------------------------------------------------------------------;
cnLevels = (/0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9/)
region = True

  pltName = outputdir+ "trees_dom_" +area1+ "_" +area2+ "_" +area3

  wks = gsn_open_wks(pltType,pltName)

;----------ColorMap-----------------;

  gsn_define_colormap(wks,"WhiteGreen")

  colors = gsn_retrieve_colormap(wks)
  colors(3,:) = (/.8,.8,.8 /)
  gsn_define_colormap(wks,colors)

;----------GeneralResources---------;

  res = True

  res@gsnFrame             = False             ; Don't advance the frame
  res@gsnDraw              = False             ; don't advance frame
                                             
  res@gsnStringFontHeightF = 0.02             ; changes font height of all labels simultaneously

  res@cnFillOn             = True             ; Turn on contour fill.
  res@cnFillMode           = "CellFill"       ; Pixels
  res@cnLineLabelsOn       = False            ; Turn off line labels.
  res@cnInfoLabelOn        = False            ; Turn off info label.

        if (region) then
                res@cnRasterSmoothingOn  = True
        end if

  res@cnLinesOn            = False            ; no contour lines

  res@gsnSpreadColors      = True             ;
  res@lbLabelBarOn         = True             ;

  res@gsnLeftString        = ""               ;
  res@gsnRightString       =  "unit: " +pft@units               ;

        if(lmask)
                res@cnFillDrawOrder      = "PreDraw"   ; Predraw cn so mask will appear
                                                       ; (if land transparent ow land will also be masked)
        end if

  res@cnLevelSelectionMode = "ExplicitLevels" ; Define your own
  res@cnLevels = cnLevels

;----------Region ------------------
if (.not.region) then
        Area = "Global"
        latS = -90
        latN =  90
        lonL =   0
        lonR = 360
else
        Area = "Amazonia"
        latS = -30
        latN = 10
        lonL = 275
        lonR = 328
end if

if(region) then
        res@gsnAddCyclic = False
end if

  res@mpMinLatF  = latS
  res@mpMaxLatF  = latN
  res@mpMinLonF  = lonL
  res@mpMaxLonF  = lonR

  res@mpCenterLonF         = 180.             ; center on dateline

  res@mpGeophysicalLineColor = "grey"

   res@tmXBLabelFontHeightF = 0.015
   res@tmYLLabelFontHeightF = 0.015

;----------Mask --------------------
;mask ocean for plots
        if(lmask)
                res@mpFillOn             = True
                res@mpOceanFillColor     = "white"
                res@mpLandFillColor      = "transparent"
        end if

;----------Titles ------------------
  res@tiMainFontHeightF = 0.025
  res@tiMainString      = pft@long_name

  res@lbLabelAutoStride       = True             ; nice label bar labels
  res@lbOrientation           = "vertical"       ; vertical label bars
  res@lbTitleDirection        = "Across"         ; title direction
  res@lbLabelFontHeightF      = .02              ; increase label bar labels

;---------- Plot -------------------

plot = gsn_csm_contour_map(wks,pft(0,{latS:latN},{lonL:lonR}),res)

;----------overly boxes for subdomains--------

trres               = True                      ; text mods desired
trres@txFontHeightF = 0.02                    ; text font height
trres@txFontColor   = "white"
trres@txFont        = 21

;AMZ_WC
xl=(/area1_corners(0,0),area1_corners(1,0)/)	;(/90,145/)
yl=(/area1_corners(0,1),area1_corners(1,1)/)	;(/60,60/)
pkres=True
pkres@gsLineThicknessF=2.5
pkres@gsLineColor=0
plot_wc1=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/area1_corners(1,0),area1_corners(2,0)/)	;(/145,145/)
yl=(/area1_corners(1,1),area1_corners(2,1)/)	;(/60,20/)
plot_wc2=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/area1_corners(2,0),area1_corners(3,0)/)	;(/145,90/)
yl=(/area1_corners(2,1),area1_corners(3,1)/)	;(/20,20/)
plot_wc3=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/area1_corners(3,0),area1_corners(0,0)/)	;(/90,90/)
yl=(/area1_corners(3,1),area1_corners(0,1)/)	;(/20,60/)
plot_wc4=gsn_add_polyline(wks,plot,xl,yl,pkres)

tx = area1_corners(0,0)+(area1_corners(1,0)-area1_corners(0,0))/2
ty = area1_corners(2,1)+(area1_corners(0,1)-area1_corners(2,1))/2
plot_amzwc=gsn_add_text(wks,plot,area1,tx,ty,trres)

delete(tx)
delete(ty)
delete(xl)
delete(yl)

;AMZ_SC
trres@txFontColor   = "black"

xl=(/area2_corners(0,0),area2_corners(1,0)/)
yl=(/area2_corners(0,1),area2_corners(1,1)/)
pkres=True
pkres@gsLineThicknessF=2.5
pkres@gsLineColor=1
plot_sc1=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/area2_corners(1,0),area2_corners(2,0)/)
yl=(/area2_corners(1,1),area2_corners(2,1)/)
plot_sc2=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/area2_corners(2,0),area2_corners(3,0)/)
yl=(/area2_corners(2,1),area2_corners(3,1)/)
plot_sc3=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/area2_corners(3,0),area2_corners(0,0)/)
yl=(/area2_corners(3,1),area2_corners(0,1)/)
plot_sc4=gsn_add_polyline(wks,plot,xl,yl,pkres)

tx = area2_corners(0,0)+(area2_corners(1,0)-area2_corners(0,0))/2
ty = area2_corners(2,1)+(area2_corners(0,1)-area2_corners(2,1))/2
plot_amzsc=gsn_add_text(wks,plot,area2,tx,ty,trres)

delete(tx)
delete(ty)
delete(xl)
delete(yl)

;all AMZ
lon1_dom=278.0-360.
lon2_dom=325.0-360.
lat1_dom=15.0
lat2_dom=-20.0

xl=(/lon1_dom,lon2_dom/)
yl=(/lat1_dom,lat1_dom/)
pkres=True
pkres@gsLineThicknessF=2.5
pkres@gsLineColor=1
plot_AMZ1=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/lon2_dom,lon2_dom/)    ;
yl=(/lat1_dom,lat2_dom/)    ;
plot_AMZ2=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/lon2_dom,lon1_dom/)    ;-59.,-79.
yl=(/lat2_dom,lat2_dom/)    ;
plot_AMZ3=gsn_add_polyline(wks,plot,xl,yl,pkres)

delete(xl)
delete(yl)
xl=(/lon1_dom,lon1_dom/)    ;
yl=(/lat2_dom,lat1_dom/)    ;
plot_AMZ4=gsn_add_polyline(wks,plot,xl,yl,pkres)

tx = lon1_dom + 5
ty = lat2_dom + 3
plot_amz=gsn_add_text(wks,plot,area3,tx,ty,trres)

delete(xl)
delete(yl)

draw(plot)
frame(wks)

delete(wks)
if(pltType .eq. "pdf")
        system("pdfcrop " + pltName + "." + pltType+ " "+ pltName + "." + pltType)
end if

end
