;-----------------------------------------------------------------------------;
; File Name : plot_coupling_metrics_present_DJF_JJA_panel.ncl
; Creation Date : 26-06-2014
; Last Modified : Tue 02 Sep 2014 14:51:10 EST
; Created By : Ruth Lorenz
; Purpose : plot different coupling metrices for DJF and JJA in one panel

;-----------------------------------------------------------------------------;
; Loading Libraries
;-----------------------------------------------------------------------------;
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"

begin
;-----------------------------------------------------------------------------;
; set-up
;-----------------------------------------------------------------------------;

Exp = "GC1A85"
if (Exp.eq."GC1A85") then
   Expname = "ExpA"
else if (Exp.eq."GC1B85") then
   Expname = "ExpB"
else
	print("Wrong experiment name")
end if
end if

AStart="1981"
AEnd="2000"

Var="T"

gccmip5_dir = "/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ACCESS/"+Exp+"/coupling/"

out_dir = "/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ACCESS/coupling/"

;define plot type
plttype = "pdf"

;-----------------------------------------------------------------------------;
; Read netcdfs
;-----------------------------------------------------------------------------;
print("reading NetCDFs")

  file_gccmip5_coup = addfile(gccmip5_dir + "GCCMIP5_omega_coupstr_"+Exp+"_CTL_" +\
       Var + "_" + AStart + "-" + AEnd+ ".nc","r")
  coup_gccmip5 = file_gccmip5_coup ->coup    ;coupling
  dsize_coup=dimsizes(coup_gccmip5)
  ntim=dsize_coup(0)
  nlat=dsize_coup(1)
  nlon=dsize_coup(2)

  file_gccmip5_var = addfile(gccmip5_dir + "GCCMIP5_variance_analysis_"+Exp+"_CTL_"\
  		    + AStart + "-" + AEnd+ ".nc","r")
  var_gccmip5 = file_gccmip5_var ->var_seas

  file_gccmip5_corrETT = addfile(gccmip5_dir + "GCCMIP5_Corr_ET_" +Var+ "_"+Exp+\
  		       "_CTL_"+ AStart + "-" + AEnd+ ".nc","r")
  corr_gccmip5 = file_gccmip5_corrETT ->ccr_ctl_temp_et

  file_gccmip5_pi = addfile(gccmip5_dir + "GCCMIP5_Pi_"+Exp+\
  		       "_CTL_"+ AStart + "-" + AEnd+ ".nc","r")
  pi_gccmip5 = file_gccmip5_pi ->Pi_ctl

  file_qregjja = addfile(out_dir+"Quantile_Slope0.9_HD_SPI3_JJA_"+ AStart + "-" + AEnd+".nc","r")
  qregjja_gccmip5 = file_qregjja->Slope_90perc
  jja_sig = file_qregjja->Significance
  ;qregjja_gccmip5 =where(jja_sig.ne.1,qregjja_gccmip5@_FillValue,qregjja_gccmip5) ;mask unsignificant values
  file_qregdjf = addfile(out_dir+"Quantile_Slope0.9_HD_SPI3_DJF_"+ AStart + "-" + AEnd+".nc","r")
  qregdjf_gccmip5 = file_qregdjf->Slope_90perc
  djf_sig = file_qregdjf->Significance
  ;qregdjf_gccmip5 =where(djf_sig.ne.1,qregdjf_gccmip5@_FillValue,qregdjf_gccmip5) ;mask unsignificant values
  ;qreg_gccmip5@_FillValue = default_fillvalue("float")
  ;delete(qreg_gccmip5@missing_value)
  ;qreg_gccmip5=where(qreg_gccmip5.gt.100 .or. qreg_gccmip5.lt.-100,qreg_gccmip5@_FillValue,qreg_gccmip5)

  file_twoleg = addfile("/srv/ccrc/data32/z3441306/GLACE-CMIP5_plots/ACCESS/CTL/coupling/GCCMIP5_twolegged_ind_dtrend_CTL_SM_H_T_LE_PR_" +AStart+ "-" +AEnd+ ".nc","r")
  twoleg_gccmip5 = file_twoleg->I_t_sm_seas

lat=coup_gccmip5&lat
lon=coup_gccmip5&lon

;---Array to hold data values
  dmins = (/-0.12,10,-0.9,0.0,-2.0,-5.5/)     ; Data min and max
  dmaxs = (/ 0.44, 90, 0.9,0.9,0.5,0.0/)
  dspas = (/ 0.04, 10, 0.2, 0.1,0.25,0.5/)

  data     = new((/nlat,nlon/),float)
  data!0   = "lat"
  data!1   = "lon"
  data&lat = lat
  data&lon = lon

;-----------------------------------------------------------------------------;
; plot data panel
;-----------------------------------------------------------------------------;
pltName = out_dir+"coup_metr_6I_panel_"+AStart+"-"+AEnd
plttype@wkPaperSize = "A4"
wks = gsn_open_wks(plttype,pltName)

  res                        = True

  res@gsnDraw                = False
  res@gsnFrame               = False
  res@tiMainOn 	     	     = False
  res@gsnStringFontHeightF = 0.035             ; changes font height of all labels simultaneously

  res@cnFillOn             = True             ; Turn on contour fill.
  res@cnFillMode           = "CellFill"       ; Pixels
  res@cnLineLabelsOn       = False            ; Turn off line labels.
  res@cnInfoLabelOn        = False            ; Turn off info label.

  res@lbLabelBarOn           = False
  res@cnLinesOn              = False
  res@cnFillDrawOrder      = "PreDraw"   ; Predraw cn so mask will appear

	;mask ocean for plots
	res@mpFillOn             = True
        res@mpOceanFillColor     = "white"
        res@mpLandFillColor      = "transparent"

  	res@mpCenterLonF         = -180.             ; center on dateline

        ; cut plot North of 80˚ and South of 60˚
	latS = -60
        latN =  80
        lonL =   0
        lonR = 360
  	res@mpMinLatF  = latS
  	res@mpMaxLatF  = latN
  	res@mpMinLonF  = lonL
  	res@mpMaxLonF  = lonR

  res@cnLevelSelectionMode   = "ManualLevels"

;---Create "nplots" dummy plots.
  nplots = 12
  plots  = new(nplots,graphic)

  colormaps = (/"coup_str_temp_15lev","sunshine_9lev","orange_to_blue_18lev","omega_green_red","RdYlBu_2grey_twolegcoup","MPL_hot_noblack"/)
  do n=0,nplots-1
    dmin = dmins(n/2)
    dmax = dmaxs(n/2)
    dspa = dspas(n/2)

;---This is a new resource added in NCL V6.1.0
    res@cnFillPalette          = colormaps(n/2)

    res@cnMinLevelValF         = dmin
    res@cnMaxLevelValF         = dmax
    res@cnLevelSpacingF        = dspa

    res@tmXBLabelsOn = False
    res@tmYLLabelFontHeightF = 0.02

    if (n.eq.0) then
    res@gsnLeftString        = "a) DJF"           
    res@gsnRightString       =  " ~F33~D~F33~W~F21~~B~"+Var+",GCCMIP5 ~N~ "+Expname
    data   = coup_gccmip5(0,:,:) ;DJF
    else if (n.eq.1) then
    res@tmYLLabelsOn = False
    res@gsnLeftString        = "b) JJA"           
    data   = coup_gccmip5(2,:,:) ;JJA
    else if (n.eq.2) then
    res@tmYLLabelsOn = True
    res@gsnLeftString        = "c) DJF"           
    res@gsnRightString       =  "~F33~s~F21~~B~"+Var+" ~N~(CTL)-~F33~s~F21~~B~"+Var+" ~N~("+Expname+")/~F33~s~F21~~B~"+Var+" ~N~(CTL)"
    data   = var_gccmip5(0,:,:) ;DJF
    else if (n.eq.3) then
    res@tmYLLabelsOn = False
    res@gsnLeftString        = "d) JJA"     
    data   = var_gccmip5(2,:,:) ;JJA
    else if (n.eq.4) then
    res@tmYLLabelsOn = True
    res@gsnLeftString        = "e) DJF" 
    res@gsnRightString       = "Correlation(ET,T) CTL"
    data   = corr_gccmip5(0,:,:) ;DJF
    else if (n.eq.5) then
    res@tmYLLabelsOn = False
    res@gsnLeftString        = "f) JJA"   
    data   = corr_gccmip5(2,:,:) ;JJA
    else if (n.eq.6) then
    res@tmYLLabelsOn = True
    res@gsnLeftString        = "g) DJF" 
    res@gsnRightString       = "~F33~P~F21~ CTL"
    data   = pi_gccmip5(0,:,:) ;DJF
    else if (n.eq.7) then
    res@tmYLLabelsOn = False
    res@gsnLeftString        = "h) JJA"   
    data   = pi_gccmip5(2,:,:) ;JJA
    else if (n.eq.8) then
    res@tmYLLabelsOn = True
    res@gsnLeftString        = "i) DJF"
    res@gsnRightString       = "Two-legged Index SM-T CTL"
    data   = twoleg_gccmip5(0,:,:) ;DJF
    else if (n.eq.9) then
    res@tmYLLabelsOn = False
    res@gsnLeftString        = "j) JJA"
    data   = twoleg_gccmip5(2,:,:) ;JJA
    else if (n.eq.10) then
    res@tmYLLabelsOn = True
    res@gsnLeftString        = "k) DJF"
    res@gsnRightString       = "90th quantile slope CTL " 
    res@tmXBLabelsOn = True
    data   = qregdjf_gccmip5(:,:) ;
    else if (n.eq.11) then
    res@tmYLLabelsOn = False
    res@tmXBLabelsOn = True
    res@gsnLeftString        = "l) JJA"
    res@gsnRightString       = "90th quantile slope CTL" 
    data   = qregjja_gccmip5(:,:) ;
    end if
    end if
    end if
    end if
    end if
    end if
    end if
    end if
    end if
    end if
    end if
    end if
    
    res@gsnMaximize	= False

    plots(n) = gsn_csm_contour_map(wks,data,res)
  end do

  pres                  = True
  pres@gsnFrame         = False
  pres@gsnPanelLabelBar = True
;  pres@lbOrientation = "Vertical"
;  pres@pmLabelBarWidthF = 0.05
  pres@pmLabelBarWidthF = 0.45
  pres@pmLabelBarHeightF = 0.02
;  pres@pmLabelBarOrthogonalPosF = 0.025
  pres@lbLabelFontHeightF = 0.01
;  pres@lbLeftMarginF	  = 0.1
;
; Panel the first set of plots without drawing them, so we
; can retrieve the bounding boxes and calculate the height.
;
  pres@gsnDraw = False
;
; "gsn_panel_return" is an unadvertised function. It behaves
; the same as "gsn_panel", except it returns all the objects 
; being paneled, including the labelbar if there is one.

;---Using height value, now we can panel all sets of plots.
  pres@gsnDraw                          = True
  pres@gsnPanelTop                      = 0.95   ; leave room for title
  pres@gsnPanelBottom                   = pres@gsnPanelTop - 0.14
;  pres@gsnPanelDebug = True

;---figure string resources
  pres@gsnPanelFigureStringsPerimOn     = False
  pres@gsnPanelFigureStringsFontHeightF = 0.01
  pres@amJust                           = "TopLeft"
  pres@gsnPanelXWhiteSpacePercent	= 1.5

;---Main title, only on first set of plots
  pres@txString = "Coupling measures during "+AStart+"-"+AEnd
  pres@txFontHeightF	  = 0.01
  pres@txFont            = "helvetica-bold"

  panel1=gsn_panel_return(wks,plots(0:1),(/1,2/),pres)

;---Set for the next panel call.
    pres@txString = ""
    pres@gsnPanelTop    = pres@gsnPanelBottom
    pres@gsnPanelBottom = pres@gsnPanelTop-0.14

    print(pres@gsnPanelTop)
    print(pres@gsnPanelBottom)

  panel2=gsn_panel_return(wks,plots(2:3),(/1,2/),pres)

;---Set for the next panel call.
    pres@txString = ""
    pres@gsnPanelTop    = pres@gsnPanelBottom
    pres@gsnPanelBottom = pres@gsnPanelTop-0.14

    print(pres@gsnPanelTop)
    print(pres@gsnPanelBottom)

  panel3=gsn_panel_return(wks,plots(4:5),(/1,2/),pres)

;---Set for the next panel call.
    pres@txString = ""
    pres@gsnPanelTop    = pres@gsnPanelBottom
    pres@gsnPanelBottom = pres@gsnPanelTop-0.14

    print(pres@gsnPanelTop)
    print(pres@gsnPanelBottom)

  panel4=gsn_panel_return(wks,plots(6:7),(/1,2/),pres)

;---Set for the next panel call.
    pres@txString = ""
    pres@gsnPanelTop    = pres@gsnPanelBottom
    pres@gsnPanelBottom = pres@gsnPanelTop-0.14

    print(pres@gsnPanelTop)
    print(pres@gsnPanelBottom)

  panel5=gsn_panel_return(wks,plots(8:9),(/1,2/),pres)

;---Set for the next panel call.
    pres@txString = ""
    pres@gsnPanelTop    = pres@gsnPanelBottom
    pres@gsnPanelBottom = pres@gsnPanelTop-0.1545

    print(pres@gsnPanelTop)
    print(pres@gsnPanelBottom)

  panel6=gsn_panel_return(wks,plots(10:11),(/1,2/),pres)

;---If maximization is desired, used this
  mres = True
  maximize_output(wks,mres)  

if(plttype .eq. "pdf")
        delete(wks)
	system("pdfcrop " + pltName + "." + plttype+ " "+ pltName + "." + plttype)
end if

end
